<?php

namespace App\Http\Controllers\Popshop;

use App\Models\Popshop\DimoProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{
    public function getLanding(Request $request){
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $listProduct = DimoProduct::with('orders')
            ->whereHas('orders',function ($query) use($startDate,$endDate){
                return $query->whereBetween('check_out_date',[$startDate,$endDate]);
            })
            ->get();




    }
}
