<?php

namespace App\Http\Controllers\Agent;

use App\Http\Helpers\Helper;
use App\Jobs\SendFCM;
use App\Models\Agent\AgentLocker;
use App\Models\Agent\Article;
use App\Models\Agent\AvailableParameter;
use App\Models\Agent\Campaign;
use App\Models\Agent\CampaignVoucher;
use App\Models\Agent\CommissionRule;
use App\Models\Agent\CommissionSchema;
use App\Models\Agent\NotificationFCM;
use App\Models\Agent\ReferralCampaign;
use App\Models\Agent\ReferralTransaction;
use App\Models\Agent\User;
use App\Models\Popbox\SepulsaProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MarketingController extends Controller
{
    /**
     * Get Agent Commission Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCommission(){
        $notActive = '#031A2B';
        $active = '#09B800';
        $expired = '#F58F08';
        $priorityOne = '#4EA600';
        $priorityTwo = '#f39c12';
        $priorityThree = '#00F5CE';

        $activeCommissionDb = CommissionSchema::with('commissionRules')
            ->where('status','=','1')
            ->orderBy('priority','asc')
            ->orderBy('created_at','desc')
            ->get();

        $allCommissionDb =  CommissionSchema::with('commissionRules')
            ->orderBy('priority','asc')
            ->orderBy('created_at','desc')
            ->get();

        $activeList= [];
        $nowDateTime = date('Y-m-d H:i:s');
        foreach ($activeCommissionDb as $item) {
            $tmp = array();
            $rules = $item->commissionRules;
            $name = "$item->values $item->type";
            if (count($rules) >0){
                foreach ($rules as $rule) {
                    $paramName = $rule->parameter->description;
                    $name .=" $paramName $rule->operator $rule->value |";
                }
            } else {
                $name .=" product : $item->product";
            }

            // color code
            $color = $notActive;
            if ($nowDateTime >= $item->start_date && $nowDateTime <= $item->end_date){
                if ($item->priority == 1) $color = $priorityOne;
                if ($item->priority == 2) $color = $priorityTwo;
                if ($item->priority == 3) $color = $priorityThree;
            } elseif ($nowDateTime > $item->end_date) {
                $color = $expired;
            }

            $tmp['name'] = $name;
            $tmp['start_time'] = date('Y-m-d',strtotime($item->start_date));
            $tmp['end_time'] = date('Y-m-d',strtotime($item->end_date));
            $tmp['priority'] = $item->priority;
            $tmp['created_at'] = date('Y-m-d H:i:s',strtotime($item->created_at));
            $tmp['color'] = $color;
            $activeList[] = $tmp;
        }
        $data = [];
        $data['activeList'] = $activeList;
        $data['commissionDb'] = $allCommissionDb;

        return view('agent.marketing.commission',$data);
    }

    /**
     * post New Commission
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCommission(Request $request){
        $type = $request->input('type');
        $rule = $request->input('rule');
        $values = $request->input('values');
        $maxAmount = $request->input('maxAmount',null);
        $minAmount = $request->input('minAmount',null);
        $activeDateRange = $request->input('activeDate');
        $priority = $request->input('priority',1);

        $tmpDateRange = explode('-',$activeDateRange);
        $startDate = date('Y-m-d',strtotime($tmpDateRange[0]))." 00:00:00";
        $endDate = date('Y-m-d',strtotime($tmpDateRange[1]))." 23:59:59";

        $commissionSchemeDb = new CommissionSchema();
        $commissionSchemeDb->type = $type;
        $commissionSchemeDb->rule = $rule;
        $commissionSchemeDb->values = $values;
        $commissionSchemeDb->max_amount = $maxAmount;
        $commissionSchemeDb->min_amount = $minAmount;
        $commissionSchemeDb->status = 0;
        $commissionSchemeDb->priority = $priority;
        $commissionSchemeDb->start_date = $startDate;
        $commissionSchemeDb->end_date = $endDate;
        $commissionSchemeDb->approved_by = null;
        $commissionSchemeDb->approval_status = 'pending';
        $commissionSchemeDb->save();

        $id = $commissionSchemeDb->id;
        return redirect('agent/marketing/commission/rule'."/$id");
    }

    /**
     * Update Commission Rule
     * @param $commissionSchemaId
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getCommissionRule($commissionSchemaId,Request $request){
        // get commission schema db
        $commissionSchemaDb = CommissionSchema::find($commissionSchemaId);
        if (!$commissionSchemaDb){
            $request->session()->flash('error','Not Found Commission Schema');
            return back();
        }

        // get current commission rule
        $commissionRuleDb = CommissionRule::with('parameter')->where('commission_schema_id',$commissionSchemaId)->get();

        // get available parameter
        $availableParamDb = AvailableParameter::where('is_commission',1)->get();
        $paramCategory = array();
        $availableParam = array();
        foreach ($availableParamDb as $param)
        {
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParam[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParam[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)));
        }
        // form data
        $typeForm = ['percent','fixed'];
        $priorityFrom = [1,2,3];
        $availableOperator = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');

        // parse to view
        $data = [];
        $data['commissionSchemaDb'] = $commissionSchemaDb;
        $data['commissionRuleDb'] = $commissionRuleDb;
        $data['availableParam'] = $availableParam;
        $data['typeForm'] = $typeForm;
        $data['priorityForm'] = $priorityFrom;
        $data['availableOperator'] = $availableOperator;

        return view('agent.marketing.commission-rule',$data);
    }

    /**
     * Save Commission Rule
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCommissionRule(Request $request){
        $multiple = ['between','exist','except'];
        $commissionSchemaId = $request->input('commissionSchemaId');

        $activeDateRange = $request->input('activeDate');
        $tmpDateRange = explode('-',$activeDateRange);
        $startDate = date('Y-m-d',strtotime($tmpDateRange[0]))." 00:00:00";
        $endDate = date('Y-m-d',strtotime($tmpDateRange[1]))." 23:59:59";

        DB::connection('popbox_agent')->beginTransaction();

        // get commission schema
        $commissionSchemaDb = CommissionSchema::find($commissionSchemaId);
        $commissionSchemaDb->type = $request->input('type');
        $commissionSchemaDb->rule = $request->input('rule');
        $commissionSchemaDb->values = $request->input('values');
        $commissionSchemaDb->min_amount = $request->input('minAmount');
        $commissionSchemaDb->max_amount = $request->input('maxAmount');
        $commissionSchemaDb->priority = $request->input('priority');
        $commissionSchemaDb->status = $request->input('status');
        $commissionSchemaDb->start_date = $startDate;
        $commissionSchemaDb->end_date = $endDate;
        $commissionSchemaDb->approved_by = null;
        $commissionSchemaDb->approval_status = 'pending';
        $commissionSchemaDb->save();

        // set rules
        $parameterId = $request->input('parameter_id');
        $operator = $request->input('operator');
        $value = $request->input('value');
        // delete previous rule
        $deleteRule = CommissionRule::where('commission_schema_id',$commissionSchemaDb->id)
            ->delete();
        foreach ($parameterId as $key => $param){
            // check on db commission rule
            $ruleDb = CommissionRule::where('commission_schema_id',$commissionSchemaDb->id)
                ->where('available_parameter_id',$param)
                ->first();
            if (!$ruleDb){
                // insert
                $ruleDb = new CommissionRule();
                $ruleDb->commission_schema_id = $commissionSchemaDb->id;
                $ruleDb->available_parameter_id =$param;
                $ruleDb->operator = $operator[$key];
                if (in_array($operator[$key],$multiple)){
                    $tmpValue = json_encode($value[$key]);
                } else $tmpValue = $value[$key][0];
                $ruleDb->value = $tmpValue;
                $ruleDb->save();
            } else {
                $ruleDb = CommissionRule::find($ruleDb->id);
                $ruleDb->commission_schema_id = $commissionSchemaDb->id;
                $ruleDb->available_parameter_id =$param;
                $ruleDb->operator = $operator[$key];
                if (in_array($operator[$key],$multiple)){
                    $tmpValue = json_encode($value[$key]);
                } else $tmpValue = $value[$key][0];
                $ruleDb->value = $tmpValue;
                $ruleDb->save();
            }
        }

        // send email notification
        unset($commissionSchemaDb->product);
        unset($commissionSchemaDb->rule);
        unset($commissionSchemaDb->transaction_rule);
        $commissionRules = $commissionSchemaDb->commissionRules;
        $multiple = ['between','exist','except'];
        foreach ($commissionRules as $rule){
            $valueText = [];
            $parameterName = $rule->parameter->name;
            $ruleValue = $rule->value;
            $ruleOperator = $rule->operator;
            if ($parameterName == 'sepulsa_product_id'){
                if (in_array($ruleOperator,$multiple)){
                    $ruleValue = json_decode($ruleValue);
                    foreach ($ruleValue as $item) {
                        // get sepulsa product
                        $sepulsaProduct = SepulsaProduct::where('product_id',$item)->first();
                        if (!$sepulsaProduct){
                            $valueText[] = 'Sepulsa Product Id '.$item;
                        } else {
                            $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                        }
                    }
                } else {
                    $sepulsaProduct = SepulsaProduct::where('product_id',$ruleValue)->first();
                    if (!$sepulsaProduct){
                        $valueText[] = 'Sepulsa Product Id '.$ruleValue;
                    } else {
                        $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                    }
                }
            }
            elseif ($parameterName == 'locker_id') {
                if (in_array($ruleOperator,$multiple)){
                    $ruleValue = json_decode($ruleValue);
                    foreach ($ruleValue as $item) {
                        // get sepulsa product
                        $agentList = AgentLocker::find($item);
                        if (!$agentList){
                            $valueText[] = 'Agent Id '.$item;
                        } else {
                            $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                        }
                    }
                } else {
                    $agentList = AgentLocker::find($ruleValue);
                    if (!$agentList){
                        $valueText[] = 'Agent Id '.$ruleValue;
                    } else {
                        $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                    }
                }
            }
            else {
                if (in_array($ruleOperator,$multiple)){
                    $ruleValue = json_decode($ruleValue);
                    foreach ($ruleValue as $item) {
                        $valueText[] = $item;
                    }
                } else {
                    $valueText[] = $ruleValue;
                }
            }
            $rule->value_text = $valueText;
            $rule->parameter_name = $rule->parameter->description;
        }
        $data = [];
        $data['schemaDb'] = $commissionSchemaDb;
        $data['commissionRules'] = $commissionRules;

        // parse to email and view
        $environment = App::environment();
        $date = date('Y-m-d');
        if ($environment == 'production') {
            Mail::send('email.agent.marketing.commission-approval', $data, function ($m) use ($environment, $date, $commissionSchemaDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('greta@popbox.asia', 'Greta PopBox Asia')->subject("[Agent] Commission Approval $date ID: $commissionSchemaDb->id");
                $m->to('lidya@popbox.asia', 'Lidya PopBox Asia')->subject("[Agent] Commission Approval $date ID: $commissionSchemaDb->id");
                $m->to('maja@popbox.asia', 'Maja PopBox Asia')->subject("[Agent] Commission Approval $date ID: $commissionSchemaDb->id");
                $m->cc('dyah@popbox.asia', 'IT PopBox Asia')->subject("[Agent] Commission Approval $date ID: $commissionSchemaDb->id");
            });
        } else {
            Mail::send('email.agent.marketing.commission-approval', $data, function ($m) use ($environment, $date, $commissionSchemaDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('arief@popbox.asia', 'Finance PopBox Asia')->subject("[Agent][$environment] Commission Approval $date ID: $commissionSchemaDb->id");
            });
        }

        DB::connection('popbox_agent')->commit();

        $request->session()->flash('success','Success Commission');
        return redirect('agent/marketing/commission');
    }

    /**
     * Delete Commission Rule
     * @param $commissionRuleId
     * @param Request $request
     * @return string
     */
    public function deleteCommissionRule($commissionRuleId,Request $request){
        if (empty($commissionRuleId)) return 'empty';

        // check on DB first
        $check = CommissionRule::find($commissionRuleId);
        if ($check){
            $check->delete();
            return 'ok';
        }
        return 'not found';
    }

    /**
     * Get Commission Helper
     * @param null $type
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getCommissionHelper($type = null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->view = null;

        if (empty($type)){
            $response->errorMsg = 'Empty Type';
            return response()->json($response);
        }

        switch ($type){
            case 'transaction_item_type' :
                // get available param
                $availableParamDb = AvailableParameter::where('param_name',$type)->first();
                $data['value'] = json_decode($availableParamDb->value);
                $view = view('agent.marketing.ajax.transaction-item-type',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'transaction_type' :
                // get available param
                $availableParamDb = AvailableParameter::where('param_name',$type)->first();
                $data['value'] = json_decode($availableParamDb->value);
                $view = view('agent.marketing.ajax.transaction-item-type',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            case 'sepulsa_product_id':
                // get sepulsa product list
                $productSepulsaDb = DB::connection('popbox_db')
                    ->table('tb_sepulsa_product')
                    ->where('status','<>','PERMANENT_CLOSE')
                    ->get();
                $data['sepulsaDb'] = $productSepulsaDb;
                $view = view('agent.marketing.ajax.sepulsa-product',$data)->render();
                $response->isSuccess = true;
                $response->view = $view;
                break;
            default :
                $response->errorMsg = 'Failed. Undefined Type';
                break;
        }

        return response()->json($response);
    }

    /**
     * Get Referral Management
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getReferral(){
        // get referral campaign
        $referralCampaignDb = ReferralCampaign::orderBy('updated_at','desc')
            ->orderBy('status','desc')
            ->paginate(10);

        $fromLockerList = AgentLocker::get();

        // available rule
        $availableType = ['topup','register'];
        $availableOperator = ['>','>=','=','<','<='];

        $data = [];
        $data['availableType'] = $availableType;
        $data['availableOperator'] = $availableOperator;
        $data['referralCampaignDb'] = $referralCampaignDb;
        $data['fromLockerList'] = $fromLockerList;

        return view('agent.marketing.referral',$data);
    }

    /**
     * Post Referral Management
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postReferral(Request $request){
        $id = $request->input('id',null);
        $name = $request->input('name');
        $activeDate = $request->input('activeDate');
        $type = $request->input('type');
        $rule = $request->input('rule');
        $ruleValue = $request->input('rule-value');
        $fromAmount = $request->input('fromAmount');
        $toAmount = $request->input('toAmount');
        $code = $request->input('code',null);
        $expired = $request->input('expired');
        $status = $request->input('status');
        $description = $request->input('description');
        $descriptionToShared = $request->input('descriptionToShared');
        $fromLockerId = $request->input('fromLockerId',null);
        $limit = $request->input('limit',0);

        DB::connection('popbox_agent')->beginTransaction();
        // create validation
        if (!empty($fromLockerId) && empty($code)){
            $request->session()->flash('error','If From Agent Exist, Special Code must available');
            return back();
        }

        // explode date
        $tmpDate = explode('-',$activeDate);
        $startDate = date('Y-m-d',strtotime($tmpDate[0]))." 00:00:00";
        $endDate = date('Y-m-d ',strtotime($tmpDate[1])). "23:59:59";

        // save to DB
        if (empty($id)){
            $dataDb = new ReferralCampaign();
        } else {
            $dataDb = ReferralCampaign::find($id);
        }
        $dataDb->name = $name;
        $dataDb->description = $description;
        $dataDb->description_to_shared = $descriptionToShared;
        $dataDb->type = $type;
        $dataDb->limit_usage = $limit;
        // rule
        if ($type == 'topup' && !empty($ruleValue)) {
            $tmpRule[$rule] = $ruleValue;
            $dataDb->rule = json_encode($tmpRule);
        }
        $dataDb->code = $code;
        $dataDb->start_date = $startDate;
        $dataDb->end_date = $endDate;
        $dataDb->status = $status;
        $dataDb->from_amount = $fromAmount;
        $dataDb->to_amount = $toAmount;
        $dataDb->expired = $expired;
        $dataDb->from_locker_id = $fromLockerId;
        $dataDb->approved_by = null;
        $dataDb->approval_status = 'pending';
        $dataDb->save();

        $data = [];
        $data['referralDb'] = $dataDb;

        // parse to email and view
        $environment = App::environment();
        $date = date('Y-m-d');
        if ($environment == 'production') {
            Mail::send('email.agent.marketing.referral-approval', $data, function ($m) use ($environment, $date, $dataDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('greta@popbox.asia', 'Greta PopBox Asia')->subject("[Agent] Referral Campaign Approval $date Name: $dataDb->name");
                $m->to('lidya@popbox.asia', 'Lidya PopBox Asia')->subject("[Agent] Referral Campaign Approval $date Name: $dataDb->name");
                $m->to('maja@popbox.asia', 'Maja PopBox Asia')->subject("[Agent] Referral Campaign Approval $date Name: $dataDb->name");
                $m->cc('dyah@popbox.asia', 'Dyah PopBox Asia')->subject("[Agent] Referral Campaign Approval $date Name: $dataDb->name");
            });
        } else {
            Mail::send('email.agent.marketing.referral-approval', $data, function ($m) use ($environment, $date, $dataDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('arief@popbox.asia', 'Finance PopBox Asia')->subject("[Agent][$environment] Referral Campaign Approval $date Name: $dataDb->name");
            });
        }

        DB::connection('popbox_agent')->commit();

        $request->session()->flash('success','Success');
        return back();
    }

    /**
     * Get Referral Transaction
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getReferralTransaction(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-1');
        $endDate = date('Y-m-t');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $code = $request->input('code',null);
        $fromAgent = $request->input('fromAgent',null);
        $toAgent = $request->input('toAgent',null);
        $type = $request->input('type',null);
        $status  =$request->input('status',null);

        // get referral transaction
        $referralTransactionDb = ReferralTransaction::with(['referralCampaign','fromAgent','toAgent'])
            ->whereBetween('created_at',[$startDate,$endDate])
            ->when($code,function($query) use($code){
                return $query->where('code','LIKE',"%$code%");
            })->when($fromAgent,function ($query) use($fromAgent){
                return $query->where('from_locker_id',$fromAgent);
            })->when($toAgent,function($query) use ($toAgent){
                return $query->where('to_locker_id',$toAgent);
            })->when($type,function ($query) use($type){
                return $query->where('type',$type);
            })->when($status,function($query) use($status){
                return $query->where('status',$status);
            })
            ->orderBy('created_at','desc')
            ->paginate(15);

        /*get all agent for from agent*/
        $fromLockerDb = AgentLocker::whereNotNull('referral_code')
            ->get();

        /*get all locker for to agent*/
        $toLockerDb = AgentLocker::get();

        // set type list
        $typeList = ['topup','register'];

        // set status list
        $statusList = ['PENDING','USED','FAILED'];

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        $data = [];
        $data['referralTransactionDb'] = $referralTransactionDb;
        $data['parameter'] = $param;
        $data['fromLockerDb'] = $fromLockerDb;
        $data['toLockerDb'] = $toLockerDb;
        $data['statusList'] = $statusList;
        $data['typeList'] = $typeList;

        return view('agent.marketing.referral-transaction',$data);
    }

    /**
     * Get Push Notification Page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getNotification(Request $request){
        // get agent user list
        $userDb = User::with('locker')->whereNotNull('gcm_token')->get();

        // type list

        $typeList = ['transaction','backend-push'];

        $data = [];
        $data['userDb'] = $userDb;
        $data['typeList'] = $typeList;

        return view('agent.marketing.notification',$data);
    }

    /**
     * Push FCM notification
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postNotification(Request $request)
    {
        $input = $request->input();
        // get type notification
        $typeNotification = $request->input('typeNotif');

        $userList = [];
        if ($typeNotification == 'all'){
            // get user list
            $userList = User::with('locker')->whereNotNull('gcm_token')->get();
        } else {
            $userInput = $request->input('user');
            $userList = User::with('locker')->whereIn('gcm_token',$userInput)->get();
        }

        if (empty($userList)){
            $request->session()->flash('error','User Cannot Empty');
            return back();
        }

        $title = $request->input('title');
        $body = $request->input('body');
        $type = $request->input('type');
        $body = $request->input('body');
        $description = $request->input('description');
        $reference = $request->input('reference');
        // image
        $fileName = "notification-".date('YmdHis').".jpg";

        // insert
        $fcmNotification = new NotificationFCM();
        foreach ($userList as $user) {
            $fcm = $user->gcm_token;
            $lockerId = $user->locker_id;
            // create parameter push
            $data = [];
            $data['id'] = (int)"9".rand(10,99);
            $data['type'] = $type;
            $data['timestamp'] = date('Y-m-d H:i:s');
            $data['description'] = $description;
            $data['reference'] = $reference;
            $data['locker_id'] = $lockerId;

            if (!empty($request->file('image'))){
                $data['img_url'] = url("/files/notification/".$fileName);
            }
            $insert = $fcmNotification->insertNew('dashboard',$fcm,'android',$title,$body,$data);
        }
        if (!empty($request->file('image'))){
            $request->file('image')->move(public_path()."/files/notification/",$fileName);
        }

        // dispatch job
        SendFCM::dispatch();

        $request->session()->flash('success','Success Push FCM');
        return back();
    }

    /**
     * Get Article
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getArticle(Request $request){
        // get all articles
        $articlesDb = Article::orderBy('created_at','desc')
            ->orderBy('status','desc')
            ->paginate(10);
        $types = ["popshop","landing","news","topup","promo"];

        // parsing data to view
        $data = [];
        $data['articles'] = $articlesDb;
        $data['types'] = $types;
        return view('agent.marketing.article',$data);
    }

    /**
     * Post Article
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postArticle(Request $request){
        $type =  $request->input('type');
        $value = $request->input('value');
        $title = $request->input('title');
        $status = $request->input('status');
        $activeDate = $request->input('dateRange');
        $image = $request->file('image');
        $shortDescription = $request->input('shortDescription');
        $longDescription = $request->input('longDescription');
        // set active date
        list($startDate,$endDate) = explode('-',$activeDate);
        $startDate = date('Y-m-d',strtotime($startDate))." 00:00:00";
        $endDate = date('Y-m-d',strtotime($endDate)). " 23:59:59";

        $id = $request->input('id',null);
        if (empty($id)){
            $imageName = "$type".Helper::generateRandomString(4).'.'.$image->getClientOriginalExtension();

            DB::connection('popbox_agent')->beginTransaction();
            // save into artcles
            $articleDb = new Article();
            $articleDb->type = $type;
            $articleDb->value = $value;
            $articleDb->title = $title;
            $articleDb->short_description = $shortDescription;
            $articleDb->long_description = $longDescription;
            $articleDb->status = $status;
            $articleDb->start_date = $startDate;
            $articleDb->end_date = $endDate;
            $articleDb->image_url = asset('files/marketing/article/'.$imageName);
            $articleDb->save();

            $tmpPath = public_path()."/files/marketing/article";
            $image->move($tmpPath,$imageName);
            DB::connection('popbox_agent')->commit();

            $request->session()->flash('success','Sukses Tambah Banner / Artikel');
        } else {
            DB::connection('popbox_agent')->beginTransaction();
            // save into artcles
            $articleDb = Article::find($id);
            $articleDb->type = $type;
            $articleDb->value = $value;
            $articleDb->title = $title;
            $articleDb->short_description = $shortDescription;
            $articleDb->long_description = $longDescription;
            $articleDb->status = $status;
            $articleDb->start_date = $startDate;
            $articleDb->end_date = $endDate;
            $articleDb->save();
            $imageName = $articleDb->image;
            if (!empty($image)){
                $tmpPath = public_path()."/files/marketing/article";
                $image->move($tmpPath,$imageName);
            }
            DB::connection('popbox_agent')->commit();

            $request->session()->flash('success','Sukses Update Banner / Artikel');
        }

        return back();
    }

    /**
     * Get List All Campaign
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCampaign(Request $request){
        // get all campaign
        $campaignDb = Campaign::orderBy('created_at','desc')->paginate(20);

        $nowDate = date('Y-m-d H:i:s');
        // get list for calendar
        $calendarCampaignDb = Campaign::where('status','enabled')
            ->where('end_time','>',$nowDate)
            ->orderBy('priority','DESC')
            ->orderBy('created_at','DESC')
            ->get();

        $calendarList = [];
        foreach ($calendarCampaignDb as $item) {
            $tmp = [];
            $tmp['name'] = $item->name;
            $tmp['start_date'] = date('Y-m-d H:i:s',strtotime($item->start_time));
            $tmp['end_date'] = date('Y-m-d H:i:s',strtotime($item->end_time));
            $calendarList[] = $tmp;
        }

        // parse data to view
        $data = [];
        $data['campaigns'] = $campaignDb;
        $data['calendarList'] = $calendarList;

        return view('agent.marketing.campaign',$data);
    }

    /**
     * Get Form Add Campaign
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAddCampaign(Request $request){
        // set parameter data
        $statusList  = ['enabled','disabled'];
        $promoType = ['transaction','topup'];
        $typeList = ['percent','fixed'];
        $categoryList = ['fixed','cutback'];
        $limitType = ['all','member','voucher'];
        $voucherRequired = [
            '0' => 'Not Required',
            '1' => 'Required Voucher/Promo Code'
        ];

        // parse data to view
        $data = [];
        $data['statusList'] = $statusList;
        $data['promoType'] = $promoType;
        $data['typeList'] = $typeList;
        $data['categoryList'] = $categoryList;
        $data['limitType'] = $limitType;
        $data['voucherRequired'] = $voucherRequired;

        return view('agent.marketing.campaign-add',$data);
    }

    /**
     * Get Form Add Campaign
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditCampaign(Request $request,$campaignId){
        $campaignDb = Campaign::with('parameters')->find($campaignId);
        if (!$campaignDb){
            $request->session()->flash('error','Invalid Campaign');
            return back();
        }

        // set parameter data
        $statusList  = ['enabled','disabled'];
        $promoType = ['transaction','topup'];
        $typeList = ['percent','fixed'];
        $categoryList = ['fixed','cutback'];
        $limitType = ['all','member','voucher'];
        $voucherRequired = [
            '0' => 'Not Required',
            '1' => 'Required Voucher/Promo Code'
        ];

        // get all available param
        $availableParamDb = AvailableParameter::where('is_campaign',1)->get();
        $availableParamList = [];
        $paramCategory = [];
        foreach ($availableParamDb as $param){
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParamList[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParamList[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)), "type"=> $param->type);
        }
        $operatorList = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');

        // parse data to view
        $data = [];
        $data['statusList'] = $statusList;
        $data['promoType'] = $promoType;
        $data['typeList'] = $typeList;
        $data['categoryList'] = $categoryList;
        $data['limitType'] = $limitType;
        $data['voucherRequired'] = $voucherRequired;
        $data['campaignDb'] = $campaignDb;
        $data['availableParam'] = $availableParamList;
        $data['operatorList'] = $operatorList;

        return view('agent.marketing.campaign-add',$data);
    }

    /**
     * Get Ajax Rule Form View
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function getAjaxRule(Request $request){
        $newRuleCount = $request->input('newRuleCount',1);
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->view = null;

        // get all available param
        $availableParamDb = AvailableParameter::where('is_campaign',1)->get();
        $availableParamList = [];
        $paramCategory = [];
        foreach ($availableParamDb as $param){
            if (!isset($paramCategory[$param->param_name]))
            {
                $i = count($paramCategory);
                $paramCategory[$param->param_name] = $i;
            }
            $i = $paramCategory[$param->param_name];

            $availableParamList[$i]['paramCategory'] = utf8_encode($param->param_name);
            $availableParamList[$i]['paramName'][] = array("id"=>$param->id,"name" => $param->name, "description" => ucfirst(utf8_encode($param->description)), "type"=> $param->type);
        }
        $operatorList = array('<'=>'Less','<='=>'Less or Same','='=>'Same','>='=>'More or Same','>'=>'More','between'=>'Between','exist'=>'Exist','except'=>'Not Exist');

        // parse data
        $data = [];
        $data['availableParam'] = $availableParamList;
        $data['operatorList'] = $operatorList;
        $data['newRuleCount'] = $newRuleCount;

        $view = view('agent.marketing.ajax.campaign-add-rule',$data)->render();

        $response->isSuccess = true;
        $response->view = $view;

        return response()->json($response);
    }

    /**
     * Add Campaign
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postAddCampaign(Request $request){
        $input = $request->input();

        // create validation
        $rules = [
            'name' => 'required',
            'description' => 'nullable',
            'startDate' => 'required|date|before:endDate',
            'endDate' => 'required|date|after:startDate',
            'status' => 'required|in:enabled,disabled',
            'priority' => 'required|numeric',
            'promoType' => 'required|in:transaction,topup',
            'type' => 'required|in:percent,fixed',
            'amount' => 'required|numeric',
            'category' => 'required_if:promoType,transaction|in:fixed,cutback',
            'voucherRequired' => 'required|in:0,1',
            'limitType' => 'required|in:all,member,voucher',
            'limitUsage' => 'nullable|numeric',
            'minAmount' => 'nullable|numeric',
            'maxAmount' => 'nullable|numeric'
        ];
        $this->validate($request,$rules);

        $voucherRequired = $request->input('voucherRequired');

        DB::connection('popbox_agent')->beginTransaction();

        // insert campaign and parameter
        $insertCampaign = Campaign::saveData($request);
        if (!$insertCampaign->isSuccess){
            DB::connection('popbox_agent')->rollback();
            $request->session()->flash('error',$insertCampaign->errorMsg);
            return back();
        }
        $campaignId = $insertCampaign->campaignId;

        // Send email
        $campaignDb = Campaign::find($campaignId);

        $campaignRules = $campaignDb->parameters;
        $multiple = ['between','exist','except'];
        foreach ($campaignRules as $rule){
            $valueText = [];
            $parameterName = $rule->parameter->name;
            $ruleValue = $rule->value;
            $ruleOperator = $rule->operator;
            if ($parameterName == 'sepulsa_product_id'){
                if (in_array($ruleOperator,$multiple)){
                    $ruleValue = json_decode($ruleValue);
                    foreach ($ruleValue as $item) {
                        // get sepulsa product
                        $sepulsaProduct = SepulsaProduct::where('product_id',$item)->first();
                        if (!$sepulsaProduct){
                            $valueText[] = 'Sepulsa Product Id '.$item;
                        } else {
                            $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                        }
                    }
                } else {
                    $sepulsaProduct = SepulsaProduct::where('product_id',$ruleValue)->first();
                    if (!$sepulsaProduct){
                        $valueText[] = 'Sepulsa Product Id '.$ruleValue;
                    } else {
                        $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                    }
                }
            }
            elseif ($parameterName == 'locker_id') {
                if (in_array($ruleOperator,$multiple)){
                    $ruleValue = json_decode($ruleValue);
                    foreach ($ruleValue as $item) {
                        // get sepulsa product
                        $agentList = AgentLocker::find($item);
                        if (!$agentList){
                            $valueText[] = 'Agent Id '.$item;
                        } else {
                            $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                        }
                    }
                } else {
                    $agentList = AgentLocker::find($ruleValue);
                    if (!$agentList){
                        $valueText[] = 'Agent Id '.$ruleValue;
                    } else {
                        $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                    }
                }
            }
            else {
                if (in_array($ruleOperator,$multiple)){
                    $ruleValue = json_decode($ruleValue);
                    foreach ($ruleValue as $item) {
                        $valueText[] = $item;
                    }
                } else {
                    $valueText[] = $ruleValue;
                }
            }
            $rule->value_text = $valueText;
            $rule->parameter_name = $rule->parameter->description;
        }

        $data = [];
        $data['campaignDb'] = $campaignDb;
        $data['campaignRules'] = $campaignRules;

        // parse to email and view
        $environment = App::environment();
        $date = date('Y-m-d');
        if ($environment == 'production') {
            Mail::send('email.agent.marketing.campaign-approval', $data, function ($m) use ($environment, $date, $campaignDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('greta@popbox.asia', 'Greta PopBox Asia')->subject("[Agent] Campaign Promo Approval $date Name: $campaignDb->name");
                $m->to('lidya@popbox.asia', 'Lidya PopBox Asia')->subject("[Agent] Campaign Promo Approval $date Name: $campaignDb->name");
                $m->to('maja@popbox.asia', 'Maja PopBox Asia')->subject("[Agent] Campaign Promo Approval $date Name: $campaignDb->name");
                $m->cc('dyah@popbox.asia', 'Dyah PopBox Asia')->subject("[Agent] Campaign Promo Approval $date Name: $campaignDb->name");
            });
        } else {
            Mail::send('email.agent.marketing.campaign-approval', $data, function ($m) use ($environment, $date, $campaignDb) {
                $m->from('it@popbox.asia', 'PopBox Asia');
                $m->to('arief@popbox.asia', 'Finance PopBox Asia')->subject("[Agent][$environment] Referral Campaign Approval $date Name: $campaignDb->name");
            });
        }

        DB::connection('popbox_agent')->commit();

        if ($voucherRequired == 0){
            $request->session()->flash('success','Success Add Campaign');
            return redirect('agent/marketing/campaign');
        }

        return redirect("agent/marketing/campaign/addVoucher/$campaignId");
    }

    /**
     * Get Voucher Management Page
     * @param Request $request
     * @param null $campaignId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getAddVoucher(Request $request, $campaignId=null){
        if (empty($campaignId)){
            $request->session()->flash('error','Empty Campaign ID');
            return redirect('agent/marketing/campaign');
        }
        $campaignData = Campaign::find($campaignId);
        if (empty($campaignData)){
            $request->session()->flash('error','Campaign Not Found');
            return redirect('agent/marketing/campaign');
        }
        // get all voucher list
        $voucherList = CampaignVoucher::where('campaign_id',$campaignId)->get();

        // parse data
        $data = [];
        $data['campaignData'] = $campaignData;
        $data['voucherList'] = $voucherList;

        return view('agent.marketing.campaign-voucher',$data);
    }

    /**
     * Post Add Voucher
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postAddVoucher(Request $request){
        $campaignId = $request->input('campaignId');
        $format = $request->input('format',null);
        $amountGenerated = $request->input('generated',1);
        $formatLength = $request->input('format_length',4);

        $voucherCodeArray = [];
        // generated Voucher Code
        for ($i=0;$i<$amountGenerated;$i++){
            $isExist = true;
            while ($isExist){
                $code = $this->generateVoucherCode($format,$formatLength,$i+1);
                $check = in_array($code,$voucherCodeArray);
                if (!$check) $isExist = false;
            }
            $voucherCodeArray[] = $code;
        }

        CampaignVoucher::saveVoucher($campaignId,$voucherCodeArray);

        $request->session()->flash('success','Success Add Voucher');
        return back();
    }

    /**
     * Get List Pending Approval
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPendingApproval(Request $request){
        $nowDateTime = date('Y-m-d H:i:s');
        // get commission pending list
        $commissionPendingDb = CommissionSchema::with('commissionRules')
            ->where('approval_status','pending')
            ->where('end_date','>',$nowDateTime)
            ->orderBy('status','desc')
            ->paginate(5);

        $referralPendingDb = ReferralCampaign::where('approval_status','pending')
            ->where('end_date','>',$nowDateTime)
            ->orderBy('status','desc')
            ->paginate(5);

        $campaignPendingDb = Campaign::where('approval_status','pending')
            ->where('end_time','>',$nowDateTime)
            ->orderBy('status','desc')
            ->paginate(5);

        $data = [];
        $data['commissionPendingDb'] = $commissionPendingDb;
        $data['referralPendingDb'] = $referralPendingDb;
        $data['campaignPendingDb'] = $campaignPendingDb;

        return view('agent.marketing.approval-pending',$data);
    }

    /**
     * Get Pending Approval Pending
     * @param Request $request
     * @param null $type
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getPendingApprovalDetail(Request $request,$type=null,$id=null){
        if (empty($type) || empty($id)){
            $request->session()->flash('error','Invalid Request');
            return back();
        }

        if ($type == 'commission'){
            $commissionSchemaDb = CommissionSchema::find($id);
            if (!$commissionSchemaDb){
                $request->session()->flash('error','Invalid Commission ID');
                return back();
            }
            unset($commissionSchemaDb->product);
            unset($commissionSchemaDb->rule);
            unset($commissionSchemaDb->transaction_rule);
            $commissionRules = $commissionSchemaDb->commissionRules;
            $multiple = ['between','exist','except'];
            foreach ($commissionRules as $rule){
                $valueText = [];
                $parameterName = $rule->parameter->name;
                $ruleValue = $rule->value;
                $ruleOperator = $rule->operator;
                if ($parameterName == 'sepulsa_product_id'){
                    if (in_array($ruleOperator,$multiple)){
                        $ruleValue = json_decode($ruleValue);
                        foreach ($ruleValue as $item) {
                            // get sepulsa product
                            $sepulsaProduct = SepulsaProduct::where('product_id',$item)->first();
                            if (!$sepulsaProduct){
                                $valueText[] = 'Sepulsa Product Id '.$item;
                            } else {
                                $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                            }
                        }
                    } else {
                        $sepulsaProduct = SepulsaProduct::where('product_id',$ruleValue)->first();
                        if (!$sepulsaProduct){
                            $valueText[] = 'Sepulsa Product Id '.$ruleValue;
                        } else {
                            $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                        }
                    }
                }
                elseif ($parameterName == 'locker_id') {
                    if (in_array($ruleOperator,$multiple)){
                        $ruleValue = json_decode($ruleValue);
                        foreach ($ruleValue as $item) {
                            // get sepulsa product
                            $agentList = AgentLocker::find($item);
                            if (!$agentList){
                                $valueText[] = 'Agent Id '.$item;
                            } else {
                                $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                            }
                        }
                    } else {
                        $agentList = AgentLocker::find($ruleValue);
                        if (!$agentList){
                            $valueText[] = 'Agent Id '.$ruleValue;
                        } else {
                            $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                        }
                    }
                }
                else {
                    if (in_array($ruleOperator,$multiple)){
                        $ruleValue = json_decode($ruleValue);
                        foreach ($ruleValue as $item) {
                            $valueText[] = $item;
                        }
                    } else {
                        $valueText[] = $ruleValue;
                    }
                }
                $rule->value_text = $valueText;
                $rule->parameter_name = $rule->parameter->description;
            }

            $data = [];
            $data['schemaDb'] = $commissionSchemaDb;
            $data['commissionRules'] = $commissionRules;
            return view('agent.marketing.approval.commission',$data);
        } elseif ($type == 'referral'){
            $referralDb = ReferralCampaign::find($id);
            if (!$referralDb){
                $request->session()->flash('error','Invalid Referral ID');
                return back();
            }
            $data['referralDb'] = $referralDb;
            return view('agent.marketing.approval.referral',$data);
        } elseif ($type == 'campaign'){
            $campaignDb = Campaign::find($id);
            if (!$campaignDb){
                $request->session()->flash('error','Invalid Referral ID');
                return back();
            }

            $campaignRules = $campaignDb->parameters;
            $multiple = ['between','exist','except'];
            foreach ($campaignRules as $rule){
                $valueText = [];
                $parameterName = $rule->parameter->name;
                $ruleValue = $rule->value;
                $ruleOperator = $rule->operator;
                if ($parameterName == 'sepulsa_product_id'){
                    if (in_array($ruleOperator,$multiple)){
                        $ruleValue = json_decode($ruleValue);
                        foreach ($ruleValue as $item) {
                            // get sepulsa product
                            $sepulsaProduct = SepulsaProduct::where('product_id',$item)->first();
                            if (!$sepulsaProduct){
                                $valueText[] = 'Sepulsa Product Id '.$item;
                            } else {
                                $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                            }
                        }
                    } else {
                        $sepulsaProduct = SepulsaProduct::where('product_id',$ruleValue)->first();
                        if (!$sepulsaProduct){
                            $valueText[] = 'Sepulsa Product Id '.$ruleValue;
                        } else {
                            $valueText[] = strtoupper($sepulsaProduct->operator)."-".$sepulsaProduct->label."(PID: $sepulsaProduct->product_id)";
                        }
                    }
                }
                elseif ($parameterName == 'locker_id') {
                    if (in_array($ruleOperator,$multiple)){
                        $ruleValue = json_decode($ruleValue);
                        foreach ($ruleValue as $item) {
                            // get sepulsa product
                            $agentList = AgentLocker::find($item);
                            if (!$agentList){
                                $valueText[] = 'Agent Id '.$item;
                            } else {
                                $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                            }
                        }
                    } else {
                        $agentList = AgentLocker::find($ruleValue);
                        if (!$agentList){
                            $valueText[] = 'Agent Id '.$ruleValue;
                        } else {
                            $valueText[] = strtoupper($agentList->id)."-".$agentList->locker_name;
                        }
                    }
                }
                else {
                    if (in_array($ruleOperator,$multiple)){
                        $ruleValue = json_decode($ruleValue);
                        foreach ($ruleValue as $item) {
                            $valueText[] = $item;
                        }
                    } else {
                        $valueText[] = $ruleValue;
                    }
                }
                $rule->value_text = $valueText;
                $rule->parameter_name = $rule->parameter->description;
            }

            $data = [];
            $data['campaignDb'] = $campaignDb;
            $data['campaignRules'] = $campaignRules;
            return view('agent.marketing.approval.campaign',$data);
        }

        $request->session()->flash('error','Invalid Pending Type');
        return back();
    }

    /**
     * Post Pending Approval
     * @param Request $request
     * @param null $type
     * @param null $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postPendingApprovalDetail(Request $request,$type=null,$id=null){
        if (empty($type) || empty($id)){
            $request->session()->flash('error','Invalid Request');
            return back();
        }

        $rules = [
          'approvalStatus' => 'required|in:approved,rejected'
        ];

        $this->validate($request,$rules);

        if ($type == 'commission'){
            $commissionSchemaDb = CommissionSchema::find($id);
            if (!$commissionSchemaDb){
                $request->session()->flash('error','Invalid Commission ID');
                return back();
            }
            // get user data
            $userDb = Auth::user();
            $userName = $userDb->name;
            $approvalStatus = $request->input('approvalStatus');

            $commissionSchemaDb->approved_by = $userName;
            $commissionSchemaDb->approval_status = $approvalStatus;
            $commissionSchemaDb->save();

            $request->session()->flash('success',"Success $approvalStatus Commission Schema");
            return redirect('agent/marketing/approval');
        } elseif ($type == 'referral'){
            $referralDb = ReferralCampaign::find($id);
            if (!$referralDb){
                $request->session()->flash('error','Invalid Referral ID');
                return back();
            }
            // get user data
            $userDb = Auth::user();
            $userName = $userDb->name;
            $approvalStatus = $request->input('approvalStatus');

            $referralDb->approved_by = $userName;
            $referralDb->approval_status = $approvalStatus;
            $referralDb->save();

            $request->session()->flash('success-referral',"Success $approvalStatus Referral Schema");
            return redirect('agent/marketing/approval');
        } elseif ($type == 'campaign'){
            $campaignDb = Campaign::find($id);
            if (!$campaignDb){
                $request->session()->flash('error','Invalid Referral ID');
                return back();
            }
            // get user data
            $userDb = Auth::user();
            $userName = $userDb->name;
            $approvalStatus = $request->input('approvalStatus');

            $campaignDb->approved_by = $userName;
            $campaignDb->approval_status = $approvalStatus;
            $campaignDb->save();

            $request->session()->flash('success-campaign',"Success $approvalStatus Campaign Promo");
            return redirect('agent/marketing/approval');
        }

        $request->session()->flash('error','Invalid Pending Type');
        return back();
    }

    /*======================== Private Function ========================*/
    /**
     * Generate Voucher Code
     * @param string $format
     * @param int $formatLength
     * @param int $count
     * @return string
     */
    private function generateVoucherCode($format='',$formatLength = 4,$count=1){
        $stringArr = explode('-',$format);
        $code = '';
        if (empty($formatLength)) $formatLength = 4;
        foreach ($stringArr as $item) {
            switch ($item){
                case "{ordernum}" :
                    $formatValue=sprintf("%0".$formatLength."d", $count);
                    $code .= $formatValue;
                    break;
                case "{randomnum}" :
                    $formatValue = $this->generateRandomNumber($formatLength);
                    $code .= $formatValue;
                    break;
                case "{randomalphanum}" :
                    $formatValue = $this->generateRandomAlphaNum($formatLength);
                    $code .= $formatValue;
                    break;
                default :
                    $code .= $item;
            }
        }
        return $code;
    }

    /**
     * Generate Random Number
     * @param int $length
     * @return string
     */
    private function generateRandomNumber($length = 4){
        $characters = '0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($j = 0; $j < $length; $j++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }

    /**
     * Generate Random Alpha Number
     * @param int $length
     * @return string
     */
    private function generateRandomAlphaNum($length = 4){
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ';
        $string = '';
        $max = strlen($characters) - 1;
        for ($k = 0; $k < $length; $k++) {
            $string .= $characters[mt_rand(0, $max)];
        }
        return $string;
    }
}
