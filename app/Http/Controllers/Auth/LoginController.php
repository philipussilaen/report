<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('auth/logout', 'logout');
    }

    /**
     * Get View Login
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin(){
        return view('auth.login');
    }

    /**
     * Post Login
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postLogin(Request $request){
        // create validation
        $rules = [
            "email" => "required|email",
            "password" => "required|min:6"
        ];
        $this->validate($request,$rules);

        $email = $request->input('email');
        $password = $request->input('password');

        $credential = [
            'email' => $email,
            'password' => $password,
        ];

        if (!Auth::attempt($credential)){
            return redirect()->back()->with("failed","Invalid Credential");
        }

        $userData = Auth::user();
        if ($userData->status != 'enable'){
            Auth::logout();
            return redirect()->back()->with("failed","User Disabled");
        }

        return redirect()->intended('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('auth/login');
    }
}
