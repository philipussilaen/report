<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Employee;
use App\Models\Popexpress\Pickup;
use App\Models\Popexpress\PickupTask;
use App\Models\UserGroup;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

class PickupTaskController extends Controller
{

    public function index(Request $request)
    {
        $pickup_no = request('pickup_no');
        $courier_id = request('courier_id');
        $created_from = request('created_from');
        $created_to = request('created_to');

        $start = substr($created_from, 6, 4).'-'.substr($created_from, 3, 2).'-'.substr($created_from, 0, 2);
        $end = substr($created_to, 6, 4).'-'.substr($created_to, 3, 2).'-'.substr($created_to, 0, 2);

        $pickupTasks = PickupTask::leftJoin('pickups', 'pickups.id', '=', 'pickup_tasks.pickup_id')
            ->leftJoin(DB::raw('employees as courier'), 'courier.id', '=', 'pickup_tasks.courier_id')
            ->leftJoin('employees', 'employees.id', '=', 'pickup_tasks.assignee_id')
            ->where('pickup_tasks.deleted_at', null)
            ->select(
                'pickup_tasks.*',
                'pickup_tasks.id as pickuptaskid',
                'pickups.pickup_no as pickup_no',
                'pickups.pickup_status as pickup_status',
                'courier.user_id as courier_user_id',
                'employees.user_id as employee_user_id',
                DB::raw("DATE_FORMAT(pickup_tasks.start_date, '%d %b %Y %H:%m') AS start_date"),
                DB::raw("DATE_FORMAT(pickup_tasks.cancel_date, '%d %b %Y %H:%m') AS cancel_date"),
                DB::raw("DATE_FORMAT(pickup_tasks.pickup_date, '%d %b %Y %H:%m') AS pickup_date"),
                DB::raw("DATE_FORMAT(pickup_tasks.created_at, '%d %b %Y %H:%m') AS created")
            )
            ->when($pickup_no, function ($query) use ($pickup_no) {
                return $query->where('pickups.pickup_no', 'LIKE', "%$pickup_no%");
            })
            ->when($courier_id, function ($query) use ($courier_id) {
                return $query->where('courier.user_id', '=', $courier_id);
            })
            ->when($created_from, function ($query) use ($start) {
                return $query->whereDate('pickup_tasks.created_at', '>=', $start);
            })
            ->when($created_to, function ($query) use ($end) {
                return $query->whereDate('pickup_tasks.created_at', '<=', $end);
            })
            ->orderBy('pickup_tasks.created_at', 'desc')
            ->paginate(20);

        $getEmployees = Employee::where('deleted_at', null)->pluck('user_id')->toArray();
        $users = User::where('deleted_at', null)->whereIn('id', $getEmployees)->pluck('name', 'id')->toArray();
        $cancelTypes = ['unfulfillment' => 'danger','bad_address' => 'danger','customer_cancel'=> 'info','reassign'=> 'warning','not_available'=> 'success'];
        $cancelDatas = ['unfulfillment' => 'Tugas tidak bisa dipenuhi','bad_address' => 'Alamat tidak ditemukan','customer_cancel'=> 'Pelanggan membatalkan','reassign'=> 'Ditugaskan kembali','not_available'=> 'Pelanggan tidak ada'];
        $pickups = Pickup::where('deleted_at', null)->whereIn('pickup_status', ['requested','pending'])->select('id as pickupid', 'pickup_no')->orderBy('pickup_no')->get();
        $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                        ->leftJoin('users', 'users.id', '=', 'user_groups.user_id')
                        ->where('user_groups.deleted_at', null)
                        ->whereIn('user_id', $getEmployees)
                        ->where('groups.name', 'popexpress_courier')
                        ->pluck('users.name', 'users.id')
                        ->toArray();

        return view('popexpress.pickup_tasks.index', compact('pickupTasks', 'users', 'cancelTypes', 'pickups', 'userGroups', 'cancelDatas'));

    }

    public function store(Request $request)
    {
        $id = request('id');
        $pickup_id = request('pickup_id');
        $courier_id = request('courier_id');
        $cancel_type = request('cancel_type');
        $cancel_remarks = request('cancel_remarks');
        $cancel = request('cancel');
        $typeData = request('type');

        if(empty($id)) {

            $this->validate(request(), [
                'pickup_id' => 'required',
                'courier_id' => 'required',
            ],[
                'pickup_id.required' => 'Pickup diperlukan.',
                'courier_id.required' => 'Name diperlukan.',
            ]);

            if(!Pickup::where('id', $pickup_id)->whereIn('pickup_status', ['requested','pending'])->first()){
                return redirect('/popexpress/pickups/list_pickup_tasks')->withInput()->withErrors([
                    'message' => 'Pilih pickup no lain. no pickup ini tidak dapat digunakan.'
                ]);
            }

            $courier = Employee::where('user_id', $courier_id)->orderBy('created_at', 'DESC')->first();
            $assignee = Employee::where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->first();

            $check = PickupTask::where('pickup_id', $pickup_id)->count();
            $pickup = Pickup::where('id', $pickup_id)->first();

            $task = new PickupTask();
            $task->pickup_id = $pickup_id;
            $task->courier_id = $courier->id;
            $task->assignee_id = $assignee->id;
            $task->task_no = $pickup->pickup_no.'-'.sprintf('%02d', ($check+1));
            $task->updated_at =  date('Y-m-d H:i:s');
            $task->server_timestamp =  date('Y-m-d H:i:s');

            if($task->save()) {
                $pickup->pickup_status = 'assigned';
                $pickup->updated_at = date('Y-m-d H:i:s');
                $pickup->server_timestamp = date('Y-m-d H:i:s');
                $pickup->save();

                $data = $task->getAttributes();
                $key = 'id';
                $module = 'pickup_tasks';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data telah berhasil disimpan.');

        } else {
            if($cancel == 1) {
                $this->validate(request(), [
                    'id' => 'required',
                    'cancel_type' => 'required',
                    'cancel_remarks' => 'required',
                ],[
                    'id.required' => 'Pickup diperlukan.',
                    'cancel_type.required' => 'Name diperlukan.',
                    'cancel_remarks' => 'Remarks diperlukan.',
                ]);

                $task = PickupTask::where('id', '=', $id)->first();
                $pickup_id = $task->pickup_id;
                $old = $task->getAttributes();

                if(is_null($task->start_date)) {
                    $task->start_date = date('Y-m-d H:i:s');
                }
                $task->cancel_date = date('Y-m-d H:i:s');
                $task->cancel_type = $cancel_type;
                $task->cancel_remarks = $cancel_remarks;

                if($task->save()) {
                    $pickup = Pickup::where('id', $pickup_id)->first();
                    $pickup->pickup_status = 'pending';
                    $pickup->updated_at = date('Y-m-d H:i:s');
                    $pickup->server_timestamp = date('Y-m-d H:i:s');
                    $pickup->save();

                    $new = $task->getAttributes();
                    $key = $task->id;
                    $module = 'pickup_tasks';
                    $type = 'edit';
                    $diffBefore = array_diff_assoc($old, $new);
                    $diffAfter = array_diff_assoc($new, $old);
                    $jsonBefore = json_encode($diffBefore);
                    $jsonAfter = json_encode($diffAfter);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);

                    session()->flash('success', 'Data telah berhasil dicancel.');
                }
            }

        }

        if($typeData == "view") {
            return redirect('/popexpress/pickups/view/'.$pickup_id);
        } else {
            return redirect('/popexpress/pickups/list_pickup_tasks');
        }

    }

}