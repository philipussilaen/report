<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Group;
use App\Models\Popexpress\Branch;
use App\Models\Popexpress\Employee;
use App\Models\UserGroup;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller {

    protected $access = false;
    protected $allowedGroups = ['popexpress_admin', 'popexpress_hr'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userid = Auth::user()->id;
            $allowed = $this->allowedGroups;
            $userGroups = UserGroup::leftJoin('groups', 'groups.id', '=', 'user_groups.group_id')
                ->where('user_groups.deleted_at', null)
                ->where('user_groups.user_id', $userid)
                ->where(function($query) use ($allowed){
                    foreach ($allowed as $allow) {
                        $query->orWhere('groups.name', '=', $allow);
                    }
                })
                ->first();
            $this->access = (!is_null($userGroups) ? true : false);

            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $code = request('code');
        $email = request('email');
        $name = request('name');
        $branch = request('branch');
        $group = request('group');
        $phone = request('phone');

        $getEmployees = Employee::when($code, function ($query) use ($code) {
                return $query->where('employees.code', 'LIKE', "%$code%");
            })
            ->when($branch, function ($query) use ($branch) {
                return $query->where('employees.branch_id', '=', $branch);
            })
            ->pluck('user_id')->toArray();

        $getUsers = User::leftjoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->whereIn('users.id', $getEmployees)
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('users.name', 'LIKE', "%$name%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->when($group, function ($query) use ($group) {
                return $query->where('user_groups.group_id', '=', $group);
            })
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email',
                DB::raw("(select GROUP_CONCAT(groups.name) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupuser")
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email,
                'groupuser' => $getUser->groupuser,
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $employees = Employee::leftjoin(DB::raw('branches as express_branches'), DB::raw('express_branches.id'), '=', DB::raw('employees.branch_id'))
            ->where('employees.deleted_at', null)
            ->select(
                DB::raw('employees.user_id as report_users_id'),
                DB::raw('employees.id as employees_id'),
                DB::raw('employees.code as code'),
                DB::raw("CONCAT(express_branches.code, ' - ', express_branches.name) as branch_name"),
                DB::raw("IF(employees.all_branch = 1, 'All Branch', '') as is_all_branch")
            )
            ->whereIn('employees.user_id', $userlist)
            ->orderBy('employees.code')
            ->paginate(20);

        $branches = Branch::where('deleted_at', null)->get();
        $groups = Group::select('id', 'name')->where('deleted_at', null)->where('name', 'LIKE', 'popexpress_%')->get();
        $access = $this->access;

        return view('popexpress.employees.index', compact('employees', 'branches', 'groups', 'access', 'users'));
    }

    public function create()
    {
        if(!$this->access) {
            abort('404');
        }
        $statuses = ['disable', 'enable'];
        $branches = Branch::where('deleted_at', null)->get();
        $groups = Group::select('id', 'name')->where('deleted_at', null)->where('name', 'LIKE', 'popexpress_%')->get();

        return view('popexpress.employees.create', compact('statuses', 'branches', 'groups'));
    }

    public function edit($id)
    {
        $statuses = ['disable', 'enable'];
        $branches = Branch::where('deleted_at', null)->get();
        $groups = Group::select('id', 'name')->where('deleted_at', null)->where('name', 'LIKE', 'popexpress_%')->get();

        $getUsers = User::leftjoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->where('users.id', $id)
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email',
                'users.status',
                DB::raw("(select GROUP_CONCAT(groups.name) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupuser"),
                DB::raw("(select GROUP_CONCAT(groups.id) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupiduser")
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email,
                'status' => $getUser->status,
                'groupuser' => $getUser->groupuser,
                'groupiduser' => $getUser->groupiduser,
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $employee = Employee::leftjoin(DB::raw('branches as express_branches'), DB::raw('express_branches.id'), '=', DB::raw('employees.branch_id'))
            ->where('employees.deleted_at', null)
            ->select(
                DB::raw('employees.user_id as report_users_id'),
                DB::raw('employees.id as employees_id'),
                DB::raw('employees.code as code'),
                DB::raw('employees.branch_id as branch_id'),
                DB::raw('employees.all_branch as all_branch'),
                DB::raw("CONCAT(express_branches.code, ' - ', express_branches.name) as branch_name"),
                DB::raw("IF(employees.all_branch = 1, 'All Branch', '') as is_all_branch")
            )
            ->whereIn('employees.user_id', $userlist)
            ->orderBy('employees.code')
            ->first();

        $access = $this->access;

        return view('popexpress.employees.edit', compact('statuses', 'branches', 'groups', 'employee', 'access', 'users'));
    }

    public function store(Request $request)
    {
        if(!$this->access) {
            abort('404');
        }
        $id = request('id');
        $delete = request('delete');
        $code = request('code');
        $name = request('name');
        $email = request('email');
        $phone = request('phone');
        $username = request('username');
        $password = request('password');
        $status = request('status');
        $branch = request('branch');
        $all_branch = request('all_branch');
        $groups = request('employee_group');
        $timestamp = date('Y-m-d H:i:s');

        if(empty($id)) {

            $this->validate(request(), [
                'code' => 'required',
                'name' => 'required',
                'password' => 'required',
                'email' => 'required|unique:users',
                'phone' => 'required|unique:users',
                'branch' => 'required',
                'employee_group' => 'required',
            ],[
                'code.required' => 'Code diperlukan.',
                'name.required' => 'Name diperlukan.',
                'password.required' => 'Password diperlukan.',
                'email.required' => 'Email diperlukan.',
                'email.unique' => 'Email sudah terdaftar.',
                'phone.required' => 'Phone diperlukan.',
                'phone.unique' => 'Phone sudah terdaftar.',
                'branch.required' => 'Branch diperlukan.',
                'employee_group.required' => 'Group diperlukan.',
            ]);

            if(Employee::where('code', $code)->first()){
                return redirect('/popexpress/employees/create')->withInput()->withErrors([
                    'message' => 'Code empployee sudah terdaftar.'
                ]);
            }

            if(!empty($username)) {
                if(User::where('username', $username)->first()){
                    return redirect('/popexpress/employees/create')->withInput()->withErrors([
                        'message' => 'Username sudah terdaftar.'
                    ]);
                }
            }

            DB::beginTransaction();

                $user = new User();
                $user->name = $name;
                $user->email = $email;
                $user->phone = $phone;
                $user->password = bcrypt($password);
                $user->status = $status;
                $user->group_id = 7;
                $user->company_id = 1;
                $user->username = $username;
                $user->popexpress_api_key = CommonPopExpressHelper::generateRandomString(10, false, true, true);
                $user->server_timestamp = $timestamp;
                $user->save();

                $user_id = $user->id;


                foreach ($groups as $group) {
                    $user_group = new UserGroup();
                    $user_group->user_id = $user_id;
                    $user_group->group_id = $group;
                    $user_group->server_timestamp = $timestamp;
                    $user_group->save();
                }

                $employee = new Employee();
                $employee->code = $code;
                $employee->user_id = $user_id;
                $employee->branch_id = $branch;
                $employee->all_branch = (is_null($all_branch) ? 0 : 1);
                $employee->server_timestamp = $timestamp;
                $employee->save();

                if($employee->save()) {
                    DB::commit();

                    $data = $employee->getAttributes();
                    $key = $employee->id;
                    $module = 'employees';
                    $type = 'add';
                    $jsonBefore = null;
                    $jsonAfter = json_encode($data);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                } else {
                    DB::rollBack();
                }

            session()->flash('success', 'Data employee telah berhasil disimpan.');

        } else {

            $employee = Employee::where('id', '=', $id)->first();

            if($delete == '1') {
                DB::beginTransaction();

                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deleteEmployee = Employee::where('deleted_at', null)->where('id', $id)->update($dataDelete);

                $userGroups = UserGroup::where('user_id', $employee->user_id)->where('deleted_at', null)->get();

                UserGroup::where('deleted_at', null)->where('user_id', $employee->user_id)->update($dataDelete);


                $deleteUser = User::where('deleted_at', null)->where('id', $employee->user_id)->update($dataDelete);

                if($deleteUser){
                    DB::commit();
                    $key = $id;
                    $module = 'employees';
                    $type = 'delete';
                    $jsonBefore = json_encode($employee);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                } else {
                    DB::rollBack();
                }

            } else {

                $this->validate(request(), [
                    'code' => 'required',
                    'name' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'branch' => 'required',
                    'employee_group' => 'required',
                ],[
                    'code.required' => 'Code diperlukan.',
                    'name.required' => 'Name diperlukan.',
                    'email.required' => 'Email diperlukan.',
                    'phone.required' => 'Phone diperlukan.',
                    'branch.required' => 'Branch diperlukan.',
                    'employee_group.required' => 'Group diperlukan.',
                ]);

                if(Employee::where('code', $code)->where('user_id', '!=', $id)->first()){
                    return redirect('/popexpress/employees')->withInput()->withErrors([
                        'message' => 'Code sudah terdaftar.'
                    ]);
                }

                if(!empty($email)) {
                    if(User::where('email', $email)->where('id', '!=', $id)->first()){
                        return redirect()->back()->withInput()->withErrors([
                            'message' => 'Email sudah terdaftar.'
                        ]);
                    }
                }

                if(!empty($phone)) {
                    if(User::where('phone', $phone)->where('id', '!=', $id)->first()){
                        return redirect()->back()->withInput()->withErrors([
                            'message' => 'Phone sudah terdaftar.'
                        ]);
                    }
                }

                if(!empty($username)) {
                    if(User::where('username', $username)->where('id', '!=', $id)->first()){
                        return redirect()->back()->withInput()->withErrors([
                            'message' => 'Username sudah terdaftar.'
                        ]);
                    }
                }

                DB::beginTransaction();

                $user = User::where('id', $id)->first();
                $user->name = $name;
                $user->email = $email;
                $user->phone = $phone;
                $user->status = $status;
                $user->username = $username;
                $user->server_timestamp = $timestamp;
                $user->save();

                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                UserGroup::where('deleted_at', null)->where('user_id', $id)->update($dataDelete);
                foreach ($groups as $group) {
                    $user_group = new UserGroup();
                    $user_group->user_id = $id;
                    $user_group->group_id = $group;
                    $user_group->server_timestamp = $timestamp;
                    $user_group->save();
                }

                $employee = Employee::where('user_id', $id)->first();
                $employee->code = $code;
                $employee->branch_id = $branch;
                $employee->all_branch = (is_null($all_branch) ? 0 : 1);
                $employee->server_timestamp = $timestamp;
                if($employee->save()) {
                    DB::commit();
                    $key = $id;
                    $module = 'employees';
                    $type = 'edit';
                    $jsonBefore = json_encode($employee);
                    $jsonAfter = json_encode($employee);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data employee telah berhasil diperbarui.');
                } else {
                    DB::rollBack();
                    session()->flash('error', 'Data employee gagal diperbarui.');
                }
            }
        }

        return redirect('/popexpress/employees');

    }

    public function export(Request $request)
    {
        if(!$this->access) {
            abort('404');
        }
        $code = request('code');
        $email = request('email');
        $name = request('name');
        $branch = request('branch');
        $group = request('group');
        $phone = request('phone');

        $getEmployees = Employee::when($code, function ($query) use ($code) {
            return $query->where('employees.code', 'LIKE', "%$code%");
        })
            ->when($branch, function ($query) use ($branch) {
                return $query->where('employees.branch_id', '=', $branch);
            })
            ->pluck('user_id')->toArray();

        $getUsers = User::leftjoin('user_groups', 'user_groups.user_id', '=', 'users.id')
            ->whereIn('users.id', $getEmployees)
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($name, function ($query) use ($name) {
                return $query->where('users.name', 'LIKE', "%$name%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->when($group, function ($query) use ($group) {
                return $query->where('user_groups.group_id', '=', $group);
            })
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.username',
                'users.phone',
                'users.email',
                DB::raw("(select GROUP_CONCAT(groups.name) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupuser")
            )
            ->get();

        $users = [];
        $userlist = [];

        foreach ($getUsers as $getUser) {

            $users[$getUser->report_users_id] = [
                'report_users_id' => $getUser->report_users_id,
                'name' => $getUser->name,
                'username' => $getUser->username,
                'phone' => $getUser->phone,
                'email' => $getUser->email,
                'groupuser' => $getUser->groupuser,
            ];
            $userlist[] = $getUser->report_users_id;

        }

        $employees = Employee::leftjoin(DB::raw('branches as express_branches'), DB::raw('express_branches.id'), '=', DB::raw('employees.branch_id'))
            ->where('employees.deleted_at', null)
            ->select(
                DB::raw('employees.user_id as report_users_id'),
                DB::raw('employees.id as employees_id'),
                DB::raw('employees.code as code'),
                DB::raw("CONCAT(express_branches.code, ' - ', express_branches.name) as branch_name"),
                DB::raw("IF(employees.all_branch = 1, 'All Branch', '') as is_all_branch")
            )
            ->whereIn('employees.user_id', $userlist)
            ->orderBy('employees.code')
            ->get();

        $groups = Group::where('deleted_at', null)->where('name', 'LIKE', 'popexpress_%')->pluck('name')->toArray();

        $filename = "employees_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($employees, $groups, $users){
            $excel->sheet('Sheet1', function($sheet) use($employees, $groups, $users){
                $row = 1;
                $arr_title = ['code','name','username','email','branch','all_branch'];
                $arr_title = array_merge($arr_title, $groups);

                $sheet->row($row, $arr_title);

                foreach ( $employees as $index => $item) {
                    $row++;
                    $arr = [
                        $item->code,
                        $users[$item->report_users_id]['name'],
                        $users[$item->report_users_id]['username'],
                        $users[$item->report_users_id]['email'],
                        $item->branch_name,
                        $item->is_all_branch
                    ];

                    $dataGroup = [];
                    foreach ($groups as $group) {
                        if(strpos($users[$item->report_users_id]['groupuser'], $group) !== false) {
                            array_push($dataGroup, 'YES');
                        } else {
                            array_push($dataGroup, 'NO');
                        }
                    }

                    $arr = array_merge($arr, $dataGroup);

                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function changePassword(Request $request)
    {
        if (!$this->access) {
            abort('404');
        }
        $id = request('id');
        $password = request('password');
        $confirm_password = request('confirm_password');

        $this->validate(request(), [
            'password' => 'required',
            'confirm_password' => 'required'
        ],[
            'password.required' => 'New password diperlukan.',
            'confirm_password.required' => 'Confirm password diperlukan.'
        ]);

        if($password != $confirm_password) {
            return redirect('/popexpress/employees/edit/'.$id)->withErrors([
                'message' => 'Password tidak sama.'
            ]);
        }

        $user = User::where('id', $id)->first();
        $user->password = bcrypt($password);
        if($user->save()) {

            $data = $user->getAttributes();
            $key = $id;
            $module = 'employees';
            $type = 'edit';
            $jsonBefore = null;
            $jsonAfter = json_encode($data);
            $remark = null;
            CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);

            session()->flash('success', 'Password berhasil diperbarui.');
            return redirect('/popexpress/employees/edit/'.$id);
        } else {
            session()->flash('error', 'Password gagal diperbarui.');
            return redirect('/popexpress/employees/edit/'.$id)->withInput()->withErrors([
                'message' => 'Password gagal diperbarui.'
            ]);
        }
    }

    public function transfer()
    {
        if(!$this->access) {
            abort('404');
        }

        $email = request('email');
        $phone = request('phone');

        $getEmployees = Employee::where('deleted_at', null)->pluck('user_id')->toArray();
        $getUsers = User::where('users.deleted_at', null)
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.phone',
                'users.email'
            )
            ->when($email, function ($query) use ($email) {
                return $query->where('users.email', 'LIKE', "%$email%");
            })
            ->when($phone, function ($query) use ($phone) {
                return $query->where('users.phone', 'LIKE', "%$phone%");
            })
            ->paginate(20);

        return view('popexpress.employees.transfer', compact('getEmployees', 'getUsers'));

    }

    public function editTransfer($id)
    {
        if(!$this->access) {
            abort('404');
        }

        $user = User::where('users.deleted_at', null)
            ->where('users.id', $id)
            ->select(
                'users.id as report_users_id',
                'users.name',
                'users.phone',
                'users.email',
                DB::raw("(select GROUP_CONCAT(groups.name) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupuser"),
                DB::raw("(select GROUP_CONCAT(groups.id) AS group_user from user_groups left join groups on groups.id = user_groups.group_id where user_groups.user_id = users.id and user_groups.deleted_at is null) as groupiduser")
            )
            ->first();

        $employee = Employee::where('user_id', $id)->first();

        $groups = Group::select('id', 'name')->where('deleted_at', null)->where('name', 'LIKE', 'popexpress_%')->get();
        $branches = Branch::where('deleted_at', null)->get();

        return view('popexpress.employees.edit_transfer', compact('user', 'groups', 'branches', 'employee'));

    }

    public function storeTransfer(Request $request)
    {
        if(!$this->access) {
            abort('404');
        }
        $id = request('id');
        $branch = request('branch');
        $all_branch = request('all_branch');
        $groups = request('employee_group');
        $timestamp = date('Y-m-d H:i:s');

        $this->validate(request(), [
            'id' => 'required',
            'employee_group' => 'required',
            'branch' => 'required'
        ],[
            'id.required' => 'Id diperlukan.',
            'employee_group.required' => 'Group diperlukan.',
            'branch.required' => 'Branch diperlukan.',
        ]);

        DB::beginTransaction();

        $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
        UserGroup::where('deleted_at', null)->where('user_id', $id)->update($dataDelete);
        foreach ($groups as $group) {
            $user_group = new UserGroup();
            $user_group->user_id = $id;
            $user_group->group_id = $group;
            $user_group->server_timestamp = $timestamp;
            $user_group->save();
        }

        $employee = Employee::where('user_id', $id)->first();
        if(is_null($employee)) {
            $employee = new Employee();
            $employee->user_id = $id;
            $employee->code = CommonPopExpressHelper::generateRandomString(5, true, false, true);
            $employee->branch_id = $branch;
            $employee->all_branch = (is_null($all_branch) ? 0 : 1);
            $employee->server_timestamp = $timestamp;
        } else {
            $employee->user_id = $id;
            $employee->branch_id = $branch;
            $employee->all_branch = (is_null($all_branch) ? 0 : 1);
            $employee->server_timestamp = $timestamp;
        }
        if($employee->save()) {
            DB::commit();
            $key = $id;
            $module = 'employees';
            $type = 'edit';
            $jsonBefore = json_encode($employee);
            $jsonAfter = json_encode($employee);
            $remark = null;
            CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            session()->flash('success', 'Data transfer user telah berhasil diperbarui.');
        } else {
            DB::rollBack();
            session()->flash('error', 'Data transfer user gagal diperbarui.');
        }

        return redirect('/popexpress/employees/transfer');

    }

}