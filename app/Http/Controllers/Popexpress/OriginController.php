<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Origin;
use Illuminate\Support\Facades\Request;

class OriginController extends Controller {

    public function index(Request $request)
    {
        $name = request('name');

        $origins = Origin::where('origins.deleted_at', null)
            ->select('origins.*')
            ->when($name, function ($query) use ($name) {
                return $query->where('origins.name', 'LIKE', "%$name%");
            })
            ->paginate(20);

        return view('popexpress.origin.index', compact('origins'));
    }

    public function store(Request $request)
    {
        $id = request('id');
        $name = request('name');
        $delete = request('delete');

        if(empty($id)) {
            $this->validate(request(), [
                'name' => 'required'
            ],[
                'name.required' => 'Nama diperlukan.'
            ]);

            if(Origin::where('name', $name)->first()){
                return redirect('/popexpress/origins')->withInput()->withErrors([
                    'message' => 'Nama origin telah terdaftar. Silakan input nama lain.'
                ]);
            }

            $origin = Origin::create([
                'name' => request('name'),
                'server_timestamp' => date('Y-m-d H:i:s')
            ]);

            if($origin) {
                $data = $origin->getAttributes();
                $key = $origin->id;
                $module = 'origins';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }

            session()->flash('success', 'Data origin telah berhasil disimpan.');
        } else {
            $origin = Origin::where('id', '=', $id)->first();
            $old = $origin->getAttributes();

            if($delete == '1') {
                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deleteOrigin = Origin::where('deleted_at', null)->where('id', $id)->update($dataDelete);
                if($deleteOrigin){
                    $key = $id;
                    $module = 'origins';
                    $type = 'delete';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                }
            } else {
                $this->validate(request(), [
                    'name' => 'required'
                ],[
                    'name.required' => 'Nama diperlukan.'
                ]);

                if(Origin::where('name', $name)->where('id', '!=', $id)->first()){
                    return redirect('/popexpress/origins')->withInput()->withErrors([
                        'message' => 'Nama origin telah terdaftar. Silakan input nama lain.'
                    ]);
                }

                $origin->name = request('name');
                $origin->server_timestamp = date("Y-m-d H:i:s");
                $origin->save();
                $new = $origin->getAttributes();
                $diff = array_diff_assoc($old, $new);

                if($origin) {
                    $key = $id;
                    $module = 'origins';
                    $type = 'edit';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($diff);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                }
                session()->flash('success', 'Data origin telah berhasil diperbarui.');
            }
        }

        return redirect('/popexpress/origins');
    }
}