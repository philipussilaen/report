<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Destination;
use App\Models\Popexpress\Origin;
use App\Models\Popexpress\Price;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class PriceController extends Controller {

    public function index(Request $request)
    {
        $origin_id = request('origin_id');
        $destination_id = request('destination_id');

        $prices = Price::leftJoin('origins', 'origins.id', '=', 'prices.origin_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
            ->where('prices.deleted_at', null)
            ->select(
                'prices.*',
                'origins.name as origin_name',
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('prices.origin_id', '=', $origin_id);
            })
            ->when($destination_id, function ($query) use ($destination_id) {
                return $query->where('prices.destination_id', '=', $destination_id);
            })
            ->orderBy('prices.origin_id')
            ->paginate(20);

        $origins = Origin::where('deleted_at', null)->get();

        $destinations = Destination::where('destinations.deleted_at', null)
            ->select(
                DB::raw('destinations.id'),
                DB::raw("CONCAT(destinations.detail_code, ' - ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
            )
            ->get();

        return view('popexpress.prices.index', compact('prices', 'origins', 'destinations'));
    }

    public function getDestination(Request $request)
    {
        $origin_id = request('origin_id');
        $destinationNames = Destination::where('destinations.deleted_at', null)
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('destinations.origin_id', '=', $origin_id);
            })
            ->select(
                DB::raw('destinations.id'),
                DB::raw("CONCAT(destinations.detail_code, ' - ', destinations.province, ', ', destinations.county, ', ', destinations.district) AS name")
            )
            ->get()->toArray();

        $output['data'] = $destinationNames;
        $output['success'] =  true;

        return response()->json($output);
    }

    public function store(Request $request)
    {
        $id = request('id');
        $delete = request('delete');
        $origin_id = request('origin_id');
        $destination_id = request('destination_id');
        $price_regular = request('price_regular');
        $price_one = request('price_one');
        $estimation_early = request('estimation_early');
        $estimation_late = request('estimation_late');


        if(empty($id)) {
            $this->validate(request(), [
                'origin_id' => 'required',
                'destination_id' => 'required',
                'price_regular' => 'required',
                'price_one' => 'required',
                'estimation_early' => 'required',
                'estimation_late' => 'required'
            ],[
                'origin_id.required' => 'Origin diperlukan.',
                'destination_id.required' => 'Destination diperlukan.',
                'price_regular.required' => 'Regular Price diperlukan.',
                'price_one.required' => 'Price One diperlukan.',
                'estimation_early.required' => 'Estimation Early diperlukan.',
                'estimation_late.required' => 'Estimation Late diperlukan.',
            ]);

            if(!empty($origin_id) && !empty($destination_id) ) {
                if(Price::where('origin_id', $origin_id)->where('destination_id', $destination_id)->where('deleted_at', null)->first()){
                    return redirect('/popexpress/prices')->withInput()->withErrors([
                        'message' => 'Harga dengan origin dan destination yang sama sudah terdaftar.'
                    ]);
                }
            }

            $price = new Price();
            $price->origin_id = $origin_id;
            $price->destination_id = $destination_id;
            $price->price_regular = $price_regular;
            $price->price_one = $price_one;
            $price->estimation_early = $estimation_early;
            $price->estimation_late = $estimation_late;
            $price->server_timestamp = date('Y-m-d H:i:s');

            if($price->save()) {
                $data = $price->getAttributes();
                $key = $price->id;
                $module = 'prices';
                $type = 'add';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }
            session()->flash('success', 'Data price telah berhasil disimpan.');
        } else {
            $price = Price::where('id', '=', $id)->first();
            $old = $price->getAttributes();

            if($delete == '1') {
                $dataDelete = ['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')];
                $deletePrice = Price::where('deleted_at', null)->where('id', $id)->update($dataDelete);
                if($deletePrice){
                    $key = $id;
                    $module = 'prices';
                    $type = 'delete';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($dataDelete);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                    session()->flash('success', 'Data telah dihapus.');
                }
            } else {
                $this->validate(request(), [
                    'origin_id' => 'required',
                    'destination_id' => 'required',
                    'price_regular' => 'required',
                    'price_one' => 'required',
                    'estimation_early' => 'required',
                    'estimation_late' => 'required'
                ],[
                    'origin_id.required' => 'Origin diperlukan.',
                    'destination_id.required' => 'Destination diperlukan.',
                    'price_regular.required' => 'Regular Price diperlukan.',
                    'price_one.required' => 'Price One diperlukan.',
                    'estimation_early.required' => 'Estimation Early diperlukan.',
                    'estimation_late.required' => 'Estimation Late diperlukan.',
                ]);

                if(!empty($origin_id) && !empty($destination_id) ) {
                    if(Price::where('origin_id', $origin_id)->where('destination_id', $destination_id)->where('deleted_at', null)->where('id', '!=', $id)->first()){
                        return redirect('/popexpress/prices')->withInput()->withErrors([
                            'message' => 'Harga dengan origin dan destination yang sama sudah terdaftar.'
                        ]);
                    }
                }

                $dataPrice = [
                    'origin_id' => $origin_id,
                    'destination_id' => $destination_id,
                    'price_regular' => $price_regular,
                    'price_one' => $price_one,
                    'estimation_early' => $estimation_early,
                    'estimation_late' => $estimation_late,
                    'server_timestamp' => date('Y-m-d H:i:s')
                ];

                $price->origin_id = $origin_id;
                $price->destination_id = $destination_id;
                $price->price_regular = $price_regular;
                $price->price_one = $price_one;
                $price->estimation_early = $estimation_early;
                $price->estimation_late = $estimation_late;;
                $price->server_timestamp = date("Y-m-d H:i:s");
                $price->save();
                $new = $price->getAttributes();
                $diff = array_diff_assoc($old, $new);

                if($price) {
                    $key = $id;
                    $module = 'prices';
                    $type = 'edit';
                    $jsonBefore = json_encode($old);
                    $jsonAfter = json_encode($diff);
                    $remark = null;
                    CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
                }
                session()->flash('success', 'Data price telah berhasil diperbarui.');
            }
        }

        return redirect('/popexpress/prices');
    }

    public function export(Request $request)
    {
        $origin_id = request('origin_id');
        $destination_id = request('destination_id');

        $prices = Price::leftJoin('origins', 'origins.id', '=', 'prices.origin_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
            ->where('prices.deleted_at', null)
            ->select(
                'prices.*',
                'origins.name as origin_name',
                'destinations.detail_code as detail_code',
                'destinations.county as county',
                'destinations.district as district'
            )
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('prices.origin_id', '=', $origin_id);
            })
            ->when($destination_id, function ($query) use ($destination_id) {
                return $query->where('prices.destination_id', '=', $destination_id);
            })
            ->orderBy('prices.origin_id')
            ->get();

        $filename = "prices_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($prices){
            $excel->sheet('Sheet1', function($sheet) use($prices){
                $row = 1;

                $arr_title = ['origin','detail_code','county','district','price_regular','price_one','estimation_early','estimation_late'];
                $sheet->row($row, $arr_title);

                foreach ( $prices as $index => $item) {
                    $row++;
                    $arr = [
                        $item->origin_name,
                        $item->detail_code,
                        $item->county,
                        $item->district,
                        $item->price_regular,
                        $item->price_one,
                        $item->estimation_early,
                        $item->estimation_late
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function template(Request $request)
    {
        $prices = Price::leftJoin('origins', 'origins.id', '=', 'prices.origin_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
            ->where('prices.deleted_at', null)
            ->select(
                'prices.*',
                'origins.name as origin_name',
                'destinations.detail_code as detail_code',
                'destinations.county as county',
                'destinations.district as district'
            )
            ->orderBy('prices.origin_id')
            ->take(10)->get();

        $filename = "prices_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($prices){
            $excel->sheet('Sheet1', function($sheet) use($prices){
                $row = 1;

                $arr_title = ['origin','detail_code','county','district','price_regular','price_one','estimation_early','estimation_late'];
                $sheet->row($row, $arr_title);

                foreach ( $prices as $index => $item) {
                    $row++;
                    $arr = [
                        $item->origin_name,
                        $item->detail_code,
                        $item->county,
                        $item->district,
                        $item->price_regular,
                        $item->price_one,
                        $item->estimation_early,
                        $item->estimation_late
                    ];
                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }

    public function import()
    {
        return view('popexpress.prices.import');
    }

    public function importData(Request $request){

        $file = request('file_upload');

        $fileExt = $file->getClientOriginalName();
        $arr_ext = explode('.', $fileExt);
        $real_ext = $arr_ext[count($arr_ext)-1];

        if($file) {
            if (!in_array($real_ext, array("xlsx", "xls"))) {
                return redirect('/popexpress/prices/import')->withInput()->withErrors([
                    'message' => 'Hanya excel file .xls & .xlsx yang diperbolehkan untuk diupload.'
                ]);
            }

            $records = Excel::load($file->path())->get()->toArray();

            $origins = Origin::where('deleted_at', null)->select('name', 'id')->get();
            $originValues = [];
            $originList = [];
            foreach ($origins as $origin) {
                $originValues[$origin->id] = strtoupper($origin->name);
                $originList[] = strtoupper($origin->name);
            }

            $total = count($records);
            $new = 0;
            $update = 0;
            $skip = 0;
            $row = '';
            foreach ($records as $key => $record)
            {
                $number = $key+1;
                if(empty($record['origin']) || empty($record['detail_code']) || empty($record['county']) || empty($record['district']) || empty($record['price_regular']) || empty($record['price_one']) || empty($record['estimation_early'])) {
                    $row .= ($row == '' ? $number : ', '.$number);
                    $skip++;
                    continue;
                }

                if(!in_array(strtoupper($record['origin']), $originList)) {
                    $row .= ($row == '' ? $number : ', '.$number);
                    $skip++;
                    continue;
                }

                $checkDestination = Destination::select('id')->where('detail_code', $record['detail_code'])->first();

                if(!isset($checkDestination->id)) {
                    $row .= ($row == '' ? $number : ', '.$number);
                    $skip++;
                    continue;
                }

                $origin_id = array_search(strtoupper($record['origin']), $originValues);

                $checkPrice = Price::leftJoin('origins', 'origins.id', '=', 'prices.origin_id')
                    ->leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
                    ->where('prices.deleted_at', null)
                    ->where('origins.name', $record['origin'])
                    ->where('destinations.detail_code', $record['detail_code'])
                    ->select('prices.id')
                    ->first();

                if(count($checkPrice) > 0) {
                    $updatePrice = Price::where('id', $checkPrice->id)
                        ->update(
                            [
                                'origin_id' => $origin_id,
                                'destination_id' => $checkDestination->id,
                                'price_regular' => $record['price_regular'],
                                'price_one' => $record['price_one'],
                                'estimation_early' => $record['estimation_early'],
                                'estimation_late' => $record['estimation_late'],
                                'server_timestamp' => date('Y-m-d H:i:s')
                            ]
                        );
                    $update++;
                } else {
                    $dataPrice = [
                        'origin_id' => $origin_id,
                        'destination_id' => $checkDestination->id,
                        'price_regular' => $record['price_regular'],
                        'price_one' => $record['price_one'],
                        'estimation_early' => $record['estimation_early'],
                        'estimation_late' => $record['estimation_late'],
                        'server_timestamp' => date('Y-m-d H:i:s')
                    ];
                    $destination = Price::create($dataPrice);
                    $new++;
                }
            }

            $data['total'] = $total;
            $data['new'] = $new;
            $data['update'] = $update;
            $data['skip'] = $skip;
            $data['row'] = $row;

            if($total > 0) {
                $key = ' ';
                $module = 'prices';
                $type = 'general';
                $jsonBefore = null;
                $jsonAfter = json_encode($data);
                $remark = null;
                CommonPopExpressHelper::insertAuditTrails($key, $module, $type, $jsonBefore, $jsonAfter, $remark);
            }

            return view('popexpress.prices.import', $data);

        }

    }

}