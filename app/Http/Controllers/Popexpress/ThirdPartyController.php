<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Models\Popexpress\City;
use App\Models\Popexpress\InternationalCode;
use App\Models\Popexpress\ThirdParty;
use Illuminate\Support\Facades\Request;

class ThirdPartyController extends Controller {

    public function index()
    {
        $CitiesStore = City::select('cities.name as val','cities.name')->orderBy('cities.name','ASC')->get();
        return view('popexpress.thirdparty.index', compact('CitiesStore'));
    }

    public function grid(Request $request)
    {
        $code = request('code');
        $nama = request('nama');
        $area = request('area');
        $kota = request('kota');

        $third_parties = ThirdParty::where('third_parties.deleted_at', null)
            ->where('third_parties.id', '!=', '0')
            ->select('third_parties.*')
            ->when($code, function ($query) use ($code) {
                return $query->where('third_parties.code', 'LIKE', "%$code%");
            })
            ->when($nama, function ($query) use ($nama) {
                return $query->where('third_parties.name', 'LIKE', "%$nama%");
            })
            ->when($area, function ($query) use ($area) {
                return $query->where('third_parties.area', 'LIKE', "%$area%");
            })
            ->when($kota, function ($query) use ($kota) {
                return $query->where('third_parties.city', 'LIKE', "%$kota%");
            });

        $length = request('length');
        $start = request('start');
        $draw = request('draw');
        $order = request('order');
        $columns = request('columns');
        $sortColumn = $columns[$order[0]['column']]['name'];
        $sortDirection = $order[0]['dir'];

        if($sortColumn == '')
        {
            $sortColumn = 'third_parties.code';
            $sortDirection = 'asc';
        }

        $countTotal = $third_parties->count();
        $output['draw'] = $draw;
        $output['start'] = $start;
        $output['length'] = $length;
        $output['recordsTotal'] = $countTotal;
        $output['recordsFiltered'] = $countTotal;
        $output['data'] = $third_parties->offset($start)->limit($length)->orderBy($sortColumn, $sortDirection)->get();

        return response()->json($output);
    }

    public function create()
    {
        $Cities = City::select('cities.id','cities.name')->orderBy('cities.name','ASC')->get();
        return view('popexpress.thirdparty.create', compact('Cities'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'code' => 'required|max:3',
            'name' => 'required',
            'area' => 'required',
            'city' => 'required',
            'address' => 'required',
            'telephone' => 'required'
        ],[
            'code.required' => 'Kode diperlukan.',
            'code.max' => 'Kode tidak boleh lebih dari 3 karakter.',
            'name.required' => 'Nama diperlukan.',
            'area.required' => 'Area diperlukan.',
            'city.required' => 'Kota diperlukan.',
            'address.required' => 'Alamat diperlukan.',
            'telephone.required' => 'Telefon diperlukan.'
        ]);

        $code = strtoupper(request('code'));
        if(ThirdParty::where('code', $code)->exists()){
            return redirect('/popexpress/3pl/create')->withInput()->withErrors([
                'message' => 'Kode yang anda masukan telah terdaftar.'
            ]);
        }

        if(InternationalCode::where('airport_code', $code)->exists()){
            return redirect('/popexpress/3pl/create')->withInput()->withErrors([
                'message' => 'Kode yang anda masukan telah terdaftar.'
            ]);
        }

        $DBCities = City::where('name', request('city'))->first();
        $citiesName = count($DBCities)>0 ? $DBCities->name : 'unknown';
        $citiesId = count($DBCities)>0 ? $DBCities->id : 'unknown';


        ThirdParty::create([
            'code' => $code,
            'name' => request('name'),
            'area' => request('area'),
            'city_id' => $citiesId,
            'city' =>  $citiesName,
            'address' => request('address'),
            'telephone' => request('telephone'),
            'server_timestamp' => date("Y-m-d H:i:s")
        ]);

        session()->flash('success', 'Data third party telah berhasil disimpan.');
        return redirect('/popexpress/3pl/create');
    }

    public function delete(Request $request)
    {
        $output = ['success' => false, 'message' => 'Permintaan anda tidak dapat diproses. Mohon dicoba lagi.'];
        $id = request('id');
        if(!empty($id)){
            if(in_array("0", $id)){
                $output['success'] = false;
                $output['message'] = "Data Sendiri tidak bisa di hapus. Silakan untick data tersebut dari seleksi anda.";
            }else{
                if(ThirdParty::where('deleted_at', null)->whereIn('id', $id)->where('id', '!=' , '0')->update(['deleted_at' => date('Y-m-d H:i:s'), 'server_timestamp' => date('Y-m-d H:i:s')])){
                    $output['success'] = true;
                    $output['message'] = "Data terpilih telah dihapus.";
                }
            }
        }
        return response()->json($output);
    }

    public function update(Request $request)
    {
        $output = ['success' => false, 'error' => false, 'message' => 'Permintaan anda tidak dapat diproses. Mohon dicoba lagi.'];
        if($thirdParty = ThirdParty::find(request('id'))){
            $error = "";

            $editorCode = strtoupper(request('code'));
            $editorArea = request('area');
            $editorCity = request('city');
            $editorName = request('name');
            $editorTelephone = request('telephone');
            $editorAddress = request('address');

            if($editorCode == ""){
                $error .= 'Kode diperlukan.<br>';
            }
            if(strlen($editorCode) > 3){
                $error .= 'Kode tidak boleh lebih dari 3 karakter.<br>';
            }
            if(ThirdParty::where('code', $editorCode)->where('id', '!=', $thirdParty->id)->exists()){
                $error .= 'Kode yang anda masukan telah terdaftar.<br>';
            }
            if(InternationalCode::where('airport_code', $editorCode)->exists()){
                $error .= 'Kode yang anda masukan telah terdaftar.<br>';
            }
            if($editorName == ""){
                $error .= 'Nama diperlukan.<br>';
            }
            if($editorTelephone == ""){
                $error .= 'Telefon diperlukan.<br>';
            }
            if($editorArea == ""){
                $error .= 'Area diperlukan.<br>';
            }
            if($editorCity == ""){
                $error .= 'Kota diperlukan.<br>';
            }
            if($editorAddress == ""){
                $error .= 'Alamat diperlukan.<br>';
            }

            if($error != ""){
                $output['message'] = $error;
                $output['error'] = true;
                return response()->json($output);
            }

            $DBCities = City::where('name', $editorCity)->first();
            $citiesName = count($DBCities)>0 ? $DBCities->name : 'unknown';
            $citiesId = count($DBCities)>0 ? $DBCities->id : 'unknown';

            $thirdParty->code = $editorCode;
            $thirdParty->city_id = $citiesId;
            $thirdParty->city = $citiesName;
            $thirdParty->area = $editorArea;
            $thirdParty->name = $editorName;
            $thirdParty->telephone = $editorTelephone;
            $thirdParty->address = $editorAddress;
            $thirdParty->server_timestamp = date("Y-m-d H:i:s");
            $thirdParty->save();

            $output['success'] = true;
            $output['message'] = 'Data telah diperbarui.';
        }
        return response()->json($output);
    }

}