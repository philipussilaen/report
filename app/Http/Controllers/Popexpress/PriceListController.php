<?php
namespace App\Http\Controllers\Popexpress;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonPopExpressHelper;
use App\Models\Popexpress\Account;
use App\Models\Popexpress\Origin;
use App\Models\Popexpress\Price;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Maatwebsite\Excel\Facades\Excel;

class PriceListController extends Controller
{

    public function index(Request $request)
    {
        $account_id = request('account_id');
        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $county = request('county');
        $district = request('district');
        $is_locker = request('is_locker');

        $prices = Price::leftJoin('origins', 'origins.id', '=', 'prices.origin_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
            ->where('prices.deleted_at', null)
            ->select(
                'prices.*',
                'destinations.airport_code',
                'destinations.detail_code',
                'destinations.province',
                'destinations.county',
                'destinations.district',
                'destinations.is_locker',
                'origins.name as origin_name',
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->when(is_null($account_id), function ($query) use ($account_id) {
                return $query->where('prices.origin_id', '=', 0);
            })
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('prices.origin_id', '=', $origin_id);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('destinations.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('destinations.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('destinations.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('destinations.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('destinations.district', 'LIKE', "%$district%");
            })
            ->when($is_locker, function ($query) use ($is_locker) {
                return $query->where('destinations.is_locker', '=', $is_locker);
            })
            ->orderBy('prices.origin_id')
            ->paginate(20);

        $account = Account::find($account_id);

        $accounts = Account::where('deleted_at', null)->orderBy('account_name')->pluck('account_name', 'id')->toArray();
        $origins = Origin::where('deleted_at', null)->orderBy('name')->pluck('name', 'id')->toArray();

        return view('popexpress.price_list.index', compact('prices', 'origins', 'accounts', 'account'));

    }

    public function export(Request $request)
    {

        $account_id = request('account_id');
        $origin_id = request('origin_id');
        $airport_code = request('airport_code');
        $detail_code = request('detail_code');
        $province = request('province');
        $county = request('county');
        $district = request('district');
        $is_locker = request('is_locker');

        $prices = Price::leftJoin('origins', 'origins.id', '=', 'prices.origin_id')
            ->leftJoin('destinations', 'destinations.id', '=', 'prices.destination_id')
            ->where('prices.deleted_at', null)
            ->select(
                'prices.*',
                'destinations.airport_code',
                'destinations.detail_code',
                'destinations.province',
                'destinations.county',
                'destinations.district',
                'destinations.is_locker',
                'origins.name as origin_name',
                DB::raw("IF(destinations.is_locker = 1, destinations.detail_code, CONCAT(destinations.county, ', ', destinations.district)) AS description")
            )
            ->when(is_null($account_id), function ($query) use ($account_id) {
                return $query->where('prices.origin_id', '=', 0);
            })
            ->when($origin_id, function ($query) use ($origin_id) {
                return $query->where('prices.origin_id', '=', $origin_id);
            })
            ->when($airport_code, function ($query) use ($airport_code) {
                return $query->where('destinations.airport_code', 'LIKE', "%$airport_code%");
            })
            ->when($detail_code, function ($query) use ($detail_code) {
                return $query->where('destinations.detail_code', 'LIKE', "%$detail_code%");
            })
            ->when($province, function ($query) use ($province) {
                return $query->where('destinations.province', 'LIKE', "%$province%");
            })
            ->when($county, function ($query) use ($county) {
                return $query->where('destinations.county', 'LIKE', "%$county%");
            })
            ->when($district, function ($query) use ($district) {
                return $query->where('destinations.district', 'LIKE', "%$district%");
            })
            ->when($is_locker, function ($query) use ($is_locker) {
                return $query->where('destinations.is_locker', '=', $is_locker);
            })
            ->orderBy('prices.origin_id')
            ->get();

        $account = Account::find($account_id);

        $filename = "price_list_".time().strtolower(str_random(6));

        Excel::create($filename, function($excel) use($prices, $account){
            $excel->sheet('Sheet1', function($sheet) use($prices, $account){
                $row = 1;
                $arr_title = ['Account','Origin','Destination','Regular','One','Estimation'];

                $sheet->row($row, $arr_title);

                foreach ( $prices as $index => $price) {
                    $row++;

                    $destinations = [
                        'id' => $price->destination_id,
                        'airport_code' => $price->airport_code,
                        'detail_code' => $price->detail_code,
                        'province' => $price->province,
                        'county' => $price->county,
                        'district' => $price->district,
                        'is_locker' => $price->is_locker,
                        'price_id' => $price->id,
                    ];
                    $discount = CommonPopExpressHelper::getDiscount($account->id, $price->origin_id, $destinations);

                    $arr = [
                        $account->account_name,
                        $price->origin_name,
                        $price->description,
                        $discount['regular'],
                        $discount['one'],
                        ($price->estimation_late == 0 ? '5 ~' : $price->estimation_early.' - '.$price->estimation_late)
                    ];

                    $sheet->row($row, $arr);
                }

            });
        })->store('xlsx', storage_path('/app'));

        $output['link'] = $filename.".xlsx";
        $output['success'] =  true;

        return response()->json($output);
    }

}