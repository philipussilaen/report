<?php

namespace App\Http\Controllers\ExecutiveSummary;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Helper;
use App\Http\Helpers\SanitizeHelper;
use App\Models\Virtual\City;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesStockController extends Controller{
    
    public function index(Request $request){
        $user_types = [
            ['id' => 'all',     'text' => 'ALL'],
            ['id' => 'agent',   'text' => 'Agent'],
            ['id' => 'warung',  'text' => 'Warung'],
        ];
        
        $cities = City::select('id as id', 'city_name as text')->orderByRaw('city_name ASC')->get();
        $city = 132;
        return view('executive_summary.sales_stock.list', compact('user_types', 'cities', 'city'));
    }
    
    public function summary(){
        $city = request('city');
        $userType = request('user_type');
        $dateRange = request('daterange_transaction');
        
        $startDate = '';
        $endDate = '';
        
        $params = [
            'where_location'                => '',
            'where_transaction_created_at'  => '',
            'where_user_type'               => '',
            'where_payment_method_id'       => '',
        ];
        
        if(!empty($dateRange)) {
            $startDate = Helper::formatDateRange($dateRange, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRange, ',')->endDate;
        }
        
        $range_between_two_date = Helper::range_between_two_date($startDate, $endDate);
        
        if(!empty($userType) && $userType != 'all'){
            $params['where_user_type'] = "and l.type = '".$userType."'";
        } else {
            $params['where_user_type'] = "and l.type in ('agent', 'warung')";
        }
        
        if(!empty($startDate) && !empty($endDate)){
            $params['where_user_created_at'] = "and u.created_at between '".$startDate."' and '".$endDate."'";
            $params['where_transaction_created_at'] = "and t.created_at between '".$startDate."' and '".$endDate."'";
        }
        
        if(!empty($city) && $city != 'all'){
            $params['where_location'] = "and l.cities_id = ".$city;
        }
        
        $detail_sales_stock = DB::connection('popbox_agent')->select($this->detail_sales_stock_query($params));
        
        // Total transaction
        $params['where_payment_method_id'] = 'and p.payment_methods_id in (1, 2)';
        $total_transaction = DB::connection('popbox_agent')->select($this->total_sales_stock_query($params));
        
        // Total transaction by deposit method
        $params['where_payment_method_id'] = 'and p.payment_methods_id = 2';
        $deposit_total_transaction = DB::connection('popbox_agent')->select($this->total_sales_stock_query($params));
        
        // Total transaction by COD method
        $params['where_payment_method_id'] = 'and p.payment_methods_id = 1';
        $cod_total_transaction =  DB::connection('popbox_agent')->select($this->total_sales_stock_query($params));
        
        
        $array_result = $this->chart_sales_stock($detail_sales_stock, $startDate, $endDate);
        
        // Verified user
        $params['where_transaction_created_at'] = "";
        $params['join_transactions_table'] = "";
        $verified_user = DB::connection('popbox_agent')->select($this->query_count_user($params));
        
        // Active user
        $params['where_transaction_created_at'] = "and t.created_at between '".$startDate."' and '".$endDate."'";
        $params['join_transactions_table'] = "join popbox_agent.`transactions` t on t.`users_id` = u.id";
        $active_user = DB::connection('popbox_agent')->select($this->query_count_user($params));
        
        $avg_sales_per_store_per_verified_user = 0;
        $avg_sales_per_store_per_verified_user_per_day = 0;
        
        $avg_sales_per_store_per_active_user = 0;
        $avg_sales_per_store_per_active_user_per_day = 0;
        
        if($verified_user[0]->total_users > 0){
            $avg_sales_per_store_per_verified_user = $total_transaction[0]->total_transaction / $verified_user[0]->total_users;
            $avg_sales_per_store_per_verified_user_per_day = $total_transaction[0]->total_transaction / $verified_user[0]->total_users / $range_between_two_date;
        }
        
        if($active_user[0]->total_users > 0){
            $avg_sales_per_store_per_active_user = $total_transaction[0]->total_transaction / $active_user[0]->total_users;
            $avg_sales_per_store_per_active_user_per_day = $total_transaction[0]->total_transaction / $active_user[0]->total_users / $range_between_two_date;
        }
        
        // Top 20 Most Purchased SKU - PopWarung / PopAgent
        $popwarung_top_purchased_sku = array();
        $popagent_top_purchased_sku = array();
        if($userType != 'agent') {
            $params['where_user_type'] = "and l.type = 'warung'";
            $params['get_sku'] = "SUBSTRING_INDEX(SUBSTRING_INDEX(ti.params, 'barcode\":\"', -1),'\",',1) AS 'sku'";
            $popwarung_top_purchased_sku = DB::connection('popbox_agent')->select($this->top_purchased_sku($params));
        }
        if($userType != 'warung') {
            $params['where_user_type'] = "and l.type = 'agent'";
            $params['get_sku'] = "SUBSTRING_INDEX(SUBSTRING_INDEX(ti.params, 'sku\":\"', -1),'\"}',1) AS 'sku'";
            $popagent_top_purchased_sku = DB::connection('popbox_agent')->select($this->top_purchased_sku($params));
        }
        
        $data = [];
        $data['payload']['deposit_total_transaction'] = $deposit_total_transaction[0]->total_transaction;
        $data['payload']['cod_total_transaction'] = $cod_total_transaction[0]->total_transaction;
        $data['payload']['total_transaction'] = $total_transaction[0]->total_transaction;
        $data['payload']['chart_line_summary_trx'] = $array_result[0];
        $data['payload']['y_max'] = ceil((int) max($array_result[1]) / 10) * 10; //Round up to nearest multiple of 10
        $data['payload']['avg_sales_per_store_per_verified_user'] = round($avg_sales_per_store_per_verified_user, 2);
        $data['payload']['avg_sales_per_store_per_verified_user_per_day'] = round($avg_sales_per_store_per_verified_user_per_day, 2);
        $data['payload']['avg_sales_per_store_per_active_user'] = round($avg_sales_per_store_per_active_user, 2);
        $data['payload']['avg_sales_per_store_per_active_user_per_day'] = round($avg_sales_per_store_per_active_user_per_day, 2);
        $data['payload']['popwarung_top_purchased_sku'] = SanitizeHelper::addCounterNumber($popwarung_top_purchased_sku);
        $data['payload']['popagent_top_purchased_sku'] = SanitizeHelper::addCounterNumber($popagent_top_purchased_sku);
        
        return $data;
    }
    
    private function total_sales_stock_query($params){
        
        return "
                select
                    sum(total_transaction) as 'total_transaction'
				from (
    				select
                        distinct t.id as 'trx_count',
                        (case when t.total_price is null then 0 else t.total_price end) as 'total_transaction'
            
                    from
                        transactions t
                    left join transaction_items ti on t.id = ti.transaction_id
                    left join users u on t.users_id = u.id
                    left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                    left join payments p on p.transactions_id = t.id
                    where 1=1
                          and t.deleted_at is null
                          ".$params['where_user_type']."
                          ".$params['where_location']."
                          ".$params['where_transaction_created_at']."
                          ".$params['where_payment_method_id']."
                          and ti.type = 'popshop'
                          and t.status in ('PAID', 'WAITING')
               ) as temp";
    }
    
    private function detail_sales_stock_query($params){
        
        return "
                select
                	trx_date,
                	sum(cod) as 'cod',
                	sum(deposit) as 'deposit',
                	sum(cod) + sum(deposit) as 'total',
                	sum(total_cod) + sum(total_deposit) as 'total_trx'
                from (
                    select
                    	trx_date,
                    	case when payment_methods_id = 1 then count(payment_methods_id) else 0 end as 'cod',
                    	case when payment_methods_id = 2 then count(payment_methods_id) else 0 end as 'deposit',
                    	case when payment_methods_id = 1 then sum(total_transaction) else 0 end as 'total_cod',
                    	case when payment_methods_id = 2 then sum(total_transaction) else 0 end as 'total_deposit'
                    from (
                        select
                        	DATE_FORMAT(t.created_at, '%Y-%m-%d') as 'trx_date',
                        	p.payment_methods_id,
                            (case when sum(t.total_price) is null then 0 else sum(t.total_price) end) as 'total_transaction'
                        from
                        	transactions t
                        left join transaction_items ti on t.id = ti.transaction_id
                        left join users u on t.users_id = u.id
                        left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                        left join payments p on p.transactions_id = t.id
                        where 1=1
                              and t.deleted_at is null
                              ".$params['where_user_type']."
                              ".$params['where_location']."
                              ".$params['where_transaction_created_at']."
                              ".$params['where_payment_method_id']."
                              and ti.type = 'popshop'
                              group by t.created_at
                    ) as temp group by trx_date, payment_methods_id
                ) as temp_sum group by trx_date order by trx_date asc;
        ";
    }
    
    private function chart_sales_stock($detail_sales_stock, $startDate, $endDate) {
        $c_start_date = Carbon::parse($startDate);
        $c_end_date = Carbon::parse($endDate);
        $length = $c_end_date->diffInDays($c_start_date);
        
        $d_start_date = new \DateTime($c_start_date);
        $array_result = array();
        $y_max = [];
        
        for($i = 0; $i < $length+1; $i++){
            $all_trx_type = 0;
            $cod = 0;
            $deposit = 0;
            foreach ($detail_sales_stock as $row){
                $row = (array) $row;
                if($row["trx_date"] == $d_start_date->format('Y-m-d')){
                    $all_trx_type = $row["total"];
                    $cod = $row["cod"];
                    $deposit = $row["deposit"];
                    
                    array_push($y_max, $row["total"]);
                    array_push($y_max, $row["cod"]);
                    array_push($y_max, $row["deposit"]);
                }
            }
            $array_result[$i]["date"] = $d_start_date->format('d-m-Y');
            $array_result[$i]["all_trx_type"] = $all_trx_type;
            $array_result[$i]["cod"] = $cod;
            $array_result[$i]["deposit"] = $deposit;
            $d_start_date->modify('+1 day');
        }
        
        if(empty($y_max)) $y_max[0] = 10;

        return [$array_result, $y_max];
    }
    
    private function query_count_user($params){
        return "
                select count(distinct l.locker_id) as 'total_users'
                from
                    users u
                    join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    join popbox_agent.lockers lk on lk.id = l.locker_id
                    ".$params['join_transactions_table']."
                where
                    l.`deleted_at` is null
                    and lk.status_verification = 'verified'
                    ".$params['where_user_type']."
                    ".$params['where_location']."
                    ".$params['where_transaction_created_at'];
    }
    
    private function top_purchased_sku($params){
        
        return "
                select qty as 'purchased', product_name, sku, sum(amount) as 'amount'
                  from (
                        select product_name, sku, sum(qty) as 'qty', sum(price) as 'amount'
                        from (
                        	select
                        		ti.name as 'product_name',
                        		".$params['get_sku'].",
                        		IFNULL(CAST(JSON_EXTRACT(ti.params, '$.amount') AS UNSIGNED), '-') AS 'qty',
                        		ti.price
                        	from transaction_items ti
                        	join transactions t on t.id = ti.`transaction_id`
                        	left join users u on t.users_id = u.id
                        	left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                        	where 1=1
                            and ti.type = 'popshop'
                            ".$params['where_user_type']."
                            ".$params['where_location']."
                            ".$params['where_transaction_created_at']."
                            order by ti.name asc
                        ) as temp
                        group by product_name, sku
                        order by product_name, sku
                  ) as temp1
                  group by product_name, sku
                  order by qty desc,
                  product_name asc
                        limit 0,20
            ";
    }
}