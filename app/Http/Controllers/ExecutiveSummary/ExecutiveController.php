<?php

namespace App\Http\Controllers\ExecutiveSummary;

use App\Http\Controllers\Controller;
use App\Models\Virtual\City;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;

class ExecutiveController extends Controller{
    
    public function index(Request $request){
        $user_types = [
            ['id' => 'all',     'text' => 'ALL'],
            ['id' => 'agent',   'text' => 'Agent'],
            ['id' => 'warung',  'text' => 'Warung'],
        ];
        
        $cities = City::select('id as id', 'city_name as text')->orderByRaw('city_name ASC')->get();
        $city = 0;
        
        $monthly = [
            ['id' => '1', 'text' => 'Januari'],
            ['id' => '2', 'text' => 'Febuari'],
            ['id' => '3', 'text' => 'Maret'],
            ['id' => '4', 'text' => 'April'],
            ['id' => '5', 'text' => 'Mei'],
            ['id' => '6', 'text' => 'Juni'],
            ['id' => '7', 'text' => 'Juli'],
            ['id' => '8', 'text' => 'Agustus'],
            ['id' => '9', 'text' => 'September'],
            ['id' => '10', 'text' => 'Oktober'],
            ['id' => '11', 'text' => 'November'],
            ['id' => '12', 'text' => 'Desember'],
        ];
        
        $month = date('m');
        return view('executive_summary.list', compact('user_types', 'cities', 'city', 'monthly', 'month'));
    }
    
    public function summary(){
        $city = request('city');
        $userType = request('user_type');
        $startMonth = request('start_month');
        $endMonth = request('end_month');
        
        $data = [];
        
        $startMonth = strlen($startMonth) < 2 ? '0'.$startMonth : $startMonth;
        $endMonth = strlen($endMonth) < 2 ? '0'.$endMonth : $endMonth;
        
        $startDate = date('Y-m-d', strtotime(date('Y').'-'.$startMonth.'-'.'01'));
        $endDate = date('Y-m-t', strtotime(date('Y').'-'.$endMonth.'-'.'01'));
        
        $week_start = Helper::getWeekFromDate($startDate);
        $week_end = Helper::getWeekFromDate($endDate);
        
        $week_range = [];
        for($i = $week_start; $i <= $week_end; $i++){
            $week_range[] = (int)$i;
        }
        
        $params = [
            'where_location'                => '',
            'where_transaction_created_at'  => '',
            'where_user_type'               => '',
            'where_status_verification'     => '',
        ];
        
        if(!empty($userType) && $userType != 'all'){
            $params['where_user_type'] = "and l.type = '".$userType."'";
        } else {
            $params['where_user_type'] = "and l.type in ('agent', 'warung')";
        }
        
        if(!empty($startDate) && !empty($endDate)){
            $params['where_transaction_created_at'] = "and t.created_at between '".$startDate." 00:00:00' and '".$endDate." 23:59:59'";
        }
        
        if(!empty($city) && $city != 'all'){
            $params['where_location'] = "and l.cities_id = ".$city;
        }
        
        // ============================ Line Chart ============================
        // Total transaction Sales Stock ( Belanja Stock )
        $params['where_payment_method_id'] = 'and p.payment_methods_id in (1, 2)';
        $params['where_status_verification'] = " and lk.status_verification = 'verified'";
        $total_transaction_belanja_stock = DB::connection('popbox_agent')->select($this->total_sales_stock_query($params));
        
        // Total transaction from Point of Sales ( Penjualan dan Penjualan Cepat )
        $params['where_status_verification'] = " and lk.status_verification = 'verified'";
        $all_user = DB::connection('popbox_agent')->select($this->user_query($params));
        $lockerid = collect($all_user)->map(function ($item){
            $item = (array) $item;
            return $item['locker_id'];
        })->toArray();
        
        $result = DB::connection('product_service')->select($this->pointofsales_activity($lockerid, $startDate, $endDate));
        $total_transaction_pos = $result;
        
        $line_template = [];
        $sales_stock_user_bar_template = [];
        $pos_user_bar_template = [];
        if(!empty($week_range)){
            foreach ($week_range as $week){
                $date_range = Helper::getStartAndEndDate($week, date('Y'), 'd-m-Y');
                $line_template[] = [
                    'week' => $week,
                    'total_trx_belanja_stock' => 0,
                    'date_range' => $date_range['week_start'].' s/d '.$date_range['week_end'],
                    'total_trx_pos' => 0,
                ];
                
                $sales_stock_user_bar_template[] = [
                    'week' => $week,
                    'active_user' => 0,
                    'date_range' => $date_range['week_start'].' s/d '.$date_range['week_end'],
                    'verified_user' => 0,
                ];
                
                $pos_user_bar_template[] = [
                    'week' => $week,
                    'active_user' => 0,
                    'date_range' => $date_range['week_start'].' s/d '.$date_range['week_end'],
                    'verified_user' => 0,
                ];
            }
        }
        
        $line_chart  = self::render_data_line_chart($total_transaction_belanja_stock, $total_transaction_pos, $line_template);
        
        $y_max = ceil((int) max($line_chart[1]) / 10000000) * 10000000;
        $data['payload']['chart_line'] = $line_chart[0];
        $data['payload']['chart_line_y_max'] = $y_max;
        
        // ====================================================================
        
        // ======================= Sales Stock Bar Chart ======================
        
        // Verified user
        $params['status_verification'] = " and lk.status_verification = 'verified'";
        $verified_user = DB::connection('popbox_agent')->select($this->user_query($params));
        $number_of_verified_user = count($verified_user);
        
        // Active user
        $params['where_transaction_created_at'] = "and t.created_at between '".$startDate." 00:00:00' and '".$endDate." 23:59:59'";
        $params['join_transactions_table'] = "join popbox_agent.`transactions` t on t.`users_id` = u.id";
        $active_user = DB::connection('popbox_agent')->select(self::query_count_active_user_per_week($params));
        $sales_stock_user_bar_chart = self::render_data_user_bar_chart($active_user, $number_of_verified_user, $sales_stock_user_bar_template);
        $y_max = ceil($sales_stock_user_bar_chart[1] / 5) * 5;
        $data['payload']['sales_stock_users_bar_chart'] = $sales_stock_user_bar_chart[0];
        $data['payload']['sales_stock_users_bar_chart_y_max'] = ceil((int) $y_max / 50) * 50;
        // ====================================================================
        
        // ===================== Point of Sales Bar Chart =====================
        
        // Verified user
        $verified_lockerid = collect($verified_user)->map(function ($item){
            $item = (array) $item;
            return $item['locker_id'];
        })->toArray();
        $active_user = DB::connection('product_service')->select(self::query_pos_active_user($startDate, $endDate, $verified_lockerid));
        
        $pos_user_bar_chart = self::render_data_user_bar_chart($active_user, $number_of_verified_user, $pos_user_bar_template);
        
        $y_max = ceil($pos_user_bar_chart[1] / 5) * 5;
        $data['payload']['pos_users_bar_chart'] = $pos_user_bar_chart[0];
        $data['payload']['pos_users_bar_chart_y_max'] = ceil((int) $y_max / 50) * 50;
        // =================================================================
        
        return $data;
    }
    
    private function total_sales_stock_query($params){
        
        return "
                select
                    WEEK(trx_date, 1) AS week, sum(total_transaction) as 'total_trx'
				from (
    				select
                        distinct t.id as 'trx_count',
                        DATE_FORMAT(t.created_at, '%Y-%m-%d') as 'trx_date',
                        (case when t.total_price is null then 0 else t.total_price end) as 'total_transaction'
            
                    from
                        transactions t
                    left join transaction_items ti on t.id = ti.transaction_id
                    left join users u on t.users_id = u.id
                    left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                    left join payments p on p.transactions_id = t.id
                    left join popbox_agent.lockers lk on lk.id = l.locker_id
                    where 1=1
                          and t.deleted_at is null
                          ".$params['where_user_type']."
                          ".$params['where_location']."
                          ".$params['where_transaction_created_at']."
                          ".$params['where_payment_method_id']."
                          ".$params['where_status_verification']."
                          and ti.type = 'popshop'
                          and t.status in ('PAID', 'WAITING')
               ) as temp
               group by WEEK(trx_date, 1)
               order by WEEK(trx_date, 1) asc;";
    }
    
    private function pointofsales_activity($lockerid, $startDate, $endDate) {
        $whereCondition = 'AND tc.status not in ("VOID")';
        if(!empty($lockerid) && $lockerid != 'all') {
            if(is_array($lockerid)){
                $locker_id = '';
                foreach ($lockerid as $id){
                    $locker_id .= "'".$id."',";
                }
                $whereCondition .= " AND tc.locker_id in (".substr($locker_id, 0, strlen($locker_id)-1).")";
            } else{
                $whereCondition .= " AND tc.locker_id = '".$lockerid."' ";
            }
        }
        
        if(!empty($startDate) && !empty($endDate) && $startDate != 'all' && $endDate != 'all') {
            $whereCondition .= " AND DATE(tc.`date_transaction`) BETWEEN DATE('".$startDate." 00:00:00') AND DATE('".$endDate." 23:59:59') ";
        }
        
        return "
                select
                    WEEK(trx_date, 1) AS week, sum(price) as 'total_trx'
                from (
                    select distinct tc.reference, DATE_FORMAT(tc.date_transaction, '%Y-%m-%d') as 'trx_date', tc.total_amount as 'price'
                    from transaction_customer_detail tcd
                    join transaction_customer tc on tc.id = tcd.transaction_customer_id
                    where 1=1
                         and tcd.type in ('popshop', 'fast_sales') ".$whereCondition."
                    order by tcd.name, tcd.sku
                ) as temp
               group by WEEK(trx_date, 1)
               order by WEEK(trx_date, 1) asc
        ";
    }
    
    private function user_query($params){
        return "select distinct l.locker_id, l.locker_name, l.type, lk.status_verification
                from
                    users u
                    join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    join popbox_agent.lockers lk on lk.id = l.locker_id
                where
                    l.`deleted_at` is null
                    ".$params['where_status_verification']."
                    ".$params['where_user_type']."
                    ".$params['where_location'];
    }
    
    private function data_chart($rows_belanja_stock, $rows_pos, $startWeek = 0, $endWeek = 0) {
        
        $length = $endWeek - $startWeek;
        
        $array_result = array();
        //$y_max = [];
        $week = 0;
        for($i = 0; $i < $length+1; $i++){
            $week = $startWeek + $i;
            $total_trx_belanja_stock = 0;
            $total_trx_pos = 0;
            foreach ($rows_belanja_stock as $row){
                $row = (array) $row;
                if($row["week"] == $week){
                    $total_trx_belanja_stock = $row["total_trx"];
                    
                    //array_push($y_max, $row["total_trx"]);
                    break;
                }
            }
            foreach ($rows_pos as $row){
                $row = (array) $row;
                if($row["week"] == $week){
                    $total_trx_pos = $row["total_trx"];
                    
                    //array_push($y_max, $row["total_trx"]);
                    break;
                }
            }
            $array_result[$i]["week"] = $week;
            $array_result[$i]["total_trx_belanja_stock"] = $total_trx_belanja_stock;
            $array_result[$i]["total_trx_pos"] = $total_trx_pos;
        }
        
        return $array_result;
    }
    
    private function render_data_line_chart($rows_belanja_stock, $rows_pos, $array_template = []) {
        
        $y_max = [3]; //  val 3 for at least 1 value for y_max 
        $array_result = [];
        foreach ($array_template as $r){
            foreach ($rows_belanja_stock as $row){
                $row = (array) $row;
                if($r['week'] == $row['week']){
                    $r["total_trx_belanja_stock"] = $row["total_trx"];
                    array_push($y_max, $row["total_trx"]);
                    break;
                }
            }
            
            foreach ($rows_pos as $row){
                $row = (array) $row;
                if($r['week'] == $row['week']){
                    $r["total_trx_pos"] = $row["total_trx"];
                    array_push($y_max, $row["total_trx"]);
                    break;
                }
            }
            
            $array_result[] = $r;
        }
        
        return [$array_result, $y_max];
    }
    
    private function render_data_user_bar_chart($rows_active_user, $verified_user, $array_template = []) {
        
        $array_result = array();
        
        foreach ($array_template as $r){
            foreach ($rows_active_user as $row){
                $row = (array) $row;
                if($r['week'] == $row['week']){
                    $r["active_user"] = $row["total_users"];
                    $r["verified_user"] = $verified_user - $row["total_users"];
                    break;
                }
            }
            
            $array_result[] = $r;
        }
        
        return [$array_result, $verified_user];
    }
    
    private function query_count_verified_user($params){
        return "
                select count(distinct l.locker_id) as 'total_users'
                from
                    users u
                    join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    join popbox_agent.lockers lk on lk.id = l.locker_id
                    ".$params['join_transactions_table']."
                where
                    l.`deleted_at` is null
                    and lk.status_verification = 'verified'
                    ".$params['where_user_type']."
                    ".$params['where_location']."
                    ".$params['where_transaction_created_at'];
    }
    
    private function query_count_active_user_per_week($params){
        return "
                select
                    WEEK(trx_date, 1) AS week, count(distinct total_users) as 'total_users'
                from(
    				select
    					distinct DATE_FORMAT(t.created_at, '%Y-%m-%d') as 'trx_date',
    					l.locker_id as 'total_users'
                    from
                        users u
                        join popbox_virtual.lockers l on u.locker_id = l.locker_id
                        join popbox_agent.lockers lk on lk.id = l.locker_id
                        join popbox_agent.`transactions` t on t.`users_id` = u.id
                     	join transaction_items ti on t.id = ti.transaction_id
                    where
                        l.`deleted_at` is null
                        and lk.status_verification = 'verified'
                        and ti.type = 'popshop'
                        ".$params['where_user_type']."
                        ".$params['where_location']."
                        ".$params['where_transaction_created_at'].'
                )  as temp
                group by WEEK(trx_date, 1)
                order by WEEK(trx_date, 1) asc;';
    }
    
    private function query_pos_active_user($startDate, $endDate, $verified_lockerid) {
        $whereCondition = 'AND tc.status not in ("VOID")';
        
        if(!empty($verified_lockerid) && $verified_lockerid != 'all') {
            if(is_array($verified_lockerid)){
                $locker_id = '';
                foreach ($verified_lockerid as $id){
                    $locker_id .= "'".$id."',";
                }
                $whereCondition .= " AND tc.locker_id in (".substr($locker_id, 0, strlen($locker_id)-1).")";
            } else{
                $whereCondition .= " AND tc.locker_id = '".$locker_id."' ";
            }
        }
        
        if(!empty($startDate) && !empty($endDate) && $startDate != 'all' && $endDate != 'all') {
            $whereCondition .= " AND DATE(tc.`date_transaction`) BETWEEN DATE('".$startDate." 00:00:00') AND DATE('".$endDate." 23:59:59') ";
        }
        
        return "
               select
                    WEEK(trx_date, 1) AS week, count(distinct total_users) as 'total_users'
                from(
				select
						distinct DATE_FORMAT(tc.date_transaction, '%Y-%m-%d') as 'trx_date',
                    	tc.locker_id as 'total_users'
                    from transaction_customer_detail tcd
                    join transaction_customer tc on tc.id = tcd.transaction_customer_id
                    where 1=1
                         and tcd.type in ('popshop', 'fast_sales') ".$whereCondition."
               )  as temp
               group by WEEK(trx_date, 1)
               order by WEEK(trx_date, 1) asc
        ";
    }
}