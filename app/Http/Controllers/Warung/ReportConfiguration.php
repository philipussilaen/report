<?php

namespace App\Http\Controllers\Warung;

use App\Http\Controllers\Controller;
use App\Http\Helpers\SanitizeHelper;
use App\Models\Warung\TransactionReportConfiguration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportConfiguration extends Controller
{
    public function index(Request $request) {
        
        $selected_time_period = '';
        $selected_hour = '';
        
        $time_period = [
            ['id' => 'daily',     'text' => 'Harian'],
            ['id' => 'weekly',    'text' => 'Mingguan'],
            ['id' => 'monthly',   'text' => 'Bulanan'],
        ];
        
        $statuses = [
            ['id' => 'active', 'text' => 'Aktif'],
            ['id' => 'inactive', 'text' => 'Tidak Aktif'],
        ];
        
        $hours = [];
        for($i = 0; $i < 24; $i++){
            $i = $i < 10 ? "0".$i : $i;
            array_push($hours, strval($i).":00");
        }
        
        return view('warung.report.transaction', compact('time_period', 'hours', 'minutes', 'selected_time_period', 'selected_hour', 'statuses'));
    }
    
    public function submit(Request $request) {
        
        $arr_receiver = explode(';', $request->get('email_list'));
        $data = [];
        $report = [];
        foreach ($arr_receiver as $email){
            $report['warung_id']    = $request->get('id_warung');
            $report['receiver']     = $email;
            $report['period']       = $request->get('time_period');
            $report['send_date']    = $request->get('time_period') == 'weekly' ? $request->get('date_send') : null;
            $report['send_hour']    = $request->get('hour_send');
            $report['is_active']    = 1;
            $report['created_at']   = date('Y-m-d H:i:s');
            $report['updated_at']   = date('Y-m-d H:i:s');
            
            array_push($data, $report);
        }
        
        TransactionReportConfiguration::insert($data);
        return back()->with('success', 'Data telah disimpan');
    }
    
    public function list(Request $request) {
        
        $id_warung = $request->get('id_warung');
        $report_type = $request->get('report_type');
        $status = $request->get('status');
        $start = $request->get('start');
        $limit = $request->get('limit');
        $draw = request('draw', 1);
        
        $rows = TransactionReportConfiguration::join('popbox_virtual.lockers', 'popbox_virtual.lockers.locker_id', 'report_configuration_jobs.warung_id')
        ->whereNotNull('warung_id')
        ->whereNull('report_configuration_jobs.deleted_at')
        ->whereNotNull('receiver')
        ->when($id_warung,  function ($query) use ($id_warung) {
            return $query->where('warung_id', $id_warung); })
            ->when($report_type,  function ($query) use ($report_type) {
                if($report_type != 'all')
                    return $query->where('period', $report_type); })
                    ->when($status,  function ($query) use ($status) {
                        if($status != 'all'){
                            return $query->where('is_active', '=', $status == 'active' ? 1 : 0);
                        }
                    })->select(DB::raw('report_configuration_jobs.id, popbox_virtual.lockers.locker_name, receiver, period, is_active, CONCAT(IFNULL(send_date, ""), " ", send_hour) as send_date, report_configuration_jobs.created_at'));
                    
                    $row_count = $rows->count();
                    $result = $rows->offset($start)->limit($limit)->get();
                    
                    $data = array();
                    $data['payload']['draw'] = $draw;
                    $data['payload']['count'] = $row_count;
                    $data['payload']['data'] = [];
                    foreach(SanitizeHelper::cleansingNull($result, $start) as $row){
                        array_push($data['payload']['data'], $row);
                    }
                    
                    return $data;
    }
    
    public function update(Request $request) {
        
        $id = $request->get('id_config');
        $update_to = $request->get('update_to');
        
        $row = TransactionReportConfiguration::find((int)$id);
        if($row){
            if($update_to == 'remove'){
                $row->is_active = 0;
                $row->delete();
            } else if($update_to == 'activate'){
                $row->is_active = 1;
                $row->save();
            } else if($update_to == 'deactivate'){
                $row->is_active = 0;
                $row->save();
            }
        }
    }
    
    private function callCommand(){
        //Artisan::call('schedule:run');
    }
}
