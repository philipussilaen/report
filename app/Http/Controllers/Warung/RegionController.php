<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ApiPopWarung;
use Illuminate\Support\Facades\DB;

use App\Models\Virtual\Region;
use App\Models\Virtual\City;

class RegionController extends Controller
{
    /**
     * get agent landing dashboard
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function region(Request $request)
    {
        return view('warung.catalog.region.index', []);
    }

    public function getListRegion(Request $request)
    {
        $mresult = Region::leftJoin('cities', 'regions.id', '=', 'cities.regions_id')
                    ->select(
                        DB::raw("
                            regions.id, regions.region_code, regions.region_name, TRIM(regions.min_purchase)+0 as min_purchase, 
                            GROUP_CONCAT(cities.city_name) AS city_name
                        ")
                    )
                    ->groupBy('regions.region_code')->orderBy('regions.region_name')
                    ->get();

        $response = new \stdClass();
        if($mresult) {
            $response->success = true;
            $response->payload = [
                'data' => $mresult
            ];
        }
        else {
            $response->success = false;
            $response->payload = [
                'data' => []
            ];
        }

        return response()->json($response);            
    }

    public function getRegionByID(Request $request, $regionID) 
    {
        $mresultRegion = Region::where('id', '=', $regionID)->first();
        $mresultCity = City::where('regions_id', '=', $mresultRegion->id)->get();
        $mresultRegion->cities = $mresultCity;

        $response = new \stdClass();

        $response->success = true;
        $response->payload = [
            'data' => $mresultRegion
        ];

        return response()->json($response);
    }

    public function crudRegion(Request $request, $flag, $cityID) 
    {
        $data['vflag'] = $flag;        
        $data['vcityid'] = $cityID;
        return view('warung.catalog.region.form', $data);
    }

    public function getListCity(Request $request)
    {
        $keyword = $request->input('search', null);        
        $mresult = City::where('city_name','like','%'.$keyword.'%')
            ->leftJoin('regions', 'cities.regions_id', '=', 'regions.id')
            ->select('cities.id','cities.city_code','cities.city_name', 'cities.provinces_id', 'cities.regions_id', 'regions.region_name')
            ->orderBy('city_name')->offset(0)->limit(10)->get();

        $response = new \stdClass();
        if($mresult) {
            $response->success = true;
            $response->payload = [
                'data' => $mresult
            ];
        }
        else {
            $response->success = false;
            $response->payload = [
                'data' => []
            ];
        }
        
        return response()->json($response);
    }

    public function checkCity(Request $request, $cityID) 
    {
        $mresult = City::where('id','=',$cityID)->first();
        $response = new \stdClass();
        if($mresult) {
            $response->success = true;
            $response->payload = [
                'data' => $mresult
            ];
        }
        else {
            $response->success = false;
            $response->payload = [
                'data' => []
            ];
        }
        
        return response()->json($response);
    }

    public function saveRegion(Request $request)
    {
        $flag = $request->input('flag', '1');
        $regionID = $request->input('regionid', null);
        $regionCode = $request->input('regioncode', null);
        $regionName = $request->input('regionname', null);
        $minimumPurchase = $request->input('minpurchase', null);
        $cityID = $request->input('cityid', null);
        $cityExp = explode(',', $cityID);

        $response = [];

        if($flag == '1') {
            DB::beginTransaction();
            $checkRegionByCode = Region::where('region_code', '=', $regionCode)->count();

            if($checkRegionByCode > 0) {
                DB::rollback();
                $message = 'Region code has been already exist';
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400,'message' =>$message], 'data' => []
                    ]                
                ];
                return response()->json($response);
            }

            $checkRegionByName = Region::whereRaw("UPPER(region_name) = UPPER(?)", [$regionName])->count();

            if($checkRegionByName > 0) {
                DB::rollback();
                $message = 'Region name has been already exist';
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400,'message' =>$message], 'data' => []
                    ]                
                ];
                return response()->json($response);
            }

            $mRegion = Region::insertGetId([
                'region_code' => $regionCode, 'region_name' => $regionName, 'min_purchase' => $minimumPurchase
            ]);
    
            if ( !$mRegion){
                DB::rollback();
                $message = $mRegion->errorMsg;
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400,'message' =>$message], 'data' => []
                    ]                
                ];
                return response()->json($response);
            }
    
            $o=0;
            foreach($cityExp as $r) {
                $mres = City::where('id', $r)->update(['regions_id' => $mRegion]);
                if($mres) $o++;
            }
    
            DB::commit();    
        }
        else {
            DB::beginTransaction();

            $checkRegionByCode = Region::where('region_code', '=', $regionCode)
                ->where('id', '<>', $regionID)
                ->count();

            if($checkRegionByCode > 0) {
                DB::rollback();
                $message = 'Region code has been already exist';
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400,'message' =>$message], 'data' => []
                    ]                
                ];
                return response()->json($response);
            }

            $checkRegionByName = Region::whereRaw("UPPER(region_name) = UPPER(?)", [$regionName])
                ->where('id', '<>', $regionID)
                ->count();

            if($checkRegionByName > 0) {
                DB::rollback();
                $message = 'Region name has been already exist';
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400,'message' =>$message], 'data' => []
                    ]                
                ];
                return response()->json($response);
            }

            $mCheckCity = City::where('regions_id', $regionID)->get();
            if(sizeof($mCheckCity) > 0) {
                City::where('regions_id', $regionID)->update(['regions_id' => null]);
            }

            $mRegion = Region::where('id', $regionID)->update([
                'region_code' => $regionCode, 
                'region_name' => $regionName,
                'min_purchase' => $minimumPurchase
            ]);
                        
            $o=0;
            foreach($cityExp as $r) {
                $mres = City::where('id', $r)->update(['regions_id' => $regionID]);
                if($mres) $o++;
            }
    
            DB::commit();    
        }

        $response = [
            'success' => true,
            'payload' => [
                'response' => ['code' => 200, 'message' => 'Data has been success'], 'data' => []
            ]            
        ];        
        return response()->json($response);
    }

    public function delRegion(Request $request)
    {
        $regionID = $request->input('regionid', null);

        DB::beginTransaction();
        $mCities = City::where('regions_id', $regionID)->update(['regions_id' => null]);
        $mRegion = Region::where('id', $regionID)->delete();

        DB::commit(); 

        $response = [
            'success' => true,
            'payload' => [
                'response' => ['code' => 200, 'message' => 'Data has been deleted'], 'data' => []
            ]            
        ];        
        return response()->json($response);        
    }
}
