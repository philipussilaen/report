<?php

namespace App\Http\Controllers\Warung;

use App\Http\Helpers\Helper;
use App\Models\Agent\Version;
use App\Models\Agent\User;
use DateTime;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class VersionController extends Controller
{   
    public function version(Request $request)
    {       
        return view('warung.version.index');
    }	

    public function getVersion(Request $request) 
    {
        $start = (int) empty($request->input('start','0')) ? 0 : $request->input('start','0');
        $limit = (int) empty($request->input('limit','0')) ? 0 : $request->input('limit','25');

        $result = Version::getListVersion($start, $limit);

        return response()->json($result);
    }

    public function getVersionByID(Request $request, $id) 
    {
        $mresultVersion = Version::where('id', '=', $id)->first();
        $response = new \stdClass();

        $response->success = true;
        $response->payload = [
            'data' => $mresultVersion
        ];

        return response()->json($response);
    }
	
    public function formVersion(Request $request, $flag, $versionID)
    {       
        $data['vflag'] = $flag;        
        $data['vVersionID'] = $versionID;
        return view('warung.version.form', $data);
    }	

    public function saveVersion(Request $request)
    {
        $flag = $request->input('flag', '1');        
        $id = $request->input('id', null);
        $version = $request->input('version', null);

        if($flag == '1') {
            $checkVersion = Version::checkVersion($version, $id, 'add');
            if($checkVersion > 0) {
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400, 'message' => 'Version has been already exist'], 'data' => []
                    ]            
                ];               
                return response()->json($response);
            }

            $mresult = Version::insertVersion($version);
        }
        else {
            $checkVersion = Version::checkVersion($version, $id, 'upd');
            if($checkVersion > 0) {
                $response = [
                    'success' => false,
                    'payload' => [
                        'response' => ['code' => 400, 'message' => 'Version has been already exist'], 'data' => []
                    ]            
                ];               
                return response()->json($response);
            }

            $mresult = Version::updateVersion($id, $version);
        }

        $response = [];
        if($mresult) {
            $response = [
                'success' => true,
                'payload' => [
                    'response' => ['code' => 200, 'message' => 'Data has been success'], 'data' => []
                ]            
            ];            
        }
        else {
            $response = [
                'success' => false,
                'payload' => [
                    'response' => ['code' => 400, 'message' => 'Data has been failure'], 'data' => []
                ]            
            ];            
        }

        return response()->json($response);
    }

    public function delVersion(Request $request) 
    {
        $versionID = $request->input('id', null);

        $mresult = Version::where('id', $versionID)->delete();
        $response = [];
        if($mresult) {
            $response = [
                'success' => true,
                'payload' => [
                    'response' => ['code' => 200, 'message' => 'Data has been delete'], 'data' => []
                ]            
            ];            
        }
        else {
            $response = [
                'success' => false,
                'payload' => [
                    'response' => ['code' => 400, 'message' => 'Data has been failure'], 'data' => []
                ]            
            ];            
        }

        return response()->json($response);
    }
	
}
