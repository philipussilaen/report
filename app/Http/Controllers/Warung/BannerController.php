<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Helpers\SanitizeHelper;
use App\Models\Warung\WarungBanners;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\HelperApi;

class BannerController extends Controller
{
    public function index(Request $request){
        
        return view('warung.banner.input');
    }
    
    public function list(Request $request){
        $userType = $request->get('user_type');
        $start = request('start', 0);
        $limit = request('limit', 0);
        
        $banners = WarungBanners::when($userType, function ($query) use ($userType) {
                return $query->where('user_type', $userType);
        });
        
        $count = $banners->count();
        
        $banners = $banners->orderBy('updated_at', 'desc')->orderBy('created_at', 'desc')->offset($start)->limit($limit)->get();
        
        $data = [];
        $data['draw'] = request('draw');
        $data['result'] = SanitizeHelper::cleansingNull($banners, $start);
        $data['recordsTotal'] = sizeof($count);
        $data['base_url'] = env('AGENT_URL');
        return $data;
    }
    
    public function store(Request $request){
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $title = request('title');
        $userType = request('user_type');
        $status = request('status_banner');
        $bannerId = request('banner_id');
        $message_response = "New banner have been saved";
        
        DB::connection('popbox_agent')->beginTransaction();
        
        try {
            
            if($bannerId){
                $banner = WarungBanners::find($bannerId);
                $banner->updated_by = Auth::user()->name;
                $banner->status = $status;
                $banner->save();
                $message_response = "Banner $banner->title status have been updated to $status";
            } else{
                
                $image = $request->file('image');
                $fileName = date('YmdHis').'_'.$image->getClientOriginalName();
                
                $banner = new WarungBanners();
                $banner->title = $title;
                $banner->user_type = $userType;
                $banner->image_name = $fileName;
                $banner->status = $status;
                $banner->created_by = Auth::user()->name;
                $banner->save();
                
//              save picture
                $subFolder = "banner";
                $api = new HelperApi();
                $result = $api->postStoreFile($fileName, base64_encode(file_get_contents($image)), $subFolder);
                if(!$result->response->code == 200){
                    throw new \Exception($result->response->message);
                }
            }
            DB::connection('popbox_agent')->commit();
        } catch (\Exception $e){
            DB::connection('popbox_agent')->rollback();
            return back()->withErrors(["Failed to create new message ".$e->getMessage()])->withInput();
        }
        return back()->with('success', $message_response);
    }
}
