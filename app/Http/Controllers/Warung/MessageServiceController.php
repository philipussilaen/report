<?php

namespace App\Http\Controllers\Warung;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PopApps\MessageService;
use App\Models\Virtual\Region;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\PopApps\MessageServiceDetail;
use App\Http\Helpers\Helper;
use App\Http\Helpers\HelperApi;
use App\Http\Helpers\SanitizeHelper;

class MessageServiceController extends Controller
{
    public function index(Request $request){
        $message_types = [
            ['id' => 'all',             'text' => 'All'],
            ['id' => 'broadcast',       'text' => 'Broadcast'],
            ['id' => 'directmessage',   'text' => 'Direct Message'],
        ];
        
        return view("warung.message.index", compact('message_types'));
    }
    
    public function list(Request $request){
        $message_type = request('message_type');
        $keyword = request('keyword');
        $message_created_at = request('message_created_at', null);
        $start = request('start', 0);
        $limit = request('limit', 0);
        
        $startDate = '';
        $endDate = '';
        if (!empty($message_created_at)) {
            $startDate = Helper::formatDateRange($message_created_at, ',')->startDate;
            $endDate = Helper::formatDateRange($message_created_at, ',')->endDate;
        }
        
        $where = '';
        if($message_type && ($message_type != 'all')){
            $where .= " and ms.message_type = '$message_type'";
        }
        
        if($keyword){
            $where .= " and (ms.title LIKE '%$keyword%' OR ms.body_message LIKE '%$keyword%')";
        }
        
        if($startDate && $endDate){
            $where .= " and ms.created_at BETWEEN '$startDate' and '$endDate'";
        }
        $selected_column = "created_at";
        $populate = DB::connection('popbox_agent')->select(self::populate_message($selected_column, $where));
        
        $selected_column = "DATE_FORMAT(created_at, '%d %M %Y') as 'created_at', created_by, message_type, region,
            	CASE WHEN message_type = 'Broadcast' THEN CONCAT(sum(total_user), ' user(s)') ELSE recipient END as 'recipient',
            	title,
            	sum(message_opened) as 'message_opened',
            	IFNULL(CASE WHEN message_type = 'Broadcast' THEN concat(ROUND((sum(message_opened)/sum(total_user))*100, 2), '%') ELSE concat(sum(message_opened)*100, '%') END, '0%') as 'conv_open',
            	SUM(url_clicked) as 'url_clicked',
                image_name";
        $limit = "LIMIT $start, $limit";
        $result = DB::connection('popbox_agent')->select(self::populate_message($selected_column, $where, $limit));
        
        $data = [];
        $data['draw'] = request('draw');
        $data['result'] = SanitizeHelper::addCounterNumber($result, $start);
        $data['recordsTotal'] = sizeof($populate);
        $data['base_url'] = env('AGENT_URL');
        return $data;
        
    }
    
    public function newMessage(Request $request){
        $message_types = [
            ['id' => 'broadcast', 'name' => 'Broadcast'],
            ['id' => 'directmessage', 'name' => 'Direct Message'],
        ];
        
        $regions = collect(Region::orderBy('region_name', 'asc')->get())->map(function($item) {
            return ['id' => $item['region_code'], 'name' => $item['region_name']];
        });
            
        $menuApps = [
            ['id' => '1',  'name' => 'Penjualan'],
            ['id' => '2',  'name' => 'Penjualan Cepat'],
            ['id' => '3',  'name' => 'Lihat Stock'],
            ['id' => '4',  'name' => 'Product Digital'],
            ['id' => '5',  'name' => 'Perbaharui Data'],
            ['id' => '6',  'name' => 'Belanja Stock'],
            ['id' => '7',  'name' => 'Topup Deposit'],
            ['id' => '8',  'name' => 'Terima Barang'],
        ];
        
        return view("warung.message.input", compact('message_types', 'regions', 'menuApps'));
    }
    
    public function create(Request $request){
        
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        
        $messageType = request('message_type');
        $recipientType = request('recipient_type');
        $region = request('region');
        $messageTitle = request('message_title');
        $message = request('message');
        $recipientList = [];
        $linkLoc = request('link_loc');
        $linkMenuLoc = request('link_menu_loc');
        $btnName = request('btn_name');
        $urlDirect = request('url_direct');
        
        $image = $request->file('image');
        $fileName = $messageType.'_'.date('YmdHis').'_'.$image->getClientOriginalName();
        
        $locker_type = '';
        $count = 0;
        
        DB::connection('popbox_agent')->beginTransaction();
        try {
            $newMessage = new MessageService();
            $newMessage->message_type = $messageType;
            if($messageType == 'broadcast'){
                $newMessage->recipient_type = $recipientType;
                $newMessage->region_code    = $region;
            }
            $newMessage->title          = $messageTitle;
            $newMessage->body_message   = $message;
            $newMessage->image_name     = $fileName;
            $newMessage->filetype       = $image->getClientMimeType();
            $newMessage->url_loc        = $linkLoc;
            
            if($linkLoc == 'menuApp'){
                $newMessage->menu_app = $linkMenuLoc;
                $newMessage->btn_name = $btnName;
            } else {
                $newMessage->url = $urlDirect;
            }
            
            $newMessage->created_at = date('Y-m-d H:i:s');
            $newMessage->created_by = Auth::user()->name;
            $newMessage->updated_at = null;
            $newMessage->updated_by = null;
            $newMessage->deleted_at = null;
            $newMessage->deleted_by = null;
            $newMessage->save();
            
            if($messageType == 'broadcast'){
                if($recipientType != 'all'){
                    $locker_type = "and l.type = '$recipientType'";
                } else {
                    $locker_type = "and l.type in ('agent', 'warung')";
                }
                
                if(!empty($region)) $region_code = "and r.region_code = '$region'";
                
                $recipientList = DB::connection('popbox_virtual')->select(self::populate_user($region_code, $locker_type));
            } elseif($messageType == 'directmessage'){
                $recipientList = request('recipient', []);
            }
            
            $created_at = date('Y-m-d H:i:s');
            foreach ($recipientList as $recipient){
                $detail = new MessageServiceDetail();
                $detail->message_service_id = $newMessage->id;
                if(is_object($recipient)) {
                    $detail->recipient = $recipient->locker_id;
                } else {
                    $detail->recipient = $recipient;
                }
                $detail->url_clicked = 0;
                
                $detail->created_at = $created_at;
                $detail->created_by = Auth::user()->name;
                $detail->save();
            }
            
//             save picture
//             $path = env('AGENT_PATH')."upload/";
//             $image->move($path, $fileName);
            DB::connection('popbox_agent')->commit();
            
            $module = $messageType == 'broadcast' ? 'Broadcast Message' : "Direct Message";
            $subFolder = "upload";
            $api = new HelperApi();
            $result = $api->postStoreFile($fileName, base64_encode(file_get_contents($image)), $subFolder);
            if($result->response->code == 200){
                $mresult = $api->postSendMessage($newMessage->id, $module);
                $count = $mresult->data->count;
            } else {
                throw new \Exception($result->response->message);
            }
            
        } catch (\Exception $e) {
            DB::connection('popbox_agent')->rollback();
            return back()->withErrors(["Failed to create new message: ".$e->getMessage()])->withInput();
        }
        return back()->with('success', "New message have been created and sent to $count recipient(s).");
    }
    
    public function totalUser(Request $request){
        $where = "";
        $region_code = $request->get('region_code');
        $user_type = $request->get('user_type');
        
        if(!empty($region_code)){
            $where .= " and r.region_code = '$region_code'";
        }
        
        if(!empty($user_type) && $user_type != 'all'){
            $where .= " and l.type = '$user_type'";
        }
        $total_user = DB::connection('popbox_virtual')->select("select count(l.locker_id) as 'total_user' from lockers l join cities c on c.id = l.cities_id join regions r on c.regions_id = r.id where 1=1 $where");
        
        return $total_user[0]->total_user;
    }
    
    private function populate_user($region_code, $locker_type){
        return "
            select l.locker_id
            from lockers l
            join cities c on c.`id` = l.`cities_id`
            join regions r on r.`id` = c.`regions_id`
            where 1=1 $region_code $locker_type
        ";
    }
    
    private function populate_message($selected_column, $where, $limit = ''){
        return "
            select $selected_column
            from (
            	select created_at, 
            		created_by, 
            		message_type, 
            		region, 
            		CASE WHEN message_type = 'broadcast' THEN region ELSE recipient END as 'recipient', 
            		title, 
            		IFNULL(count(message_service_id), 0) as 'total_user', 
            		CASE WHEN message_opened = 1 THEN count(message_service_id) ELSE 0 END as 'message_opened', 
            		CASE WHEN url_clicked = 1 THEN count(message_service_id) ELSE 0 END as 'url_clicked',
        			image_name
            	from (
            		select ms.created_at, ms.created_by, 
            			CASE WHEN ms.message_type = 'directmessage' then 'DM' ELSE 'Broadcast' END as 'message_type', 
            			CASE WHEN ms.region_code is null THEN '-' ELSE r.region_name END as 'region', 
            			ms.title, msd.message_service_id, IFNULL(l.locker_name, 0) as 'recipient',
            			IFNULL(msd.message_opened, 0) as 'message_opened',
            			IFNULL(msd.url_clicked, 0) as 'url_clicked',
        			    ms.image_name
            		from message_service ms
            		left join message_service_detail msd on ms.id = msd.message_service_id
            		left join popbox_virtual.`regions` r on ms.region_code = r.`region_code`
            		left join popbox_virtual.lockers l on l.locker_id = msd.recipient
            		where r.deleted_at is null $where
            	) as temp
            	group by created_at, created_by, message_type, region, title, CASE WHEN message_type = 'broadcast' THEN region ELSE recipient END, message_opened, url_clicked, image_name
            ) as result
            group by created_at, created_by, message_type, region, title, recipient, image_name
    		order by DATE_FORMAT(created_at, '%Y-%m-%d %H:%i:%s') desc
    		$limit;
        ";
    }
    
}
