<?php

namespace App\Http\Controllers\PopApps;

use App\Http\Helpers\Helper;
use App\Http\Helpers\CustomeLocker;
use App\Http\Library\ApiProx;
use App\Models\PopApps\BalanceRecord;
use App\Models\PopApps\Delivery;
use App\Models\PopApps\DeliveryDestination;
use App\Models\PopApps\PopSafe;
use App\Models\PopApps\Transaction;
use App\Models\PopApps\PaymentMethods;
use App\Models\PopApps\User;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\NewLocker\Box;

class TransactionController extends Controller
{
    /**
     * Get Transaction list
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTransactionList(Request $request){
        $input = $request->input();
        $dateRange = $request->input('dateRange', null);
        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $transactionType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $userId = $request->input('user',null);
        $invoiceId = $request->input('invoiceId',null);
        $country = $request->input('country',null);
        $groupName = Auth::user()->group->name;

        
        $sort = '';

        //dd($sort);
        
        $sortNo = $request->input('sort_no', null);
        if(!empty($sortNo)) {
            $sort = 'id';
        }
        $sortUser = $request->input('sort_user', null);
        if(!empty($sortUser)) {
            $sort = 'user_id';
        }
        $sortTransactionDate = $request->input('sort_transaction', null);
        if(!empty($sortTransactionDate)) {
            $sort = 'created_at';
        }
        $sortDescription = $request->input('sort_description', null);
        if(!empty($sortDescription)){
            $sort = 'invoice_id';
        }
        $sortPrice = $request->input('sort_price', null);
        if(!empty($sortPrice)) {
            $sort = 'total_price';
        }
        $sortPromo = $request->input('sort_promo', null);
        if(!empty($sortPromo)){
            $sort = 'promo_amount'; 
        }
        $sortPaid = $request->input('sort_paid', null);
        if(!empty($sortPaid)){
            $sort = 'paid_amount'; 
        }
        $sortInvoice = $request->input('sort_invoice', null);
        if(!empty($sortInvoice)){
            $sort = 'invoice_id'; 
        }
        $sortStatus = $request->input('sort_status', null);
        if(!empty($sortStatus)){
            $sort = 'status'; 
        }
        $sortType = $request->input('sort_type', null);
        if(!empty($sortType)){
            $sort = 'transaction_type'; 
        }
        
        // DB::connection('popsend')->enableQueryLog();
        $transactionDb = Transaction::with(['user','topup','popsafe','delivery', 'delivery.destinations', 'campaign', 'campaign.campaign', 'campaign.voucher', 'campaign.voucher_code'])
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereIn('transactions.transaction_type', ['delivery','popsafe']);
                $query->leftJoin('popsafes', function($join)
                {
                    $join->on('popsafes.id', '=', 'transactions.transaction_id_reference');
                    $join->on('transactions.transaction_type','=', DB::raw("'popsafe'"));

                })->whereRaw("popsafes.locker_name LIKE '%pepito%'")->orWhereRaw("popsafes.locker_name LIKE '%popular%'");
                $query->leftJoin('deliveries', function($join)
                {
                    $join->on('deliveries.id', '=', 'transactions.transaction_id_reference');
                    $join->on('transactions.transaction_type','=', DB::raw("'delivery'"));
                })->orWhereRaw("deliveries.origin_name LIKE '%pepito%'")->orWhereRaw("deliveries.origin_name LIKE '%popular%'");
            })
            ->whereBetween('transactions.created_at',[$startDate,$endDate])
            ->when($invoiceId,function ($query) use ($invoiceId, $groupName){
                $query->WhereHas('topup',function ($query) use ($invoiceId){
                    $query->where('reference','like',$invoiceId)->where('transaction_type','topup');
                })
                    ->orWhereHas('popsafe',function ($query) use ($invoiceId, $groupName){
                        $query->where('invoice_code','like',$invoiceId)->where('transaction_type','popsafe')
                            ->when($groupName == 'pepito',function ($query) use ($groupName){
                                $query->where(function ($query) {
                                    $query->whereRaw("locker_name LIKE '%pepito%'")->orWhereRaw("locker_name LIKE '%popular%'");
                                });
                            });
                    })
                    ->orWhereHas('delivery',function ($query) use ($invoiceId, $groupName){
                        $query->where('invoice_code','like',$invoiceId)->where('transaction_type','delivery')
                            ->when($groupName == 'pepito',function ($query) use ($groupName){
                                $query->where(function ($query) {
                                    $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
                                });
                            });
                    })
                    ->orWhereHas('items.deliveryDestination',function ($query) use ($invoiceId){
                    $query->where('invoice_sub_code','like',$invoiceId)->where('transaction_type','delivery');
                });
            })
            ->when($transactionType,function ($query) use ($transactionType){
                $query->where('transaction_type',$transactionType);
            })
            ->when($transactionStatus,function ($query) use ($transactionStatus){
                $query->where('status',$transactionStatus);
            })
            ->when($userId,function ($query) use ($userId){
                $query->where('user_id',$userId);
            })
            ->when($country,function ($query) use ($country){
                $query->whereHas('user',function ($query) use ($country){
                   $query->where('country',$country);
                });
            })
            ->when(!is_null($sortPrice),function ($query) use ($sort,$sortPrice){
                $query->orderBy($sort, $sortPrice);
            })
            ->when(!is_null($sortPaid),function ($query) use ($sort,$sortPaid){
                $query->orderBy($sort, $sortPaid);
            })
            ->when(!is_null($sortTransactionDate),function ($query) use ($sort,$sortTransactionDate){
                $query->orderBy($sort, $sortTransactionDate);
            })
            ->when(!is_null($sortPromo),function ($query) use ($sort,$sortPromo){
                $query->orderBy($sort, $sortPromo);
            })
            ->when(!is_null($sortDescription),function ($query) use ($sort,$sortDescription){
                $query->orderBy($sort, $sortDescription);
            })
            ->when(!is_null($sortInvoice),function ($query) use ($sort,$sortInvoice){
                $query->orderBy($sort, $sortInvoice);
            })
            ->when(!is_null($sortStatus),function ($query) use ($sort,$sortStatus){
                $query->orderBy($sort, $sortStatus);
            })
            ->when(!is_null($sortUser),function ($query) use ($sort,$sortUser){
                $query->orderBy($sort, $sortUser);
            })
            ->when(!is_null($sortType),function ($query) use ($sort,$sortType){
                $query->orderBy($sort, $sortType);
            })
            ->when(!is_null($sortNo),function ($query) use ($sort,$sortNo){
                $query->orderBy($sort, $sortNo);
            })
            ->select([
                'transactions.id',
                'transactions.user_id',
                'transactions.transaction_type',
                'transactions.invoice_id',
                'transactions.transaction_id_reference',
                'transactions.status',
                'transactions.created_at',
                'transactions.total_price',
                'transactions.promo_amount',
                'transactions.paid_amount',
                'transactions.description',
            ])
            ->orderBy('transactions.created_at','desc')
            ->paginate(15);

        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        // create for filtering list
        $transactionStatusList = ['CREATED','PAID','REFUND'];
        if($groupName == 'pepito') {
            $transactionTypeList = ['delivery','popsafe'];
            $countryList = [
                'ID' => 'Indonesia'
            ];
        } else {
            $transactionTypeList = ['delivery','popsafe','topup','refund','referral'];
            $countryList = [
                'ID' => 'Indonesia',
                'MY' => 'Malaysia'
            ];
        }
        $userMemberList = User::get();

        // parse view
        $data = [];
        $data['transactionDb'] = $transactionDb->appends($request->input());
        $data['parameter'] = $param;
        $data['transactionTypeList'] = $transactionTypeList;
        $data['transactionStatusList'] = $transactionStatusList;
        $data['userMemberList'] = $userMemberList;
        $data['countryList'] = $countryList;

        return view('popapps.transactions.index',$data);
    }

    /**
     * Get Detail Transaction
     * @param Request $request
     * @param null $invoiceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getTransactionDetail(Request $request, $invoiceId=null){
        if (empty($invoiceId)) {
            $request->session()->flash('error','Empty Invoice Id');
            return back();
        }

        // Get User
        $user = Auth::user()->group->groupname;
        $groupUser = CustomeLocker::getGroupUser();
        // get transaction item
        $transactionDb = Transaction::with(['user','items','histories','campaign', 'payment'])
            ->where('invoice_id',$invoiceId)
            ->first();

        $paymentMethod = null;
        if (in_array($user, $groupUser)) {
            $paymentMethod = PaymentMethods::where('id', $transactionDb->payment->payment_methods_id)->first();
            $paymentMethod = $paymentMethod->code;
        }            
        
        if (!$transactionDb){
            $request->session()->flash('error','Transaction not Found');
            return back();
        }

//        dd($transactionDb->items[0]->deliveryDestination->histories);
//        dd($transactionDb->delivery->histories);

        // parse data to view
        $data = [];
        $data['transactionDb'] = $transactionDb;
        $data['user'] = $user;
        $data['paymentMethod'] = $paymentMethod;
        $data['groupUser'] = $groupUser;

        return view('popapps.transactions.detail',$data);
    }

    /**
     * Post Refund Transaction
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRefundTransaction(Request $request){
        // validation
        $rules = [
            'invoiceId' => 'required|exists:popsend.transactions,invoice_id'
        ];

        $this->validate($request,$rules);

        $invoiceId = $request->input('invoiceId');
        $remarks = $request->input('remarks',null);

        // get transaction
        $transactionDb = Transaction::where('invoice_id',$invoiceId)->first();
        if (!$transactionDb){
            $request->session()->flash('error','Invalid Transaction Invoice ID');
            return back();
        }
        $transactionId = $transactionDb->id;

        $allowedRefund = ['delivery','popsafe'];

        // get transaction type
        $transactionType = $transactionDb->transaction_type;
        $transactionStatus = $transactionDb->status;
        if (!in_array($transactionType,$allowedRefund)){
            $request->session()->flash('error','Transaction Not Allowed to Refund');
            return back();
        }
        if ($transactionStatus!='PAID'){
            $request->session()->flash('error','Transaction Not PAID. Status: '.$transactionStatus);
            return back();
        }

        DB::connection('popsend')->beginTransaction();
        if ($transactionType == 'popsafe'){
            $transactionModel = new Transaction();
            $refundTransaction = $transactionModel->refundTransaction($transactionId,$remarks);
            if (!$refundTransaction->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$refundTransaction->errorMsg);
                return back();
            }

            $popSafeModel = new PopSafe();
            $cancelPopSafe = $popSafeModel->cancelOrder($transactionId,$remarks);
            if (!$cancelPopSafe->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$cancelPopSafe->errorMsg);
                return back();
            }
            $request->session()->flash('success','Success Refund');
        }
        elseif ( $transactionType == 'delivery'){
            // cancel first
            $deliveryModel = new Delivery();
            $cancelDelivery = $deliveryModel->cancelOrder($transactionId,$remarks);
            if (!$cancelDelivery->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$cancelDelivery->errorMsg);
                return back();
            }
            $refundAmount = $cancelDelivery->refundAmount;
            $refundList = $cancelDelivery->refundList;
            $totalDestinations = $cancelDelivery->totalDestinations;

            if ($refundAmount == 0){
                DB::connection('popsend')->rollback();
                $failedRefund = "";
                foreach ($refundList as $item) {
                    $failedRefund .= "$item->subInvoice - $item->errorMsg, ";
                }
                $request->session()->flash('error','Failed Refund '.$failedRefund);
                return back();
            }

            $transactionModel = new Transaction();
            $refundDelivery = $transactionModel->refundDelivery($transactionId,$refundAmount,$refundList,$remarks);
            if (!$refundDelivery->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$refundDelivery->errorMsg);
                return back();
            }
            $successRefund = "";
            $failedRefund = "";

            foreach ($refundList as $item) {
                if ($item->isRefund){
                    $successRefund .= "$item->subInvoice, ";
                } else {
                    $failedRefund .= "$item->subInvoice - $item->errorMsg";
                }
            }

            $request->session()->flash('success','Success Refund '.$successRefund);
            $request->session()->flash('error','Failed Refund '.$failedRefund);
        }
        else {
            $request->session()->flash('error','Invalid Type');
            DB::connection('popsend')->rollback();
        }

        DB::connection('popsend')->commit();

        return back();
    }

    /**
     * Post Cancel Refund Transaction
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postCancelRefundTransaction(Request $request){
        // validation
        $rules = [
            'invoiceId' => 'required|exists:popsend.transactions,invoice_id'
        ];

        $this->validate($request,$rules);

        $invoiceId = $request->input('invoiceId');
        $remarks = $request->input('remarks',null);

        // get transaction
        $transactionDb = Transaction::where('invoice_id',$invoiceId)->first();
        if (!$transactionDb){
            $request->session()->flash('error','Invalid Transaction Invoice ID');
            return back();
        }
        $transactionId = $transactionDb->id;

        $allowedRefund = ['delivery','popsafe'];

        // get transaction type
        $transactionType = $transactionDb->transaction_type;
        $transactionStatus = $transactionDb->status;
        if (!in_array($transactionType,$allowedRefund)){
            $request->session()->flash('error','Transaction Not Allowed to Refund');
            return back();
        }
        if ($transactionStatus!='PAID'){
            $request->session()->flash('error','Transaction Not PAID. Status: '.$transactionStatus);
            return back();
        }

        DB::connection('popsend')->beginTransaction();
        if ($transactionType == 'popsafe'){
            $transactionModel = new Transaction();
            $refundTransaction = $transactionModel->refundTransaction($transactionId,$remarks);
            if (!$refundTransaction->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$refundTransaction->errorMsg);
                return back();
            }

            $popSafeModel = new PopSafe();
            $cancelPopSafe = $popSafeModel->cancelOrder($transactionId,$remarks);
            if (!$cancelPopSafe->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$cancelPopSafe->errorMsg);
                return back();
            }
            $request->session()->flash('success','Success Refund');
        }
        elseif ( $transactionType == 'delivery'){
            // cancel first
            $deliveryModel = new Delivery();
            $cancelDelivery = $deliveryModel->cancelOrder($transactionId,$remarks);
            if (!$cancelDelivery->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$cancelDelivery->errorMsg);
                return back();
            }
            $refundAmount = $cancelDelivery->refundAmount;
            $refundList = $cancelDelivery->refundList;
            $totalDestinations = $cancelDelivery->totalDestinations;

            if ($refundAmount == 0){
                DB::connection('popsend')->rollback();
                $failedRefund = "";
                foreach ($refundList as $item) {
                    $failedRefund .= "$item->subInvoice - $item->errorMsg, ";
                }
                $request->session()->flash('error','Failed Refund '.$failedRefund);
                return back();
            }

            $transactionModel = new Transaction();
            $refundDelivery = $transactionModel->refundDelivery($transactionId,$refundAmount,$refundList,$remarks);
            if (!$refundDelivery->isSuccess){
                DB::connection('popsend')->rollback();
                $request->session()->flash('error',$refundDelivery->errorMsg);
                return back();
            }
            $successRefund = "";
            $failedRefund = "";

            foreach ($refundList as $item) {
                if ($item->isRefund){
                    $successRefund .= "$item->subInvoice, ";
                } else {
                    $failedRefund .= "$item->subInvoice - $item->errorMsg";
                }
            }

            $request->session()->flash('success','Success Refund '.$successRefund);
            $request->session()->flash('error','Failed Refund '.$failedRefund);
        }
        else {
            $request->session()->flash('error','Invalid Type');
            DB::connection('popsend')->rollback();
        }

        DB::connection('popsend')->commit();

        return back();
    }

    /**
     * Re Push Delivery
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRePushDelivery(Request $request){
        $destinationId = $request->input('itemId');
        $subInvoice = $request->input('subInvoice');

        // get delivery
        $deliveryDestinationDb = DeliveryDestination::where('id',$destinationId)->where('invoice_sub_code',$subInvoice)->first();

        if (!$deliveryDestinationDb){
            $request->session()->flash('error','Invalid Sub Invoice');
            return back();
        }

        $rePush = DeliveryDestination::rePushExpress($destinationId);
        if (!$rePush->isSuccess){
            $request->session()->flash('error',"Failed : ".$rePush->errorMsg);
            return back();
        }

        $request->session()->flash('success','Success RePush '.$subInvoice);
        return back();
    }

    /*------------------------------------------TRANSACTION V2-------------------------------------------*/

    public function getLanding() {
        $groupUser = CustomeLocker::getGroupUser();
        $user = Auth::user()->group->groupname;
        $name = Auth::user()->group->name;
        $customeLocker = new CustomeLocker;
        $paymentMethod = $customeLocker->paymentMethods();

        $lockerLocation = '';

        if($name == 'ap1_level01') {
            $lockerID = [
                '3404703e47ac8cb1ca645f87bee3c0b3',
                '92457d6a5e86c3c2c09e3b4898eee9c6'
            ];
    
            // get from box where delete flag 0
            $lockerLocation = Box::select('tb_newlocker_box.name')
            ->leftJoin('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')      
            ->whereIn('id', $lockerID)
            ->where('deleteFlag',0)
            ->get();
        } else {
            if ($user != 'PDSL' || $user != null) {
                $lockerLocation = $customeLocker->lockerLocations($user);
            }
        }
        
        return view('popapps.transactions.index2', compact('user', 'lockerLocation', 'groupUser', 'paymentMethod'));
    }

    public function getDefaultData() {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $groupName = Auth::user()->group->name;
        // create for filtering list
        $transactionStatusList = ['CREATED','PAID','REFUND'];
        if($groupName == 'pepito') {
            $transactionTypeList = ['DELIVERY','POPSAFE'];
        } else {
            $transactionTypeList = ['DELIVERY','POPSAFE','TOPUP','REFUND','REFERRAL'];
        }

        $groupUser = CustomeLocker::getGroupUser();
        $userGroup = Auth::user()->group->groupname;
        if (in_array($userGroup, $groupUser)) {
            $customeLocker = new CustomeLocker;
            $userMemberList = $customeLocker->getAllUserCustome($userGroup);
        } else {
            $userMemberList = User::all();
        }
        // dd($userMemberList);

        // parse data to view
        $data = [];
        $data['transactionTypeList'] = $transactionTypeList;
        $data['transactionStatusList'] = $transactionStatusList;
        $data['userMemberList'] = $userMemberList;


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);

    }

    public function getFilterData(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $input = $request->input();

        $transactionType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $userId = $request->input('user',null);
        $invoiceId = $request->input('invoiceid',null);
        $country = $request->input('country',null);
        $promoCode = $request->input('promoCode',null);
        $paymentMethod = $request->input('paymentMethod',null);
        $dateRangeTransaction = $request->input('dateRange', null);
        $groupName = Auth::user()->group->name;
        $userGroup = Auth::user()->group->groupname;
        
        $name = Auth::user()->group->name;

        $customeLocker = new CustomeLocker;
        $groupCompany = $customeLocker->getCompany($userGroup);
        $userMemberList = $customeLocker->getAllUserCustome($userGroup);
        $groupUser = CustomeLocker::getGroupUser();
        
        $startDate = null;
        $endDate = null;
        if (!empty($dateRangeTransaction)) {
            $startDate = Helper::formatDateRange($dateRangeTransaction, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRangeTransaction, ',')->endDate;
        }

        // DB::connection('popsend')->enableQueryLog();
        $transactionDb = Transaction::with(['user','topup','popsafe','delivery', 'delivery.destinations', 'campaign', 'campaign.campaign', 'campaign.voucher', 'campaign.voucher_code'])
            ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
            ->leftjoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
            ->select('transactions.*', 'payment_methods.code', 'payment_methods.name')
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereIn('transactions.transaction_type', ['delivery','popsafe']);
                $query->leftJoin('popsafes', function($join)
                {
                    $join->on('popsafes.id', '=', 'transactions.transaction_id_reference');
                    $join->on('transactions.transaction_type','=', DB::raw("'popsafe'"));

                })->whereRaw("popsafes.locker_name LIKE '%pepito%'")->orWhereRaw("popsafes.locker_name LIKE '%popular%'");
                $query->leftJoin('deliveries', function($join)
                {
                    $join->on('deliveries.id', '=', 'transactions.transaction_id_reference');
                    $join->on('transactions.transaction_type','=', DB::raw("'delivery'"));
                })->orWhereRaw("deliveries.origin_name LIKE '%pepito%'")->orWhereRaw("deliveries.origin_name LIKE '%popular%'");
            })
            ->when($dateRangeTransaction, function ($query) use ($startDate, $endDate){
                $query->whereBetween('transactions.created_at',[$startDate,$endDate]);
            })
            ->when($invoiceId,function ($query) use ($invoiceId, $groupName){
                $query->where('transactions.invoice_id', $invoiceId)
                    ->when($groupName == 'pepito',function ($query) use ($groupName){
                                $query->where(function ($query) {
                                    $query->whereRaw("transactions.description LIKE '%pepito%'")->orWhereRaw("transactions.description LIKE '%popular%'");
                                });
                            });

//                $query->WhereHas('topup',function ($query) use ($invoiceId){
//                    $query->where('reference','like',$invoiceId)->where('transaction_type','topup');
//                })
//                    ->orWhereHas('popsafe',function ($query) use ($invoiceId, $groupName){
//                        $query->where('invoice_code','like',$invoiceId)->where('transaction_type','popsafe')
//                            ->when($groupName == 'pepito',function ($query) use ($groupName){
//                                $query->where(function ($query) {
//                                    $query->whereRaw("locker_name LIKE '%pepito%'")->orWhereRaw("locker_name LIKE '%popular%'");
//                                });
//                            });
//                    })
//                    ->orWhereHas('delivery',function ($query) use ($invoiceId, $groupName){
//                        $query->where('invoice_code','like',$invoiceId)->where('transaction_type','delivery')
//                            ->when($groupName == 'pepito',function ($query) use ($groupName){
//                                $query->where(function ($query) {
//                                    $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
//                                });
//                            });
//                    })
//                    ->orWhereHas('items.deliveryDestination',function ($query) use ($invoiceId){
//                        $query->where('invoice_sub_code','like',$invoiceId)->where('transaction_type','delivery');
//                    });

            })
            ->when($transactionType,function ($query) use ($transactionType){
                $query->where('transaction_type',$transactionType);
            })
            ->when($transactionStatus,function ($query) use ($transactionStatus){
                $query->where('transactions.status',$transactionStatus);
            })
            ->when($promoCode, function ($query) use ($promoCode) {
                $query->WhereHas('campaign', function ($query) use ($promoCode) {
                    $query->WhereHas('voucher', function ($query) use ($promoCode){
                        $query->where('code', $promoCode);
                    });
                });
            })
            ->when($paymentMethod, function ($query) use ($paymentMethod) {
                $query->where('payment_methods.id',$paymentMethod);                
            })
            ->when($userId,function ($query) use ($userId){
                $query->where('transactions.user_id',$userId);
            })
            ->when($country,function ($query) use ($country, $userGroup, $groupUser){
                if (in_array($userGroup, $groupUser)) {
                    $query->leftjoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id');
                    $query->select('popsafes.locker_name', 'transactions.*', 'payment_methods.*');
                    return $query->where('popsafes.locker_name', $country);                    
                } else {
                    $query->whereHas('user',function ($query) use ($country){
                        $query->where('country',$country);
                    });
                }
            })
            ->when($name == 'ap1_level01' && $country == null,function ($query) use ($country){
                $query->leftjoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id');
                $query->whereIn('popsafes.locker_name', ['DPS 01','DPS 02']);
            })
            ->when($groupCompany, function ($query) use ($groupCompany){
                return $query->where('transactions.client_id_companies', $groupCompany);
            })
            ->orderBy('transactions.created_at','desc')
            ->get();
        
        // count total customer
        $totalCostumer = Transaction::with(['user','topup','popsafe','delivery', 'delivery.destinations', 'campaign', 'campaign.campaign', 'campaign.voucher', 'campaign.voucher_code'])
        ->leftjoin('payments', 'payments.transactions_id', '=', 'transactions.id')
        ->leftjoin('payment_methods', 'payments.payment_methods_id', '=', 'payment_methods.id')
        ->distinct('transactions.user_id')
        ->when($groupName == 'pepito',function ($query) use ($groupName){
            $query->whereIn('transactions.transaction_type', ['delivery','popsafe']);
            $query->leftJoin('popsafes', function($join)
            {
                $join->on('popsafes.id', '=', 'transactions.transaction_id_reference');
                $join->on('transactions.transaction_type','=', DB::raw("'popsafe'"));

            })->whereRaw("popsafes.locker_name LIKE '%pepito%'")->orWhereRaw("popsafes.locker_name LIKE '%popular%'");
            $query->leftJoin('deliveries', function($join)
            {
                $join->on('deliveries.id', '=', 'transactions.transaction_id_reference');
                $join->on('transactions.transaction_type','=', DB::raw("'delivery'"));
            })->orWhereRaw("deliveries.origin_name LIKE '%pepito%'")->orWhereRaw("deliveries.origin_name LIKE '%popular%'");
        })
        ->when($dateRangeTransaction, function ($query) use ($startDate, $endDate){
            $query->whereBetween('transactions.created_at',[$startDate,$endDate]);
        })
        ->when($invoiceId,function ($query) use ($invoiceId, $groupName){
            $query->where('transactions.invoice_id', $invoiceId)
                ->when($groupName == 'pepito',function ($query) use ($groupName){
                            $query->where(function ($query) {
                                $query->whereRaw("transactions.description LIKE '%pepito%'")->orWhereRaw("transactions.description LIKE '%popular%'");
                            });
                        });
        })
            ->when($transactionType,function ($query) use ($transactionType){
                $query->where('transaction_type',$transactionType);
            })
            ->when($transactionStatus,function ($query) use ($transactionStatus){
                $query->where('transactions.status',$transactionStatus);
            })
            ->when($promoCode, function ($query) use ($promoCode) {
                $query->WhereHas('campaign', function ($query) use ($promoCode) {
                    $query->WhereHas('voucher', function ($query) use ($promoCode){
                        $query->where('code', $promoCode);
                    });
                });
            })
            ->when($paymentMethod, function ($query) use ($paymentMethod) {
                $query->where('payment_methods.id',$paymentMethod);                
            })
            ->when($userId,function ($query) use ($userId){
                $query->where('transactions.user_id',$userId);
            })
            ->when($country,function ($query) use ($country, $userGroup, $groupUser){
                if (in_array($userGroup, $groupUser)) {
                    $query->leftjoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id');
                    $query->select('popsafes.locker_name', 'transactions.*', 'payment_methods.*');
                    return $query->where('popsafes.locker_name', $country);                    
                } else {
                    $query->whereHas('user',function ($query) use ($country){
                        $query->where('country',$country);
                    });
                }
            })
            ->when($name == 'ap1_level01' && $country == null,function ($query) use ($country){
                $query->leftjoin('popsafes', 'transactions.transaction_id_reference', '=', 'popsafes.id');
                $query->whereIn('popsafes.locker_name', ['DPS 01','DPS 02']);
            })
            ->when($groupCompany, function ($query) use ($groupCompany){
                return $query->where('transactions.client_id_companies', $groupCompany);
            })
        ->count('transactions.user_id');

        $allType = 0;
        $delivery = 0;
        $popsafe = 0;
        $topup = 0;
        $referral = 0;
        $refund = 0;
        $amount = 0;
        // $totalCostumer = count($userMemberList); // count user di query atas

        foreach ($transactionDb as $item) {
            if ($item->status == 'PAID') {
                $amount += $item->total_price;
            }
            if ($item->transaction_type == 'delivery') {
                $delivery += 1;
            } else if ($item->transaction_type == 'popsafe') {
                $popsafe += 1;
            } else if ($item->transaction_type == 'topup') {
                $topup += 1;
            } else if ($item->transaction_type == 'referral') {
                $referral += 1;
            } if ($item->transaction_type == 'refund') {
                $refund += 1;
            }
        }
        
        $allType = $delivery + $popsafe + $topup + $referral + $refund;

        // parse data to view
        $data = [];
        $data['allType'] = $allType;
        $data['delivery'] = $delivery;
        $data['popsafe'] = $popsafe;
        $data['topup'] = $topup;
        $data['referral'] = $referral;
        $data['refund'] = $refund;
        $data['amount'] = $amount;
        $data['totalCostumer'] = $totalCostumer;
        $data['transactionDb'] = $transactionDb;
        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function getAjaxDownloadTransaction(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $input = $request->input();

        $transactionType = $request->input('type',null);
        $transactionStatus = $request->input('status',null);
        $userId = $request->input('user',null);
        $invoiceId = $request->input('invoiceid',null);
        $country = $request->input('country',null);
        $promoCode = $request->input('promoCode',null);
        $dateRangeTransaction = $request->input('dateRange', null);
        $userGroup = $request->input('userGroup', null);
        $groupUser = $request->input('groupUser', null);
        $groupName = Auth::user()->group->name;

        $company = new CustomeLocker;
        $getCompany = null;

        if (in_array($userGroup, $groupUser)) {
            $getCompany = $company->getCompany($userGroup);
        }

        $startDate = null;
        $endDate = null;
        if (!empty($dateRangeTransaction)) {
            $startDate = Helper::formatDateRange($dateRangeTransaction, ',')->startDate;
            $endDate = Helper::formatDateRange($dateRangeTransaction, ',')->endDate;

            $startDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $startDate)->format('d M Y');
            $endDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $endDate)->format('d M Y');
        }

        // DB::connection('popsend')->enableQueryLog();
        $transactionDb = Transaction::with(['user','topup','popsafe','delivery', 'delivery.destinations', 'campaign', 'campaign.campaign', 'campaign.voucher', 'campaign.voucher_code'])
            ->when($groupName == 'pepito',function ($query) use ($groupName){
                $query->whereIn('transactions.transaction_type', ['delivery','popsafe']);
                $query->leftJoin('popsafes', function($join)
                {
                    $join->on('popsafes.id', '=', 'transactions.transaction_id_reference');
                    $join->on('transactions.transaction_type','=', DB::raw("'popsafe'"));

                })->whereRaw("popsafes.locker_name LIKE '%pepito%'")->orWhereRaw("popsafes.locker_name LIKE '%popular%'");
                $query->leftJoin('deliveries', function($join)
                {
                    $join->on('deliveries.id', '=', 'transactions.transaction_id_reference');
                    $join->on('transactions.transaction_type','=', DB::raw("'delivery'"));
                })->orWhereRaw("deliveries.origin_name LIKE '%pepito%'")->orWhereRaw("deliveries.origin_name LIKE '%popular%'");
            })
            ->when($dateRangeTransaction, function ($query) use ($startDate, $endDate){
                $query->whereBetween('transactions.created_at',[$startDate,$endDate]);
            })
            ->when($invoiceId,function ($query) use ($invoiceId, $groupName){
                $query->where('transactions.invoice_id', $invoiceId)
                    ->when($groupName == 'pepito',function ($query) use ($groupName){
                        $query->where(function ($query) {
                            $query->whereRaw("transactions.description LIKE '%pepito%'")->orWhereRaw("transactions.description LIKE '%popular%'");
                        });
                    });
//                $query->WhereHas('topup',function ($query) use ($invoiceId){
//                    $query->where('reference','like',$invoiceId)->where('transaction_type','topup');
//                })
//                    ->orWhereHas('popsafe',function ($query) use ($invoiceId, $groupName){
//                        $query->where('invoice_code','like',$invoiceId)->where('transaction_type','popsafe')
//                            ->when($groupName == 'pepito',function ($query) use ($groupName){
//                                $query->where(function ($query) {
//                                    $query->whereRaw("locker_name LIKE '%pepito%'")->orWhereRaw("locker_name LIKE '%popular%'");
//                                });
//                            });
//                    })
//                    ->orWhereHas('delivery',function ($query) use ($invoiceId, $groupName){
//                        $query->where('invoice_code','like',$invoiceId)->where('transaction_type','delivery')
//                            ->when($groupName == 'pepito',function ($query) use ($groupName){
//                                $query->where(function ($query) {
//                                    $query->whereRaw("origin_name LIKE '%pepito%'")->orWhereRaw("origin_name LIKE '%popular%'");
//                                });
//                            });
//                    })
//                    ->orWhereHas('items.deliveryDestination',function ($query) use ($invoiceId){
//                        $query->where('invoice_sub_code','like',$invoiceId)->where('transaction_type','delivery');
//                    });
            })
            ->when($transactionType,function ($query) use ($transactionType){
                $query->where('transaction_type',$transactionType);
            })
            ->when($transactionStatus,function ($query) use ($transactionStatus){
                $query->where('status',$transactionStatus);
            })
            ->when($getCompany ,function ($query) use ($getCompany){
                $query->where('transactions.client_id_companies',$getCompany);
            })
            ->when($promoCode, function ($query) use ($promoCode) {
                $query->WhereHas('campaign', function ($query) use ($promoCode) {
                    $query->WhereHas('voucher', function ($query) use ($promoCode){
                        $query->where('code', $promoCode);
                    });
                });
            })
            ->when($userId,function ($query) use ($userId){
                $query->where('user_id',$userId);
            })
            ->when($country,function ($query) use ($country){
                $query->whereHas('user',function ($query) use ($country){
                    $query->where('country',$country);
                });
            })
            ->orderBy('transactions.created_at','desc')
            ->get();

        // Init Data Excel
        $filename = "[".date('ymd')."]"." Transaction List";

        Excel::create($filename, function ($excel) use ($transactionDb, $startDateFilename, $endDateFilename){
            $excel->sheet('Sheet 1', function ($sheet) use ($transactionDb, $startDateFilename, $endDateFilename){

                $sheet->cell('B1:C1:D1:B2:C2:D2', function($cell) {

                    $cell->setFontSize(26);
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('B2:D2');

                $header = $startDateFilename." - ".$endDateFilename;
                $sheet->row(1, ['', 'Transaction List']);
                $sheet->row(2, ['', $header]);

                $row = 4;
                $no_urut = 1;

                $arr_title = ['No', 'User Id', 'User Name', 'User Phone', 'User Email', 'Transaction Type',
                    'Transaction ID', 'Invoice ID', 'Description', 'Status', 'Transaction Date', 'Price',
                    'Promo Amount', 'Paid Amount', 'Promo Code'];

                $sheet->row($row, $arr_title);

                foreach ($transactionDb as $index => $item) {

//                    dd($item);

                    $row++;

                    if (isset($item->user)) {
                        $memberId = $item->user->member_id;
                        $name = $item->user->name;
                        $phone = $item->user->phone;
                        $email = $item->user->email;
                    } else {
                        $memberId = '';
                        $name = '';
                        $phone = '';
                        $email = '';
                    }

                    $transactionType = '';
                    if (isset($item->transaction_type)) {
                        $transactionType = $item->transaction_type;
                    }

                    $destinationInvoice = '';
                    if(isset($item->delivery->destinations)) {
                        if (count($item->delivery->destinations) > 1) {
                            foreach ($item->delivery->destinations as $destination) {
                                $destinationInvoice .= $destination->invoice_sub_code;
                                $destinationInvoice .= ',';
                            }
                        } else {
                            $destinationInvoice = $item->delivery->destinations[0]->invoice_sub_code;
                        }
                    }

                    $popsafeInvoice = '';
                    if (isset($item->popsafe->invoice_code)) {
                        $popsafeInvoice = $item->popsafe->invoice_code;
                    }

                    if (isset($item->campaign->voucher->code)) {
                        $promoCode = $item->campaign->voucher->code;
                    } else {
                        $promoCode = '';
                    }

                    $invoiceId = '';
                    if ($item->transaction_type == 'delivery') {
                        $invoiceId = $destinationInvoice;
                    } else if ($item->transaction_type = 'popsafe') {
                        $invoiceId = $popsafeInvoice;
                    }

                    $arr = [$no_urut,
                        $memberId,
                        $name,
                        $phone,
                        $email,
                        $transactionType,
                        $item->invoice_id,
                        $invoiceId,
                        $item->description,
                        $item->status,
                        $item->created_at,
                        $item->total_price,
                        $item->promo_amount,
                        $item->paid_amount,
                        $promoCode];

                    $sheet->row($row, $arr);
                    $no_urut++;

                }

            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }
}
