<?php

namespace App\Http\Controllers\PopApps;

use App\Http\Helpers\Helper;
use App\Models\PopApps\User;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Get List User
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(Request $request){
        $input = $request->input();

        $phone = $request->input('phone',null);
        $email = $request->input('email',null);
        $name = $request->input('name',null);
        $country = $request->input('country',null);

        // get all users
        $usersDb = User::with('transactions')
            ->when($phone,function ($query) use ($phone){
                $query->where('phone','LIKE',"%$phone%");
            })
            ->when($email,function ($query) use ($email){
                $query->where('email','LIKE',"%$email%");
            })
            ->when($name,function ($query) use ($name){
                $query->where('name','LIKE',"%$name%");
            })
            ->when($country,function ($query) use ($country){
                $query->where('country',$country);
            })
            ->orderBy('id','desc')
            ->paginate(15);

        // query get counter
        $groupByCountry = User::groupBy('country')
            ->select(DB::raw('COUNT(*) as count, country'))
            ->get();

        // data for filtering user
        $country = [
            'ID' => 'Indonesia',
            'MY' => 'Malaysia'
        ];

        // parse data to view
        $param = [];
        foreach ($input as $key => $item) {
            $param[$key] = $item;
        }

        $data = [];
        $data['parameter'] = $param;
        $data['countryList'] = $country;
        $data['userDb'] = $usersDb;
        $data['groupByCountry'] = $groupByCountry;

        return view('popapps.user.list',$data);
    }

    /**
     * Get User Detail
     * @param Request $request
     * @param null $memberId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getUserDetail(Request $request,$memberId=null){
        if (empty($memberId)){
            $request->session()->flash('error','Empty Member Id');
            return back();
        }

        // get member based on member ID
        $userDb = User::with(['balanceRecords','balanceRecords.transaction'])
            ->where('member_id',$memberId)
            ->first();

        if (!$userDb){
            $request->session()->flash('error','User Not Found');
            return back();
        }

        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $balanceRecordDb = $userDb->balanceRecords()
            ->whereBetween('created_at',[$startDate,$endDate])
            ->orderBy('id','desc')
            ->paginate(10);

        // parse data to view
        $data = [];
        $data['userDb'] = $userDb;
        $data['balanceRecordDb'] = $balanceRecordDb;

        return view('popapps.user.detail',$data);
    }

    /**
     * Get User Detail
     * @param Request $request
     * @param null $memberId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getUserDetailByID(Request $request, $userId=null){
        if (empty($userId)){
            $request->session()->flash('error','Empty Member Id');
            return back();
        }

        // get member based on member ID
        $userDb = User::with(['balanceRecords','balanceRecords.transaction'])
            ->where('id',$userId)
            ->first();

        if (!$userDb){
            $request->session()->flash('error','User Not Found');
            return back();
        }

        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $balanceRecordDb = $userDb->balanceRecords()
            ->whereBetween('created_at',[$startDate,$endDate])
            ->orderBy('id','desc')
            ->paginate(10);

        // parse data to view
        $data = [];
        $data['userDb'] = $userDb;
        $data['balanceRecordDb'] = $balanceRecordDb;

        return view('popapps.user.detail',$data);
    }

    /**
     * Get Ajax User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxUser(Request $request){
        $termsSearch = $request->input('term',null);

        // search user
        $userDb = User::where('phone','LIKE',"$termsSearch%")
            ->orWhere('name','LIKE',"%$termsSearch%")
            ->get();

        $totalCount = count($userDb);

        $data = new \stdClass();
        $data->total_count = $totalCount;
        $data->items = $userDb->toArray();

        return response()->json($data);
    }

    /*----------------------------------------USER CONTROLLER V2---------------------------------------------------*/

    public function getLandingV2() {
        return view('popapps.user.list2');
    }

    public function getListV2(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $input = $request->input();

        $phone = $request->input('phone',null);
        $email = $request->input('email',null);
        $name = $request->input('name',null);
        $dateRangeRegistered = $request->input('daterange_registered', null);
        $referralCode = $request->input('referral_code',null);
        $country = $request->input('country',null);

        $registeredStartDate = null;
        $registeredEndDate = null;
        if (!empty($dateRangeRegistered)) {
            $registeredStartDate = Helper::formatDateRange($dateRangeRegistered, ',')->startDate;
            $registeredEndDate = Helper::formatDateRange($dateRangeRegistered, ',')->endDate;
        }

        // get all users
        $usersDb = User::with('transactions')
            ->when($phone,function ($query) use ($phone){
                $query->where('phone','LIKE',"%$phone%");
            })
            ->when($email,function ($query) use ($email){
                $query->where('email','LIKE',"%$email%");
            })
            ->when($name,function ($query) use ($name){
                $query->where('name','LIKE',"%$name%");
            })
            ->when($country,function ($query) use ($country){
                $query->where('country',$country);
            })
            ->when($dateRangeRegistered, function ($query) use ($registeredStartDate, $registeredEndDate){
                return $query->whereBetween('users.created_at', [$registeredStartDate, $registeredEndDate]);
            })
            ->when($referralCode, function ($query) use ($referralCode){
                return $query->where('referral_code', $referralCode);
            })
            ->orderBy('id','desc')
            ->get();

        // query get counter
        $groupByCountry = User::groupBy('country')
            ->select(DB::raw('COUNT(*) as count, country'))
            ->get();

        $data = [];
        $data['param'] = $input;
        $data['country'] = $groupByCountry;
        $data['resultList'] = $usersDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getAjaxDownloadList(Request $request) {

        set_time_limit(0);
        ini_set('memory_limit', '2048M');

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $phone = $request->input('phone',null);
        $email = $request->input('email',null);
        $name = $request->input('name',null);
        $dateRangeRegistered = $request->input('daterange_registered', null);
        $referralCode = $request->input('referral_code',null);
        $country = $request->input('country',null);

        $registeredStartDate = null;
        $registeredEndDate = null;
        if (!empty($dateRangeRegistered)) {
            $registeredStartDate = Helper::formatDateRange($dateRangeRegistered, ',')->startDate;
            $registeredEndDate = Helper::formatDateRange($dateRangeRegistered, ',')->endDate;

            $startDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $registeredStartDate)->format('d M Y');
            $endDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $registeredEndDate)->format('d M Y');
        }

        // get all users
        $usersDb = User::with('transactions')
            ->when($phone,function ($query) use ($phone){
                $query->where('phone','LIKE',"%$phone%");
            })
            ->when($email,function ($query) use ($email){
                $query->where('email','LIKE',"%$email%");
            })
            ->when($name,function ($query) use ($name){
                $query->where('name','LIKE',"%$name%");
            })
            ->when($country,function ($query) use ($country){
                $query->where('country',$country);
            })
            ->when($dateRangeRegistered, function ($query) use ($registeredStartDate, $registeredEndDate){
                return $query->whereBetween('users.created_at', [$registeredStartDate, $registeredEndDate]);
            })
            ->when($referralCode, function ($query) use ($referralCode){
                return $query->where('referral_code', $referralCode);
            })
            ->orderBy('id','desc')
            ->get();

        // Init Data Excel
        $filename = "[".date('ymd')."]"." User Data";

        Excel::create($filename, function ($excel) use ($usersDb, $startDateFilename, $endDateFilename){
            $excel->sheet('Sheet 1', function ($sheet) use ($usersDb, $startDateFilename, $endDateFilename){

                $sheet->cell('B1:C1:D1:B2:C2:D2', function($cell) {

                    $cell->setFontSize(26);
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('B2:D2');

                $header = $startDateFilename." - ".$endDateFilename;
                $sheet->row(1, ['', 'User Data List']);
                $sheet->row(2, ['', $header]);

                $row = 4;
                $no_urut = 1;

                $arr_title = ['No', 'Country', 'Member ID', 'Name', 'Email', 'Phone', 'Balance', 'Register Date',
                    'Last Transaction Date', 'Referral Code', 'Favourite Locker'];

                $sheet->row($row, $arr_title);

                foreach ($usersDb as $index => $item) {

                    $row++;

                    $lastTransaction = '';
                    if (sizeof($item->transactions) > 0) {
                        $lastTransaction = $item->transactions[sizeof($item->transactions) - 1]->created_at;
                    }

                    $favorite_locker = '';
                    if (!empty($item->favorite_locker)) {
                        $favorite_locker = $item->favorite_locker;
                    }

                    $arr = [
                        $no_urut,
                        $item->country,
                        $item->member_id,
                        $item->name,
                        $item->email,
                        $item->phone,
                        $item->balance,
                        $item->created_at,
                        $lastTransaction,
                        $item->referral_code,
                        $favorite_locker
                    ];

                    $sheet->row($row, $arr);
                    $no_urut++;
                }

            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            abort(404);
        }
    }
}
