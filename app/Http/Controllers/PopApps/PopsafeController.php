<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 12/09/18
 * Time: 14.02
 */

namespace App\Http\Controllers\PopApps;


use App\Http\Controllers\Controller;
use App\Http\Helpers\Helper;
use App\Http\Helpers\CustomeLocker;
use App\Models\PopApps\PopSafe;
use App\Models\PopApps\PopSafeHistory;
use App\Models\PopApps\Transaction;
use App\Models\PopApps\User;
use App\Models\PopApps\Company;
use DateTime;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use App\Models\NewLocker\Box;


class PopsafeController extends Controller {

    public function getLanding() {
        $user = Auth::user()->group->groupname;
        $name = Auth::user()->group->name;
        $groupUser = CustomeLocker::getGroupUser();

        $lockerLocation = '';
        
        if($name == "ap1_level01") {
            $lockerID = [
                '3404703e47ac8cb1ca645f87bee3c0b3',
                '92457d6a5e86c3c2c09e3b4898eee9c6'
            ];

            // get from box where delete flag 0
            $lockerLocation = Box::select('tb_newlocker_box.name as locker_name')
            ->leftJoin('tb_newlocker_machinestat','tb_newlocker_machinestat.locker_id','=','tb_newlocker_box.id')      
            ->whereIn('id', $lockerID)
            ->where('deleteFlag',0)
            ->get(); 
        } else {
            if ($user != 'PDSL' || $user != null) {
                $customeLocker = new CustomeLocker;
                $lockerLocation = $customeLocker->lockerLocations($user);
            }
        }
        
        return view('popapps.popsafe.index', compact('user', 'lockerLocation', 'groupUser'));
    }

    private function lockerLocations($userGroup)
    {
        $customeLocker = new CustomeLocker;
        $company = $customeLocker->getCompany($userGroup);
        $dataLocker = DB::connection('popsend')
                    ->table('transactions')
                    ->select('popsafes.locker_name')
                    ->join('popsafes', 'popsafes.id', '=', 'transactions.transaction_id_reference')
                    ->where('transactions.client_id_companies', $company)
                    ->groupBy('popsafes.locker_name')
                    ->get();

        return $dataLocker->toArray();
    }

    public function getAjaxLiveStatus(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $transaction_date = $request->input('transaction_date', null);
        $customeLocker = new CustomeLocker;
        $groupCompany = $customeLocker->getCompany($request->input('user_groupname', null));
        $country = $request->input('country', null);
        $name = Auth::user()->group->name;

        $startDate = null;
        $endDate = null;
        if (!empty($transaction_date)) {
            $startDate = Helper::formatDateRange($transaction_date, ',')->startDate;
            $endDate = Helper::formatDateRange($transaction_date, ',')->endDate;
        }

        $liveStatusDb = PopSafe::leftJoin('users', 'users.id', '=', 'popsafes.user_id')
            // ->leftJoin('transactions', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->when($transaction_date, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })
            // ->when($groupCompany, function ($query) use ($groupCompany){
            //     return $query->where('transactions.client_id_companies', $groupCompany);
            // })
            ->when($name == 'ap1_level01' && $country == null,function ($query) use ($country){
                $query->whereIn('popsafes.locker_name', ['DPS 01','DPS 02']);
            })
            // ->where('transactions.transaction_type', 'popsafe')
            ->select(DB::raw('sum(IF(popsafes.status = "CREATED", 1, 0)) AS "created",
                        sum(IF(popsafes.status = "EXPIRED", 1, 0)) AS "expired",
                        sum(IF(popsafes.status = "CANCEL", 1, 0)) AS "cancel",
                        sum(IF(popsafes.status = "IN STORE", 1, 0)) AS "in_store",
                        sum(IF(popsafes.status = "OVERDUE", 1, 0)) AS "overdue",
                        sum(IF(popsafes.status = "COMPLETE", 1, 0)) AS "complete"'
            ))
            ->first();

        $data = [];
        $data['created'] = $liveStatusDb->created;
        $data['expired'] = $liveStatusDb->expired;
        $data['cancel'] = $liveStatusDb->cancel;
        $data['in_store'] = $liveStatusDb->in_store;
        $data['overdue'] = $liveStatusDb->overdue;
        $data['complete'] = $liveStatusDb->complete;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getAllUser() {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $userGroup = Auth::user()->group->groupname;
        $groupUser = CustomeLocker::getGroupUser();
        if (in_array($userGroup, $groupUser)) {
            $allUserDb = $this->getAllUserCustome($userGroup);
        } else {
            $allUserDb = User::all();
        }

        $data = [];
        $data['allUser'] = $allUserDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getAllUserCustome($userGroup)
    {
        $customeLocker = new CustomeLocker;
        $company = $customeLocker->getCompany($userGroup);
        $getUser = Transaction::with('user')->where('client_id_companies', $company)->groupBy('user_id')->get();
        $user = [];
        foreach ($getUser->toArray() as $key => $value) {
            $user[] = $value['user'];
        }
        return $user;
    }

    public function getAjaxFilterPopsafe(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $name = Auth::user()->group->name;
        $customeLocker = new CustomeLocker;
        $country = $request->input('country', null);
        $status = $request->input('status', null);
        $user = $request->input('user', null);
        $transaction_date = $request->input('transaction_date', null);
        $promocode= $request->input('promocode', null);
        $invoiceId = $request->input('invoiceid', null);
        $usergroup = $request->input('user_groupname', null);
        $groupCompany = $customeLocker->getCompany($usergroup);
        $startDate = null;
        $endDate = null;
        if (!empty($transaction_date)) {
            $startDate = Helper::formatDateRange($transaction_date, ',')->startDate;
            $endDate = Helper::formatDateRange($transaction_date, ',')->endDate;
        }

        $liveStatusDb = DB::connection('popsend')
            ->table('popsafes')
            ->leftJoin('users', 'users.id', '=', 'popsafes.user_id')
            ->leftJoin('transactions', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('campaign_usages', 'campaign_usages.transaction_id', '=', 'transactions.id')
            ->leftJoin('campaigns', 'campaigns.id', '=', 'campaign_usages.campaign_id')
            ->leftJoin('vouchers', 'vouchers.id', '=', 'campaign_usages.voucher_id')
            ->when($country, function ($query) use ($country, $groupCompany) {
                if ($groupCompany != '001' && $groupCompany != null) {
                    return $query->where('popsafes.locker_name', $country);
                } 
                return $query->where('users.country', $country);
            })->when($name == 'ap1_level01' && $country == null,function ($query) use ($country){
                $query->whereIn('popsafes.locker_name', ['DPS 01','DPS 02']);
            })->when($status, function ($query) use ($status){
                return $query->where('popsafes.status', $status);
            })->when($user, function ($query) use ($user){
                return $query->where('users.id', $user);
            })->when($promocode, function ($query) use ($promocode){
                return $query->where('vouchers.code', $promocode);
            })->when($transaction_date, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })->when($invoiceId, function ($query) use ($invoiceId){
                return $query->where('popsafes.invoice_code', $invoiceId);
            })->when($groupCompany, function ($query) use ($groupCompany){
                return $query->where('transactions.client_id_companies', $groupCompany);
            })
            ->where('transactions.transaction_type', 'popsafe')
            ->select('users.country', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_number', 'popsafes.locker_size', 'users.member_id', 'users.name', 'users.phone','popsafes.locker_name', 'popsafes.created_at','transactions.promo_amount', 'vouchers.code', 'campaigns.name AS campaign_name', 'campaigns.id AS campaign_id', DB::raw('sum(transactions.total_price) as total_price'), DB::raw('sum(transactions.paid_amount) as paid_amount'))
            // (DB::raw('(select sum(transactions.total_price) from popsafes left join transactions on transactions.transaction_id_reference = popsafes.id where transactions.transaction_type = "popsafe" and popsafes.invoice_code = "'.$invoiceId.'") as total_price,'))
            //->select('(select sum(transactions.total_price) from popsafes left join transactions on transactions.transaction_id_reference = popsafes.id where transactions.transaction_type = "popsafe" and popsafes.invoice_code = "'.$invoiceId.'") as total_price')
            // ->select(DB::raw('(select sum(transactions.paid_amount) from popsafes left join transactions on transactions.transaction_id_reference = popsafes.id where transactions.transaction_type = "popsafe" and popsafes.invoice_code = "'.$invoiceId.'") as paid_amount'))
            // groupBy nya bikin salah pas counting complete
            ->groupBy('popsafes.invoice_code')
            ->get();


        $data = [];
        $data['param'] = $request->input();
        $data['resultList'] = $liveStatusDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getDetailPage(Request $request, $invoice_code) {

        $user = Auth::user()->group->groupname;
        // dd(Auth::user()->group->name);
        $groupUser = CustomeLocker::getGroupUser();
        $data = [];
        $data['invoice_code'] = $invoice_code;
        $data['user'] = $user;
        $data['groupUser'] = $groupUser;

        return view('popapps.popsafe.detail', $data);
    }

    public function getDetailPin(Request $request, $invoice_code) {
        $role = Auth::user()->group->name;
        if($role == 'superadmin') {
            // show pin
            $permission = true;
            $code_pin = PopSafe::getPinByInvoiceCode($invoice_code);
        } else {
            // hide pin
            $permission = false;
            $code_pin = null;
        }
        return response()->json([
            'code_pin' => $code_pin,
            'invoice_code' => $invoice_code,
            'permission' => $permission,
        ]);
    }

    public function getDetailPopsafe(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $popsafeCode = $request->input('invoice_id', null);

        $popsafeDb = PopSafe::where('invoice_code', $popsafeCode)
            ->leftJoin('users', 'users.id', '=', 'user_id')
            ->select('popsafes.*', 'users.name', 'users.phone', 'users.member_id')
            ->first();

        $data = [];
        $data['param'] = $request->input();
        $data['popsafe'] = $popsafeDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getDetailHistoriesPopsafe(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $popsafeCode = $request->input('popsafe_id', null);

        //        $popsafeDb = PopSafe::where('invoice_code', $popsafeCode)->first();
        $popsafeHistoriesDb = null;
        if (!empty($popsafeCode)) {
            $popsafeHistoriesDb = PopSafeHistory::where('popsafe_id', $popsafeCode)
                ->orderBy('created_at', 'desc')
                ->get();
        }

        $data = [];
        $data['param'] = $request->input();
        $data['popsafeHistories'] = $popsafeHistoriesDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getDetailTransactionsPopsafe(Request $request) {

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $popsafeCode = $request->input('popsafe_id', null);

//        $popsafeDb = PopSafe::where('invoice_code', $popsafeCode)->first();
        if (!empty($popsafeCode)) {
            $popsafeTransactionDb = Transaction::where('transaction_id_reference', $popsafeCode)
            ->where('transaction_type', 'popsafe')
            ->orderBy('created_at', 'desc')
            ->get();
        }

        $data = [];
        $data['param'] = $request->input();
        $data['popsafeTransactions'] = $popsafeTransactionDb;

        $response->isSuccess = true;
        $response->data = $data;
        return response()->json($response);
    }

    public function getAjaxDownloadPopsafe(Request $request) {

        set_time_limit(0);
        ini_set('memory_limit', '2048M');

        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $country = (!empty($request->input('country', null))) ? $request->input('country', null) : 'Indonesia';
        $status = $request->input('status', null);
        $user = $request->input('user', null);
        $userGroup = $request->input('userGroup', null);
        $groupUser = $request->input('groupUser', null);
        $transaction_date = $request->input('transaction_date', null);
        $promocode= $request->input('promocode', null);
        
        // GetCompany 
        $company = new CustomeLocker;
        $getCompany = null;
        if (in_array($userGroup, $groupUser)) {
            $getCompany = $company->getCompany($userGroup);
        }
        $name = Auth::user()->group->name;

        $startDate = null;
        $endDate = null;
        if (!empty($transaction_date)) {
            $startDate = Helper::formatDateRange($transaction_date, ',')->startDate;
            $endDate = Helper::formatDateRange($transaction_date, ',')->endDate;

            $startDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $startDate)->format('d M Y');
            $endDateFilename = DateTime::createFromFormat('Y-m-d H:i:s', $endDate)->format('d M Y');
        }

        $liveStatusDb = DB::connection('popsend')
            ->table('popsafes')
            ->leftJoin('users', 'users.id', '=', 'popsafes.user_id')
            ->leftJoin('transactions', 'transactions.transaction_id_reference', '=', 'popsafes.id')
            ->leftJoin('campaign_usages', 'campaign_usages.transaction_id', '=', 'transactions.id')
            ->leftJoin('campaigns', 'campaigns.id', '=', 'campaign_usages.campaign_id')
            ->leftJoin('vouchers', 'vouchers.id', '=', 'campaign_usages.voucher_id')
            ->when($country, function ($query) use ($country) {
                $co = null;
                if (strtolower($country) == 'indonesia') {
                    $co = 'ID';
                } else if (strtolower($country) == 'malaysia') {
                    $co = 'MY';
                }
                return $query->where('users.country', $co);
            })->when($name == 'ap1_level01',function ($query) use ($country){
                $query->whereIn('popsafes.locker_name', ['DPS 01','DPS 02']);
            })->when($status, function ($query) use ($status){
                return $query->where('popsafes.status', $status);
            })->when($user, function ($query) use ($user){
                return $query->where('users.id', $user);
            })->when($promocode, function ($query) use ($promocode){
                return $query->where('vouchers.code', $promocode);
            })->when($transaction_date, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('popsafes.created_at', [$startDate, $endDate]);
            })->when($getCompany, function ($query) use ($getCompany){
                return $query->where('client_id_companies', $getCompany);
            })
            ->where('transactions.transaction_type', 'popsafe')
            ->select('users.country', 'popsafes.invoice_code', 'popsafes.status', 'users.member_id', 'users.name', 'users.phone',
                'users.email', 'popsafes.locker_name', 'popsafes.created_at', 'transactions.promo_amount', 'vouchers.code', DB::raw('sum(transactions.total_price) as total_price'), DB::raw('sum(transactions.paid_amount) as paid_amount'))
            ->groupBy('popsafes.invoice_code')
            ->get();
        
        // Init Data Excel
        $filename = "[".date('ymd')."]"." PopSafe Data";

        Excel::create($filename, function ($excel) use ($liveStatusDb, $startDateFilename, $endDateFilename){
            $excel->sheet('Sheet 1', function ($sheet) use ($liveStatusDb, $startDateFilename, $endDateFilename){

                $sheet->cell('B1:C1:D1:B2:C2:D2', function($cell) {

                    $cell->setFontSize(26);
                    $cell->setFontWeight('bold');
                });

                $sheet->mergeCells('B1:D1');
                $sheet->mergeCells('B2:D2');

                $header = $startDateFilename." - ".$endDateFilename;
                $sheet->row(1, ['', 'PopSafe Transaction List']);
                $sheet->row(2, ['', $header]);

                $row = 4;
                $no_urut = 1;

                $arr_title = ['No', 'Country', 'Invoice ID', 'Status', 'User Name', 'User Phone', 'User Email',
                    'Locker', 'Transaction Date', 'Price', 'Promo Amount', 'Paid Amount', 'Promo Code'];

                $sheet->row($row, $arr_title);

                foreach ($liveStatusDb as $index => $item) {

//                    dd($item);

                    $row++;

                    $voucherCode = '';
                    if ($item->code != null) {
                        $voucherCode = $item->code;
                    }

                    $arr = [
                        $no_urut,
                        $item->country,
                        $item->invoice_code,
                        $item->status,
                        $item->name,
                        $item->phone,
                        $item->email,
                        $item->locker_name,
                        $item->created_at,
                        $item->total_price,
                        $item->promo_amount,
                        $item->paid_amount,
                        $voucherCode
                    ];

                    $sheet->row($row, $arr);
                    $no_urut++;
                }

            });
        })->store('xlsx', storage_path('/app'));

        // parse data to view
        $data = [];
        $data['link'] = $filename.".xlsx";


        $response->data = $data;

        $response->isSuccess = true;
        return response()->json($response);
    }

    public function download(Request $request)
    {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            abort(404);
        }
    }

    private function getCompany($groupname)
    {
        $groupCompany = null;
        if (isset($groupname)) {
            $dataGroup = DB::connection('popsend')->table('companies')->where('code', $groupname)->first();
            $groupCompany = $dataGroup->client_id;
        }
        return $groupCompany;
    }

}
