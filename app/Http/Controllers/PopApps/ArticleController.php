<?php
/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 13/06/18
 * Time: 11.14
 */

namespace App\Http\Controllers\PopApps;

use App\Http\Controllers\Controller;
use App\Http\Controllers\NotificationController;
use App\Http\Helpers\APIv2;
use App\Models\PopApps\Article\Articles;
use App\Models\PopApps\Country;
use App\Models\PopApps\Services;
use App\Models\PopApps\Article\ArticleStatus;
use App\Models\PopApps\Article\ArticleTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller {

    public function getArticleData() {

        $dataa = new Articles();
        $result = $dataa->getArticle();

        $countries = new Country();
        $country = $countries->getCountry();

        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $param = [];
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
        $param['dateRange'] = "$startDate - $endDate";

        $data = [];
        $data['data'] = $result;
        $data['countryList'] = $country;
        $data['parameter'] = $param;

        return $data;
    }

    public function getListArticle() {

        $data = $this->getArticleData();

        return view('popapps.article.list',$data);
    }

    public function createNewArticle() {

        $services = new Services();
        $service = $services->getServices();

        $types = new ArticleTypes();
        $type = $types->getTypes();

        $statuss = new ArticleStatus();
        $status = $statuss->getStatus();

        $countries = new Country();
        $country = $countries->getCountry();

        $dateRange = $this->getDateRangeMonth();

        $param = [];
        $param['beginDate'] = $dateRange->start_date;
        $param['endDate'] = $dateRange->end_date;
        $param['dateRange'] = "$dateRange->start_date - $dateRange->end_date";

        $data = [];
        $data['country'] = $country;
        $data['status'] = $status;
        $data['type'] = $type;
        $data['service'] = $service;
        $data['parameter'] = $param;

        return view('popapps.article.create', $data);
    }

    public function saveNewArticle(Request $request) {

        $articles = new Articles();
        $response = $articles->saveData($request, null);

        if ($response->isSuccess = false) {
            return back();
        }

        $data = $this->getArticleData();

        return redirect('popapps/article/list');
    }

    public function editArticle(Request $request, $articleId) {

        $services = new Services();
        $service = $services->getServices();

        $types = new ArticleTypes();
        $type = $types->getTypes();

        $statuss = new ArticleStatus();
        $status = $statuss->getStatus();

        $countries = new Country();
        $country = $countries->getCountry();

        $articles = new Articles();
        $article = $articles->getArticleById($articleId);

        $startDate = $article->article_detail->start_date;;
        $endDate = $article->article_detail->end_date;;

        $param = [];
        $param['beginDate'] = $startDate;
        $param['endDate'] = $endDate;
//        $param['dateRange'] = $startDate - $endDate;

        $data = [];
        $data['article'] = $article;
        $data['country'] = $country;
        $data['status'] = $status;
        $data['type'] = $type;
        $data['service'] = $service;
        $data['parameter'] = $param;

        return view('popapps.article.edit', $data);

    }

    public function editArticleById(Request $request, $id) {

        $fileImageLockerPath = null;
        $fileImageWebPath = null;
        $fileImageMobilePath = null;

        if (!empty($request->image_locker)) {
            $fileImageLockerPath = $request->image_locker->storeAs('images', time(). '-' .$request->input('title'). '-LOCKER-IMAGE'.'.jpg', 'store_images_article');
        }

        if (!empty($request->image_web)) {
            $fileImageWebPath = $request->image_web->storeAs('images', time(). '-' .$request->input('title'). '-WEB-IMAGE'.'.jpg', 'store_images_article');
        }

        if (!empty($request->image_mobile)) {
            $fileImageMobilePath = $request->image_mobile->storeAs('images', time(). '-' .$request->input('title'). '-MOBILE-IMAGE'.'.jpg', 'store_images_article');
        }

        $updateData = [
            'notification_title' => $request->input('notif_title'),
            'notification_subtitle' => $request->input('notif_subtitle'),
            'title' => $request->input('title'),
            'subtitle' => $request->input('subtitle'),
            'content' => $request->input('content'),
            'other_desc' => $request->input('other_desc'),
            'article_service_id' => $request->input('service'),
            'article_type_id' => $request->input('type'),
            'article_status_id' => $request->input('status'),
            'article_country_id' => $request->input('country'),
        ];

        if(!is_null($fileImageLockerPath)) {
            $updateData['image_locker'] = $fileImageLockerPath;
        }

        if(!is_null($fileImageWebPath)) {
            $updateData['image_web'] = $fileImageWebPath;
        }

        if(!is_null($fileImageMobilePath)) {
            $updateData['image_mobile'] = $fileImageMobilePath;
        }

        Articles::whereId($id)->update(
            $updateData
        );

        return redirect('popapps/article/list');
    }

    public function changeStatusArticle(Request $request, $articleId, $status) {

        $statusToSend = null;
        if (!empty($status)) {
            $statusToSend = $status;
        }

        $articles = new Articles();
        $response = $articles->updateStatusArticle($articleId, $status);

        if ($response->isSuccess = false) {
            return back();
        }

        return redirect('popapps/article/list');
    }

    public function pushArticle(Request $request, $articleId) {

        $articleQuery = new Articles();
        $article = $articleQuery->getArticleById($articleId);

        $push = new NotificationController();
        $pushResponse = $push->sendPushArticle($article);

        return redirect('popapps/article/list');
    }

    private function getDateRangeMonth() {

        // get all transaction this month *Default
        $startDate = date('Y-m-d',strtotime("-30 days"));
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

//        $param = [];
//        foreach ($input as $key => $item) {
//            $param[$key] = $item;
//        }
//        $param['beginDate'] = $startDate;
//        $param['endDate'] = $endDate;
//        $param['dateRange'] = "$startDate - $endDate";

        $dateRange = new \stdClass();
        $dateRange->start_date = $startDate;
        $dateRange->end_date = $endDate;

        return $dateRange;
    }

    public function pushNotification() {

        return view('popapps.article.notification');
    }
}