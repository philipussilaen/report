<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 08/11/2017
 * Time: 14.20
 */

namespace App\Http\Helpers;


use Illuminate\Support\Facades\DB;

class ApiSepulsa
{
    protected $headers = array (
        'Content-Type: application/json'
    );
    protected $is_post = 0;

    public function get_sepulsa($url,$param = array()){
        $username=env('SEPULSA_USER');
        $password=env('SEPULSA_PASSWORD');
        $query = http_build_query($param);

        $url = "$url?$query";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt ( $curl, CURLOPT_COOKIEJAR, "" );
        curl_setopt ( $curl, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt ( $curl, CURLOPT_ENCODING, "" );
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $curl, CURLOPT_AUTOREFERER, true );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
        curl_setopt ( $curl, CURLOPT_CONNECTTIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_TIMEOUT, 5 );
        curl_setopt ( $curl, CURLOPT_MAXREDIRS, 10 );
        curl_setopt ( $curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt ( $curl, CURLOPT_USERPWD, "$username:$password");
        $result=curl_exec ($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);   //get status code
        $info = curl_getinfo($curl);
        curl_close ($curl);

        $tmp = $result;
        $result = json_decode($result,true);
        if (empty($result)){
            $this->saveResponse($url,$param,$tmp);
        } else $this->saveResponse($url,$param,json_encode($result));
        return $result;
    }

    private function saveResponse($url,$param,$response){
        DB::table('companies_response')
            ->insert([
                'api_url' => $url,
                'api_send_data' => json_encode($param),
                'api_response'  => $response,
                'response_date'     => date("Y-m-d H:i:s")
            ]);
        return;
    }
}