<?php

namespace App\Console\Commands;

use App\Models\Module;
use App\Models\Privilege;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class RouteUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'routes:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Routes List';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get module from route
        $routeList = Route::getRoutes();
        $registeredRoutes = [];
        foreach ($routeList as $item) {
            if (!in_array($item->uri,$registeredRoutes)) $registeredRoutes[] = $item->uri;
        }
        $routeLists = [];
        foreach ($registeredRoutes as $route) {
            if (strpos($route, 'ajax') !== false) continue;
            if (strpos($route, 'Ajax') !== false) continue;
            elseif (strpos($route, '_debugbar') !== false) continue;

            if (strpos($route,'{') !== false){
                $tmpExplode = explode('/',$route);
                $tmpRoute = '';
                foreach ($tmpExplode as $index => $item){
                    if (strpos($item,'{') !== false) break;
                    if ($index > 0) $tmpRoute.='/';
                    $tmpRoute .= $item;
                }
                $route = $tmpRoute;
            }

            $tmp = new \stdClass();
            $tmp->id = null;
            $tmp->group = null;
            $tmp->name = $route;
            $tmp->description = null;
            $tmp->active = null;
            // set group
            $exploded = explode('/',$route);
            $group = $exploded[0];
            if (empty($group)) $group = 'landing';
            $tmp->group = $group;
            // check on DB
            $moduleDb = Module::where('name',$route)->first();
            if (!$moduleDb){
                echo "Add $group - $route";
                $moduleDb = new Module();
                $moduleDb->module_group = $group;
                $moduleDb->name = $route;
                $moduleDb->description = null;
                $moduleDb->active = 1;
                $moduleDb->save();
            }
            $tmp->id = $moduleDb->id;
            $tmp->name = $moduleDb->name;
            $tmp->description = $moduleDb->description;
            $tmp->status = $moduleDb->active;
            $routeLists[] = $route;
        }
        
        // get module not in registered route
        $moduleDb = Module::whereNotIn('name',$routeLists)->get();
        foreach ($moduleDb as $item){
            // delete the module and privilege
            $moduleId = $item->id;
            // delete model
            $deleteModule = Module::where('id',$moduleId)->delete();
            if($deleteModule) {
                echo "Delete $moduleId";
                $deletePrivilege = Privilege::where('module_id',$moduleId)->delete();
            }
        }
        
//         // Reset privilege that deleted by other job
//         $privileges = DB::connection('popbox_report')->select('select * from privileges where DATE_FORMAT(deleted_at, "%Y-%m-%d") = "'.date('Y-m-d').'"');
//         if(count($privileges) > 0){
//             DB::connection('popbox_report')->select('update privileges set deleted_at = NULL where DATE_FORMAT(deleted_at, "%Y-%m-%d") = "'.date('Y-m-d').'"');
//             $privileges = DB::connection('popbox_report')->select('select * from privileges where DATE_FORMAT(deleted_at, "%Y-%m-%d") = "'.date('Y-m-d').'"');
//         }
        
//         // update module_id on privilege 
//         $privileges = Privilege::whereNull('deleted_at')->get();
//         foreach ($privileges as $p){
//             $module = Module::withTrashed()->where('id', '=', $p->module_id)->first();
//             if($module['deleted_at']){
//                 $new_module = Module::whereNull('deleted_at')->where('name', '=', $module['name'])->first();
//                 $p->module_id = $new_module['id'];
//                 $p->update();
//             }
//         }
    }
}
