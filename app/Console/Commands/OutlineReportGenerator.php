<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\OutlineReport;
use Carbon\Carbon;

class OutlineReportGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'outline:report';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate data for Outline / Stock Purchase Summary dashboard report';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start_date = '2018-06-01';
        
        $report_date = OutlineReport::max('report_date');
        if(!empty($report_date)){
            $start_date = date('Y-m-d', strtotime($report_date . "+1 days"));
        }
        
        $d_date = new \DateTime($start_date);
        while ($d_date->format('Y-m-d') < date('Y-m-d')){
            
            $outline = OutlineReport::where('report_date', '=', $d_date->format('Y-m-d'))->first();
            if(empty($outline)){
                $outline = new OutlineReport();
                $outline->report_date = $d_date->format('Y-m-d');
                // Agent
                $result = self::process($d_date->format('Y-m-d'), 'agent');
                $outline->agent_data = json_encode($result);
                // Warung
                $result = self::process($d_date->format('Y-m-d'), 'warung');
                $outline->warung_data = json_encode($result);
                
            }
            $outline->save();
            $d_date->modify('+1 day');
        }
        
        // Looking duplicate entry by date
//         $rows = OutlineReport::where('report_date', '=', date('Y-m-d', strtotime(date('Y-m-d') . "-1 days")))->get();
//         if(count($rows) > 1){
//             OutlineReport::where('report_date', '=', date('Y-m-d', strtotime(date('Y-m-d') . "-1 days")))->where('id', '>', $rows[0]->id)->delete();
//             $next_val = $rows[1]->id;
//             DB::connection('popbox_agent')->statement( "ALTER TABLE outline_report AUTO_INCREMENT = $next_val" );
//         }
    }
    
    private function active_user($params){
        return "
                select count(distinct l.locker_id) as 'total_users'
                from
                    users u
                    join popbox_virtual.lockers l on u.locker_id = l.locker_id
                    join popbox_agent.lockers lk on lk.id = l.locker_id
                    join transactions t on t.users_id = u.id
                    join transaction_items ti on t.id = ti.transaction_id
                where
                    l.`deleted_at` is null
                    and lk.status_verification = 'verified'
                    and ti.type = 'popshop'
                    ".$params['where_user_type']."
                    ".$params['where_transaction_created_at']."
        ";
    }
    
    private function verified_user($params){
        return "
            select count(distinct l.locker_id) as 'total_users'
            from
                users u
                join popbox_virtual.lockers l on u.locker_id = l.locker_id
                join popbox_agent.lockers lk on lk.id = l.locker_id
            
            where
                l.`deleted_at` is null
                and lk.status_verification = 'verified'
                ".$params['where_user_type']."
        ";
    }
    
    private function process($date, $userType, $locker_id = ''){
        $result = [];
        $endDate = '';
        $date_format_key = 'd-M-Y';
        if(!empty($date)) {
            $c_start_date = Carbon::parse($date);
            $c_end_date = Carbon::parse($date);
        }
        $params = [
            'where_locker'                  => '',
            'where_transaction_created_at'  => '',
            'where_user_type'               => '',
        ];
        
        if(!empty($userType) && $userType != 'all'){
            $params['where_user_type'] = "and l.type = '".$userType."'";
        } else {
            $params['where_user_type'] = "and l.type in ('agent', 'warung')";
        }
        
        if(!empty($locker_id) && $locker_id != 'all'){
            $params['where_locker'] = "and l.locker_id = ".$locker_id;
        }
        
        if($c_start_date != null && $c_end_date != null){
            $length = $c_end_date->diffInDays($c_start_date);
        }
        
        $d_start_date = new \DateTime($c_start_date);
        
        for($i = 0; $i < $length+1; $i++){
            $endDate = new \DateTime($d_start_date->format('Y-m-d'));
            $endDate = $endDate->format('Y-m-d');
            
            $index = $d_start_date->format($date_format_key);
            $key = 'detail_item';
            
            $result[$index]['active_user'][$key] = 'Active Warung';
            $result[$index]['total_sales'][$key] = 'Total Sales';
            $result[$index]['percentage_active'][$key] = '% Active';
            $result[$index]['avg_sales_by_verified'][$key] = 'Average Sales/Total Verified Warung';
            $result[$index]['avg_sales_by_active'][$key] = 'Average Sales/Active Warung';
            
            $key = 'past_7_days';
            $past_7_days = new \DateTime($d_start_date->format('Y-m-d'));
            $past_7_days = $past_7_days->modify('-6 days')->format('Y-m-d');
            $past_7_days_result = self::list($result, $params, $past_7_days, $endDate, $index, $key);
            
            $key = 'past_14_days';
            $past_14_days = new \DateTime($d_start_date->format('Y-m-d'));
            $past_14_days = $past_14_days->modify('-13 days')->format('Y-m-d');
            $past_14_days_result = self::list($past_7_days_result, $params, $past_14_days, $endDate, $index, $key);
            
            $key = 'past_30_days';
            $past_30_days = new \DateTime($d_start_date->format('Y-m-d'));
            $past_30_days = $past_30_days->modify('-29 days')->format('Y-m-d');
            $result = self::list($past_14_days_result, $params, $past_30_days, $endDate, $index, $key);
            
            $d_start_date->modify('+1 day');
        }
        
        return $result;
    }
    
    private function list($result, $params, $startDate, $endDate, $index, $key){
        $params['where_transaction_created_at'] = "and t.created_at between '".$startDate." 00:00:00' and '".$endDate." 23:59:59'";
        $rows = DB::connection('popbox_agent')->select($this->query($params));
        foreach ($rows as $row){
            $row = (array)$row;
            $result[$index]['active_user'][$key] = $row['active_user'];
            $result[$index]['total_sales'][$key] = $row['total_sales'];
            $result[$index]['percentage_active'][$key] = $row['percentage_active'];
            $result[$index]['avg_sales_by_verified'][$key] = $row['avg_sales_by_verified'];
            $result[$index]['avg_sales_by_active'][$key] = $row['avg_sales_by_active'];
        }
        
        return $result;
    }
    
    private function query($params){
        
        return "
            select
    			active_user,
    			total_sales,
    			ROUND((active_user/verified_user)*100, 2) as 'percentage_active',
    			ROUND(total_sales/verified_user, 2) as 'avg_sales_by_verified',
    			ROUND(total_sales/active_user, 2) as 'avg_sales_by_active'
    		from (
    			select (
    				select
                        case when sum(total_transaction) is null then 0 else sum(total_transaction) end as 'total_transaction'
    				from (
        				select
                            distinct t.id as 'trx_count',
                            (case when t.total_price is null then 0 else t.total_price end) as 'total_transaction'
            
                        from
                            transactions t
                        left join transaction_items ti on t.id = ti.transaction_id
                        left join users u on t.users_id = u.id
                        left join popbox_virtual.lockers l on l.locker_id = u.locker_id
                        left join payments p on p.transactions_id = t.id
                        join popbox_agent.lockers lk on lk.id = l.locker_id
                        where 1=1
                              and t.deleted_at is null
                              and lk.status_verification = 'verified'
                              and p.payment_methods_id in (1, 2)
                              and ti.type = 'popshop'
                              and t.status in ('PAID', 'WAITING')
                              ".$params['where_user_type']."
                              ".$params['where_transaction_created_at']."
                   ) as total_transaction
                ) as 'total_sales',
                (
                	select count(distinct l.locker_id) as 'total_users'
                    from
                        users u
                        join popbox_virtual.lockers l on u.locker_id = l.locker_id
                        join popbox_agent.lockers lk on lk.id = l.locker_id
                        join transactions t on t.users_id = u.id
                        join transaction_items ti on t.id = ti.transaction_id
                    where
                        l.`deleted_at` is null
                        and lk.status_verification = 'verified'
                        and ti.type = 'popshop'
                        ".$params['where_user_type']."
                        ".$params['where_transaction_created_at']."
                ) as 'active_user',
                (
                	select count(distinct l.locker_id) as 'total_users'
                    from
                        users u
                        join popbox_virtual.lockers l on u.locker_id = l.locker_id
                        join popbox_agent.lockers lk on lk.id = l.locker_id
                            
                    where
                        l.`deleted_at` is null
                        and lk.status_verification = 'verified'
                        ".$params['where_user_type']."
                ) as 'verified_user'
    		) as temp
        ";
    }
}
