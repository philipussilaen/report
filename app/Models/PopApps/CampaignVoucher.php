<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class CampaignVoucher extends Model
{
    protected $connection = 'popsend';
    protected $table = 'vouchers';

    /**
     * Save Voucher
     * @param $campaignId
     * @param array $voucherArray
     */
    public static function saveVoucher($campaignId,$voucherArray=[]){
        foreach ($voucherArray as $item) {
            $dataDb = new self();
            $dataDb->campaign_id = $campaignId;
            $dataDb->code = $item;
            $dataDb->usage = 0;
            $dataDb->save();
        }

        return;
    }

}
