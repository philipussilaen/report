<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class PopSafeHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'popsafe_histories';

    /**
     * Insert PopSafe History
     * @param $popsafeId
     * @param $status
     * @param null $user
     * @param null $remarks
     * @return \stdClass
     */
    public function insertPopSafeHistory($popsafeId,$status,$user=null,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $historyDb = new self();
        $historyDb->popsafe_id = $popsafeId;
        $historyDb->status = $status;
        $historyDb->user = $user;
        $historyDb->remarks = $remarks;
        $historyDb->save();

        $response->isSuccess = true;
        return $response;
    }
}
