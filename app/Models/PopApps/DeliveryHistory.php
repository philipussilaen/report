<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class DeliveryHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'delivery_histories';
}
