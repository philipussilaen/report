<?php

namespace App\Models\PopApps;

use App\Http\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    protected $connection = 'popsend';
    protected $table = 'transactions';

    /**
     * Refund Transaction
     * @param $transactionId
     * @param string $remarks
     * @return \stdClass
     */
    public function refundTransaction($transactionId,$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get transaction
        $transactionDb = Transaction::with('items')
            ->find($transactionId);

        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        // get balance record debit
        $balanceRecordDb = BalanceRecord::where('transaction_id',$transactionId)->first();
        if (!$balanceRecordDb){
            $response->errorMsg = 'Balance Record Not Found';
            return $response;
        }
        $debitAmount = $balanceRecordDb->debit;

        $refundAmount = $debitAmount;
        // create refund transaction
        $userId = $transactionDb->user_id;
        $description = "Refund Transaction ".$transactionDb->description." by ".Auth::user()->name;

        // create refund Transaction
        $createRefundTransaction = $this->createRefundTransaction($userId,$description,$transactionDb->id,$refundAmount);
        if (!$createRefundTransaction->isSuccess){
            $response->errorMsg = 'Failed Create Refund Transaction';
            return $response;
        }
        $refundTransactionId = $createRefundTransaction->transactionId;

        // change transaction to refund
        $transactionDb->status = 'REFUND';
        $transactionDb->refund_amount = $refundAmount;
        $transactionDb->save();

        // change transaction item to refund
        $transactionItems = $transactionDb->items;
        foreach ($transactionItems as $item) {
            $itemDb = TransactionItem::find($item->id);
            $itemDb->status = 'REFUND';
            $itemDb->refund_amount = $itemDb->paid_amount;
            $itemDb->save();
        }

        // create transaction history
        $userName = "PopBox: ".Auth::user()->name;
        $transactionHistoryModel = new TransactionHistory();
        $insertHistory = $transactionHistoryModel->insertHistory($transactionId,$userName,$description,$transactionDb->total_price,$transactionDb->paid_amount,$transactionDb->promo_amount,$transactionDb->refund_amount,'REFUND',$remarks);

        // refund user balance
        $balanceModel = new BalanceRecord();
        $creditBalance = $balanceModel->creditDeposit($userId,$refundAmount,$refundTransactionId);
        if (!$creditBalance->isSuccess){
            $response->errorMsg = $creditBalance->errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Refund Delivery
     * @param $transactionId
     * @param $refundAmount
     * @param array $refundList
     * @param $remarks
     * @return \stdClass
     */
    public function refundDelivery($transactionId,$refundAmount, $refundList=[],$remarks){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get transaction
        $transactionDb = Transaction::with('items')
            ->find($transactionId);

        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        $isAllRefunded = false;
        $transactionItems = $transactionDb->items;
        $refundAmount = 0;

        $tmpItemRefund = 0;
        $descriptionRefund = "";
        foreach ($refundList as $item){
            $transactionItemId = $item->transactionItemId;
            $itemRefundAmount = $item->refundAmount;
            $isRefund = $item->isRefund;
            $subInvoice = $item->subInvoice;

            if ($isRefund){
                $tmpItemRefund++;

                $itemDb = TransactionItem::find($transactionItemId);
                $itemDb->status = 'REFUND';
                $itemDb->refund_amount = $itemRefundAmount;
                $itemDb->save();
                $refundAmount += $itemRefundAmount;
                $descriptionRefund .= "Sub Invoice $subInvoice ";
            }
        }
        if (count($transactionItems) == $tmpItemRefund) {
            $isAllRefunded = true;
            $descriptionRefund = $transactionDb->description;
        }

        if (empty($refundAmount)){
            $response->errorMsg = 'Refund Amount 0 ';
            return $response;
        }

        // create refund transaction
        $userId = $transactionDb->user_id;
        $description = "Refund Transaction ".$descriptionRefund." by ".Auth::user()->name;

        // create refund Transaction
        $createRefundTransaction = $this->createRefundTransaction($userId,$description,$transactionDb->id,$refundAmount);
        if (!$createRefundTransaction->isSuccess){
            $response->errorMsg = 'Failed Create Refund Transaction';
            return $response;
        }
        $refundTransactionId = $createRefundTransaction->transactionId;

        // change transaction to refund
        if ($isAllRefunded) $transactionDb->status = 'REFUND';
        $transactionDb->refund_amount = $refundAmount;
        $transactionDb->save();

        // create transaction history
        $userName = "PopBox: ".Auth::user()->name;
        $transactionHistoryModel = new TransactionHistory();
        $insertHistory = $transactionHistoryModel->insertHistory($transactionId,$userName,$description,$transactionDb->total_price,$transactionDb->paid_amount,$transactionDb->promo_amount,$transactionDb->refund_amount,'REFUND',$remarks);

        // refund user balance
        $balanceModel = new BalanceRecord();
        $creditBalance = $balanceModel->creditDeposit($userId,$refundAmount,$refundTransactionId);
        if (!$creditBalance->isSuccess){
            $response->errorMsg = $creditBalance->errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Credit Transaction
     * @param $type
     * @param $userId
     * @param $remarks
     * @param $amount
     * @return \stdClass
     */
    public function createTransaction($type, $userId, $remarks="", $amount){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionRef = null;

        // create invoice ID
        $invoiceId = 'TRX-' . $userId . date('ymdhi') . Helper::generateRandomString(3);

        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_type = $type;
        $transactionDb->description = "$remarks Action from : " . Auth::user()->name;
        // create total price
        $transactionDb->total_price = $amount;
        $transactionDb->paid_amount = $amount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;

        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->item_reference = null;
        $itemsDb->description = "$remarks Action from : " . Auth::user()->name;
        $itemsDb->price = $amount;
        $itemsDb->paid_amount = $amount;
        $itemsDb->status = 'PAID';
        $itemsDb->remarks = $remarks;
        $itemsDb->save();

        // insert history
        $history = new TransactionHistory();
        $userName = "System : ".Auth::user()->name;
        $description = "$remarks Action from : ".Auth::user()->name;
        $insertHistory = $history->insertHistory($transactionId,$userName,$description,$amount,$amount,0,0,'PAID',$remarks);

        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $invoiceId;
        return $response;
    }

    /**
     * Create PopSafe Transaction
     * @param $userId
     * @param $popSafeId
     * @param int $promoAmount
     * @param null $remarks
     * @return \stdClass
     */
    public function createPopsafeTransaction ($userId,$popSafeId,$promoAmount=0,$remarks = null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionInvoice = null;
        $response->transactionAmount = null;

        // get user DB
        $userDb = User::find($userId);
        if (!$userId){
            $response->errorMsg = "User Not Valid";
            return $response;
        }

        $popSafeDb = Popsafe::find($popSafeId);
        if (!$popSafeDb){
            $response->errorMsg = 'Invalid PopSafe Data';
            return $response;
        }
        $popSafeCode = $popSafeDb->invoice_code;
        $lockerName = $popSafeDb->locker_name;

        $totalPrice = $popSafeDb->total_price;
        $paidAmount = $totalPrice - $promoAmount;

        if (empty($remarks)) $remarks = "PopSafe $popSafeCode @ $lockerName";

        // generate invoice id
        $invoiceId = "TRX-".$userId.date('ymdh').Helper::generateRandomString(4);

        // save to table Transaction
        // save to table Transaction
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->admin_id = Auth::user()->id;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_id_reference = $popSafeId;
        $transactionDb->transaction_type = 'popsafe';
        $transactionDb->description = $remarks;
        $transactionDb->total_price = $totalPrice;
        $transactionDb->paid_amount = $paidAmount;
        $transactionDb->promo_amount = $promoAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // create items transaction
        $items = [];
        $tmpItem = new \stdClass();
        $tmpItem->itemReference = $popSafeId;
        $tmpItem->description = $remarks;
        $tmpItem->price = $totalPrice;
        $tmpItem->paidAmount = $paidAmount;
        $tmpItem->promoAmount = $promoAmount;
        $tmpItem->status = 'PAID';
        $tmpItem->remarks = $remarks;

        $items[] = $tmpItem;

        // initiate model item
        $transactionItemModel = new TransactionItem();
        // save to items
        $itemsDb = $transactionItemModel->insertTransactionItems($transactionDb->id,$items);

        if (!$itemsDb->isSuccess){
            $response->errorMsg = $itemsDb->errorMsg;
            return $response;
        }

        // insert transaction history
        $historyModel = new TransactionHistory();
        $transactionId = $transactionDb->id;
        $userName = $userDb->name;
        $description = $remarks;
        $insertHistory = $historyModel->insertHistory($transactionId,$userName,$description,$totalPrice,$paidAmount,$promoAmount,0,'CREATED',$remarks);

        $response->transactionId = $transactionDb->id;
        $response->transactionInvoice = $invoiceId;
        $response->transactionAmount = $totalPrice;
        $response->isSuccess = true;
        return $response;
    }

    /*=========================Private Function=========================*/

    /**
     * Create Refund Transaction
     * @param $userId
     * @param $description
     * @param $previousTransactionId
     * @param $refundAmount
     * @return \stdClass
     */
    private function createRefundTransaction($userId,$description,$previousTransactionId,$refundAmount){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        // create transaction Reference
        $reference = "REF-".$userId.date('ymdh').Helper::generateRandomString(4);

        // save to Database
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $reference;
        $transactionDb->transaction_id_reference = $previousTransactionId;
        $transactionDb->transaction_type = 'refund';
        $transactionDb->description = $description;
        $transactionDb->total_price = $refundAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->paid_amount = $refundAmount;
        $transactionDb->save();
        
        // get refunded transactionDb
        $refundedTransactionDb = self::with('items')->find($previousTransactionId);

        // create item transaction
        foreach ($refundedTransactionDb->items as $item) {
            // insert transaction item
            $transactionItemDb = new TransactionItem();
            $transactionItemDb->transaction_id = $transactionDb->id;
            $transactionItemDb->item_reference = $item->id;
            $transactionItemDb->description = "Refund Transaction ".$item->description;
            $transactionItemDb->price = 0;
            $transactionItemDb->status = 'PAID';
            $transactionItemDb->remarks = $description;
            $transactionItemDb->save();
        }

        $response->isSuccess = true;
        $response->transactionId = $transactionDb->id;
        return $response;
    }

    /*Relationship*/
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function items(){
        return $this->hasMany(TransactionItem::class);
    }

    public function topup(){
        return $this->hasOne(Payment::class,'id','transaction_id_reference');
    }

    public function popsafe(){
        return $this->hasOne(PopSafe::class,'id','transaction_id_reference');
    }

    public function payment(){
        return $this->hasOne(PaymentParent::class,'transactions_id','id');
    }

    public function company(){
        return $this->hasOne(Company::class,'client_id','client_id_companies');
    }

    public function delivery(){
        return $this->hasOne(Delivery::class,'id','transaction_id_reference');
    }

    public function histories(){
        return $this->hasMany(TransactionHistory::class);
    }

    public function campaign(){
        return $this->hasOne(CampaignUsage::class);
    }
}
