<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class CampaignParameter extends Model
{
    protected $connection = 'popsend';
    protected $table = 'campaign_parameters';
    protected static $singleValue = array('<','<=','=','>=','>');
    protected static $doubleValue = array('between');
    protected static $multipleValue = array('exist','except');

    /**
     * Save Parameter
     * @param $campaignId
     * @param array $availableParamId
     * @param array $operator
     * @param array $value
     * @return \stdClass
     */
    public static function saveParameter($campaignId,$availableParamId=[],$operator=[],$value=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // sof deletes where campaign ID
        $deletedRows = self::where('campaign_id',$campaignId)->delete();
        foreach ($availableParamId as $paramIndex => $paramValue){
            $paramId = $paramValue;
            $operatorSign = $operator[$paramIndex];
            $valueData = $value[$paramIndex];

            $insertValue = $valueData[0];
            if (in_array($operatorSign,self::$singleValue)){
                $insertValue = $valueData[0];
            } else{
                $insertValue = json_encode($valueData);
            }

            // insert
            $paramDb = new self();
            $paramDb->campaign_id = $campaignId;
            $paramDb->available_parameter_id = $paramId;
            $paramDb->operator = $operatorSign;
            $paramDb->value = $insertValue;
            $paramDb->save();
        }

        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function parameter(){
        return $this->belongsTo(AvailableParameter::class,'available_parameter_id','id');
    }
}
