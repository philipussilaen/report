<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class Choise extends Model
{
    protected $connection = 'popsend';
	protected $fillable = ['question_id', 'text'];

    /*=================Relationship=================*/

	public function questions(){
        return $this->belongsTo(FreeGiftQuestion::class,'question_id');
    }
	

}
