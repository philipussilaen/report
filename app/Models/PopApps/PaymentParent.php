<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class PaymentParent extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payments';
}
