<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class DeliveryDestinationHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'delivery_destination_histories';


}
