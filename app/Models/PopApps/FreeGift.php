<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class FreeGift extends Model
{
    protected $connection = 'popsend';
    protected $table = 'free_gifts';

    /*=================Relationship=================*/
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo(FreeGiftCategory::class,'merchandise_code','category_code');
    }
    
    public function questions() { 
        return $this->belongsToMany(FreeGiftQuestion::class, 'free_gift_answers', 'free_gift_id', 'question_id');
    }
    

}
