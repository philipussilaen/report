<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class FreeGiftQuestion extends Model
{
    protected $connection = 'popsend';
    protected $table = 'questions';

	protected $fillable = ['title', 'category_code', 'icon', 'avaliable_country'];
	
	public function freeGiftCategories() {
		return $this->belongsTo(FreeGiftCategory::class, 'category_id');
	}
	
	public function choise(){
        return $this->hasMany(Choise::class, 'question_id');
    }
}
