<?php

namespace App\Models\PopApps;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payment_transactions';
}
