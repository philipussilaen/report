<?php
namespace App\Models\PopApps\Article;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: anggasetiawan
 * Date: 15/06/18
 * Time: 22.15
 */
class ArticleTypes extends Model {

    protected $connection = 'popsend';
    protected $table = 'article_types';

    public function getTypes() {

        $types = self::all();

        return $types;
    }
}