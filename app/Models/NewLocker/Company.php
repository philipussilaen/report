<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    const UPDATED_AT = 'last_update';
    const CREATED_AT = 'last_update';

    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_company';
    protected $primaryKey = 'id_company';
    public $incrementing = false;
    public $timestamps = false;
}
