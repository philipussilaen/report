<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class PaymentChannel extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'payment_channels';

    /*Relationship*/

    /**
     * has Many Transaction
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientTransactions(){
        return $this->hasMany(ClientTransaction::class);
    }

    public function paymentVendor(){
        return $this->belongsTo(PaymentVendor::class,'payment_vendors_id','id');
    }

}
