<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class BNITransaction extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'bni_virtual_accounts';
}
