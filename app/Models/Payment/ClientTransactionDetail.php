<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class ClientTransactionDetail extends Model
{
    protected $connection = 'popbox_payment';
    protected $table = 'client_transaction_details';

    /*Relationship*/
    public function clientTransaction(){
        return $this->belongsTo(ClientTransaction::class,'client_transactions_id','id');
    }
}
