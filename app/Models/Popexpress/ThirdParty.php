<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThirdParty extends Model
{

    protected $connection = 'pop_express';
    protected $table = 'third_parties';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    use SoftDeletes;

}
