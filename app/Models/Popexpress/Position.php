<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'positions';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

}
