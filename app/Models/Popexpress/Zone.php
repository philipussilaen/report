<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'zones';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    use SoftDeletes;

    public function regularHandler()
    {
        return $this->belongsTo(ThirdParty::class, 'regular_handler');
    }

    public function oneDayHandler()
    {
        return $this->belongsTo(ThirdParty::class, 'one_day_handler');
    }

    public function internationalCode()
    {
        return $this->belongsTo(InternationalCode::class);
    }
}
