<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccountBranch extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'account_branch';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    use SoftDeletes;
}
