<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InternationalCode extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'international_codes';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    use SoftDeletes;
}
