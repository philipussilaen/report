<?php

namespace App\Models\Popexpress;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Origin extends Model
{
    protected $connection = 'pop_express';
    protected $table = 'origins';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    use SoftDeletes;
}
