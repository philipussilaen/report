<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'transaction_items';

    public static function insertTransactionItem($transactionId,$items=[],$userId){
        // generate default response
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $purchaseList = ['pulsa','popshop'];
        $paymentList = ['electricity','electricity_postpaid','pdam','bpjs_kesehatan','telkom_postpaid'];


        foreach ($items as $item) {
            // calculate schema
            $commissionId = null;
            $commissionAmount = null;

            $itemDb = new self();
            $itemDb->transaction_id = $transactionId;
            $itemDb->commission_schemas_id = $commissionId;
            $itemDb->commission_amount = $commissionAmount;
            $itemDb->name = $item->name;
            $itemDb->type = $item->type;
            $itemDb->picture = $item->picture;
            $itemDb->params = $item->params;
            $itemDb->price = $item->price;
            $itemDb->save();
        }
        $response->isSuccess = true;
        return $response;
    }

    /*Relationship*/
    public function transaction(){
        return $this->belongsTo(Transaction::class,'transaction_id','id');
    }
}
