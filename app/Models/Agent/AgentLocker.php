<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentLocker extends Model
{
    protected $connection = 'popbox_agent';
    protected $primaryKey = 'id';
    protected $table = 'lockers';
    public $incrementing = false;

    public function users(){
        return $this->hasMany(User::class,'locker_id','id');
    }

    public function vaOpen(){
        return $this->hasMany(LockerVAOpen::class,'lockers_id','id');
    }
}
