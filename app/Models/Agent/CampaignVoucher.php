<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignVoucher extends Model
{
    use SoftDeletes;
    protected $connection = 'popbox_agent';
    protected $table = 'campaign_vouchers';

    /**
     * Save Voucher
     * @param $campaignId
     * @param array $voucherArray
     */
    public static function saveVoucher($campaignId,$voucherArray=[]){
        foreach ($voucherArray as $item) {
            $dataDb = new self();
            $dataDb->campaign_id = $campaignId;
            $dataDb->code = $item;
            $dataDb->usage = 0;
            $dataDb->save();
        }

        return;
    }
}
