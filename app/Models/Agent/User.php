<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'users';
    use SoftDeletes;

    /*relationship*/
    public function transactions(){
        return $this->hasMany(Transaction::class,'users_id','id');
    }

    public function locker()
    {
        return $this->belongsTo(AgentLocker::class, 'locker_id', 'id');
    }
}
