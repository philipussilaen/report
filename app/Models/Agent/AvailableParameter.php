<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class AvailableParameter extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'available_parameters';
}
