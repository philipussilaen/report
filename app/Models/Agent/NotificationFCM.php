<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\PopApps\MessageService;

class NotificationFCM extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'notification_fcm';

    /**
     * Insert New
     * @param null $module
     * @param $to
     * @param string $device
     * @param string $title
     * @param string $body
     * @param array $data
     * @return \stdClass
     */
    public function insertNew($module=null,$to,$device='android',$title='',$body='',$data=[]){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        if (!is_array($to)){
            $to = [$to];
        }

        foreach ($to as $item) {
            // insert to DB
            $dataDb = new self();
            $dataDb->module = $module;
            $dataDb->to = $item;
            $dataDb->device = $device;
            $dataDb->title = $title;
            $dataDb->body = $body;
            $dataDb->data = json_encode($data);
            $dataDb->status = 'created';
            $dataDb->save();
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Create Transaction Notification
     * @param $transactionId
     * @param array $fcmToken
     * @param $title
     * @param string $status
     * @param $module
     * @param $remarks
     * @return \stdClass
     */
    public static function createTransactionNotification($transactionId,$fcmToken=[],$title='',$status='process',$module='',$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $transactionDb = Transaction::find($transactionId);
        $transactionItem = $transactionDb->items;
        $description='';
        $description.="<b>No. Trx</b>: $transactionDb->reference <br>";
        $transactionStatus = $transactionDb->status;
        if ($transactionDb->status == 'PAID') $transactionStatus = 'Terbayar';
        if ($transactionDb->status == 'REFUND') $transactionStatus = 'Refund';
        $description.="<b>Status</b>: $transactionStatus<br>";
        if (!empty($remarks)){
            $description .= "$remarks<br><br>";
        }
        foreach ($transactionItem as $item){
            if ($item->type == 'pulsa'){
                $param = json_decode($item->params);
                $phone = $param->phone;
                $operator = strtoupper($param->operator);
                $price = number_format($item->price);
                $description.="<b>No. Handphone</b>: $phone <br>";
                $description.="<b>Produk</b>: $item->name <br>";
                $description.="<b>Operator</b>: $operator <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'electricity' || $item->type == 'electricity_postpaid'){
                $param = json_decode($item->params);
                $meterNumber = $param->meter_number;
                $productName = $item->name;
                $price = number_format($item->price);
                $description.="<b>No. Pelanggan</b>: $meterNumber <br>";
                $description.="<b>Produk</b>: $productName <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'admin_fee'){
                $price = number_format($item->price);
                $description.="<br><br>";
                $description.="<b>Produk</b>: Biaya Administrasi <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'bpjs_kesehatan'){
                $param = json_decode($item->params);
                $bpjsNumber = $param->bpjs_number;
                $productName = 'BPJS Kesehatan';
                $price = number_format($item->price);
                $description.="<b>No. BPJS</b>: $bpjsNumber <br>";
                $description.="<b>Produk</b>: $productName <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'pdam'){
                $param = json_decode($item->params);
                $meterNumber = $param->pdam_number;
                $operator = strtoupper($param->operator_code);
                $operator = str_replace('_',' ',$operator);
                $price = number_format($item->price);
                $description.="<b>No. Pelanggan</b>: $meterNumber <br>";
                $description.="<b>Produk</b>: PDAM <br>";
                $description.="<b>Operator</b>: $operator <br>";
                $description.="<b>Harga</b>: Rp$price";
            } elseif ($item->type == 'telkom_postpaid'){
                $param = json_decode($item->params);
                $telkomNumber = $param->telkom_number;
                $productName = 'Telkom';
                $price = number_format($item->price);
                $description.="<b>No. Telkom</b>: $telkomNumber <br>";
                $description.="<b>Produk</b>: $productName <br>";
                $description.="<b>Harga</b>: Rp$price";
            }
        }

        $to = $fcmToken;
        $device='android';
        $body=$transactionDb->description;

        $imageUrl = '';
        if ($status == 'process') $imageUrl = url('img/transaction/trx-onprocess.png');
        elseif ($status == 'success') $imageUrl = url('img/transaction/trx-success.png');
        elseif ($status == 'failed') $imageUrl = url('img/transaction/trx-failed.png');

        $fcmData = [];
        $fcmData['id'] = (int)"1".$transactionId;
        $fcmData['type'] = 'transaction';
        $fcmData['timestamp'] = date('Y-m-d H:i:s');
        $fcmData['description'] = $description;
        $fcmData['reference'] = $transactionDb->reference;
        $fcmData['img_url'] = $imageUrl;
        $fcmData['locker_id'] = $transactionDb->user->locker_id;

        // insert into fcm
        $notificationFCM =  new NotificationFCM();
        $insert = $notificationFCM->insertNew($module,$to,$device,$title,$body,$fcmData);

        $response->isSuccess = true;
        return $response;
    }

    public static function createMessageNotification($message_id, $module){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        
        $device = 'android';
        $row_id = 0;
        
        $message_service = DB::connection('popbox_agent')->select("
                select ms.title, ms.body_message, ms.url_loc, ms.menu_app, ms.url, ms.image_name, msd.recipient, u.gcm_token
                from message_service ms 
                join message_service_detail msd on ms.id = msd.message_service_id
                join users u on u.locker_id = msd.recipient 
                where ms.id = $message_id
        ");

        if(!empty($message_service)){
            foreach ($message_service as $row){
                if(!empty($row->gcm_token)){
                    
                    $row_id = $row_id + 1;
                    
                    $fcmData = [];
                    $fcmData['id'] = $row_id;
                    $fcmData['timestamp'] = date('Y-m-d H:i:s');
                    $fcmData['img_url'] = env('AGENT_URL').'upload/'.$row->image_name;
                    
                    $notificationFCM =  new NotificationFCM();
                    $notificationFCM->insertNew($module, $row->gcm_token, $device, $row->title, $row->body_message, $fcmData);
                }
            }
        }
        
        $response->count = $row_id;
        $response->isSuccess = true;
        return $response;
    }
}
