<?php

namespace App\Models\Agent;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $connection = 'popbox_agent';
    protected $table = 'payment_methods';
}
