<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'provinces';

    /*Relationship*/
    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'countries_id', 'id');
    }
}
