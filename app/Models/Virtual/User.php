<?php

namespace App\Models\Virtual;

use App\Models\Group;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'users';
}
