<?php

namespace App\Models\Virtual;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $connection = 'popbox_virtual';
    protected $table = 'cities';
    public $timestamps = false;

    /*Relationship*/
    public function province()
    {
        return $this->belongsTo(Province::class, 'provinces_id', 'id');
    }

    public function locker()
    {
        return $this->hasMany(Locker::class, 'cities_id', 'id');
    }
}
