<?php

namespace App\Models\Popshop;

use Illuminate\Database\Eloquent\Model;

class DimoProductCategoryName extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'dimo_product_category_name';
    protected $primaryKey = 'id_category';

    /*Relationship*/
    public function products(){
        return $this->belongsToMany(DimoProduct::class,'dimo_product_category','id_category','product_sku');
    }
}
