<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerActivitiesAll extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_activities_all';
}
