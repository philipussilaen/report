<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerLocation extends Model
{
    // set connection and table
    protected $connection = 'popbox_db';
    protected $table = 'locker_locations';
}
