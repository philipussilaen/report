<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class SepulsaProduct extends Model
{
    // set connection and table
    protected $connection = 'popbox_db';
    protected $table = 'tb_sepulsa_product';
}
