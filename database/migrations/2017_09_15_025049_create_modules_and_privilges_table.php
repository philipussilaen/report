<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesAndPrivilgesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('module_group');
            $table->string('name',255);
            $table->string('description',500);
            $table->integer('active')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('privileges',function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('group_id');
            $table->unsignedInteger('module_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('module_id')->references('id')->on('modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privileges');
        Schema::dropIfExists('modules');
    }
}
