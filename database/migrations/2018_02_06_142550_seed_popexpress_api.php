<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeedPopexpressApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = date("Y-m-d H:i:s");
        DB::statement("UPDATE users SET server_timestamp = '$now', updated_at = '$now';");
        DB::statement("UPDATE user_groups SET server_timestamp = '$now', updated_at = '$now';");
        DB::statement("UPDATE groups SET server_timestamp = '$now', updated_at = '$now';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
