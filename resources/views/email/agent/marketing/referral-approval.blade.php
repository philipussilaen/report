<!DOCTYPE html>
<html>
<head>
	<title>PopBox Agent Commission Approval </title>
</head>
<body>
	<p align="center">
		<h3>PopBox Agent</h3>
		<h3>Commission Schema Approval</h3>
	</p>
	<p>
		Date : {{ date('Y-m-d H:i:s') }}
	</p>
	<table style="border-collapse: collapse;">
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Referral Name</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ $referralDb->name }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Status</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if ($referralDb->status == 1)
					<b style="color: green">Need To Activate</b>
				@elseif ($referralDb->status == 0)
					<b style="color: red">Need To Disabled</b>
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Description</td>
			<td style="border: 1px solid black;padding: 10px;">
				{!! $referralDb->description !!}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Description Shared</td>
			<td style="border: 1px solid black;padding: 10px;">
				{!! $referralDb->description_to_shared !!}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Rule</td>
			<td style="border: 1px solid black;padding: 10px;">
				After <b>{{ $referralDb->type }}</b>
				@php
					if (!empty($referralDb->rule)) {
						$rule = json_decode($referralDb->rule);
					}
				@endphp
				@if (!empty($rule))
					@foreach ($rule as $key => $element)
						{{ $key }} {{ number_format($element) }}
					@endforeach
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Referral Code</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if (empty($referralDb->code))
					- (*using each code from member)
				@else
					{{ $referralDb->code }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Limit Usage</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if (empty($referralDb->limit_usage))
					-
				@else
					{{ $referralDb->limit_usage }}
				@endif
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Active Date</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ date('D, j F Y',strtotime($referralDb->start_date)) }} - {{ date('D, j F Y',strtotime($referralDb->end_date)) }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">From Amount</td>
			<td style="border: 1px solid black;padding: 10px;">
				@if (!empty($referralDb->from_locker_id))
					@php
						$agentLockerDb = \App\Models\Agent\AgentLocker::find($referralDb->from_locker_id);
					@endphp
					@if ($agentLockerDb)
						{{ $agentLockerDb->locker_name }} <br>
					@endif
				@endif
				Rp {{ number_format($referralDb->from_amount) }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">To Amount</td>
			<td style="border: 1px solid black;padding: 10px;">
				Rp {{ number_format($referralDb->to_amount) }}
			</td>
		</tr>
		<tr>
			<td style="font-weight: bold; border: 1px solid black;padding: 10px;">Expired Days</td>
			<td style="border: 1px solid black;padding: 10px;">
				{{ number_format($referralDb->expired) }} days after submitted
			</td>
		</tr>
	</table>
</body>
</html>