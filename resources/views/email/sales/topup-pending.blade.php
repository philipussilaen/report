<h3>Sales Agent Top Up : {{ $data['transactionRef'] }}</h3>
<p>
    <strong>Sales</strong> : {{ $data['salesName'] }}
</p>
<p>
    <strong>Amount</strong> : Rp {{ number_format($data['topupAmount']) }}
</p>
@if (!empty($data['bank']))
    <p>
        <strong>Description</strong> : {{ $data['bank'] }}
    </p>
@endif
