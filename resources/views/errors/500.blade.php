@extends('layout.main-error')
@section('css')
@stop

@section('page-header')
    Something Went Wrong
@stop

@section('content')
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-red">500</h2>
            <div class="error-content">
                <h3>
                    <i class="fa fa-warning text-red"></i>
                    Oops! Something Went Wrong
                </h3>
                <p>
                    Maaf, terjadi kesalahan sistem.
                </p>
            </div>
        </div>
    </section>
@stop

@section('js')
@stop