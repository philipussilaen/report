@extends('layout.main-error')
@section('css')
@stop

@section('page-header')
   Unauthorized
@stop

@section('content')
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-yellow">401</h2>
            <div class="error-content">
                <h3>
                    <i class="fa fa-warning text-yellow"></i>
                    Oops! Unauthorized Access
                </h3>
                <p>
                    Maaf, Anda tidak mempunyai hak untuk mengakses halaman ini.
                </p>
            </div>
        </div>
    </section>
@stop

@section('js')
@stop