@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Profile - Change Password
@endsection

@section('css')
	{{-- expr --}}
@endsection

@section('pageTitle')
	Change Password
@endsection

@section('pageDesc')
	
@endsection

@section('content')
	<div class="box box-solid">
		<div class="box-body">
			@if (session('success'))
		        <div class="alert alert-success">
		            {{ session('success') }}
		        </div>
		    @endif
		    @if (session('error'))
		        <div class="alert alert-danger">
		            {{ session('error') }}
		        </div>
		    @endif

		    @if ($errors->any())
		        <div class="alert alert-danger">
		            <ul>
		                @foreach ($errors->all() as $error)
		                    <li>{{ $error }}</li>
		                @endforeach
		            </ul>
		        </div>
		    @endif
			<form method="post">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Old Password</label>
					<input type="password" name="oldPassword" class="form-control" required>
				</div>
				<div class="form-group">
					<label>New Password</label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Re-type New Password</label>
					<input type="password" name="rePassword" class="form-control" required>
				</div>
				<button class="btn btn-flat btn-info">Submit</button>
			</form>
		</div>
	</div>
@endsection

@section('js')
	{{-- expr --}}
@endsection