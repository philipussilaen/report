@extends('layout.main')
@section('title')
    PopApps Finance
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
    PopApps Finance
@endsection

@section('pageDesc')
    Credit / Add Deposit
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Users</h3>
        </div>
        <div class="box-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="form-user">
                {{ csrf_field() }}
                <input type="hidden" name="type" value="credit">
                <div class="form-group">
                    <label>User</label>
                    <select class="form-control select2" name="memberId">
                        @foreach ($users as $user)
                            <option value="{{ $user->member_id }}">{{ $user->name }} ({{ $user->phone }}
                                )
                            </option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-info" type="button" id="btn-search">Find User</button>
            </form>
        </div>
    </div>

    <div id="view-user-detail">
    </div>
    <!-- Modal -->
    <div class="modal fade" id="creditConfirmation" tabindex="-1" role="dialog"
         aria-labelledby="creditConfirmationLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="creditConfirmationLabel">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-stripped">
                        <tr>
                            <td><strong>User Name</strong></td>
                            <td id="user-name"></td>
                        </tr>
                        <tr>
                            <td><strong>User Phone</strong></td>
                            <td id="user-phone"></td>
                        </tr>
                        <tr>
                            <td><strong>Tipe</strong></td>
                            <td id="trans-type"></td>
                        </tr>
                        <tr>
                            <td><strong>Amount</strong></td>
                            <td id="amount"></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>
    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function after() {
            $('#btn-balance').on('click', function (event) {
                var amount = $('input[name=amount]').val();
                var transType = $('select[name=type]').val();
                amount = numberWithCommas(amount);
                $('#amount').html(amount);
                $('#trans-type').html(transType);
                $('#creditConfirmation').modal('show');
            });
            $('#btn-submit').on('click', function (event) {
                $('#form-balance').submit();
            });
            $('select[name=paymentType]').on('change', function (event) {
                var selectedType = $(this).val();
                if (selectedType == 'cash') {
                    $('#bank-transfer').addClass('hide');
                } else if (selectedType == 'transfer') {
                    $('#bank-transfer').removeClass('hide');
                }
            });
            $('select[name=paymentType]').trigger('change');
            $('select[name=type]').on('change', function (event) {
                var selectedType = $(this).val();
                if (selectedType == 'topup') {
                    $('#payment-type').removeClass('hide');
                } else {
                    $('#payment-type').addClass('hide');
                }
            });
        }

        jQuery(document).ready(function ($) {
            // prevent form submit
            $('#form-user').on('submit', function (event) {
                event.preventDefault();
            });

            // ajax get agent detail
            $('#btn-search').on('click', function (event) {
                var formData = $('#form-user').serialize();
                $.ajax({
                    url: '{{ url('popapps/finance/ajaxUser') }}',
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    success: function (data) {
                        if (data.isSuccess == true) {
                            $('#view-user-detail').html(data.view);
                            $('#user-name').html(data.data.users.name);
                            $('#user-phone').html(data.data.users.phone);
                            after();
                        } else {
                            alert(data.errorMsg);
                        }
                    }
                })
                .done(function () {
                    console.log("success");
                })
                .always(function () {
                    console.log("complete");
                });
            });
        });
    </script>
@endsection