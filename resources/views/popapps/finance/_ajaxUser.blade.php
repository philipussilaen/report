<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">PopBox User</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-3 border-right">
                @php
                    $action = url('popapps/finance/credit');
                    if ($type == 'credit') {
                        $action = url('popapps/finance/credit');
                    } else {
                        $action = url('popapps/finance/debit');
                    }
                @endphp
                <form method="post" action="{{ $action }}" id="form-balance">
                    {{ csrf_field() }}
                    <input type="hidden" name="memberId" value="{{ $users->member_id }}">
                    <div class="form-group">
                        <label>Type</label>
                        <select class="form-control" name="type">
                            @foreach ($transactionType as $element)
                                <option value="{{ $element }}">{{ $element }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{ $type }} Amount</label>
                        <input type="text" name="amount" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea class="form-control" name="remark" rows="4" required></textarea>
                    </div>
                    <button class="btn btn-success" type="button" id="btn-balance">Submit</button>
                </form>
            </div>
            <div class="col-md-3 border-right">
                <div class="form-group">
                    <label>Users</label>
                    <p>
                        <strong>{{ $users->name }}</strong> <br>
                        {{ $users->phone }}
                    </p>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <p>{{ $users->email }}</p>
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <p>
                        {{ $users->country }}
                    </p>
                </div>
                <div class="form-group">
                    <label>Balance Deposit</label>
                    @php
                        $currency = 'Rp';
                        if ($users->country == 'MY') {
                            $currency = 'RM';
                        }
                    @endphp
                    <p>{{ $currency }} {{ number_format($users->balance) }}</p>
                </div>
            </div>
            <div class="col-md-6 table-responsive">
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th>Invoice ID</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Paid Price</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($transaction as $element)
                        <tr>
                            <td>{{ $element->invoice_id }}</td>
                            <td>{{ $element->transaction_type }}</td>
                            <td>
                                @if ($element->status == 'WAITING')
                                    <span class="label label-warning">Waiting Method</span>
                                @elseif ($element->status == 'UNPAID')
                                    <span class="label label-warning">Waiting Payment</span>
                                @elseif ($element->status=='PAID')
                                    <span class="label label-success">Paid</span>
                                @elseif ($element->status == 'EXPIRED')
                                    <span class="label bg-black text-white">Expired</span>
                                @elseif ($element->status == 'REFUND')
                                    <span class="label label-warning">ReFund</span>
                                @endif
                            </td>
                            <td>
                                {{ $element->description }}
                            </td>
                            <td>{{ number_format($element->total_price) }}</td>
                            <td>{{ number_format($element->paid_amount) }}</td>
                            <td>{{ date('j M Y H:i',strtotime($element->created_at)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>