@php
	$deliveryDb = $transaction->delivery;
	$transactionItems = $transaction->items;
@endphp
<div class="box box-widget widget-user-2">
	<div class="widget-user-header bg-red">
		<h3 class="widget-user-username">
			<strong>Delivery</strong>
			<br>{{ $deliveryDb->invoice_code }}
		</h3>
	</div>
	<div class="box box-body">
		<table class="table table-striped">
			<tr>
				<td>Status</td>
				<td>
					@if ($deliveryDb->status == 'PAID')
                        <span class="label label-success">PAID</span>
                    @elseif ($deliveryDb->status == 'CREATED')
                        <span class="label label-info">CREATED</span>
                    @else
                        <span class="label label-primary">{{ $deliveryDb->status }}</span>
                    @endif
				</td>
			</tr>
			<tr>
				<td>Pickup Type</td>
				<td>
					@if ($deliveryDb->pickup_type == 'locker')
						<span class="label label-info">Locker</span>
					@else
						<span class="label label-primary">{{ $deliveryDb->pickup_type }}</span>
					@endif
				</td>
			</tr>
			<tr>
				<td>Pickup Address</td>
				<td>
					@if ($deliveryDb->pickup_type == 'locker')
						{{ $deliveryDb->origin_name }} <br>
					@endif
					{{ $deliveryDb->origin_address }}
				</td>
			</tr>
			<tr>
				<td>Pickup Date</td>
				<td>
					{{ date('D, j F Y H:i:s',strtotime($deliveryDb->pickup_date)) }}
				</td>
			</tr>
			<tr>
				<td>Sender Name</td>
				<td>{{ $deliveryDb->name_sender }}</td>
			</tr>
			<tr>
				<td>Sender Phone</td>
				<td>{{ $deliveryDb->phone_sender }}</td>
			</tr>
			<tr>
				<td>Notes</td>
				<td>{{ $deliveryDb->notes }}</td>
			</tr>
			<tr>
				<td>Total Price</td>
				<td>
					<strong>{{ number_format($deliveryDb->total_price) }}</strong>
				</td>
			</tr>
            <tr>
                <td>Histories</td>
                <td>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Status</th>
                                <th>Remarks</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($deliveryDb->histories as $element)
                                <tr>
                                    <td>{{ $element->user }}</td>
                                    <td>
                                        <span class="label label-default">{{ $element->status }}</span>
                                    </td>
                                    <td>{{ $element->remarks }}</td>
                                    <td>{{ date('Y-m-d H:i:s',strtotime($element->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>

        <h4><strong>Destinations</strong></h4>
        @foreach ($transactionItems as $item)
        	@php
        		$destinationDb = $item->deliveryDestination;
        	@endphp
        	<table class="table table-bordered">
        		<tr>
        			<td>Sub Invoice</td>
        			<td><strong>{{ $destinationDb->invoice_sub_code }}</strong></td>
        		</tr>
        		<tr>
        			<td>Destination Type</td>
        			<td>
        				@if ($destinationDb->destination_type == 'locker')
							<span class="label label-info">Locker</span>
						@else
							<span class="label label-primary">{{ $destinationDb->destination_type }}</span>
						@endif
        			</td>
        		</tr>
	    		<tr>
					<td>Status</td>
					<td>
						@if ($destinationDb->status == 'PAID')
	                        <span class="label label-success">PAID</span>
	                    @elseif ($destinationDb->status == 'CREATED')
	                        <span class="label label-info">CREATED</span>
	                    @else
	                        <span class="label label-primary">{{ $destinationDb->status }}</span>
	                    @endif
					</td>
				</tr>
        		<tr>
        			<td>Destination Address</td>
        			<td>
        				@if ($destinationDb->destination_type == 'locker')
							{{ $destinationDb->destination_name }} <br>
						@endif
						{{ $destinationDb->destination_address }}
        			</td>
        		</tr>
        		<tr>
        			<td>Weight</td>
        			<td>
        				{{ number_format($destinationDb->weight) }}
        			</td>
        		</tr>
        		<tr>
        			<td>Recipient</td>
        			<td>
        				{{ $destinationDb->name_recipient }} <br>
        				{{ $destinationDb->phone_recipient }}
        			</td>
        		</tr>
        		<tr>
        			<td>Notes</td>
        			<td>{{ $destinationDb->notes }}</td>
        		</tr>
				@if (!empty($deliveryDb->item_photo))
				<tr>
					<td>Item Photo</td>
					<td colspan="2">
						<img src="{{ env('POPSEND_URL') }}/{{ $deliveryDb->item_photo }}" height="300px">
					</td>
				</tr>
				@endif
        		<tr>
        			<td>Express Pickup Number</td>
        			<td>
        				{{ $destinationDb->pickup_number }}
        				@if (empty($destinationDb->pickup_number) && $destinationDb->status == 'CREATED')
			        		<button class="btn btn-flat btn-warning btn-repush-delivery" data-id="{{ $destinationDb->id }}" data-invoice="{{ $destinationDb->invoice_sub_code }}">Re-Push</button>
			        	@endif
        			</td>
        		</tr>
        		<tr>
        			<td>Price</td>
        			<td>{{ number_format($destinationDb->price) }}</td>
        		</tr>
        		<tr>
        			<td>Paid Price</td>
        			<td>{{ number_format($item->paid_amount) }}</td>
        		</tr>
        		<tr>
        			<td>Promo Amount</td>
        			<td>{{ number_format($item->promo_amount) }}</td>
        		</tr>
                <tr>
                    <td>Histories</td>
                    <td>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Status</th>
                                    <th>Remarks</th>
                                    <th>Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($destinationDb->histories as $element)
                                    <tr>
                                        <td>{{ $element->user }}</td>
                                        <td>
                                            <span class="label label-info">{{ $element->status }}</span>
                                        </td>
                                        <td>{{ $element->remarks }}</td>
										{{--<td>{{ date('Y-m-d H:i:s',strtotime($element->created_at)) }}</td>--}}
										@if($element->express_date != null)
											<td>{{ date('D, d M Y H:i:s',strtotime($element->express_date)) }}</td>
											@else
											<td>{{ date('D, d M Y H:i:s',strtotime($element->created_at)) }}</td>
										@endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
        	</table> 
        	<hr>
        @endforeach
	</div>
</div>

@if(\App\Models\Privilege::hasAccess('popapps/transaction/rePushDelivery'))
    @if (empty($destinationDb->pickup_number) && $destinationDb->status == 'CREATED')
        <div class="modal fade" id="modalRepushDelivery" tabindex="-1" role="dialog" aria-labelledby="modalRepushLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalRepushLabel">Repush Item</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('popapps/transaction/rePushDelivery') }}" id="form-repush-delivery">
                            {{ csrf_field() }}
                            <input type="hidden" name="itemId">
                            <div class="form-group">
                            	<label>Sub Invoice</label>
	                            <input type="text" name="subInvoice" class="form-control" readonly="">
                            </div>
                            <div id="itemDetail">

                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-warning" id="btn-repush-delivery">Repush</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif