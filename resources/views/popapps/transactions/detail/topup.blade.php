<div class="box box-widget widget-user-2">
	<div class="widget-user-header bg-green">
		<h3 class="widget-user-username">
			<strong>TopUp</strong>
			<br>{{ $topup->reference }}
		</h3>
		<h4 class="widget-user-desc">{{ strtoupper($topup->method) }}</h4>
	</div>
	<div class="box box-body">
		<table class="table table-striped">
            <tr>
                <td>Reference</td>
                <td>{{ $topup->reference }}</td>
            </tr>
            <tr>
                <td>Method</td>
                <td>{{ $topup->method }}</td>
            </tr>
            <tr>
                <td>Amount</td>
                <td>{{ number_format($topup->amount) }}</td>
            </tr>
            <tr>
                <td>Paid Amount</td>
                <td>{{ number_format($topup->paid_amount) }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>
                    @if ($topup->status == 'PAID')
                        <span class="label label-success">PAID</span>
                    @elseif ($topup->status == 'CREATED')
                        <span class="label label-info">CREATED</span>
                    @else
                        <span class="label label-primary">{{ $topup->status }}</span>
                    @endif
                </td>
            </tr>
            @php
               $data = $topup->data_response;
               $data = json_encode($data);
               $vaNumber = null;
               if (isset($data->virtual_account_number)) {
                    $vaNumber = $data->virtual_account_number;
                    $paymentId = $data->payment_id;
                    $timeLimit = $data->datetime_expired;
               }
               $url = null;
               if (isset($data->url)) {
                   $url = $data->url;
               }
            @endphp
            @if (!empty($vaNumber))
                <tr>
                    <td>Virtual Account Number</td>
                    <td>{{ $vaNumber }}</td>
                </tr>
                <tr>
                    <td>Time Limit</td>
                    <td>{{ $timeLimit }}</td>
                </tr>
                <tr>
                    <td>Payment ID</td>
                    <td>{{ $paymentId }}</td>
                </tr>
            @elseif (!empty($url))
                <a href="{{ $url }}" target="_blank">{{ $url }}</a>
            @endif
        </table>
	</div>
</div>