@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
    PopBox - Create Article
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
    PopBox Article Create
@endsection

@section('pageDesc')
    Create an Article
@endsection

@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">Create new article</div>
        <div class="panel-body">
            <div class="container">
                <h2>New Article</h2>
                <p>Create your article here</p>
                <form method="post" action="{{ url('popapps/article/save') }}" id="form-article" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="notif_title">Notification Title: Title for Push Notification</label>
                        <input type="text" name="notif_title" class="form-control" id="notif_title">
                    </div>
                    <div class="form-group">
                        <label for="notif_subtitle">Notification Subtitle: Subtitle for Push Notification</label>
                        <input type="text" name="notif_subtitle" class="form-control" id="notif_subtitle">
                    </div>
                    <div class="form-group">
                        <label for="title">Title:</label>
                        <input type="text" name="title" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label for="subtitle">Subtitle:</label>
                        <input type="text" name="subtitle" class="form-control" id="subtitle">
                    </div>
                    <div class="form-group">
                        <label for="content">Content:</label>
                        <textarea class="form-control" name="content" rows="5" id="content"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="other_desc">Other Description:</label>
                        <textarea class="form-control" name="other_desc" rows="5" id="other_desc"></textarea>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="image_locker">Image Locker:</label>
                        <input class="imageOption" type="file" name="image_locker" class="form-control" id="image_locker">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="image_web">Image Web:</label>
                        <input class="imageOption" type="file" name="image_web" class="form-control" id="image_web">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="image_mobile">Image Mobile:</label>
                        <input class="imageOption" type="file" name="image_mobile" class="form-control" id="image_mobile">
                    </div>
                    <div class="form-group">
                        <label for="service">Service:</label>
                        <select class="form-control" name="service" id="service">
                            @foreach($service as $key => $item)
                                @php
                                    $serviceSelected = '';
                                    if (isset($service)) {
                                        if ($service == $item) {
                                            $serviceSelected = "selected='serviceSelected'";
                                        }
                                    }
                                @endphp
                                <option value="{{$item->id}}" {{$serviceSelected}}> {{strtoupper($item->name)}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="type">Type:</label>
                        <select class="form-control" name="type" id="type">
                            @foreach($type as $key => $item)
                                @php
                                    $typeSelected = '';
                                    if (isset($type)) {
                                        if ($type == $item) {
                                            $typeSelected = "selected='typeSelected'";
                                        }
                                    }
                                @endphp
                                <option value="{{$item->id}}" {{$typeSelected}}>
                                    {{strtoupper($item->name)}} ({{strtoupper($item->description)}})
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status:</label>
                        <select class="form-control" name="status" id="status">
                            @foreach($status as $key => $item)
                                @php
                                    $statusSelected = '';
                                    if (isset($status)) {
                                        if ($status == $item) {
                                            $statusSelected = "selected='statusSelected'";
                                        }
                                    }
                                @endphp
                                <option value="{{$item->id}}" {{$statusSelected}}> {{strtoupper($item->name)}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="country">Country:</label>
                        <select class="form-control" name="country" id="country">
                            @foreach($country as $key => $item)
                                @php
                                    $countrySelected = '';
                                    if (isset($country)) {
                                        if ($country == $item) {
                                            $countrySelected = "selected='countrySelected'";
                                        }
                                    }
                                @endphp
                                <option value="{{$item->id}}" {{$countrySelected}}> {{strtoupper($item->country_name)}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="start_date">Start Date - End Date:</label>
                        <input type="text" class="form-control" name="date_range"  id="date_range">
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <button type="submit" class="btn btn-block btn-info btn-flat" onclick="$('#form-article').submit()">Add Article</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>

    {{-- DateRange Picker --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#date_range').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>
@endsection




