@extends('layout.main')

@section('title')
	Privileges
@endsection

@section('css')
	<!-- DataTables -->
  	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('pageTitle')
	Group Management
@endsection

@section('pageDesc')
	Add / Edit Group
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session('success'))
	                    <div class="alert alert-success">
	                        {{ session('success') }}
	                    </div>
	                @endif
	                @if (session('error'))
	                    <div class="alert alert-danger">
	                        {{ session('error') }}
	                    </div>
	                @endif
					<table id="group-table" class="table table-borderd table-striped">
						<thead>
							<tr>
								<th>Group Name</th>
								<th>Users</th>
								{{-- <th>Action</th> --}}
							</tr>
						</thead>
						<tbody>
							@foreach ($groupData as $element)
								<tr>
									<td>{{ $element->name }}</td>
									<td>{{ count($element->users) }}</td>
									{{-- <td><button class="btn btn-flat">Edit</button></td> --}}
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<button class="btn btn-flat btn-info pull-right" id="btn-modal-create">Add Group</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-default">
    	<div class="modal-dialog">
            <div class="modal-content">
              	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  		<span aria-hidden="true">&times;</span>
                  	</button>
                	<h4 class="modal-title"><span id="modal-header">Create </span>Group</h4>
              	</div>
              	<div class="modal-body">
                	<form id="group-form" method="post">
                		{{ csrf_field() }}
                		<input type="hidden" name="groupId">
                		<div class="form-group">
                			<label>Name</label>
                			<input type="text" name="groupName" class="form-control">
                		</div>
                	</form>
              	</div>
              	<div class="modal-footer">
                	<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                	<button type="button" class="btn btn-primary" id="btn-form">Save changes</button>
              	</div>
            </div>
        </div>
    </div>
@endsection

@section('js')
	<!-- DataTables -->
	<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#group-table').DataTable();

			// Button click show create modal
			$('#btn-modal-create').on('click', function(event) {
				$('#modal-default').modal('show');
			});
			// Modal Click Save then submit form
			$('#btn-form').on('click', function(event) {
				$('#group-form').submit();
			});
		});
	</script>
@endsection