@extends('layout.main')

@section('title')
	Privileges
@endsection

@section('css')
	<!-- DataTables -->
  	<link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('pageTitle')
	Privileges Management
@endsection

@section('pageDesc')
	Change Privileges for Group
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session('success'))
	                    <div class="alert alert-success">
	                        {{ session('success') }}
	                    </div>
	                @endif
	                @if (session('error'))
	                    <div class="alert alert-danger">
	                        {{ session('error') }}
	                    </div>
	                @endif
					<form id="form-group" method="get">
						{{ csrf_field() }}
						<div class="form-group">
							<label>Group</label>
							<select class="form-control" name="groupId">
								@foreach ($groupData as $element)
									@php
										$selected = '';
										if (!empty($selectedGroup)) {
											$groupId = $selectedGroup->id;
											if ($element->id == $groupId) {
												$selected = 'selected';
											}
										}
									@endphp
									<option value="{{ $element->id }}" {{ $selected }}>{{ $element->name }}</option>
								@endforeach
							</select>
						</div>
						<button id="btn-group" class="btn btn-info btn-flat">Get Group</button>
					</form>
				</div>
			</div>
			@if (!empty($selectedGroup))
				<div class="box box-default">
					<div class="box-header">
	                    <h3 class="pull-left">Group : {{ $selectedGroup->name }}</h3>
	                    <button class="btn btn-success pull-right" id="btn-privilege">Change Privileges</button>
					</div>
					<div class="box-body">
						<form id="form-privilege" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="groupId" value="{{ $selectedGroup->id }}">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Module</th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($moduleList as $element)
										<tr>
											<td>
												<div class="checkbox">
													<label>
														@php
															$checked = '';
															if ($element->isChecked) $checked = 'checked';
														@endphp
														<input type="checkbox" name="modules[]" value="{{ $element->id }}" {{ $checked }}>
														<strong>{{ $element->module_name }}</strong>
													</label>
												</div>
											</td>
											<td>{{ $element->description }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</form>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('js')
	<!-- DataTables -->
	<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#btn-privilege').on('click', function() {
				$('#form-privilege').submit();
			});
		});
	</script>
@endsection