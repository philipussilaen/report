@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <style type="text/css">
        .border-right{
            border-right: 1px solid #f4f4f4
        }
    </style>
@stop

@section('title')
    Summary Transactions
@endsection

@section('pageTitle')
    Summary Transactions
@endsection

@section('pageDesc')
    
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control" id="dateRange" name="dateRange" placeholder="Tanggal Transaksi">
                                    </div>
                                </div>
                                {{-- <div class="col-md-2">
                                    <select class="form-control" id="type">
                                        <option value="count">Number Of</option>
                                        <option value="sum">Amount</option>
                                    </select>
                                </div> --}}
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-flat btn-block" id="btn-submit">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="nav-tabs-custom box-graph">
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_count_1" data-toggle="tab" id="daily" data-type="daily">Daily</a></li>
                        <li class="active"><a href="#tab_count_2" id="weekly" data-toggle="tab" data-type="weekly">Weekly</a></li>
                        <li><a href="#tab_count_3" data-toggle="tab" id="monthly" data-type="monthly">Monthly</a></li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane" id="tab_count_1">
                            <div class="chart">
                                <canvas id="transaction-daily" style="height: 180px; width: 1072px;"></canvas>
                                <br><br>
                                <canvas id="sum-daily" style="height: 180px; width: 1072px;"></canvas>
                            </div>                     
                        </div>

                        <div class="tab-pane active" id="tab_count_2">
                            <div class="chart">
                                <canvas id="transaction-weekly" style="height: 180px; width: 1072px;"></canvas>
                                <br><br>
                                <canvas id="sum-weekly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab_count_3">
                            <div class="chart">
                                <canvas id="transaction-monthly" style="height: 180px; width: 1072px;"></canvas>
                                <br><br>
                                <canvas id="sum-monthly" style="height: 180px; width: 1072px;"></canvas>
                            </div>     
                        </div>

                    </div>
                </div>

                    <div class="box-footer">
                        <div class="row">
                            <div class="col-md-12">

                                <div id="data-daily">
                                    <div class="box-header with-border">
                                        <div class="col-md-6">
                                            <h3 style="margin-left: -3px; margin-top: -3px">Summary Transactions by Periode ( Daily )</h3>
                                        </div>
                                    </div>
                                    <br>   
                                    <div id="data-transaction-daily" class="box-body">
                                        <table id="table-transaction-daily" class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Periode</th>
                                                    <th>Number Of Parcel</th>
                                                    <th>Number Of Transaction</th>
                                                    <th>BNI Yap</th>
                                                    <th>Emoney</th>
                                                    <th>Gopay</th>
                                                    <th>Tcash</th>
                                                    <th>Ottopay</th>
                                                    <th>Total Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Total:</th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                    <th class="sum"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        
                                    </div>                          
                                </div>

                                <div id="data-weekly">
                                    <div class="box-header with-border">
                                        <div class="col-md-6">
                                            <h3 style="margin-left: -3px; margin-top: -3px">Summary Transactions by Periode ( Weekly )</h3>
                                        </div>
                                    </div>
                                    <br>                             
                                    <table id="table-transaction-weekly" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Periode</th>
                                                <th>Number Of Parcel</th>
                                                <th>Number Of Transaction</th>
                                                <th>BNI Yap</th>
                                                <th>Emoney</th>
                                                <th>Gopay</th>
                                                <th>Tcash</th>
                                                <th>Ottopay</th>
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total:</th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                                <div id="data-monthly">
                                    <div class="box-header with-border">
                                        <div class="col-md-6">
                                            <h3 style="margin-left: -3px; margin-top: -3px">Summary Transactions by Periode ( Monthly )</h3>
                                        </div>
                                    </div>
                                    <br>                             
                                    <table id="table-transaction-monthly" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Periode</th>
                                                <th>Number Of Parcel</th>
                                                <th>Number Of Transaction</th>
                                                <th>BNI Yap</th>
                                                <th>Emoney</th>
                                                <th>Gopay</th>
                                                <th>Tcash</th>
                                                <th>Ottopay</th>
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Total:</th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                                <th class="sum"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>  
                    </div>

                </div>
            </div>
        </div>
    </section>
@stop

@section('js')

    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
    {{-- DataTables --}}
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    {{-- Animate Number --}}
    <script type="text/javascript" src="{{ asset('plugins/animate-number/jquery.animateNumber.min.js') }}"></script>


    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['status']))
            $('select[name=status]').val("{{ $parameter['status'] }}").trigger('change');
            @endif
            @if (!empty($parameter['type']))
            $('select[name="type[]"]').val({!! json_encode($parameter['type']) !!}).trigger('change');
            @endif
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            $('#data-monthly').hide();
            $('#data-daily').hide();
            $('#data-weekly').show();
            $('#table-transaction-weekly').DataTable().draw();
            
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });

            $('#daily').click(function () {
                $('#data-monthly').hide();
                $('#data-daily').show();
                $('#data-weekly').hide();
                $('#table-transaction-daily').DataTable().draw();
            });

            $('#weekly').click(function () {
                $('#data-monthly').hide();
                $('#data-daily').hide();
                $('#data-weekly').show();
                $('#table-transaction-weekly').DataTable().draw();
            });

            $('#monthly').click(function () {
                $('#data-monthly').show();
                $('#data-daily').hide();
                $('#data-weekly').hide();
                $('#table-transaction-monthly').DataTable().draw();
            });
        });
    </script>

    {{-- Chart --}}
    <script type="text/javascript">
        var dailyLabel = [];
        var dailyData = [];
        var dailySumData = [];

        var weeklyLabel = [];
        var weeklyData = [];
        var weeklySumData = [];

        var monthlyLabel = [];
        var monthlyData = [];
        var monthlySumData = [];

        var allAgent = 0;
        var activeAgent = 0;

        var type = 'count';
        var transactionChart;


        /**
         * Create Daily Chart
         * @return {[type]} [description]
         */
        function createDaily(){
            $('#transaction-daily').css({
                "height": '250px',
                "width": '1072px'
            });
            // console.log(dailyData, 'dailyData');    
            // var dailyDataEmoney = [5,44,60,375,500,493,437,400,443,342,1272,1368,1429,1690,1306,1193,1330,129,19,19,25,27,26,68,2227,2310,2458,2466];
            
            /*Daily Chart*/
            var transactionChartCanvas = $("#transaction-daily");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                options: {
                    title: {
                        display: true,
                        text: 'Transactions By Number'
                    }
                },
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "BNI Yap!",
                        data: dailyCountBni,
                        fill: false,
                        borderColor: '#4f9afd'
                    }, {
                        label: "EMONEY",
                        data: dailyCountEmoney,
                        fill: false,
                        borderColor: '#f39c12'
                    },{
                        label: "GOPAY",
                        data: dailyCountGopay,
                        fill: false,
                        borderColor: '#13135b'
                    },{
                        label: "TCASH",
                        data: dailyCountTcash,
                        fill: false,
                        borderColor: '#f31212'
                    },{
                        label: "OTTOPAY",
                        data: dailyCountOttopay,
                        fill: false,
                        borderColor: '#3ac33a'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
            $('#sum-daily').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Daily Chart*/
            var transactionChartCanvas = $("#sum-daily");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                options: {
                    title: {
                        display: true,
                        text: 'Transactions By Number'
                    }
                },
                data : {
                    labels : dailyLabel,
                    datasets :[{
                        label: "BNI Yap!",
                        data: dailyDataBni,
                        fill: false,
                        borderColor: '#4f9afd'
                    }, {
                        label: "EMONEY",
                        data: dailyDataEmoney,
                        fill: false,
                        borderColor: '#f39c12'
                    },{
                        label: "GOPAY",
                        data: dailyDataGopay,
                        fill: false,
                        borderColor: '#13135b'
                    },{
                        label: "TCASH",
                        data: dailyDataTcash,
                        fill: false,
                        borderColor: '#f31212'
                    },{
                        label: "OTTOPAY",
                        data: dailyDataOttopay,
                        fill: false,
                        borderColor: '#3ac33a'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return "Rp" + value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
        };

        /**
         * Create Weekly Charts
         * @return {[type]} [description]
         */
        function createWeekly(){
            $('#transaction-weekly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var transactionChartCanvas = $("#transaction-weekly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : weeklyLabel,
                    datasets :[{
                        label: "BNI Yap!",
                        data: weeklyCountBni,
                        fill: false,
                        borderColor: '#4f9afd'
                    }, {
                        label: "EMONEY",
                        data: weeklyCountEmoney,
                        fill: false,
                        borderColor: '#f39c12'
                    },{
                        label: "GOPAY",
                        data: weeklyCountGopay,
                        fill: false,
                        borderColor: '#13135b'
                    },{
                        label: "TCASH",
                        data: weeklyCountTcash,
                        fill: false,
                        borderColor: '#f31212'
                    },{
                        label: "OTTOPAY",
                        data: weeklyCountOttopay,
                        fill: false,
                        borderColor: '#3ac33a'
                    }]
                },
                 options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
            $('#sum-weekly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var transactionChartCanvas = $("#sum-weekly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : weeklyLabel,
                    datasets :[{
                        label: "BNI Yap!",
                        data: weekDataBni,
                        fill: false,
                        borderColor: '#4f9afd'
                    }, {
                        label: "EMONEY",
                        data: weekDataEmoney,
                        fill: false,
                        borderColor: '#f39c12'
                    },{
                        label: "GOPAY",
                        data: weekDataGopay,
                        fill: false,
                        borderColor: '#13135b'
                    },{
                        label: "TCASH",
                        data: weekDataTcash,
                        fill: false,
                        borderColor: '#f31212'
                    },{
                        label: "OTTOPAY",
                        data: weekDataOttopay,
                        fill: false,
                        borderColor: '#3ac33a'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return "Rp" + value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
        }

        /**
         * Create Monthly Chart
         * @return {[type]} [description]
         */
        function createMonthly(){
            $('#transaction-monthly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var transactionChartCanvas = $("#transaction-monthly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : monthlyLabel,
                    datasets :[{
                        label: "BNI Yap!",
                        data: monthlyCountBni,
                        fill: false,
                        borderColor: '#4f9afd'
                    }, {
                        label: "EMONEY",
                        data: monthlyCountEmoney,
                        fill: false,
                        borderColor: '#f39c12'
                    },{
                        label: "GOPAY",
                        data: monthlyCountGopay,
                        fill: false,
                        borderColor: '#13135b'
                    },{
                        label: "TCASH",
                        data: monthlyCountTcash,
                        fill: false,
                        borderColor: '#f31212'
                    },{
                        label: "OTTOPAY",
                        data: monthlyCountOttopay,
                        fill: false,
                        borderColor: '#3ac33a'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
            $('#sum-monthly').css({
                "height": '250px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var transactionChartCanvas = $("#sum-monthly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : monthlyLabel,
                    datasets :[{
                        label: "BNI Yap!",
                        data: monthlyDataBni,
                        fill: false,
                        borderColor: '#4f9afd'
                    }, {
                        label: "EMONEY",
                        data: monthlyDataEmoney,
                        fill: false,
                        borderColor: '#f39c12'
                    },{
                        label: "GOPAY",
                        data: monthlyDataGopay,
                        fill: false,
                        borderColor: '#13135b'
                    },{
                        label: "TCASH",
                        data: monthlyDataTcash,
                        fill: false,
                        borderColor: '#f31212'
                    },{
                        label: "OTTOPAY",
                        data: monthlyDataOttopay,
                        fill: false,
                        borderColor: '#3ac33a'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return "Rp" + value.toLocaleString();
                                }
                            }
                        }]
                    }
                }
            });
        };

        function createDonutActiveAgent(){
            var transactionChartCanvas = $("#donut-active-agent");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'pie',
                data : {
                    datasets: [{
                        data: [activeAgent, (allAgent-activeAgent)],
                        backgroundColor: ['#05FF00','#FF1700']
                    }],

                    // These labels appear in the legend and in the tooltips when hovering different arcs
                    labels: [
                        'Active',
                        'Not Active'
                    ]
                },
                options:{
                    legend: {
                        labels: {
                            fontColor: "white"
                        }
                    },
                }
            });
        }

        $(document).ready(function() {
            // createWeekly();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var type = $(this).data('type');
                if (type=='daily') {
                    createDaily();
                }
                if (type=='weekly') {
                    createWeekly();
                }
                if (type=='monthly') {
                    createMonthly();
                }
            });
            $('#type').on('change', function() {
                type = $(this).val();
                createWeekly();
            });
        });
    </script>

    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function getGraph(){
            var comma_separator_number_step = $.animateNumber.numberStepFactories.separator('.')
            var formData = $('#form-transaction').serialize();
            showLoading('.box-graph','graph-loading');
            showLoading('.box-parcel','box-loading');
            $.ajax({
                url: '{{ url('angkasapura/payment/ajax/getAjaxGraph') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: formData,
                success: function(data){
                    if (data.isSuccess==true) {
                        dailyLabel = data.dayLabels;
                        dailyData = data.dayCountTransaction;
                        dailySumData = data.daySumTransaction;
                        
                        dailyDataEmoney = data.emoneyDay;
                        dailyDataOttopay = data.ottoDay;
                        dailyDataTcash = data.tcashDay;
                        dailyDataBni = data.bniDay;
                        dailyDataGopay = data.gopayDay;

                        dailyCountEmoney = data.emoneyDayCount;
                        dailyCountOttopay = data.ottoDayCount;
                        dailyCountTcash = data.tcashDayCount;
                        dailyCountBni = data.bniDayCount;
                        dailyCountGopay = data.gopayDayCount;

                        weekDataEmoney = data.emoneyWeek;
                        weekDataOttopay = data.ottoWeek;
                        weekDataTcash = data.tcashWeek;
                        weekDataBni = data.bniWeek;
                        weekDataGopay = data.gopayWeek;

                        weeklyCountEmoney = data.emoneyWeekCount;
                        weeklyCountOttopay = data.ottoWeekCount;
                        weeklyCountTcash = data.tcashWeekCount;
                        weeklyCountBni = data.bniWeekCount;
                        weeklyCountGopay = data.gopayWeekCount;
                        // console.log(weekDataBni);

                        monthlyDataEmoney = data.emoneyMonthly;
                        monthlyDataOttopay = data.ottoMonthly;
                        monthlyDataTcash = data.tcashMonthly;
                        monthlyDataBni = data.bniMonthly;
                        monthlyDataGopay = data.gopayMonthly;

                        monthlyCountEmoney = data.emoneyMonthlyCount;
                        monthlyCountOttopay = data.ottoMonthlyCount;
                        monthlyCountTcash = data.tcashMonthlyCount;
                        monthlyCountBni = data.bniMonthlyCount;
                        monthlyCountGopay = data.gopayMonthlyCount;

                        weeklyLabel = data.weekLabels;
                        weeklyData = data.weekCountTransaction;
                        weeklySumData = data.weekSumTransaction;

                        monthlyLabel = data.monthLabels;
                        monthlyData = data.monthCountTransaction;
                        monthlySumData = data.monthSumTransaction;

                        allAgent = data.countAllAgent;
                        activeAgent = data.countActiveAgent;

                        createWeekly();
                        showDataToView(data.dataTableDaily, data.dataTableWeekly, data.dataTableMonthly);

                        // createDonutActiveAgent();

                        $('#pulsa-count').animateNumber({
                            number:data.pulsaCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#pulsa-sum').animateNumber({
                            number:data.pulsaSum,
                            numberStep: comma_separator_number_step
                        });

                        $('#payment-count').animateNumber({
                            number:data.paymentCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#payment-sum').animateNumber({
                            number:data.paymentSum,
                            numberStep: comma_separator_number_step
                        });

                        $('#popshop-count').animateNumber({
                            number:data.popshopCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#popshop-sum').animateNumber({
                            number:data.popshopSum,
                            numberStep: comma_separator_number_step
                        });

                        $('#delivery-count').animateNumber({
                            number:data.deliveryCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#delivery-sum').animateNumber({
                            number:data.deliverySum,
                            numberStep: comma_separator_number_step
                        });

                        // calculation transaction
                        $('#transaction-count').animateNumber({
                            number:data.transactionCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#transaction-sum').animateNumber({
                            number:data.transactionAmount,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_count = (data.transactionCount/(data.weekCountTransaction.length));
                        $('#transaction-week-count').animateNumber({
                            number:transaction_week_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_sum = (data.transactionAmount/(data.weekCountTransaction.length));
                        $('#transaction-week-sum').animateNumber({
                            number:transaction_week_sum,
                            numberStep: comma_separator_number_step
                        });

                        var transaction_day_count = (data.transactionCount/(data.dayCountTransaction.length));
                        $('#transaction-day-count').animateNumber({
                            number:transaction_day_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_day_sum = (data.transactionAmount/(data.dayCountTransaction.length));
                        $('#transaction-day-sum').animateNumber({
                            number:transaction_day_sum,
                            numberStep: comma_separator_number_step
                        });

                        // calculation topup
                        $('#topup-count').animateNumber({
                            number:data.topupCount,
                            numberStep: comma_separator_number_step
                        });
                        $('#topup-sum').animateNumber({
                            number:data.topupAmount,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_count = (data.topupCount/(data.weekCountTransaction.length));
                        $('#topup-week-count').animateNumber({
                            number:transaction_week_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_week_sum = (data.topupAmount/(data.weekCountTransaction.length));
                        $('#topup-week-sum').animateNumber({
                            number:transaction_week_sum,
                            numberStep: comma_separator_number_step
                        });

                        var transaction_day_count = (data.topupCount/(data.dayCountTransaction.length));
                        $('#topup-day-count').animateNumber({
                            number:transaction_day_count,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_day_sum = (data.topupAmount/(data.dayCountTransaction.length));
                        $('#topup-day-sum').animateNumber({
                            number:transaction_day_sum,
                            numberStep: comma_separator_number_step
                        });

                        $('.day-count').html(data.dayCountTransaction.length);
                        $('.week-count').html(data.weekCountTransaction.length);

                        // calculate transaction based on active
                        var transaction_count_active = (data.transactionCount / data.countActiveAgent);
                        $('#transaction-count-active').animateNumber({
                            number:transaction_count_active,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_sum_active = (data.transactionAmount / data.countActiveAgent);
                        $('#transaction-sum-active').animateNumber({
                            number:transaction_sum_active,
                            numberStep: comma_separator_number_step
                        });

                        // calculate topup based on active
                        var topup_count_active = (data.topupCount / data.countActiveAgent);
                        $('#topup-count-active').animateNumber({
                            number:topup_count_active,
                            numberStep: comma_separator_number_step
                        });
                        var topup_sum_active = (data.topupAmount / data.countActiveAgent);
                        $('#topup-sum-active').animateNumber({
                            number:topup_sum_active,
                            numberStep: comma_separator_number_step
                        });

                        // calculate transaction based on active per day
                        var transaction_count_active_day = ((data.transactionCount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#transaction-count-active-day').animateNumber({
                            number:transaction_count_active_day,
                            numberStep: comma_separator_number_step
                        });
                        var transaction_sum_active_day = ((data.transactionAmount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#transaction-sum-active-day').animateNumber({
                            number:transaction_sum_active_day,
                            numberStep: comma_separator_number_step
                        });

                        // calculate topup based on active per day
                        var topup_count_active_day = ((data.topupCount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#topup-count-active-day').animateNumber({
                            number:topup_count_active_day,
                            numberStep: comma_separator_number_step
                        });
                        var topup_sum_active_day = ((data.topupAmount / data.countActiveAgent) / data.dayCountTransaction.length);
                        $('#topup-sum-active-day').animateNumber({
                            number:topup_sum_active_day,
                            numberStep: comma_separator_number_step
                        });

                        $('#total-agent').animateNumber({
                            number:allAgent,
                            numberStep: comma_separator_number_step
                        });

                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    alert('Failed');
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getParcel");
            })
            .fail(function() {
                console.log("error - getParcel");
            })
            .always(function() {
                hideLoading('.box-graph','graph-loading');
                hideLoading('.box-parcel','box-loading');
                console.log("complete - getParcel");
                console.log('===================')
            });   
        }

        jQuery(document).ready(function($) {
            getGraph();
        });

        function showDataToView(data, week, monthly) {
            var tableDaily = $('#table-transaction-daily').DataTable({
                'destroy'      : true,
                'retreive'      : true,
                'paging'      : true,
                'searching'   : true,
                'ordering'    : true,
                'autoWidth'   : true,
                'responsive'  : true,
                'processing'  : true,
                'scrollY'     : 200,
                'deferRender' : true,
                'scroller'    : true,
                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                'order': [[ 0, "desc" ]],
                // columns: [ 
                //     // { type: 'currency-cust', targets: 7 }
                //     // { data: "Periode"},
                //     // { data: "Number Of Parcel", className: "sum" },
                //     // { data: "Number Of Transaction", className: "sum" },
                //     // { data: "BNI Yap", className: "sum" },
                //     // { data: "Emoney", className: "sum" },
                //     // { data: "Tcash", className: "sum" },
                //     // { data: "Ottopay", className: "sum" },
                //     // { data: "Total Amount", className: "sum" }
                // ], 
                // createdRow: function ( row, data, index ) {
                //     $(row).addClass('sum')
                // }, 
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                     // Total over all pages
                    total = api
                        .column(4)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column(4, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(4).footer() ).html(
                        numberWithCommas(pageTotal) +' ( all: '+ numberWithCommas(total) +' )'
                    );

                    totalA = api
                        .column(1)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalA = api
                        .column(1, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(1).footer() ).html(
                        pageTotalA +' ( all: '+ totalA +' )'
                    );

                    totalB = api
                        .column(2)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalB = api
                        .column(2, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(2).footer() ).html(
                        pageTotalB +' ( all: '+ totalB +' )'
                    );

                    totalC = api
                        .column(3)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalC = api
                        .column(3, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(3).footer() ).html(
                        numberWithCommas(pageTotalC) +' ( all: '+ numberWithCommas(totalC) +' )'
                    );

                    totalD = api
                        .column(5)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalD = api
                        .column(5, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(5).footer() ).html(
                        numberWithCommas(pageTotalD) +' ( all: '+ numberWithCommas(totalD) +' )'
                    );

                    totalE = api
                        .column(6)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalE = api
                        .column(6, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(6).footer() ).html(
                        numberWithCommas(pageTotalE) +' ( all: '+ numberWithCommas(totalE) +' )'
                    );

                    totalF = api
                        .column(7)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalF = api
                        .column(7, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(7).footer() ).html(
                        numberWithCommas(pageTotalF) +' ( all: '+ numberWithCommas(totalF) +' )'
                    );

                    totalG = api
                        .column(8)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalG = api
                        .column(8, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(8).footer() ).html(
                        numberWithCommas(pageTotalG) +' ( all: '+ numberWithCommas(totalG) +' )'
                    );
                }
            });

            tableDaily.clear();
            for(i = 0; i < data.length; i++) {
                tableDaily.row.add([
                    data[i]['periode'],
                    data[i]['totalParcel'],
                    data[i]['totalTrx'],
                    numberWithCommas(data[i]['bni']),
                    numberWithCommas(data[i]['emoney']),
                    numberWithCommas(data[i]['gopay']),
                    numberWithCommas(data[i]['tcash']),
                    numberWithCommas(data[i]['ottopay']),
                    numberWithCommas(data[i]['total']),
                ]).draw(false);
            }

            var tableWeekly = $('#table-transaction-weekly').DataTable({
                'destroy'     : true,
                'retreive'    : true,
                'paging'      : true,
                'searching'   : true,
                'ordering'    : false,
                'autoWidth'   : true,
                'responsive'  : true,
                'processing'  : true,
                'scrollY'     : 200,
                'deferRender' : true,
                'scroller'    : true,
                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                // 'order': [[ 0, "desc" ]],
                columnDefs: [
                    // { type: 'currency-cust', targets: 7 },
                ], 
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                     // Total over all pages
                    total = api
                        .column(4)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column(4, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(4).footer() ).html(
                        numberWithCommas(pageTotal) +' ( all: '+ numberWithCommas(total) +' )'
                    );

                    totalA = api
                        .column(1)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalA = api
                        .column(1, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(1).footer() ).html(
                        pageTotalA +' ( all: '+ totalA +' )'
                    );

                    totalB = api
                        .column(2)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalB = api
                        .column(2, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(2).footer() ).html(
                        pageTotalB +' ( all: '+ totalB +' )'
                    );

                    totalC = api
                        .column(3)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalC = api
                        .column(3, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(3).footer() ).html(
                        numberWithCommas(pageTotalC) +' ( all: '+ numberWithCommas(totalC) +' )'
                    );

                    totalD = api
                        .column(5)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalD = api
                        .column(5, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(5).footer() ).html(
                        numberWithCommas(pageTotalD) +' ( all: '+ numberWithCommas(totalD) +' )'
                    );

                    totalE = api
                        .column(6)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalE = api
                        .column(6, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(6).footer() ).html(
                        numberWithCommas(pageTotalE) +' ( all: '+ numberWithCommas(totalE) +' )'
                    );

                    totalF = api
                        .column(7)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalF = api
                        .column(7, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(7).footer() ).html(
                        numberWithCommas(pageTotalF) +' ( all: '+ numberWithCommas(totalF) +' )'
                    );

                    totalG = api
                        .column(8)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );
                    
                    // Total over this page
                    pageTotalG = api
                        .column(8, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(8).footer() ).html(
                        numberWithCommas(pageTotalG) +' ( all: '+ numberWithCommas(totalG) +' )'
                    );
                }
            });

            tableWeekly.clear();
            for(i = 0; i < week.length; i++) {
                tableWeekly.row.add([
                    week[i]['periode'],
                    week[i]['totalParcel'],
                    week[i]['totalTrx'],
                    numberWithCommas(week[i]['bni']),
                    numberWithCommas(week[i]['emoney']),
                    numberWithCommas(week[i]['gopay']),
                    numberWithCommas(week[i]['tcash']),
                    numberWithCommas(week[i]['ottopay']),
                    numberWithCommas(week[i]['total']),
                ]).draw(false);
            }

            var tableMonthly = $('#table-transaction-monthly').DataTable({
                'destroy'      : true,
                'retreive'      : true,
                'paging'      : true,
                'searching'   : true,
                'ordering'    : true,
                'autoWidth'   : true,
                'responsive'  : true,
                'processing'  : true,
                'scrollY'     : 80,
                'deferRender' : true,
                'scroller'    : true,
                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                'order': [[ 0, "desc" ]],
                columnDefs: [
                    // { type: 'currency-cust', targets: 7 },
                ],
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                     // Total over all pages
                    total = api
                        .column(4)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );

                    // Total over this page
                    pageTotal = api
                        .column(4, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(4).footer() ).html(
                        numberWithCommas(pageTotal) +' ( all: '+ numberWithCommas(total) +' )'
                    );

                    totalA = api
                        .column(1)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalA = api
                        .column(1, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(1).footer() ).html(
                        pageTotalA +' ( all: '+ totalA +' )'
                    );

                    totalB = api
                        .column(2)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalB = api
                        .column(2, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(2).footer() ).html(
                        pageTotalB +' ( all: '+ totalB +' )'
                    );

                    totalC = api
                        .column(3)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalC = api
                        .column(3, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(3).footer() ).html(
                        numberWithCommas(pageTotalC) +' ( all: '+ numberWithCommas(totalC) +' )'
                    );

                    totalD = api
                        .column(5)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalD = api
                        .column(5, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(5).footer() ).html(
                        numberWithCommas(pageTotalD) +' ( all: '+ numberWithCommas(totalD) +' )'
                    );

                    totalE = api
                        .column(6)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalE = api
                        .column(6, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(6).footer() ).html(
                        numberWithCommas(pageTotalE) +' ( all: '+ numberWithCommas(totalE) +' )'
                    );

                    totalF = api
                        .column(7)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalF = api
                        .column(7, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(7).footer() ).html(
                        numberWithCommas(pageTotalF) +' ( all: '+ numberWithCommas(totalF) +' )'
                    );

                    totalG = api
                        .column(8)
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                    }, 0 );

                    // Total over this page
                    pageTotalG = api
                        .column(8, { page: 'current'})
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
        
                    // Update footer
                    $( api.column(8).footer() ).html(
                        numberWithCommas(pageTotalG) +' ( all: '+ numberWithCommas(totalG) +' )'
                    );
                }
            });

            tableMonthly.clear();
            for(i = 0; i < monthly.length; i++) {
                tableMonthly.row.add([
                    monthly[i]['periode'],
                    monthly[i]['totalParcel'],
                    monthly[i]['totalTrx'],
                    numberWithCommas(monthly[i]['bni']),
                    numberWithCommas(monthly[i]['emoney']),
                    numberWithCommas(monthly[i]['gopay']),
                    numberWithCommas(monthly[i]['tcash']),
                    numberWithCommas(monthly[i]['ottopay']),
                    numberWithCommas(monthly[i]['total']),
                ]).draw(false);
            }
        }
    </script>

    {{-- Button Click --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#btn-submit').on('click', function(event) {
                $('#form-transaction').attr('method', 'get');
                $('#form-transaction').submit();
            });
            $('#btn-export').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/export') }}');
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
        });
    </script>
@stop