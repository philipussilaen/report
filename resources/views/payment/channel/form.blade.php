@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Payment Channel
@endsection

@section('css')
	{{-- expr --}}
@endsection

@section('pageTitle')
	Payment Channel Management
@endsection

@section('pageDesc')
	List and Manage Payment Channel
@endsection

@section('content')
	<div class="box box-solid">
		<div class="box-body">
			<form method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="channelId" @isset ($channelDb) value="{{ $channelDb->id }}" @endisset>
				<div class="row">
					<div class="col-md-6 border-right">
						<div class="form-group">
							<label>Payment Vendor</label>
							<input type="text" name="paymentVendor" class="form-control" @isset ($channelDb) value="{{ $channelDb->paymentVendor->name }}" @endisset readonly>
						</div>
						<div class="form-group">
							<label>Code</label>
							<input type="text" name="code" class="form-control" @isset ($channelDb) value="{{ $channelDb->code }}" @endisset readonly>
						</div>
						<div class="form-group">
							<label>Type</label>
							<input type="text" name="type" class="form-control" @isset ($channelDb) value="{{ $channelDb->type }}" @endisset>
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" class="form-control" @isset ($channelDb) value="{{ $channelDb->name }}" @endisset>
						</div>
						<div class="form-group">
							<label>Status</label>
							<select name="status" class="form-control">
								<option value="OPEN" @isset ($channelDb) @if($channelDb->status=='OPEN') selected @endif @endisset>OPEN</option>
								<option value="CLOSE" @isset ($channelDb) @if($channelDb->status=='CLOSE') selected @endif @endisset>CLOSE</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Text</label>
							<input type="text" name="text" class="form-control" @isset ($channelDb) value="{{ $channelDb->text }}" @endisset>
						</div>
						<div class="form-group">
							<label>Description</label>
							<input type="text" name="description" class="form-control" @isset ($channelDb) value="{{ $channelDb->description }}" @endisset>
						</div>
						<div class="form-group">
							<label>Image</label>
							<input type="file" name="image" class="form-control">
							@isset ($channelDb)
							    @if (!empty($channelDb->image_url))
									<img src="{{ env('PAYMENT_URL') }}/{{ $channelDb->image_url }}" class="img-responsive">
								@endif
							@endisset
						</div>
						<button class="btn btn-flat btn-block btn-info">Save</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('js')
	{{-- expr --}}
@endsection