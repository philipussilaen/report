@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    
@stop

@section('title')
    Executive Summary
@endsection

@section('pageTitle')
    Pop Warung - Registration, Topup &amp; Digital Transaction
@endsection

@section('pageDesc')
    
@endsection

@section('content')
<style>
 .header-title-custom{
     background-color: #D3D3D3;
     border-radius: 15px; 
     padding-left: 15px; 
     padding-bottom: 2px;
     height: 30px;
 }
</style>
    <section class="content">
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-filter">
                    <div class="box-body border-radius-none">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row header-filter">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-city" name="city" id="city">
                                                    <option value="all">Semua Kota</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-user-type" name="userType" id="userType">
                                                
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row" style="margin-right:0px;margin-top: 15px;">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control" id="daterange-transaction" name="dateRange-transaction" placeholder="Transaction Date" readonly="readonly" style="background-color: white;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div style="margin-right:0px;margin-top: 15px;">
                                                    <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <br>
                        
                        <!-- Mitra Registration -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.mitra_registration.title')}}</strong></h4>
                        </div>
                        <div class="row box-info">
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.total_registered')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-users"></i>
                                        <label class="counter" id="total_registered_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.mitra_registration.total_registered')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.total_verified')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: green;"></i> 
                                        <label class="counter" id="total_verified_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.mitra_registration.total_verified')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.total_active_user')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: #4d79ff;"></i> 
                                        <label class="counter" id="total_active_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.mitra_registration.total_active_user')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.total_inactive_user')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: red;"></i> 
                                        <label class="counter" id="total_inactive_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.mitra_registration.total_inactive_user')}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Warung User Summary -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.user_summary.warung')}}</strong></h4>
                        </div>
                        <div class="row box-info">
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users"></i>
                                        <label class="counter" id="warung_total_register_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.total_reg')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: #800000;"></i> 
                                        <label class="counter" id="warung_total_disable_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.disable')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: #4db8ff;"></i> 
                                        <label class="counter" id="warung_total_enable_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.enable')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: orange;"></i> 
                                        <label class="counter" id="warung_total_pending_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.pending')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: green;"></i> 
                                        <label class="counter" id="warung_total_verified_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.verified')}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Agent User Summary -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.user_summary.agent')}}</strong></h4>
                        </div>
                        <div class="row box-info">
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users"></i>
                                        <label class="counter" id="agent_total_register_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.total_reg')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: #800000;"></i> 
                                        <label class="counter" id="agent_total_disable_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.disable')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: #4db8ff;"></i> 
                                        <label class="counter" id="agent_total_enable_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.enable')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: orange;"></i> 
                                        <label class="counter" id="agent_total_pending_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.pending')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-2 col-sm-6" style="width: 20%">
                                <div class="card-box text-center">
                                    <h3 class="m-t-0"><i class="fa fa-users" style="color: green;"></i> 
                                        <label class="counter" id="agent_total_verified_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.user_summary.verified')}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Topup Deposit -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.topup_deposit.title')}}</strong></h4>
                        </div>
                        <div class="row box-info">
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.gross_amount')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                        <label class="counter" id="gross_amount"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.topup_deposit.gross_amount')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.nett_amount')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                        <label class="counter" id="net_amount"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.topup_deposit.nett_amount')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.successfull_topup')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-check-circle-o" style="color: green;"></i>
                                        <label class="counter" id="successful_topup_attempt"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.topup_deposit.successfull_topup')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.unique_user')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-user" style="color: black;"></i> 
                                        <label class="counter" id="unique_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.topup_deposit.unique_user')}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Digital Product -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.digital_product.title')}}</strong></h4>
                        </div>
                        
                        <div class="row box-info">
                            <div class="col-lg-4 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.digital_product.gross_amount')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                        <label class="counter" id="dp_gross_amount"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.digital_product.gross_amount')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-4 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.digital_product.gross_order')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-bar-chart" aria-hidden="true" style="color: brown;"></i>
                                        <label class="counter" id="dp_gross_numb_trx"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.digital_product.gross_order')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-4 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.digital_product.gross_unique_user')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-user" style="color: green;"></i>
                                        <label class="counter" id="dp_gross_unique_users"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.digital_product.gross_unique_user')}}</p>
                                </div>
                            </div>
                            
                            <div class="col-lg-4 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.digital_product.nett_amount')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                        <label class="counter" id="dp_nett_amount"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.digital_product.nett_amount')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-4 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.digital_product.nett_order')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-bar-chart" aria-hidden="true" style="color: brown;"></i>
                                        <label class="counter" id="dp_nett_numb_trx"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.digital_product.nett_order')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-4 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.digital_product.nett_unique_user')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0">
                                    	<i class="fa fa-user" style="color: green;"></i>
                                        <label class="counter" id="dp_nett_unique_users"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.digital_product.nett_unique_user')}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Top 10 Performing Warung -->
                        <div class="row box-info">
                        	<div class="header-title-custom" style="margin-bottom: 10px; width: 98%; margin-left: 12px" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.purchase_popagent')}}">
                        		<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.performing_warung.title')}}</strong></h4>
                        		<p class="glyphicon glyphicon-info-sign info-icon-tooltip" style="padding-top: 8px; margin-right: 1%"></p>
                        	</div>
                        </div>
                        
                        <div class="row box-info">
                        	<div class="col-lg-12">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent-top-performing-popwarung" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No.</th>
                                                <th class="text-center">{{trans('executive_summary.performing_warung.locker_id')}}</th>
                                                <th class="text-center">{{trans('executive_summary.performing_warung.warung_name')}}</th>
                                                <th class="text-right">{{trans('executive_summary.performing_warung.digital_product')}}</th>
                                                <th class="text-right">{{trans('executive_summary.performing_warung.non_digital_product')}}</th>
                                                <th class="text-right">{{trans('executive_summary.performing_warung.total_transaction')}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Top 10 Performing Agent -->
                        <div class="row box-info">
                        	<div class="header-title-custom" style="margin-bottom: 10px; width: 98%; margin-left: 12px" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.purchase_popagent')}}">
                        		<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.performing_agent.title')}}</strong></h4>
                        		<p class="glyphicon glyphicon-info-sign info-icon-tooltip" style="padding-top: 8px; margin-right: 1%"></p>
                        	</div>
                        </div>
                        
                        <div class="row box-info">
                        	<div class="col-lg-12">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent-top-performing-agent" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No.</th>
                                                <th class="text-center">{{trans('executive_summary.performing_agent.locker_id')}}</th>
                                                <th class="text-center">{{trans('executive_summary.performing_agent.agent_name')}}</th>
                                                <th class="text-right">{{trans('executive_summary.performing_agent.digital_product')}}</th>
                                                <th class="text-right">{{trans('executive_summary.performing_agent.non_digital_product')}}</th>
                                                <th class="text-right">{{trans('executive_summary.performing_agent.total_transaction')}}</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    
    {{-- NUMERATOR --}}
    <script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 15;
        var isBoxHide = true;
        var userTypes = {!! json_encode($user_types) !!};
        var cities =  {!! json_encode($cities) !!};
        var executed = false;
        var top_performing_agent_dataSet = [];
        
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });

        $(function(){

            $('.select2').select2();

            $(".select2-user-type").select2({
                  data: userTypes
            })//.val({!! $city !!}).trigger('change')
            ;

            $(".select2-city").select2({
                  data: cities
            })//.val({!! $city !!}).trigger('change')
            ;

            dashboard();

            $('#gridcontent-top-performing-popwarung').DataTable({
                'paging'        : false,
                'lengthChange'  : false,
                'ordering'      : false,
                'info'          : false,
                'autoWidth'     : false,
                "processing"    : true,
                "serverSide"    : true,
                'searching'     : false,
                "pageLength"    : limit,
                "ajax": {
                    
                    "url": "{{ route('top-performing-popwarung-list') }}",
                    "data": function ( d ) {
                        var info				= $('#gridcontent-top-performing-popwarung').DataTable().page.info();                        
                        _token                  = '{{ csrf_token() }}',
                        d.city                	= $('#city').val() ? $('#city').val() : 'all';
                        d.daterange_transaction	= transactionDateRange;
                        d.user_type             = $("#userType").val() ? $("#userType").val() : 'all';
                        d.start             	= info.start;
                        d.limit             	= limit;
                    },
                    "dataSrc": function(json){
                        json.draw = json.payload.draw;
                        json.recordsTotal = json.payload.count;
                        json.recordsFiltered = json.payload.count;

                        return json.payload.data;
                    }
                },
                                
                "columnDefs" : [
                    { "targets": 0, "data": 'no', "className": "text-center", "width" : "3%" },
                    { "targets": 1, "data": "locker_id", "className": "text-center", "width" : "17%" },
                    { "targets": 2, "data": "locker_name", "className": "text-center", "width" : "20%" },
                    { "targets": 3, "data": "total_transaction_digital", "className": "text-right", "width" : "20%",
						"render": function ( data, type, row, meta ) {
							return formatPriceTopPerforming(data);
						}
					},
                    { "targets": 4, "data": "total_transaction_non_digital", "className": "text-right", "width" : "20%",
						"render": function ( data, type, row, meta ) {
							return formatPriceTopPerforming(data);
						}
					},
                    { "targets": 5, "data": "total_both_type_transaction", "className": "text-right", "width" : "20%",
						"render": function ( data, type, row, meta ) {
							return formatPriceTopPerforming(data);
						}
					}
                ]
            });

            $('#gridcontent-top-performing-agent').DataTable({
                'paging'        : false,
                'lengthChange'  : false,
                'ordering'      : false,
                'info'          : false,
                'autoWidth'     : false,
                "processing"    : true,
                "serverSide"    : true,
                'searching'     : false,
                "pageLength"    : limit,
                "ajax": {
                    
                    "url": "{{ route('top-performing-agent-list') }}",
                    "data": function ( d ) {
                        var info				= $('#gridcontent-top-performing-agent').DataTable().page.info();                        
                        _token                  = '{{ csrf_token() }}',
                        d.city                 	= $('#city').val() ? $('#city').val() : 'all';
                        d.daterange_transaction	= transactionDateRange;
                        d.user_type             = $("#userType").val() ? $("#userType").val() : 'all';
                        d.start             	= info.start;
                        d.limit             	= limit;
                    },
                    "dataSrc": function(json){
                        json.draw = json.payload.draw;
                        json.recordsTotal = json.payload.count;
                        json.recordsFiltered = json.payload.count;

                        return json.payload.data;
                    }
                },
                                
                "columnDefs" : [
                    { "targets": 0, "data": 'no', "className": "text-center", "width" : "3%" },
                    { "targets": 1, "data": "locker_id", "className": "text-center", "width" : "17%" },
                    { "targets": 2, "data": "locker_name", "className": "text-center", "width" : "20%" },
                    { "targets": 3, "data": "total_transaction_digital", "className": "text-right", "width" : "20%",
						"render": function ( data, type, row, meta ) {
							return formatPriceTopPerforming(data);
						}
					},
                    { "targets": 4, "data": "total_transaction_non_digital", "className": "text-right", "width" : "20%",
						"render": function ( data, type, row, meta ) {
							return formatPriceTopPerforming(data);
						}
					},
                    { "targets": 5, "data": "total_both_type_transaction", "className": "text-right", "width" : "20%",
						"render": function ( data, type, row, meta ) {
							return formatPriceTopPerforming(data);
						}
					}
                ]
            });
            
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                $('#gridcontent-top-performing-popwarung').DataTable().ajax.reload();
                $('#gridcontent-top-performing-agent').DataTable().ajax.reload();
                dashboard();
            });
        });

        function dashboard(){
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-info', 'box-info');
            
            $.ajax({
                url: "{{ route('topup-digital-summary') }}",
                data: {
                    _token                  : '{{ csrf_token() }}',
                    city                    : $('#city').val() ? $('#city').val() : 'all',
                    daterange_transaction 	: transactionDateRange,
                    user_type             	: $("#userType").val() ? $("#userType").val() : 'all',
                },
                type: 'GET',
                success: function(data){
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-info', 'box-info');
                	refresh_dasboard(data.payload);
                }
            });
        }

        function refresh_dasboard(data){
            // Mitra Registration
            $('#total_registered_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.mitra_registration.total_registered_user
            });
            
            $('#total_verified_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.mitra_registration.total_verified_user
            });
            
            $('#total_active_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.mitra_registration.total_active_user
            });
            
            $('#total_inactive_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.mitra_registration.total_inactive_user
            });

            // Topup Deposit
            $('#gross_amount').numerator({ 
                easing		: 'linear',
            	duration	: 1000,
            	toValue		: data.topup_deposit.gross_amount,
            	onComplete	: function(){
                	$('#gross_amount').html(formatPrice(data.topup_deposit.gross_amount));
            	}
            });
            
            $('#net_amount').numerator({ 
                easing		: 'linear',
            	duration	: 1000,
            	toValue		: data.topup_deposit.net_amount,
            	onComplete	: function(){
                	$('#net_amount').html(formatPrice(data.topup_deposit.net_amount));
            	}
            });
            
            $('#successful_topup_attempt').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.topup_deposit.successful_topup_attempt 
            });
            
            $('#unique_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.topup_deposit.unique_user 
            });

            // Digital Product
            // =============== Gross ===============
            $('#dp_gross_amount').numerator({ 
                easing		: 'linear',
            	duration	: 1000,
            	toValue		: data.gross_digital_product.total_transaction_digital,
            	onComplete	: function(){
                	$('#dp_gross_amount').html(formatPrice(data.gross_digital_product.total_transaction_digital));
            	}
            });
            
            $('#dp_gross_numb_trx').numerator({ 
                easing		: 'linear',
            	duration	: 1000,
            	toValue		: data.gross_digital_product.total_order_transaction_digital
            });
            
            $('#dp_gross_unique_users').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.gross_digital_product.unique_user_order_transaction_digital 
            });

            // =============== Nett ===============
            $('#dp_nett_amount').numerator({ 
                easing		: 'linear',
            	duration	: 1000,
            	toValue		: data.nett_digital_product.total_transaction_digital,
            	onComplete	: function(){
                	$('#dp_nett_amount').html(formatPrice(data.nett_digital_product.total_transaction_digital));
            	}
            });
            
            $('#dp_nett_numb_trx').numerator({ 
                easing		: 'linear',
            	duration	: 1000,
            	toValue		: data.nett_digital_product.total_order_transaction_digital
            });
            
            $('#dp_nett_unique_users').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.nett_digital_product.unique_user_order_transaction_digital 
            });

            // =========== Agent Summary ===========
            
            $('#agent_total_register_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.agent_total_register_user 
            });
            
            $('#agent_total_disable_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.agent_total_disable_user 
            });
            
            $('#agent_total_enable_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.agent_total_enable_user 
            });
            
            $('#agent_total_pending_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.agent_total_pending_user 
            });
            
            $('#agent_total_verified_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.agent_total_verified_user 
            });

            // =========== Warung Summary ===========
            
            $('#warung_total_register_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.warung_total_register_user 
            });
            
            $('#warung_total_disable_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.warung_total_disable_user 
            });
            
            $('#warung_total_enable_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.warung_total_enable_user 
            });
            
            $('#warung_total_pending_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.warung_total_pending_user 
            });
            
            $('#warung_total_verified_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.user_summary.warung_total_verified_user 
            });
        }
        
        function formatPrice(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return 'Rp '+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
        function formatPriceTopPerforming(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
    </script>
    
    {{-- Date Range Picker --}}
    <script type="text/javascript">
    	var transactionDateRange = '';
        var startDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
        var endDate = moment().format('YYYY-MM-DD');

        var startDateToShow = moment().subtract(29, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().format('DD/MM/YYYY');
        transactionDateRange = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();

            $('#daterange-transaction').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }
    </script>
@stop