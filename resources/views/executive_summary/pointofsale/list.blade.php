@extends('layout.main')
@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    {{-- Morris CSS --}}
  	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
  	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@stop

@section('title')
    Executive Summary
@endsection

@section('pageTitle')
    Pop Warung - Point of Sales
@endsection

@section('pageDesc')
    
@endsection

@section('content')
<style>
 .header-title-custom{
     background-color: #D3D3D3;
     border-radius: 15px; 
     padding-left: 15px; 
     padding-bottom: 2px;
     height: 30px;
 }
 #graph {
    width: 800px;
    height: 250px;
    margin: 20px auto 0 auto;
 }
 pre {
    height: 250px;
    overflow: auto;
 }
</style>
    <section class="content">
        {{-- Table --}}
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-filter">
                    <div class="box-body border-radius-none">
                        <form id="form-transaction">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row header-filter">
                                        
                                        <div class="col-md-3">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-city" name="city" id="city">
                                                    <option value="all">Semua Kota</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-3" style="display: none;">
                                            <div class="form-group" style="margin-right:0px;margin-top: 15px;">
                                                <select class="form-control select2 select2-user-type" name="userType" id="userType">
                                                
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="row" style="margin-right:0px;margin-top: 15px;">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" class="form-control" id="daterange-transaction" name="dateRange-transaction" placeholder="Transaction Date" readonly="readonly" style="background-color: white;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div style="margin-right:0px;margin-top: 15px;">
                                                    <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <br>
                        
                        <!-- Product Sales -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.pos.warung')}} - {{trans('executive_summary.pos.product_sales')}}</strong></h4>
                        </div>
                        <div class="row box-info">
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.penjualan_cepat')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-money"></i>
                                        <label class="counter" id="penjualan_cepat"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.pos.product_sales_box.penjualan_cepat')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.penjualan')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-money" style="color: green;"></i> 
                                        <label class="counter" id="penjualan"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.pos.product_sales_box.penjualan')}}</p>
                                </div>
                            </div>
            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.total_penjualan')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-money" style="color: orange;"></i> 
                                        <label class="counter" id="total"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.pos.product_sales_box.total')}}</p>
                                </div>
                            </div>
                            
                            <div class="col-lg-3 col-sm-6">
                                <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.by_active_user')}}">
                                    <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                    <h3 class="m-t-0"><i class="fa fa-user"></i>
                                        <label class="counter" id="active_user"></label>
                                    </h3>
                                    <p class="text-muted">{{trans('executive_summary.pos.product_sales_box.active_user')}}</p>
                                </div>
                            </div>
                        </div>
                        
                        <!-- Morris chart sales stock -->
                        
                        <div class="row box-info" style="display: none">

                			<div class="col-sm-12">
                                <div class="card-box text-center">
                                    <h4 class="m-t-0">Pergerakan Total Seluruh Transaksi, Transaksi dengan Deposit, dan COD</h4>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#458bc4"></i>Total Seluruh Transaksi</h5>
                                            </li>
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#23b195"></i>Deposit</h5>
                                            </li>
                                            <li>
                                                <h5 class="font-normal"><i class="fa fa-circle m-r-10 text-muted" style="color:#f98009"></i>COD</h5>
                                            </li>
                                        </ul>
                                    </div>
            
                                    <div id="dashboard-line-stacked" style="height: 300px;"></div>
            
                                </div>
                            </div>
            
                        </div> 
                        
                        <!-- Stock Purchase -->
                        <div class="row box-info">
                        	<div class="col-lg-6 col-sm-6">
                        		<div class="header-title-custom" style="margin-bottom: 10px">
                        			<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.pos.warung')}} - {{trans('executive_summary.pos.verified_user')}}</strong></h4>
                    			</div>
                    			
                        		<div class="col-lg-6 col-sm-6">
                                    <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.verified_user.avg_sales_per_store', ['type' => 'Penjualan Produk'])}}">
                                        <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                        <h3 class="m-t-0">
                                            <i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                            <label class="counter" id="verified_avg_sales_per_stock"></label>
                                        </h3>
                                        <p class="text-muted">{{trans('executive_summary.verified_user.avg_sales_per_store')}}</p>
                                    </div>
                                </div>
                
                                <div class="col-lg-6 col-sm-6">
                                    <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.verified_user.avg_sales_per_store_per_day', ['type' => 'Penjualan Produk'])}}">
                                        <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                        <h3 class="m-t-0">
                                            <i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                            <label class="counter" id="verified_avg_sales_per_stock_per_day"></label>
                                        </h3>
                                        <p class="text-muted">{{trans('executive_summary.verified_user.avg_sales_per_store_per_day')}}</p>
                                    </div>
                                </div>
                        			
                        	</div>
                        	<div class="col-lg-6 col-sm-6">
                        		<div class="header-title-custom" style="margin-bottom: 10px">
                        			<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.pos.warung')}} - {{trans('executive_summary.pos.active_user')}}</strong></h4>
                    			</div>
                        		<div class="col-lg-6 col-sm-6">
                                    <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.active_user.avg_sales_per_store', ['type' => 'Penjualan Produk'])}}">
                                        <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                        <h3 class="m-t-0">
                                            <i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                            <label class="counter" id="active_avg_sales_per_stock"></label>
                                        </h3>
                                        <p class="text-muted">{{trans('executive_summary.active_user.avg_sales_per_store')}}</p>
                                    </div>
                                </div>
                
                                <div class="col-lg-6 col-sm-6">
                                    <div class="card-box text-center" data-toggle="tooltip" title="{{trans('executive_summary.tooltip.active_user.avg_sales_per_store_per_day', ['type' => 'Penjualan Produk'])}}">
                                        <p class="glyphicon glyphicon-info-sign info-icon-tooltip"></p>
                                        <h3 class="m-t-0">
                                            <i class="fa fa-money" aria-hidden="true" style="color: #006400;"></i>
                                            <label class="counter" id="active_avg_sales_per_stock_per_day"></label>
                                        </h3>
                                        <p class="text-muted">{{trans('executive_summary.active_user.avg_sales_per_store_per_day')}}</p>
                                    </div>
                                </div>
                        	</div>
                        </div>
                        
                        <br>
                        
                        <!-- Performing Warung -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.best_selling.title')}}</strong></h4>
                        </div>
                        <div class="row box-info">
                        	<div class="col-lg-6">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent-popular-product-1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="vertical-align: middle">No.</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.sku')}}</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.product_name')}}</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.qty')}}</th>
                                                <th class="text-right" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.amount')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        	<div class="col-lg-6">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent-popular-product-2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="vertical-align: middle">No.</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.sku')}}</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.product_name')}}</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.qty')}}</th>
                                                <th class="text-right" style="vertical-align: middle">{{trans('executive_summary.purchase_popwarung.amount')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <br>
                        
                        <!-- Performing Agent -->
                        <div class="header-title-custom" style="margin-bottom: 10px">
                        	<h4 class="m-b-20" style="padding-top: 5px"><strong>{{trans('executive_summary.top_product_sales.title')}}</strong></h4>
                        </div>
                        
                        <div class="row box-info">
                        	<div class="col-lg-6">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent-top-performing-agent-1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="vertical-align: middle">No.</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popagent.locker_id')}}</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popagent.locker_name')}}</th>
                                                <th class="text-right" style="vertical-align: middle">{{trans('executive_summary.purchase_popagent.amount')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        	<div class="col-lg-6">
                                <div class="table-responsive div_content">
                                    <table id="gridcontent-top-performing-agent-2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="text-center" style="vertical-align: middle">No.</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popagent.locker_id')}}</th>
                                                <th class="text-center" style="vertical-align: middle">{{trans('executive_summary.purchase_popagent.locker_name')}}</th>
                                                <th class="text-right" style="vertical-align: middle">{{trans('executive_summary.purchase_popagent.amount')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    
    {{-- NUMERATOR --}}
    <script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    {{-- Morris Chart --}}
	<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 15;
        var isBoxHide = true;
        var userTypes = {!! json_encode($user_types) !!};
        var cities =  {!! json_encode($cities) !!};
        var executed = false;
        var top_performing_agent_dataSet = [];
        var lineChart = null;
        
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });

        $(function(){

            $('.select2').select2();

            $(".select2-user-type").select2({
                  data: userTypes
            }).val('warung').trigger('change')
            ;

            $(".select2-city").select2({
                  data: cities
            })//.val({!! $city !!}).trigger('change')
            ;

            lineChart = new Morris.Line({
                element				: 'dashboard-line-stacked',
                xkey				: 'date',
                ykeys				: ['all_trx_type','cod', 'deposit'],
                ymin				: 0,
                labels				: ['All','COD', 'Deposit'],
                fillOpacity			: ['0.1'],
                pointFillColors		: ['#ffffff'],
                pointStrokeColors	: ['#999999'],
                behaveLikeLine		: true,
                gridLineColor		: '#eef0f2',
                hideHover			: 'auto',
                lineWidth			: '3px',
                pointSize			: 0,
                parseTime			: false,
                xLabelAngle			: 60,
                resize				: true, //defaulted to true
                lineColors			: ['#458bc4', '#23b195', '#f98009'],
                gridIntegers		: true
            });

            dashboard();
            
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                dashboard();
            });
        });

        function dashboard(){
            showLoading('.box-filter', 'box-filter');
            showLoading('.box-info', 'box-info');
            
            $.ajax({
                url: "{{ route('point-of-sales-summary') }}",
                data: {
                    _token                  : '{{ csrf_token() }}',
                    city                    : $('#city').val() ? $('#city').val() : 'all',
                    daterange_transaction 	: transactionDateRange,
                    user_type             	: $("#userType").val() ? $("#userType").val() : 'all',
                },
                type: 'GET',
                success: function(data){
                    hideLoading('.box-filter', 'box-filter');
                    hideLoading('.box-info', 'box-info');
                	refresh_dasboard(data.payload);
                	products = data.payload.popular_product;
                	count = 0;
                	removeField('gridcontent-popular-product-1');
                	removeField('gridcontent-popular-product-2');
                	removeField('gridcontent-top-performing-agent-1');
                	removeField('gridcontent-top-performing-agent-2');
                	
                	$.each(products, function(index, value){
                    	if(count < 10){
                            count++;
                            $('#gridcontent-popular-product-1 > tbody:last-child').append(
                                    '<tr style="height: 60px">' +
                                    '<td style="width: 2%;vertical-align: middle" class="text-center">'+products[index].no+'</td>' +
                                    '<td style="width: 25%;vertical-align: middle">'+products[index].sku+'</td>' +
                                    '<td style="width: 43%;vertical-align: middle">'+products[index].product_name+'</td>' +
                                    '<td style="width: 10%;vertical-align: middle" class="text-center">'+products[index].purchased+'</td>' +
                                    '<td style="width: 20%;vertical-align: middle" class="text-right">'+formatPriceTopPurchased(products[index].amount)+'</td>' +
                                    '</tr>'
                            );
                    	} else {
                            $('#gridcontent-popular-product-2 > tbody:last-child').append(
                                    '<tr style="height: 60px">' +
                                    '<td style="width: 2%;vertical-align: middle" class="text-center">'+products[index].no+'</td>' +
                                    '<td style="width: 25%;vertical-align: middle">'+products[index].sku+'</td>' +
                                    '<td style="width: 43%;vertical-align: middle">'+products[index].product_name+'</td>' +
                                    '<td style="width: 10%;vertical-align: middle" class="text-center">'+products[index].purchased+'</td>' +
                                    '<td style="width: 20%;vertical-align: middle" class="text-right">'+formatPriceTopPurchased(products[index].amount)+'</td>' +
                                    '</tr>'
                            );
                    	}
                	});
                	
                	popwarung = data.payload.popular_popwarung;
                	count = 0;
                	$.each(popwarung, function(index, value){
                    	if(count < 5){
                            count++;
                            $('#gridcontent-top-performing-agent-1 > tbody:last-child').append(
                                    '<tr style="height: 60px">' +
                                    '<td style="width: 4%;vertical-align: middle" class="text-center">'+popwarung[index].no+'</td>' +
                                    '<td style="width: 32%;vertical-align: middle">'+popwarung[index].locker_id+'</td>' +
                                    '<td style="width: 32%;vertical-align: middle">'+popwarung[index].name+'</td>' +
                                    '<td style="width: 32%;vertical-align: middle" class="text-right">'+formatPriceTopPurchased(popwarung[index].amount)+'</td>' +
                                    '</tr>'
                            );
                    	} else {
                            $('#gridcontent-top-performing-agent-2 > tbody:last-child').append(
                                    '<tr style="height: 60px">' +
                                    '<td style="width: 4%;vertical-align: middle" class="text-center">'+popwarung[index].no+'</td>' +
                                    '<td style="width: 32%;vertical-align: middle">'+popwarung[index].locker_id+'</td>' +
                                    '<td style="width: 32%;vertical-align: middle">'+popwarung[index].name+'</td>' +
                                    '<td style="width: 32%;vertical-align: middle" class="text-right">'+formatPriceTopPurchased(popwarung[index].amount)+'</td>' +
                                    '</tr>'
                            );
                    	}
                	});

                	lineChart.options['ymax'] = data.payload.y_max+1;
                	lineChart.setData(data.payload.chart_line_summary_trx);
                }
            });
        }

        function removeField(tableId){
        	        
	    	var rowCount = $('#'+tableId+' tr').length;
	    	for(i = 1; i < rowCount; i++){
	    		document.getElementById(tableId).deleteRow(1);
	    	}
	        
	    }

        function refresh_dasboard(data){
            // Product Stock Purchase
            $('#penjualan').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.popshop,
            	onComplete	: function(){
                	$('#penjualan').html(formatPrice(data.popshop));
            	}
            });
            
            $('#penjualan_cepat').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.fast_sales,
            	onComplete	: function(){
                	$('#penjualan_cepat').html(formatPrice(data.fast_sales));
            	}
            });
            
            $('#total').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.total_transaction,
            	onComplete	: function(){
                	$('#total').html(formatPrice(data.total_transaction));
            	}
            });
            
            $('#active_user').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.active_user
            });

            // Average by Verified User
            $('#verified_avg_sales_per_stock').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.avg_sales_per_store_per_verified_user,
            	onComplete	: function(){
                	$('#verified_avg_sales_per_stock').html(formatPrice(data.avg_sales_per_store_per_verified_user));
            	}
            });
            $('#verified_avg_sales_per_stock_per_day').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.avg_sales_per_store_per_verified_user_per_day,
            	onComplete	: function(){
                	$('#verified_avg_sales_per_stock_per_day').html(formatPrice(data.avg_sales_per_store_per_verified_user_per_day));
            	}
            });

            // Average by Active User
            $('#active_avg_sales_per_stock').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.avg_sales_per_store_per_active_user,
            	onComplete	: function(){
                	$('#active_avg_sales_per_stock').html(formatPrice(data.avg_sales_per_store_per_active_user));
            	}
            });
            $('#active_avg_sales_per_stock_per_day').numerator({ 
                easing		: 'linear', 
                duration	: 1000, 
                toValue		: data.avg_sales_per_store_per_active_user_per_day,
            	onComplete	: function(){
                	$('#active_avg_sales_per_stock_per_day').html(formatPrice(data.avg_sales_per_store_per_active_user_per_day));
            	}
            });
            
        }
        
        function formatPrice(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return 'Rp '+val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
        function formatPriceTopPurchased(value) {
            var val = (value/1).toFixed(0).replace('.', ',')
            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
    </script>
    
    {{-- Date Range Picker --}}
    <script type="text/javascript">
    	var transactionDateRange = '';
        var startDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
        var endDate = moment().format('YYYY-MM-DD');

        var startDateToShow = moment().subtract(29, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().format('DD/MM/YYYY');
        transactionDateRange = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();

            $('#daterange-transaction').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }
    </script>
@stop