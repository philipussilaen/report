@extends('layout.main')

@section('title')
    Email Report
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <!-- Sweet Alert -->
    <link href="{{ asset('plugins/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('pageTitle')
    Email Report - Transaction Warung
@endsection

@section('pageDesc')
    Email Report - Transaction Warung
@endsection

@section('content')
<style>
/*multi email input*/
.multipleInput-container {
     border:1px #ccc solid;
     padding-bottom:0;
     cursor:text;
     font-size:13px;
     width:100%;
     height: 50px;
     overflow: auto;
     background-color: white;
     border-radius:3px;
}
 
.multipleInput-container input {
     font-size:13px;
     /*clear:both;*/
     width:150px;
     height:24px;
     border:0;
     margin-bottom:1px;
     outline: none
}
 
.multipleInput-container ul {
     list-style-type:none;
     padding-left: 0px !important;
}
 
li.multipleInput-email {
     float:left;
     margin-right:2px;
     margin-bottom:1px;
     border:1px #BBD8FB solid;
     padding:2px;
     background:#F3F7FD;
}
 
.multipleInput-close {
     width:16px;
     height:16px;
     display:block;
     float:right;
     margin:0 3px;
}
.email_search{
    width: 100% !important;
    margin: 0px;
}

.btn-custom-active-primary{
    border-color: #245580;
}

.btn-custom-active-primary:hover{
    background-color: #00a65a !important;
    color: #FFFFFF
}

.btn-custom-inactive-primary{
    border-color: #245580;
}

.btn-custom-inactive-primary:hover{
    background-color: #dd4b39 !important;
    color: #FFFFFF
}

.tip{
    font-size: x-small;
    font-style: italic;
    color: red;
    display: none;
    margin-left: 25px;
}

.panel-heading {
  padding: 0;
    border:0;
}
.panel-title>a, .panel-title>a:active{
    display:block;
    padding:15px;
    color:#555;
    font-size:16px;
    font-weight:bold;
    text-transform:uppercase;
    letter-spacing:1px;
    word-spacing:3px;
    text-decoration:none;
}
.panel-heading  a:before {
   font-family: 'Glyphicons Halflings';
   content: "\e114";
   float: right;
   transition: all 0.5s;
}
.panel-heading.active a:before {
    -webkit-transform: rotate(180deg);
    -moz-transform: rotate(180deg);
    transform: rotate(180deg);
}
.custom-swal {
    height: 50px !important;
    width: 300px !important;
    font-size: 2rem;
}

.header-title {
  margin-top: 0;
  font-weight: bold;
  border-bottom: 1px solid gainsboro;
  padding-bottom: 10px; 
}
</style>
    <section class="content">
        <!-- Create config -->
        <div class="row">
            <div class="box box-solid box-configuration">
                <div class="box-header">
                    <h4 class="m-b-20 header-title">{{trans('report_transaction.page.configuration')}}</h4>
                    <table id="grid-config" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td style="width: 30%">{{trans('report_transaction.page.id_warung')}}<span class="text-danger">*</span> </td>
                                <td style="height: 69px">
                                    <select id="id_warung" class="form-control select2 select2-warung" name="id_warung"></select>
                                    <span id="warung_tip" class="tip">Tidak boleh kosong</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">{{trans('report_transaction.page.receiver')}}<span class="text-danger">*</span> </td>
                                <td style="height: 55px">
                                    <input id="receiver" name="receiver" type="email" class="form-control select2 select2-receiver" style="width: 100%">
                                    <span id="receiver_tip" class="tip">Tidak boleh kosong dan pastikan menekan tombol spasi, koma, atau enter setelah melakukan input data</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">{{trans('report_transaction.page.period')}} </td>
                                <td style="height: 70px">
                                    <select id="id_time_period" class="form-control select2 select2-time-period" name="time_period" style="width: 105px"></select>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 30%">{{trans('report_transaction.page.hour_send')}} </td>
                                <td style="height: 0px;">
                                	<div class="form-group row">
                                		<div class="col-sm-2">
                                    		<select id="id_hour_send" style="width: 105px" class="form-control select2 select2-hour-send" name="hour_send"></select>
                                    	</div>
                                    	<div class="row" >
                                    		<div class="col-sm-1" style="width: 24%;margin-top: 5px;display: none;" id="config_date_label">
                                        		<span style="">Tanggal Pengiriman Report:</span>
                                        	</div>
                                    		<div class="row col-sm-2" style="display: none;"  id="config_date_select2">
                                        		<div class="row input-group" style="">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input class="form-control" placeholder="dd/mm/yyyy" type="text" id="id_date_send" name="date_send" disabled="disabled"
                                                        value="{{old('birth_date')}}" readonly="readonly" style="background-color: white;">
                                                </div>
                                            </div>
                                            <div style="margin-top: 5px">
    											<span id="warung_tip2" class="tip">Perhitungan 7 hari kebelakang sejak tanggal yang ditentukan</span>
    										</div>
										</div>
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <div class="form-group row">
                        <div class="col-sm-8">
                            <button id="submitForm" style="width: 45%" type="button" class="btn btn-primary bg-green" onclick="submitConfig()">
                                Create
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Filter list -->
        <div class="row">
            <div class="wrapper" style="background-color: transparent;">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title" style="height: 50px;">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Filter
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body" style="border: none;">
                      <div class="col-md-3"> 
                          <div class="form-group">
                              <label>{{trans('report_transaction.filter.id_warung')}}</label>
                              <div>
                                  <select id="filter_id_warung" class="form-control" name="filter_warung"></select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group" style="width: 100%">
                          <label>{{trans('report_transaction.filter.report_type')}}</label>
                          <div>
                            <select id="filter_id_report_type" class="form-control select2 select2-report-type" name="filter_report_type" style="width: 100%">
                                <option value="all">Semua Tipe</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group" style="width: 100%">
                          <label>{{trans('report_transaction.filter.status')}}</label>
                          <div>
                            <select id="filter_id_status" class="form-control select2 select2-status" name="filter_status" style="width: 100%">
                                <option value="all">Semua Status</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-1">
                          <div class="form-group">
                              <div style="margin-top: 25px">
                                  <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                              </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        
        <div class="row">
            <div class="box box-solid box-configuration-list">
                <div class="box-body">
                    <div class="table-responsive div_content">
                        <table id="gridcontent" class="table table-bordered table-hover">
                            <thead  style="text-align: center;">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Create At</th>
                                    <th class="text-center">Agent / Warung</th>
                                    <th class="text-center">Receipt</th>
                                    <th class="text-center">Type</th>
                                    <th class="text-center">Send Date / Time</th>
                                    <th class="text-center"></th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- Date Picker --}}
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset ('/plugins/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset ('assets/js/underscore-min.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 25;
        var isBoxHide = true;
        var timePeriod = {!! json_encode($time_period) !!};
        var hour_send = {!! json_encode($hours) !!};
        var statuses = {!! json_encode($statuses) !!};
        var email_counter = 0;
        
        $(function(){

            $("#warung_tip").hide();
            $("#warung_tip2").hide();
            $("#receiver_tip").hide();
            
            $(".select2-time-period").select2({
                data: timePeriod
            })//.val({!! $selected_time_period !!}).trigger('change')
            .on('change', function(e){
                if($('#id_time_period').val() == 'weekly'){
                    $('#id_date_send').removeAttr('disabled');
                    $("#config_date_label").show();
                    $("#config_date_select2").show();
                    $("#warung_tip2").show();
                } else if($('#id_time_period').val() == 'daily'){
                    $("#config_date_label").hide();
                    $("#config_date_select2").hide();
                    $("#warung_tip2").hide();
                } else{
                    $('#id_date_send').attr('disabled', 'disabled');
                    $("#config_date_label").hide();
                    $("#config_date_select2").hide();
                    $("#warung_tip2").hide();
                    
                    $("#id_date_send").datepicker("setDate", moment().format('DD-MM-YYYY'));
                    $(".select2-hour-send").select2({}).val('00:00').trigger('change');
                }
            });

            $(".select2-hour-send").select2({
                data: hour_send
            })//.val({!! $selected_hour !!}).trigger('change')
            ;

            $(".select2-report-type").select2({
                data: timePeriod
            })//.val({!! $selected_hour !!}).trigger('change')
            ;

            $(".select2-status").select2({
                data: statuses
            })//.val({!! $selected_hour !!}).trigger('change')
            ;

            $('.panel-collapse').on('show.bs.collapse', function () {
                $(this).siblings('.panel-heading').addClass('active');
            });
    
            $('.panel-collapse').on('hide.bs.collapse', function () {
                $(this).siblings('.panel-heading').removeClass('active');
            });
            
            $('#id_warung').select2({
                width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: "{{ url('warung/transaction/warung/getlistmasterwarung') }}",
                    method: "GET",
                    dataType: 'JSON',
                    data: function (params) {
                        var query = {
                            search: params.term || '*',
                            type: 'public'
                        }
                        return query;
                    },
                    processResults: function (data, params) {
                        if(data.isSuccess) {
                            return {
                                results: $.map(data.data.lockerData, function (item) {
                                    return {
                                        region_code: item.region_code,
                                        text: item.locker_name,
                                        id: item.locker_id
                                    }
                                })
                            };
                        }
                    }
                }
            });
            
            $('#filter_id_warung').select2({
                width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: "{{ url('warung/transaction/warung/getlistmasterwarung') }}",
                    method: "GET",
                    dataType: 'JSON',
                    data: function (params) {
                        var query = {
                            search: params.term || '*',
                            type: 'public'
                        }
                        return query;
                    },
                    processResults: function (data, params) {
                        if(data.isSuccess) {
                            return {
                                results: $.map(data.data.lockerData, function (item) {
                                    return {
                                        region_code: item.region_code,
                                        text: item.locker_name,
                                        id: item.locker_id
                                    }
                                })
                            };
                        }
                    }
                }
            });
        });

        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }
        
        (function( $ ){
             $.fn.multipleInput = function() {
                  return this.each(function() {
                       // list of email addresses as unordered list
                       $list = $('<ul id="email_list" style="margin:0px;"/>');
                       // input
                        var $input = $('<input type="email" placeholder="{{trans('report_transaction.page.place_holder_receiver')}}" id="email_search" class="email_search multiemail" />').keyup(function(event) { 

                            if(event.which == 13 || event.which == 32 || event.which == 188) {
                                if(event.which==188){
                                    var val = $(this).val().slice(0, -1);// remove space/comma from value
                                }else{
                                    var val = $(this).val(); // key press is space or comma                        
                                }                         
                                if(validateEmail(val)){
                                     // append to list of emails with remove button
                                    $list.append($('<li class="multipleInput-email"><span>' + val + '</span></li>')
                                          .append($('<a href="#" class="multipleInput-close" title="Remove"><i class="glyphicon glyphicon-remove-sign"></i></a>')
                                               .click(function(e) {
                                                    $(this).parent().remove();
                                                    e.preventDefault();
                                               })
                                          )
                                    );
                                    $(this).attr('placeholder', '');
                                    // empty input
                                    $(this).val('');
                                } else {
                                    alert('Please enter valid email id, Thanks!');
                                }
                            }
                       }).keydown(function (event){
                            var val = $(this).val().slice(0, -1);
                            if(val.length > 0 && event.which == 9) {
                                if(validateEmail(val)){
                                    // append to list of emails with remove button
                                    $list.append($('<li class="multipleInput-email"><span>' + val + '</span></li>')
                                         .append($('<a href="#" class="multipleInput-close" title="Remove"><i class="glyphicon glyphicon-remove-sign"></i></a>')
                                              .click(function(e) {
                                                   $(this).parent().remove();
                                                   e.preventDefault();
                                              })
                                         )
                                    );
                                    $(this).attr('placeholder', '');
                                    // empty input
                                    $(this).val('');
                                } else {
                                    alert('Please enter valid email id, Thanks!');
                                }
                            }
                       });
                       // container div
                       var $container = $('<div class="multipleInput-container" />').click(function() {
                            $input.focus();
                       });
                       // insert elements into DOM
                       $container.append($list).append($input).insertAfter($(this));
                       return $(this).hide();
                  });
             };
        })( jQuery );
        $('#receiver').multipleInput();

        $('#gridcontent').DataTable({
            'paging'        : true,
            'lengthChange'  : false,
            'ordering'      : false,
            'info'          : true,
            'autoWidth'     : false,
            "processing"    : true,
            "serverSide"    : true,
            'searching'     : false,
            "pageLength"    : limit,
            "ajax": {
                "url": "{{ route('report-config-list') }}",
                "data": function ( d ) {
                    var info            = $('#gridcontent').DataTable().page.info();                        
                    d.id_warung         = $('#filter_id_warung').val();
                    d.report_type       = $("#filter_id_report_type").val();
                    d.status            = $("#filter_id_status").val();
                    d.start             = info.start;
                    d.limit             = limit;
                },
                "dataSrc": function(json){
                    json.draw = json.payload.draw;
                    json.recordsTotal = json.payload.count;
                    json.recordsFiltered = json.payload.count;

                    return json.payload.data;
                }
            },
                            
            "columnDefs" : [
                { "targets": 0, "data": 'no', "className": "text-center", "width" : "3%" },
                { "targets": 1, "data": "created_at", "className": "text-center", "width" : "11%" },
                { "targets": 2, "data": "locker_name", "className": "text-center", "width" : "15%" },
                { "targets": 3, "data": "receiver", "className": "text-center", "width" : "15%" },
                { "targets": 4, "data": "period", "className": "text-center", "width" : "5%" },
                { "targets": 5, "data": "send_date", "className": "text-center", "width" : "11%" },
                { "targets": 6, "data": null, "className": "text-center", "width" : "20%",
                    "render": function ( data, type, row, meta ) {
                        var classActiveButton  = data.is_active == 1 ? 'class="btn btn-primary bg-green" disabled="disabled"' : 'class="btn btn-custom-active-primary"'
                        var classInActiveButton  = data.is_active == 0 ? 'class="btn btn-primary bg-red" disabled="disabled"' : 'class="btn btn-custom-inactive-primary"'
                        return '<button id="activate_'+data.id+'" '+classActiveButton+' onclick="activate('+data.id+')">Active</button>' +
                        ' <button id="deactivated"'+data.id+'" '+classInActiveButton+' onclick="deactivate('+data.id+')">Inactive</button>';
                    } 
                },
                { "targets": 7, "data": null, "className": "text-center", "width" : "5%",
                    "render": function ( data, type, row, meta ) {
                        return '<button class="fa fa-trash fa-2x" aria-hidden="true"  onclick="delete_config('+data.id+')" '+
                            'style="border-color:transparent; background-color:transparent;" id="delete_'+data.id+'"></button>';
                    } 
                }
            ]
        });
        
        $('#id_btn_filter').on('click',function(e) {
            e.stopImmediatePropagation();
            $('#gridcontent').DataTable().ajax.reload();
        });

    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        $("#id_date_send").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          startDate: "-29d",
          todayHighlight: true
        }).datepicker("setDate", moment().format('DD-MM-YYYY'));

        function submitConfig() {

            $("#warung_tip").hide();
            $("#receiver_tip").hide();
            
            var email_list = $('#email_list').find('li').filter(
                    function() {
                        return $(this).find('ul').length === 0;
                    }).map(function(i, e) {
                        return $(this).text();
                    }).get().join(';');

            var id_warung = $("#id_warung").val();
            
            if(!email_list || !id_warung){
                if(!email_list){
                    $("#receiver_tip").show();
                }
                if(id_warung == null){
                    $("#warung_tip").show();
                }
                return false;
            }
            $('#submitForm').attr('disabled', 'disabled');
            
            swal({
                text: "Pastikan data yang dimasukkan sudah benar",
                type: 'warning',
                customClass: 'custom-swal',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjut',
                cancelButtonText: 'Batal'
            }).then(function (confirmed) {
                var email_list = $('#email_list').find('li').filter(
                        function() {
                            return $(this).find('ul').length === 0;
                        }).map(function(i, e) {
                            return $(this).text();
                        }).get().join(';');

                var data = {
                        _token      : '{{ csrf_token() }}',
                        id_warung   : $('#id_warung').val(),
                        email_list  : email_list,
                        time_period : $('#id_time_period').val(),
                        date_send   : $('#id_date_send').val(),
                        hour_send   : $('#id_hour_send').val(),
                };
        
                return $.post('{{ route('submit-report-config') }}', data)
                    .done(function (result) {
                        swal({
                            text: 'Config berhasil ditambahkan', 
                            type: 'success',
                            customClass: 'custom-swal'
                        }).then( function() {
                            location.href = ('{{ route('report-config') }}');
                        }, function(dismiss){
                           	if(dismiss == 'overlay'){
                                location.href = ('{{ route('report-config') }}');
                           	}
                        });
                    })
                    .fail(function (request) {
                        var msg = '<ul>';
                        _.each(request.responseJSON.errors, function (item) {
                            msg = msg + '<li style="text-align:left">'+item[0]+'</li>\n';
                        }, this);
                        msg = msg + "</ul>"
                        swal({
                            text: 'Config gagal ditambahkan', 
                            type: 'error',
                            customClass: 'custom-swal'
                        });
                    })
                    .always(function () {
        
                    });
            }, function(dismiss){
                   	if(dismiss == 'cancel'){
                       	$('#submitForm').removeAttr('disabled');
                   	}
                }
            ).catch(swal.noop);
        }

        function activate(id_config){
            
            var data = {
                    _token      : '{{ csrf_token() }}',
                    id_config   : id_config,
                    update_to   : 'activate',
            };

            return $.post('{{ route('activate-report-config') }}', data)
                .done(function (result) {
                    swal({
                        text: 'Config telah diaktifkan', 
                        type: 'success',
                        customClass: 'custom-swal'
                    }).then( function() {
                        $('#gridcontent').DataTable().ajax.reload();
                    }, function(dismiss){
                       	if(dismiss == 'overlay'){
                            $('#gridcontent').DataTable().ajax.reload();
                       	}
                    });
                })
                .fail(function (request) {
                    var msg = '<ul>';
                    _.each(request.responseJSON.errors, function (item) {
                        msg = msg + '<li style="text-align:left">'+item[0]+'</li>\n';
                    }, this);
                    msg = msg + "</ul>"
                    swal('Failed to create report config', msg, 'error', );
                })
                .always(function () {
    
                });
        }

        function deactivate(id_config){
            
            var data = {
                    _token      : '{{ csrf_token() }}',
                    id_config   : id_config,
                    update_to   : 'deactivate',
            };

            return $.post('{{ route('deactivate-report-config') }}', data)
            .done(function (result) {
                swal({
                    text: 'Config telah dinonaktifkan', 
                    type: 'success',
                    customClass: 'custom-swal'
                }).then( function() {
                    $('#gridcontent').DataTable().ajax.reload();
                }, function(dismiss){
                   	if(dismiss == 'overlay'){
                        $('#gridcontent').DataTable().ajax.reload();
                   	}
                });
            })
            .fail(function (request) {
                var msg = '<ul>';
                _.each(request.responseJSON.errors, function (item) {
                    msg = msg + '<li style="text-align:left">'+item[0]+'</li>\n';
                }, this);
                msg = msg + "</ul>"
                swal('Order Failed to Create', msg, 'error', );
            })
            .always(function () {

            });
        }

        function delete_config(id_config){
            swal({
                text: "Anda akan menghapus data ini ?",
                type: 'warning',
                customClass: 'custom-swal',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Lanjut',
                cancelButtonText: 'Batal'
            }).then(function (confirmed) {
                var data = {
                        _token      : '{{ csrf_token() }}',
                        id_config   : id_config,
                        update_to   : 'remove',
                };
    
                return $.post('{{ route('remove-report-config') }}', data)
                .done(function (result) {
                    swal({
                        text: 'Config telah dihapus', 
                        type: 'success',
                        customClass: 'custom-swal'
                    }).then( function() {
                        $('#gridcontent').DataTable().ajax.reload();
                    }, function(dismiss){
                       	if(dismiss == 'overlay'){
                            $('#gridcontent').DataTable().ajax.reload();
                       	}
                    });
                })
                .fail(function (request) {
                    var msg = '<ul>';
                    _.each(request.responseJSON.errors, function (item) {
                        msg = msg + '<li style="text-align:left">'+item[0]+'</li>\n';
                    }, this);
                    msg = msg + "</ul>"
                    swal('Order Failed to Create', msg, 'error', );
                })
                .always(function () {
    
                });
            }).catch(swal.noop);
        }
    </script>
@endsection