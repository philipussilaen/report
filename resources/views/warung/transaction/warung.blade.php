@extends('layout.main')

@section('title')
    Transaction Warung
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Transaction Warung
@endsection

@section('pageDesc')
    Transaction Warung
@endsection

@section('content')
    <style>
        table.dataTable tbody td {
            vertical-align: middle;
        }
        
    </style>
    <section class="content">
        <div class="row">
            <div class="box box-solid box-filter">
                <!-- /.box-header -->
                <div id="id_box_body" class="box-body">
					<div class="row">
                        <div class="col-md-12">
    						<div class="col-md-3">
    							<div class="form-group">
    								<label>Date of Transaction</label>
    								<div class="row" style="margin-left:0px;margin-right:0px;">
    									<div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="daterange-transaction" name="dateRange-transaction" placeholder="Transaction Date" readonly="readonly" style="background-color: white;">
                                        </div>
    								</div>
    							</div>
    						</div>
    						<div class="col-md-2">
    							<div class="form-group">
    								<label>Transaction Type</label>
    								<div>
                                        <select id="transaction_type" class="form-control select2-transaction-type" name="transaction_type"></select>
    								</div>
    							</div>
    						</div>
    						<div class="col-md-3">
    							<div class="form-group">
    								<label>Warung</label>
    								<div>
    									<select id="id_warung" class="form-control" name="warungid"></select>
    								</div>
    							</div>
    						</div>
    						<div class="col-md-3">
    							<div class="form-group">
    								<label>Transaction ID</label>
                                    <div>
                                        <input id="transaction_id" class="form-control" name="transaction_id"/>
                                    </div>
    							</div>
                            </div>
    						
                            <div class="col-md-1">
                                <div class="form-group">
                                    <div style="margin-top: 25px">
                                        <a id="id_btn_filter" class="btn btn-flat btn-primary">Filter</a>
                                    </div>
                                </div>
            				</div>
        				</div>
					</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box box-solid box-transaction-table">
                <div class="box-header">
                    <div class="form-group">
                        <div class="col-md-4">
                            <h3 class="box-title">Transaction Warung</h3>
                        </div>

                        <div class="col-md-8 no-padding">
                            <button id="btn-export" type="button" class="btn btn-success btn-primary btn-sm pull-right bg-green" onclick="getAjaxTransactionDownload()" style="margin-right: 10px; width: 50px">
                                Excel
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
						<table id="gridcontent" class="table table-bordered table-hover">
							<thead>
								<tr>
                                    <th width="30">No</th>
                                    <th width="80">Date</th>
                                    <th>Name</th>
                                    <th>User</th>
                                    <th>Payment Type</th>
                                    <th>Status Transaction</th>
                                    <th>Reference</th>
                                    <th>Total Qty</th>
                                    <th>Total Price</th>
                                    <th></th>
								</tr>
							</thead>
						</table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        var start = 0;
        var limit = 25;
        var isBoxHide = true;
        var trxTypes = {!! json_encode($transaction_types) !!}

        $(function(){

            $('.select2').select2();

            $(".select2-transaction-type").select2({
				width: '100%',
          		data: trxTypes
            })
            ;
            
            $.fn.detailTransaction = function(id, lockerid) {        		
        		window.open(
                    ('{{ route('warung-detail-transaction', ['id' => '', 'lockerid' => '']) }}/' + id +'/'+lockerid),
                    '_blank' 
              	);
            };

            $('#id_warung').select2({
				width: '100%',
                minimumInputLength: 3,
                ajax: {
                    url: "{{ url('warung/transaction/warung/getlistmasterwarung') }}",
                    method: "GET",
                    dataType: 'JSON',
                    data: function (params) {
                        var query = {
                            search: params.term || '*',
                            type: 'public'
                        }
                        return query;
                    },
                    processResults: function (data, params) {
						if(data.isSuccess) {
                            return {
                                results: $.map(data.data.lockerData, function (item) {
                                    return {
                                        region_code: item.region_code,
                                        text: item.locker_name,
                                        id: item.locker_id
                                    }
                                })
                            };
						}
                    }
                }
            });
			
			$('#gridcontent').DataTable({
                                
				'paging'      : true,
				'lengthChange': false,
				'ordering'    : false,
				'info'        : true,
				'autoWidth'   : false,
				"processing": true,
				"serverSide": true,
				'searching' : false,
				
				"pageLength": limit,
				"ajax": {
					"url": "{{ url('warung/transaction/warung/getlisttransaction') }}",
					"data": function ( d ) {
						var info = $('#gridcontent').DataTable().page.info();						
						d.userid = $('#id_warung').val();
						d.daterange_transaction = transactionDateRange;
						d.transactionid = $("#transaction_id").val();
						d.transactiontype = $("#transaction_type").val();
						d.start = info.start;
						d.limit = limit;
					},
					"dataSrc": function(json){
						json.draw = json.payload.draw;
						json.recordsTotal = json.payload.count;
						json.recordsFiltered = json.payload.count;

						return json.payload.data;
					}
				},
                                
				"columnDefs" : [
					{ "targets": 0, "data": "no", "className": "text-center" },
					{ "targets": 1, "data": "date_transaction",
						"render": function ( data, type, row, meta ) {
                            var momentDateTime = moment(data);
                            return momentDateTime.format('DD-MM-YYYY HH:mm:ss');
                        }
                    },
					{ "targets": 2, "data": "locker_name", "className": "text-center" },
					{ "targets": 3, "data": "users_name", "className": "text-center" },
					{ "targets": 4, "data": "payment_type", "className": "text-center" },
					{ "targets": 5, "data": "status", "className": "text-center",
						"render": function ( data, type, row, meta ) {
							return data.toUpperCase() === 'Void'.toUpperCase() ? '<span class="label label-danger">Void</span>' : '<span class="label label-success">Success</span>';
						}
					},
					{ "targets": 6, "data": "reference", "className": "text-center" },
					{ "targets": 7, "data": "total_qty", "width": "7%", "className": "text-center" },
					{ "targets": 8, "data": "total_price", "className": "text-right",
						"render":function( data, type, row, meta ){
							var val = (data/1).toFixed(2).replace('.', ',');
							return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
						}
					},
					{ "targets": 9, "data": null, "className": "text-center",
						"render": function ( data, type, row, meta ) {
							return '<a onclick=$(this).detailTransaction("'+data.id+'","'+data.locker_id+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-external-link"></i></a>';
						}
					}
				]
			});
			
            $('#id_btn_filter').on('click',function(e) {
                e.stopImmediatePropagation();
                $('#gridcontent').DataTable().ajax.reload();
            });
        });
        
        function getAjaxTransactionDownload() {
            
           var value = collectParameters();
           
           if($("#gridcontent").DataTable().rows().count() == 0){
               alert('No Data');
           } else {

                showLoading('.box-filter', 'box-filter');
                showLoading('.box-transaction-table', 'box-transaction-table');
                
                $.ajax({
                	url: '{{ route('download-excel') }}',
                    data: {
                        _token                  : '{{ csrf_token() }}',
                        daterange_transaction   : value.dateRange,
                        id_warung               : value.id_warung,
                        transactionid           : value.transaction_id,
                        transactiontype         : value.transaction_type
                    },
                    type: 'get',
                    responseType: 'blob', // important
                    async : true,
                    success: function (response, textStatus, request) {

                        hideLoading('.box-filter', 'box-filter');
                        hideLoading('.box-transaction-table', 'box-transaction-table');
                        
                        var a = document.createElement("a");
                        a.href = response.file; 
                        a.download = response.name;
                        document.body.appendChild(a);
                        a.click();
                        a.remove();
                    },
                    error: function (ajaxContext) {
                        hideLoading('.box-filter', 'box-filter');
                        hideLoading('.box-transaction-table', 'box-transaction-table');
                        alert('Export error: '+ajaxContext.responseText);
                    }
                });
               
           }
        }
        
        function collectParameters(){
                        
            var dateRange           = transactionDateRange;
            var id_warung           = $('#id_warung').val();
            var transaction_id      = $("#transaction_id").val();
            var transaction_type    = $("#transaction_type").val();
            
            var params = [];
            
            params.push({
                token               : '{{ csrf_token() }}',
                dateRange           : dateRange,
                id_warung           : id_warung,
                transaction_id      : transaction_id,
                transaction_type    : transaction_type
            });
            
            return params[0];
        }
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        var transactionDateRange = '';
        var transactionPerformanceDateRange = '';
        var transactionNonGroupDateRange = '';
        var startDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
        var endDate = moment().format('YYYY-MM-DD');

        var startDateToShow = moment().subtract(29, 'days').format('DD/MM/YYYY');
        var endDateToShow = moment().format('DD/MM/YYYY');
        transactionDateRange = startDate + ',' + endDate;
        transactionPerformanceDateRange  = startDate + ',' + endDate;

        jQuery(document).ready(function ($) {
            dateRangeTransaction();
            performanceDateRangeTransaction();
            nonGroupDateRangeTransaction();

            $('#daterange-transaction').val(startDateToShow + ' - ' + endDateToShow);
            $('#daterange-transaction-nongroup').val(startDateToShow + ' - ' + endDateToShow);
            $('#daterange-transaction-performance').val(startDateToShow + ' - ' + endDateToShow);

        });

        function dateRangeTransaction() {
            var inputDateRangeTransaction = $('#daterange-transaction');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }

        function performanceDateRangeTransaction() {
            var inputDateRangeperformanceTransaction = $('#daterange-transaction-performance');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeperformanceTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeperformanceTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionPerformanceDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeperformanceTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionPerformanceDateRange = '';
            });
        }

        function nonGroupDateRangeTransaction() {
            var inputDateRangeNonGroupTransaction = $('#daterange-transaction-nongroup');

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeNonGroupTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeNonGroupTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionNonGroupDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeNonGroupTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionNonGroupDateRange = '';
            });
        }

    </script>
@endsection