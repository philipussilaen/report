@extends('layout.main')

@section('title')
    Form Convertion UOM
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Form Convertion UOM
@endsection

@section('pageDesc')
    Form Convertion UOM
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-left">
                <a href="{{ url('warung/catalog/convertionuom') }}" class="btn btn-flat btn-default">Back</a>
            </div>
            <div class="pull-right">
                <a id="id_save" class="btn btn-flat bg-olive">Save</a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title" style="font-weight: bold;">Form Convertion UOM</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body form_content">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1" />
                        <input type="hidden" name="uomconvertionid" />
                        <input type="hidden" name="productid" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Convertion Name *</label>
                                    <input type="text" class="form-control" name="convertionname" placeholder="Convertion Name ...">
                                </div>
                                <div class="form-group">
                                    <label>UOM Source *</label>
                                    <select id="id_uom_source" class="form-control" name="uomsourceid">
                                        <option value="0">- Choose UOM Source -</option>
                                        @foreach ($uom as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>UOM Target *</label>
                                    <select id="id_uom_target" class="form-control" name="uomtargetid">
                                        <option value="0">- Choose UOM Target -</option>
                                        @foreach ($uom as $r)
                                            <option value="{{ $r->id }}">{{ $r->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Type Rate *</label>
                                    <select class="form-control" name="targetype">
                                        <option value="0">- Choose Type Rate -</option>
                                        <option value="1">Multiply</option>
                                        <option value="2">Divide</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Value Rate *</label>
                                    <input type="text" class="form-control" name="valuerate" placeholder="Value Rate ...">
                                </div>
                                <hr style="border:1px solid #9b9b9b" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row" style="margin-left:0px;margin-right:0px;">
                                    <div class="pull-right">
                                        <a id="id_add_product" class="btn btn-flat bg-olive">Add</a>
                                    </div>
                                </div>
                                <div class="table-responsive div_content">
                                    <table id="gridcontentproduct" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th width="180">SKU</th>
                                                <th>Product</th>
                                                <th width="60"></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div id="winContentListProduct" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">List Product</h4>
                </div>
                <div class="modal-body" style="padding:25px;">
                    <div class="row">
                        <div class="pull-left">
                            <div class="input-group">
                                <input id="id_product_keyword" type="text" name="keyword" class="form-control">
                                <a class="input-group-addon"><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive div_content">
                            <table id="id_table_listproduct" class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>Product</th>
                                    <th>SKU</th>
                                    <th>Purchase Price</th>
                                    <th>Selling Price</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_save_list" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winNotifContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div id="id_content_notif" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/fileinput/bootstrap-filestyle.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            var formContent = $('#id_form_content');
            var flag = '{{$vflag}}';
            var uomconvertionid = '{{ $vconvertionid }}';
            formContent[0].reset();

            formContent.validate({
                focusCleanup: true,
                onfocusout: false,
                rules: {
                    convertionname : {required: true},
                    uomsourceid : {valueNotEquals: "0"},
                    uomtargetid : {valueNotEquals: "0"},
                    targetype : {valueNotEquals: "0"},
                    valuerate : {required: true},
                },
                highlight: function(element) {
                    $(element).closest('.form-group').addClass('has-error');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    }
                    else {
                        error.insertAfter(element);
                    }
                },
            });

            $.fn.loadGridListProduct = function(keyword) {
                showLoading('.div_content', 'div_content');

                $.ajax({
                    url: "{{ url('warung/catalog/getlistproduct') }}",
                    type: 'GET',
                    data: {
                        search: keyword
                    },
                    success: function(response){
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr data-productid="'+record.productid+'" data-name="'+record.name+'" data-sku="'+record.sku+'">';
                            html += '<td style="width: 10px"><input type="checkbox" class="styled styled-primary" name="chkQ5" value="option1" aria-label="Single checkbox One"></td>';
                            html += '<td style="width: 10px">'+(index+1)+'</td>';
                            html += '<td>'+record.name+'</td>';
                            html += '<td>'+record.sku+'</td>';
                            html += '<td>'+record.purchase_price+'</td>';
                            html += '<td>'+record.selling_price+'</td>';
                            html += '<td></td>';
                            html += '</tr>';
                        });
                        $('#id_table_listproduct > tbody').html(html);
                        hideLoading('.div_content', 'div_content');
                    }
                });

            };

            $.fn.winListProduct = function() {
                $('#winContentListProduct').modal('show');
                $('#winContentListProduct').on('hide.bs.modal', function (e) {
                    if (e.isPropagationStopped()) return;
                    e.stopPropagation();

                    $('#id_product_keyword').val('');
                });
                $.fn.loadGridListProduct('');

                // id_product_keyword
                $('#id_product_keyword').keypress(function(e) {
                    if(e.which == 13) {
                        var keyword = $(this).val();
                        $.fn.loadGridListProduct(keyword);
                    }
                });


                $('#id_btn_save_list').on('click', function(e) {
                    e.stopImmediatePropagation();

                    var tempProductID = [], tempProductName = [];

                    var grid = $('#id_table_listproduct');
                    var countChecked = 0; var resSelected = [];
                    grid.find('tr').each(function () {
                        var row = $(this);
                        if (row.find('input[type="checkbox"]').is(':checked')) {
                            var tempSelected = {
                                product_id: row.attr('data-productid'),
                                sku: row.attr('data-sku'),
                                name: row.attr('data-name'),
                            };
                            resSelected.push(tempSelected);
                            countChecked++;
                        }
                    });

                    $('#winContentListProduct').modal('hide');

                    if(countChecked > 0) {
                        var tempGridContentProduct = [];
                        $('#gridcontentproduct > tbody').find('tr').each(function (i2, rec2) {
                            tempGridContentProduct.push($(rec2).attr('data-product_id'));
                        });

                        $.each(resSelected, function(i, rec) {
                            if($.inArray(rec.product_id, tempGridContentProduct) == -1) {
                                $('#gridcontentproduct > tbody:last-child').append('<tr data-product_id="'+rec.product_id+'"><td>'+rec.sku+'</td><td>'+rec.name+'</td><td><a onclick=$.fn.removeProduct("'+rec.product_id+'") style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a></td></tr>');
                            }
                        });
                    }
                });
            };

            $.fn.removeProduct = function(id) {
                $('#gridcontentproduct > tbody:last-child').find('[data-product_id="'+id+'"]')[0].remove();
            };

            $('#id_add_product').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winListProduct();
            });

            $('#id_save').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var paramProduct = [];

                if(formContent.valid()) {
                    var grid = $('#gridcontentproduct > tbody');

                    if(grid.find('tr').length > 0) {
                        grid.find('tr').each(function (i, rec) {
                            paramProduct.push($(rec).attr('data-product_id'));
                        });

                        formContent.find('input[name="productid"]').val(paramProduct.join(','));
                        $.ajax({
                            url: "{{ url('warung/catalog/crudconvertionuom') }}",
                            type: 'POST',
                            data: formContent.serialize(),
                            success: function (response) {
                                var obj = jQuery.parseJSON(response);
                                if(obj.success) {
                                    $('#id_content_notif').html('<p>'+obj.payload.message+'</p>');
                                    $('#winNotifContent').modal('show');
                                    setTimeout(function(){
                                        $('#winNotifContent').modal('hide');
                                        window.location.href = "{{ url('warung/catalog/convertionuom') }}";
                                    }, 1000);
                                }
                                else {
                                    $('#id_content_notif').html('<p>'+obj.payload.message+'</p>');
                                    $('#winNotifContent').modal('show');
                                }
                            }
                        });
                    }
                    else {
                        $('#id_content_notif').html('<p>Product is required</p>');
                        $('#winNotifContent').modal('show');
                    }
                }
            });

            if(flag == '2') {
                showLoading('.form_content', 'form_content');
                var urlParams = "{{ url('warung/catalog/getconvertionuombyid') }}" + '/' + uomconvertionid;
                $.getJSON(
                    urlParams, '',
                    function(response) {
                        if(response.success) {
                            var objData = response.payload.data;

                            formContent.find('input[name="flag"]').val(flag);
                            formContent.find('input[name="uomconvertionid"]').val(uomconvertionid);
                            formContent.find('input[name="convertionname"]').val(objData.name);
                            $('#id_uom_source').val(objData.uom_source_id);
                            $('#id_uom_target').val(objData.uom_target_id);

                            formContent.find('select[name="targetype"]').val(objData.type_rate);
                            formContent.find('input[name="valuerate"]').val(objData.value_rate);

                            $.each(objData.products, function(i, rec) {
                                $('#gridcontentproduct > tbody:last-child').append('<tr data-product_id="'+rec.id+'"><td>'+rec.sku+'</td><td>'+rec.name+'</td><td><a onclick=$.fn.removeProduct("'+rec.id+'") style="cursor:pointer;"><i class="fa fa-fw fa-trash"></i></a></td></tr>');
                            });

                            hideLoading('.form_content', 'form_content');
                        }
                    }
                );
            }
        });
    </script>
    <style>
        .imageproduct {
            width: 100%;
            height: 100%;
        }

        .strikethrough {
            position: relative;
        }
        .strikethrough:before {
            position: absolute;
            content: "";
            left: 0;
            top: 50%;
            right: 0;
            border-top: 1px solid;
            border-color: green;
            -webkit-transform:rotate(-5deg);
            -moz-transform:rotate(-5deg);
            -ms-transform:rotate(-5deg);
            -o-transform:rotate(-5deg);
            transform:rotate(-5deg);
        }
        .price {
            margin-left: 10px;
        }
    </style>
@endsection