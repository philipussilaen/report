@extends('layout.main')

@section('title')
    Warung Product Catalog
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Warung Category
@endsection

@section('pageDesc')
    Warung Category
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <button id="id_btn_add" class="btn btn-flat bg-olive" type="button">Tambah</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Master Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
                        <table id="gridcontent" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Prefix Code</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <button id="id_btn_add_sub" class="btn btn-flat bg-olive" type="button">Tambah</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Master Sub Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_subcontent">
                        <table id="gridsubcontent" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Sub Kategori</th>
                                    <th>Prefix Code</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <button id="id_btn_add_sub_sub" class="btn btn-flat bg-olive" type="button">Tambah</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Master Sub Sub Category</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_subsubcontent">
                        <table id="gridsubsubcontent" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Sub Sub Kategori</th>
                                    <th>Prefix Code</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Kategori</h4>
                </div>
                <div class="modal-body">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag">
                        <input type="hidden" name="categoryid">
                        <input type="hidden" name="level">
                        <div class="form-group">
                            <label>Kategori</label>
                            <input type="text" name="category" class="form-control" placeholder="Nama Kategori ...">
                        </div>
                        <div class="form-group">
                            <label>Prefix Code</label>
                            <input type="text" name="prefixcode" class="form-control" placeholder="Prefix Code ...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winNotif" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Informasi</h4>
                </div>
                <div class="modal-body">
                    <p>Harap memilih data terlebih dahulu ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>	
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            var formContent = $('#id_form_content');
            formContent[0].reset();

            var vCategoryID = null;
            var vSubCategoryID = null;

            $.fn.loadGridContent = function() {
                showLoading('.div_content', 'div_content');
                $.getJSON(
                    "{{ url('warung/catalog/getlistcategory') }}", '',
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr data-categoryid="'+record.id+'">';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+record.name+'</td>';
                                html += '<td>'+record.prefix_code+'</td>';
                                html += '<td style="width: 110px;" class="td_action">';
                                    html += '<a onclick=$(this).winContent("4","'+record.id+'","'+record.parent_category_id+'","'+record.level+'","'+Base64.encode(record.name)+'","'+Base64.encode(record.prefix_code)+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a>';
                                    html += '<a onclick=$(this).winDelContent("4","'+record.id+'","'+record.parent_category_id+'","'+record.level+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
                                html += '</td>';
                            html += '</tr>';
                        });
                        $('#gridcontent > tbody').html(html);
                        $('#gridcontent').on('click', 'tbody tr td', function(e) {
                            e.stopImmediatePropagation();
                            var el = $(this)[0];
                            var elParentNode = e.target.parentNode;

                            var selected = $(elParentNode).hasClass("activated");
							
							if(el.cellIndex != 3) {
                                $('#id_btn_add_sub').show();
                                $('#id_btn_add_sub_sub').hide();

								$("#gridcontent tr").removeClass("activated");
                                $.fn.loadGridSubContent($(elParentNode).attr('data-categoryid'),'1');
                                $.fn.loadGridSubSubContent('','');								
							}
                            if (!selected) $(elParentNode).addClass("activated");							
                        });
                        hideLoading('.div_content', 'div_content');
                    }
                );
            };

            $.fn.loadGridSubContent = function(categoryid, level) {
                vCategoryID = categoryid;
                showLoading('.div_subcontent', 'div_subcontent');
                $.getJSON(
                    "{{ url('warung/catalog/getlistcategory') }}", 'categoryid='+categoryid+'&level='+level,
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr data-categoryid="'+record.id+'">';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+record.name+'</td>';
                                html += '<td>'+record.prefix_code+'</td>';
                                html += '<td style="width: 110px;">';
                                    html += '<a onclick=$(this).winContent("5","'+record.id+'","'+record.parent_category_id+'","'+record.level+'","'+Base64.encode(record.name)+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a>';
                                    html += '<a onclick=$(this).winDelContent("5","'+record.id+'","'+record.parent_category_id+'","'+record.level+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
                                html += '</td>';
                            html += '</tr>';
                        });
                        $('#gridsubcontent > tbody').html(html);

                        $('#gridsubcontent').on('click', 'tbody tr td', function(e) {
                            e.stopImmediatePropagation();
                            var el = $(this)[0];
                            var elParentNode = e.target.parentNode;

                            var selected = $(elParentNode).hasClass("activated");
                            $("#gridsubcontent tr").removeClass("activated");
                            if (!selected) $(elParentNode).addClass("activated");

                            if(el.cellIndex != 2) {
                                $('#id_btn_add_sub_sub').show();
                                $.fn.loadGridSubSubContent($(elParentNode).attr('data-categoryid'), '2');
                            }
                        });
                        hideLoading('.div_subcontent', 'div_subcontent');
                    }
                );
            };

            $.fn.loadGridSubSubContent = function(categoryid, level) {
                vSubCategoryID = categoryid;
                showLoading('.div_subsubcontent', 'div_subsubcontent');
                $.getJSON(
                    "{{ url('warung/catalog/getlistcategory') }}", 'categoryid='+categoryid+'&level='+level,
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr data-categoryid="'+record.id+'">';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+record.name+'</td>';
                                html += '<td>'+record.prefix_code+'</td>';
                                html += '<td style="width: 110px;">';
                                    html += '<a onclick=$(this).winContent("6","'+record.id+'","'+record.parent_category_id+'","'+record.level+'","'+Base64.encode(record.name)+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a>';
                                    html += '<a onclick=$(this).winDelContent("6","'+record.id+'","'+record.parent_category_id+'","'+record.level+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
                                html += '</td>';
                            html += '</tr>';
                        });
                        $('#gridsubsubcontent > tbody').html(html);
                        hideLoading('.div_subsubcontent', 'div_subsubcontent');
                    }
                );
            };

            $.fn.winContent = function(flag, categoryid, parentcategoryid, level, category, prefix_code) {
                if(flag == '2' || flag == '3') {
                    if( isEmpty(categoryid)) {
                        $('#winNotif').modal('show');
                        return;
                    }
                }
				
				formContent.validate({
					focusCleanup: true,
					onfocusout: false,
					rules: {
						category : {required: true},
						prefixcode : {required: true},
					},
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						}
						else {
							error.insertAfter(element);
						}
					},
				});
								
				console.log('flag = ' + flag);
								
                $('#winContent').modal('show');
                $('#winContent').on('hide.bs.modal', function (e) {
                    if (e.isPropagationStopped()) return;
                    e.stopPropagation();

					formContent.data('validator').resetForm();
					formContent.get(0).reset();
                    formContent.find('input[name="flag"]').val('1');
                    formContent.find('input[name="categoryid"]').val('');
                    formContent.find('input[name="level"]').val('');
                    formContent.find('input[name="prefixcode"]').attr('readonly', false);
                });

                $('#id_btn_save').on('click',function(e){
					if (e.isPropagationStopped()) return;
					e.stopPropagation();
					
					if(formContent.valid()) {
						$.ajax({
							url: "{{ url('warung/catalog/crudcategory') }}",
							type: 'POST',
							data: formContent.serialize(),
							success: function(response){
								var obj = jQuery.parseJSON(response);
								if(obj.success) {
									$('#winContent').modal('hide');

									if(flag == '1') {
										$.fn.loadGridContent();
									}
									else if(flag == '2') {
										$.fn.loadGridSubContent(categoryid,level);
									}
									else if(flag == '3') {
										$.fn.loadGridSubSubContent(categoryid,level);
									}
									else if(flag == '4') {
										$.fn.loadGridContent();
									}
									else if(flag == '5') {
										$.fn.loadGridSubContent(parentcategoryid,level);
									}
									else if(flag == '6') {
										$.fn.loadGridSubSubContent(parentcategoryid,level);
									}
								}
							}
						});						
					}
                });

                formContent.find('input[name="flag"]').val(flag);
                formContent.find('input[name="categoryid"]').val(categoryid);
                formContent.find('input[name="level"]').val(level);

                if(flag == '4' || flag == '5' || flag == '6') {
                    formContent.find('input[name="category"]').val(Base64.decode(category));
                    formContent.find('input[name="prefixcode"]').val(Base64.decode(prefix_code));
                    formContent.find('input[name="prefixcode"]').attr('readonly', true);
                }
            };

            $.fn.winDelContent = function(flag, categoryid, parentcategoryid, level) {
                $('#winDelContent').modal('show');
                $('#id_btn_del').one('click',function() {
                    $.ajax({
                        url: "{{ url('warung/catalog/delcategory') }}",
                        type: 'POST',
                        data: {
                            "_token": "{{csrf_token()}}",
                            "categoryid": categoryid,
                        },
                        success: function(response){
                            var obj = jQuery.parseJSON(response);
                            if(obj.success) {
                                $('#winDelContent').modal('hide');
                                if(flag == '4') {
                                    $.fn.loadGridContent();
                                }
                                else if(flag == '5') {
                                    $.fn.loadGridSubContent(parentcategoryid,level);
                                }
                                else if(flag == '6') {
                                    $.fn.loadGridSubSubContent(parentcategoryid,level);
                                }
                            }
                        }
                    });
                });
            };


            $.fn.loadGridContent();
            $('#id_btn_add').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winContent('1','', '', 0, '');
            });

            $('#id_btn_add_sub').hide();
            $('#id_btn_add_sub').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winContent('2',vCategoryID, '', 1, '');
            });

            $('#id_btn_add_sub_sub').hide();
            $('#id_btn_add_sub_sub').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winContent('3',vSubCategoryID, '', 2, '');
            });
        });
    </script>

    <style>
        #gridcontent .activated{
            background: rgba(38,185,154,.16);
        }
        #gridsubcontent .activated{
            background: rgba(38,185,154,.16);
        }
    </style>
@endsection