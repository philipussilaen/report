@extends('layout.main')

@section('title')
    Agent Stock
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Agent Stock
@endsection

@section('pageDesc')
    Agent Stock
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <button id="id_btn_add" class="btn btn-flat bg-olive" type="button">Tambah</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Stockopname</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
                        <table id="gridcontent" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Date Opname</th>
                                    <th>Product</th>
                                    <th>UOM</th>
                                    <th>Qty Stock</th>
                                    <th>Remark</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Stock</h4>
                </div>
                <div class="modal-body">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1">
                        <input type="hidden" name="lowuomconversion">
                        <div class="form-group">
                            <label>Date Opname</label>
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right" id="datepicker" name="dateopname">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Product</label>
                            <div class="select-group">
                                <select id="id_product" class="form-control" name="productid"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>UOM</label>
							<div>
                                <select id="id_uom" class="form-control" name="uomid"></select>							
							</div>
                        </div>
                        <div class="form-group">
                            <label>Qty Stock</label>
                            <input type="text" class="form-control" name="qtystock" placeholder="Qty Stock ...">
                        </div>
                        <div class="form-group">
                            <label>Remark</label>
                            <input type="text" class="form-control" name="remark" placeholder="Remark ...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            var formContent = $('#id_form_content');
            formContent[0].reset();

            $.fn.loadGridContent = function() {
                showLoading('.div_content', 'div_content');
                $.getJSON(
                    "{{ url('warung/catalog/getliststock') }}", '',
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr>';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+record.date_opname+'</td>';
                                html += '<td>'+record.product_name+'</td>';
                                html += '<td>'+record.product_uom+'</td>';
                                html += '<td>'+record.qty+'</td>';
                                html += '<td>'+record.description+'</td>';
                            html += '</tr>';
                        });
                        $('#gridcontent > tbody').html(html);
                        hideLoading('.div_content', 'div_content');
                    }
                );
            };

            $.fn.winContent = function(flag) {
                formContent.validate({
                    focusCleanup: true,
                    onfocusout: false,
                    rules: {
                        dateopname : {required: true},
                        qtystock : {required: true}
                    },
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if(element.parent('.input-group').length || element.parent('.select-group').length) {
                            error.insertAfter(element.parent());
                        }
                        else {
                            error.insertAfter(element);
                        }
                    },
                });

                $('#datepicker').datepicker({
                    autoclose: true,
                    format: 'yyyy-mm-dd',
                    todayHighlight: true,
                    autoclose: true,
                    toggleActive: true,
                });
                $("#datepicker").datepicker("setDate", moment().format());

                $('#id_product').select2({
                    width: '100%',
                    minimumInputLength: 3,
                    ajax: {
                        url: "{{ url('warung/catalog/getsuggestproduct') }}",
                        method: "GET",
                        data: function (params) {
                            var query = {
                                search: params.term || '*',
                                type: 'public'
                            }
                            return query;
                        },
                        processResults: function (data, params) {
                            var obj = jQuery.parseJSON(data);
                            return {
                                results: $.map(obj.payload.data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.productid,
                                        low_uom_conversion: item.low_uom_conversion
                                    }
                                })
                            };
                        }
                    }
                });

				$('#id_uom').select2({width: '100%'});

                $('#id_product').on('select2:select', function (e) {
                    e.stopImmediatePropagation();
                    
					var data = e.params.data;										
                    formContent.find('input[name="lowuomconversion"]').val(data.low_uom_conversion);															
					$('#id_uom').html('').select2({data: [{id: '', text: ''}]});
					
					$.ajax({
						type: 'GET',
						url: "{{ url('warung/catalog/getgroupproductuom') }}/" + data.id
					}).then(function (response) {
						var obj = jQuery.parseJSON(response);
						if(obj.success) {
							$.each(obj.payload.data, function(index, record) {
								var newData = {
									id: record.product_uom_id,
									text: record.product_uom
								};								
								var newOption = new Option(newData.text, newData.id, false, false);		
								$('#id_uom').append(newOption);
							});
							
						}
						
					});					
                });
				
                $('#winContent').modal('show');
                $('#winContent').on('hide.bs.modal', function (e) {
                    if (e.isPropagationStopped()) return;
                    e.stopPropagation();

                    console.log('hide');
                    $("#id_form_content").validate().resetForm();
                    formContent[0].reset();
                    formContent.find('input[name="lowuomconversion"]').val('');
                    $("#id_product").val('').trigger('change');
                    $("#id_uom").val('').trigger('change');
                });

                $('#id_btn_save').on('click',function(e) {
                    e.stopImmediatePropagation();

                    $('#id_product').rules("add", 'required');
                    if(formContent.valid()) {
                        $.ajax({
                            url: "{{ url('warung/catalog/addstockopname') }}",
                            type: 'POST',
                            data: formContent.serialize(),
                            success: function (response) {
                                var obj = jQuery.parseJSON(response);
                                if (obj.success) {
                                    $('#winContent').modal('hide');
                                    $.fn.loadGridContent();
                                }
                                else {
                                    $('#winContent').modal('hide');
                                }
                            }
                        });
                    }
                });
            };

            $.fn.loadGridContent();
            $('#id_btn_add').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winContent('1');
            });
        });
    </script>
@endsection