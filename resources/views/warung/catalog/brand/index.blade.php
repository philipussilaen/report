@extends('layout.main')

@section('title')
    Warung Brand
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Warung Brand
@endsection

@section('pageDesc')
    Warung Brand
@endsection

@section('content')
    <section class="content">
        <div class="row" style="padding: 5px 0px 5px 0px;">
            <div class="pull-right">
                <button id="id_btn_add" class="btn btn-flat bg-olive" type="button">Tambah</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Master Brand</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive div_content">
                        <table id="gridcontent" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Brand</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="winContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Brand</h4>
                </div>
                <div class="modal-body">
                    <form id="id_form_content" role="form">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="flag" value="1">
                        <input type="hidden" name="brandid">
                        <div class="form-group">
                            <label>Brand</label>
                            <input type="text" class="form-control" name="brand" placeholder="Brand ...">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winDelContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Delete</h4>
                </div>
                <div id="id_content_del" class="modal-body div_content_del">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="id_btn_del" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div id="winNotifContent" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Information</h4>
                </div>
                <div id="id_content_notif" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/validation/additional-methods.js') }}"></script>
    <script src="{{ asset('plugins/validation/jquery.validate.file.js') }}"></script>
    <script src="{{ asset('js/helper.js') }}"></script>

    <script type="text/javascript">
        $(function(){
            var formContent = $('#id_form_content');
            formContent[0].reset();

            $.fn.loadGridContent = function() {
                showLoading('.div_content', 'div_content');
                $.getJSON(
                    '{{ url('warung/catalog/getlistbrand') }}', '',
                    function(response) {
                        var html = '';
                        $.each(response.payload.data, function(index, record){
                            html += '<tr>';
                                html += '<td style="width: 10px">'+(index+1)+'</td>';
                                html += '<td>'+record.name+'</td>';
                                html += '<td style="width: 110px;">';
                                    html += '<a onclick=$(this).winContent("2","'+record.id+'","'+Base64.encode(record.name)+'") class="btn btn-flat btn-info btn-update"><i class="fa fa-fw fa-pencil"></i></a>';
                                    html += '<a onclick=$(this).winDelContent("'+record.id+'") class="btn btn-flat btn-info btn-danger"><i class="fa fa-fw fa-trash"></i></a>';
                                html += '</td>';
                            html += '</tr>';
                        });
                        $('#gridcontent > tbody').html(html);
                        hideLoading('.div_content', 'div_content');
                    }
                );
            };

            $.fn.winContent = function(flag, id, name) {
                formContent.validate({
                    focusCleanup: true,
                    onfocusout: false,
                    rules: {
                        brand : {required: true},
                    },
                    highlight: function(element) {
                        $(element).closest('.form-group').addClass('has-error');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').removeClass('has-error');
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if(element.parent('.input-group').length) {
                            error.insertAfter(element.parent());
                        }
                        else {
                            error.insertAfter(element);
                        }
                    },
                });

                $('#winContent').modal('show');
                $('#winContent').on('hide.bs.modal', function (e) {
                    formContent.data('validator').resetForm();
                    formContent.get(0).reset();
                    formContent.find('input[name="flag"]').val('1');
                    formContent.find('input[name="brandid"]').val('');
                });

                $('#id_btn_save').on('click',function(e) {
                    if (e.isPropagationStopped()) return;
                    e.stopPropagation();

                    if(formContent.valid()) {
                        $.ajax({
                            url: "{{ url('warung/catalog/crudbrand') }}",
                            type: 'POST',
                            data: formContent.serialize(),
                            success: function(response){
                                var obj = jQuery.parseJSON(response);
                                if(obj.success) {
                                    $('#winContent').modal('hide');
                                    $.fn.loadGridContent();
                                }
                                else {
                                    $('#id_content_notif').html('<p>'+obj.payload.message+'</p>');
                                    $('#winNotifContent').modal('show');
                                }
                            }
                        });
                    }
                });

                if(flag == '2') {
                    formContent.find('input[name="flag"]').val(flag);
                    formContent.find('input[name="brandid"]').val(id);
                    formContent.find('input[name="brand"]').val(Base64.decode(name));
                }
            };

            $.fn.winDelContent = function(id) {
                $('#winDelContent').modal('show');
                $.getJSON(
                    "{{ url('warung/catalog/checkproductbybrand') }}/"+id, '',
                    function(response) {
                        var message = '';
                        if(response.success) {
                            message += '<p>Anda memiliki '+response.payload.data.jml+' item product</p>';
                            $('#id_content_del').html();
                        }
                        message += '<p>Apakah anda yakin akan menghapus data ini ?</p>';
                        $('#id_content_del').html(message);

                        $('#id_btn_del').off('click').on('click',function(e) {
                            $.ajax({
                                url: "{{ url('warung/catalog/delbrand') }}",
                                type: 'POST',
                                data: {
                                    "_token": "{{csrf_token()}}",
                                    "brandid": id
                                },
                                cache: false,
                                success: function(response){
                                    var obj = jQuery.parseJSON(response);
                                    if(obj.success) {
                                        $('#winDelContent').modal('hide');
                                        $.fn.loadGridContent();
                                    }
                                }
                            });
                        });
                    }
                );
            };

            $.fn.loadGridContent();
            $('#id_btn_add').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $.fn.winContent('1','','');
            });
        });
    </script>
@endsection