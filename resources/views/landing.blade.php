@extends('layout.main')

@section('title')
	Home
@endsection

@section('css')
	<meta http-equiv="refresh" content="180">
    <style type="text/css">
        #maps {
            width: 100%;
            height: 512px;
        }
    </style>
@endsection

@section('pageTitle')
	
@endsection

@section('pageDesc')
	
@endsection

@section('content')
	{{-- Hightlight Locker And Parcel --}}
    <div class="row">
        {{-- Locker Online --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-maroon box-locker">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-maroon btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Online <strong><span class="locker-count"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-maroon" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3><span id="locker-online">0</span></h3>
                            <p><strong>Online Locker</strong></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Locker Offline --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-black text-white box-locker">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-black text-white btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Offline <strong><span class="locker-count"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-black text-white" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3><span id="locker-offline">0</span></h3>
                            <p><strong>Offline Locker</strong></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin text-white" style="color: rgba(244,244,244,0.47);"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Agent Total --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-blue text-white box-locker">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-blue text-white btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Total <strong><span class="locker-count"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-blue text-white" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3><span id="agent-total">0</span></h3>
                            <p><strong>Total Agent</strong></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Point Total --}}
        <div class="col-lg-3 col-xs-6">
            <div class="box box-solid bg-yellow text-white box-locker">
                <div class="box-header">
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <button type="button" class="btn bg-yellow text-white btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <i class="fa fa-map-marker"></i>
                    <h3 class="box-title">Point<strong><span class="locker-count"></span></strong></h3>
                </div>
                <div class="box-body" style="padding: 0px;">
                    <div class="small-box bg-yellow text-white" style="margin-bottom: 0px;">
                        <div class="inner text-white">
                            <h3><span id="point-total">0</span></h3>
                            <p><strong>Total Point</strong></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-android-pin text-white" ></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Hightlight Agent Transaction And Parcel --}}
    <div class="row">
        {{-- Parcel --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-light-blue-active box-parcel">
                <span class="info-box-icon"><i class="ion ion-cube"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Parcel</span>
                    <span class="info-box-number" id="parcel-count">0</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span id="parcel-begin-date"></span> - <span id="parcel-end-date"></span>
                    </span>
                </div>
            </div>
        </div>
        {{-- Delivery --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-light-blue-active box-delivery">
                <span class="info-box-icon"><i class="ion ion-cube"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Delivery</span>
                    <span class="info-box-number" id="delivery-count">0</span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span id="delivery-begin-date"></span> - <span id="delivery-end-date"></span>
                    </span>
                </div>
            </div>
        </div>
        {{-- Purchase --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-light-blue-active box-transaction">
                <span class="info-box-icon"><i class="ion ion-arrow-graph-up-right"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Purchase & Payment</span>
                    <span class="info-box-number">Rp <span id="purchase-sum">0</span></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span id="purchase-begin-date"></span> - <span id="purchase-end-date"></span>
                    </span>
                </div>
            </div>
        </div>
        {{-- Payment --}}
        <div class="col-lg-3 col-xs-12">
            <div class="info-box bg-light-blue-active box-transaction">
                <span class="info-box-icon"><i class="ion ion-calculator"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">TopUp</span>
                    <span class="info-box-number">Rp <span id="topup-sum"></span></span>

                    <div class="progress">
                        <div class="progress-bar" style="width: 50%"></div>
                    </div>
                    <span class="progress-description">
                        <span id="topup-begin-date"></span> - <span id="topup-end-date"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
    {{-- latest Transaction and latest parcel --}}
    <div class="row">
        {{-- Parcel --}}
        <div class="col-md-3">
            <div class="box box-default box-parcel">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Parcel</h3>
                </div>
                <div class="box-body chat" id="parcel-latest">
                    
                </div>
            </div>
        </div>
        {{-- Non Digital Product --}}
        <div class="col-md-3">
            <div class="box box-default box-transaction">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Purchase Non Digital</h3>
                </div>
                <div class="box-body chat" id="transaction-non-digital-latest">
                    
                </div>
            </div>
        </div>
        {{-- Purchase and Payment --}}
        <div class="col-md-3">
            <div class="box box-default box-transaction">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Purchase Digital</h3>
                </div>
                <div class="box-body chat" id="transaction-latest">
                    
                </div>
            </div>
        </div>
        {{-- Top Up  --}}
        <div class="col-md-3">
            <div class="box box-default box-transaction">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Top Up</h3>
                </div>
                <div class="box-body chat" id="topup-latest">
                    
                </div>
            </div>
        </div>
    </div>
    {{-- Map Section --}}
    {{-- <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="box box-solid">
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <div id="maps">
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <div class="pad box-pane-right text-black">
                                <form id="form-map-filter" class="text-center">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control" name="type">
                                            <option value="all">Semua Locker & Agent</option>
                                            <option value="locker">Loker</option>
                                            <option value="agent">Agent</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="form-control select2" name="buildingType">
                                            <option value="all">Semua Tipe</option>
                                        </select>
                                    </div>
                                    <button class="btn btn-flat btn-block" type="button" id="button-filter">Filter</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="overlay hide" id="map-loading">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        /**
         * [ajaxLocker get Locker Online, Offline, Agent Total, Point Total]
         */
        
        function ajaxLocker(){
            showLoading('.box-locker', 'box-locker');
            $.ajax({
                url: '{{ url('ajax/getLocker') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {},
                success: function(data){
                   if (data.isSuccess==true) {
                        var data = data.data;
                        $('#locker-online').html(data.lockerOnline);
                        $('#locker-offline').html(data.lockerOffline);
                        $('#agent-total').html(data.agentTotal);
                        $('#point-total').html(data.pointTotal);
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getLocker");
            })
            .fail(function() {
                console.log("error - getLocker");
            })
            .always(function() {
                hideLoading('.box-locker', 'box-locker');
                console.log("complete - getLocker");
                console.log('===================')
            });   
        }

        var countParcelAjax = 0;
        function ajaxParcel(){
            showLoading('.box-parcel', 'box-parcel');
            $.ajax({
                url: '{{ url('ajax/getParcel') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {count:countParcelAjax},
                success: function(data){
                   if (data.isSuccess==true) {
                        var data = data.data;
                        if (data.latest != '') countParcelAjax = 1;
                        $('#parcel-count').html(data.parcelTotal);
                        $('#parcel-begin-date').html(data.beginWeekDate);
                        $('#parcel-end-date').html(data.endWeekDate);
                        $('#parcel-latest').prepend(data.latest);
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getParcel");
            })
            .fail(function() {
                console.log("error - getParcel");
            })
            .always(function() {
                hideLoading('.box-parcel', 'box-parcel');
                $('#parcel-latest').slimscroll({});
                console.log("complete - getParcel");
                console.log('===================');
            });   
        }

        var countTransactionAjax = 0;
        var countTopUpAjax = 0;
        function ajaxTransaction(){
            showLoading('.box-transaction', 'box-transaction');
            $.ajax({
                url: '{{ url('ajax/getTransaction') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {countTransaction:countTransactionAjax,countTopUp:countTopUpAjax},
                success: function(data){
                   if (data.isSuccess==true) {
                        var data = data.data;
                        if (data.latestDigital != '') {countTransactionAjax = 1;}
                        if (data.latestTopUp != '') {countTopUpAjax = 1;}

                        $('#purchase-sum').html(numberWithCommas(data.purchaseTransactionSum));
                        $('#purchase-begin-date').html(data.beginWeekDate);
                        $('#purchase-end-date').html(data.endWeekDate);
                        $('#topup-sum').html(numberWithCommas(data.topupTransactionSum));
                        $('#topup-begin-date').html(data.beginWeekDate);
                        $('#topup-end-date').html(data.endWeekDate);
                        $('#transaction-latest').prepend(data.latestDigital);
                        $('#topup-latest').prepend(data.latestTopUp);   
                        $('#transaction-non-digital-latest').prepend(data.latestShop);           
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    // console.log(data);
                }
            })
            .done(function() {
                // console.log("success - getTransaction");
            })
            .fail(function() {
                console.log("error - getTransaction");
            })
            .always(function() {
                hideLoading('.box-transaction', 'box-transaction');
                $('#transaction-latest').slimscroll({});
                $('#transaction-non-digital-latest').slimscroll({});
                $('#topup-latest').slimscroll({});
                console.log("complete - getTransaction");
                console.log('===================')
            });   
        }

        var countDeliveryAjax=0;
        function ajaxDelivery(){
            showLoading('.box-delivery', 'box-delivery');
            $.ajax({
                url: '{{ url('ajax/getDelivery') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {count:countParcelAjax},
                success: function(data){
                   if (data.isSuccess==true) {
                        var data = data.data;
                        if (data.latest != '') countParcelAjax = 1;
                        $('#delivery-count').html(data.parcelTotal);
                        $('#delivery-begin-date').html(data.beginWeekDate);
                        $('#delivery-end-date').html(data.endWeekDate);
                        $('#delivery-latest').prepend(data.latest);
                    } else {
                        alert(data.errorMsg);
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                }
            })
            .done(function() {
                console.log("success - getDelivery");
            })
            .fail(function() {
                console.log("error - getDelivery");
            })
            .always(function() {
                hideLoading('.box-delivery', 'box-delivery');
                $('#delivery-latest').slimscroll({});
                console.log("complete - getDelivery");
                console.log('===================');
            });   
        }

        ajaxLocker();
        ajaxParcel();
        ajaxTransaction();
        // ajaxDelivery();

        jQuery(document).ready(function ($) {
            var ajax_call = function () {
                ajaxLocker();
                ajaxParcel();
                ajaxTransaction();
                // ajaxDelivery();
            };

            var interval = 1000 * 180 * 1;
            setInterval(ajax_call, interval);
        });
    </script>
    {{-- <script type="text/javascript">
        var map;
        var locationMap = [];
        var markers = Array();

        function initMap() {

            var map = new google.maps.Map(document.getElementById('maps'), {
              zoom: 11,
              // center: {lat: -3.8752905, lng: 117.2135979}
              center : {lat:-6.2487563,lng:106.8301447}
            });

            var imageList = [
                {
                    url: '{{ asset('assets/img/map/marker-waiting.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('assets/img/map/marker-online.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('assets/img/map/marker-offline.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('assets/img/map/marker-waiting-blue.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                }
            ];
            var infoWin = new google.maps.InfoWindow();
            var markers = locationMap.map(function(location, i) {
                var html = '<div align="center">';
                html += '<strong style="color:#FF6300;font-size:15px;">'+location.locker_name+'</strong><br>'+location.locker_id+'<br>';
                html += '<strong>'+location.address+'</strong><br>';
                html += '<a href="{{ url('dashboard/agent/detail') }}/'+location.locker_id+'"><button class="btn btn-xs btn-primary">Detail</button></a>';
                html+='</div>';

                var marker = new google.maps.Marker({
                    position: location,
                    icon : imageList[location.map_icon],
                    map: map,
                });
                google.maps.event.addListener(marker, 'click', function(evt) {
                    infoWin.setContent(html);
                    infoWin.open(map, marker);
                })
                return marker;
            }); 
            // Add a marker clusterer to manage the markers.
            //var markerCluster = new MarkerClusterer(map, markers,{imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        }
    </script> --}}

{{-- <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script> --}}
{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA87ADAAfFte0kA7HqOHz5UFRRiYvWSKk4&callback=initMap"></script> --}}
@endsection