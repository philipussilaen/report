@extends('layout.main')

@section('title')
    Sepulsa Product
@endsection

@section('css')
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('pageTitle')
    Sepulsa Product
@endsection

@section('pageDesc')
    List of Sepulsa Product Status and Price
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-maroon">
                <div class="inner text-white">
                    <h3 id="onlineAllCounter">{{ $closeCount }}</h3>
                    <p>Product Close</p>
                </div>
                <div class="icon">
                    <i class="ion ion-close text-white"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner text-white">
                    <h3 id="onlineAllCounter">{{ $closeManualCount }}</h3>
                    <p>Product Manual Close</p>
                </div>
                <div class="icon">
                    <i class="ion ion-close text-white"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner text-white">
                    <h3 id="allLockerCounter">{{ $openCount }}</h3>
                    <p>Product Open</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark text-white"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner text-white">
                    <h3 id="allLockerCounter">{{ $openManualCount }}</h3>
                    <p>Product Manual Open</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark text-white"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Daftar Product</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body border-radius-none">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Tipe</label>
                                    <select class="form-control select2" name="type">
                                        <option value="">Semua Tipe</option>
                                        @foreach ($typeList as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['type'])) {
                                                    if ($element == $param['type']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element }}" {{ $selected }}>{{ $element }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Operator</label>
                                    <select class="form-control select2" name="operator">
                                        <option value="">Semua Operator</option>
                                        @foreach ($operatorList as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['operator'])) {
                                                    if ($element == $param['operator']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element }}" {{ $selected }}>{{ $element }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                             <div class="col-md-2">
                                <div class="form-group">
                                    <label>Paket Data</label>
                                    <select class="form-control select2" name="paket_data">
                                        <option value="">Pilih</option>
                                        @if($param['paket_data'] == '1')
                                            <option value="1" selected>Paket Data</option>
                                            <option value="0">Pulsa</option>
                                        @elseif($param['paket_data'] == '-1')
                                            <option value="1">Paket Data</option>
                                            <option value="0" selected>Pulsa</option>
                                        @else
                                            <option value="1">Paket Data</option>
                                            <option value="0">Pulsa</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    @if (!empty($param['label']))
                                        <input type="text" name="label" class="form-control"
                                               value="{{ $param['label'] }}">
                                    @else
                                        <input type="text" name="label" class="form-control">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control select2" name="status">
                                        <option value="">Semua Status</option>
                                        @foreach ($status as $element)
                                            @php
                                                $selected = '';
                                                if (!empty($param['status'])) {
                                                    if ($element == $param['status']) $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $element }}" {{ $selected }}>{{ $element }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Product ID</label>
                                    @if (!empty($param['product_id']))
                                        <input type="text" name="product_id" class="form-control"
                                               value="{{ $param['product_id'] }}">
                                    @else
                                        <input type="text" name="product_id" class="form-control">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-success btn-block btn-flat">Cari</button>
                                <a href="{{ url('popbox/sepulsa/updateStatus') }}"><button class="btn btn-warning btn-flat btn-block" type="button">Update Buddy</button></a>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-info btn-block btn-flat" type="button" id="btn-status">Change Status</button>
                                <button class="btn btn-danger btn-block btn-flat" type="button" id="btn-download" onclick="getDownloadData()">Download</button>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive no-padding table-bordered">   
                        <form id="form-status" method="post" action="{{ url('popbox/sepulsa/closeProduct') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="status">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" id="check-all">
                                                    Product Id
                                                </label>
                                            </div>
                                        </th>
                                        <th>Tipe</th>
                                        <th>Operator</th>
                                        <th>Paket Data</th>
                                        <th>Product Names</th>
                                        <th>Description</th>
                                        <th>Nominal/Value</th>
                                        <th>Sepulsa Price</th>
                                        <th>Customer Price</th>
                                        <th>Margin Price</th>
                                        <th data-priority="3">Status</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $key => $item)
                                    <tr>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="checkbox-product" name="product_id[]" value="{{ $item->product_id }}" data-label="{{ $item->label }}">
                                                    <strong>{{ $item->product_id }}</strong>
                                                </label>
                                            </div>
                                        </td>
                                        <td>{{ $item->type }}</td>
                                        <td>
                                            @php
                                                $operator = str_replace('_', ' ', $item->operator);
                                            @endphp
                                            {{ strtoupper($operator) }}
                                        </td>
                                        <td>
                                                @if($item->type == 'mobile')
                                                    @if($item->is_packet_data != 1)
                                                        <span class="label label-danger">Pulsa</span>
                                                    @else
                                                        <span class="label label-success">Paket Data</span>
                                                    @endif
                                                @endif
                                        </td>
                                        <td>{{ $item->label }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>{{ $item->nominal }}</td>
                                        <td>{{ number_format($item->price) }}</td>
                                        <td>{{ number_format($item->publish_price) }}</td>
                                        <td>
                                            @php
                                            $marginPrice = $item->publish_price - $item->price;
                                            @endphp
                                            {{ number_format($marginPrice) }}
                                        <td>
                                            @if ($item->status == 'OPEN')
                                                <span class="label label-success">OPEN</span>
                                            @elseif ($item->status == 'MANUAL_OPEN')
                                                <span class="label label-info">MANUAL OPEN</span>
                                            @elseif ($item->status == 'CLOSED')
                                                <span class="label label-danger">CLOSED</span>
                                            @elseif ($item->status == 'MANUAL_CLOSE')
                                                <span class="label label-warning">MANUAL CLOSE</span>
                                            @elseif ($item->status == 'PERMANENT_CLOSE')
                                                <span class="label bg-black text-white">PERMANENT CLOSE</span>
                                            @endif
                                        </td>
                                        <td>{{ $item->remarks  }}</td>
                                        <td>
                                            <button class="btn btn-flat btn-info btn-update" type="button" 
                                                    data-id="{{ $item->product_id }}" data-label="{{ $item->label }}"
                                                    data-description="{{ $item->description }}"
                                                    data-remarks="{{ $item->remarks }}"
                                                    data-status="{{ $item->status }}"
                                                    data-publish="{{ $item->publish_price }}">Update
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </form> 
                    </div>
                    {{ $products->appends($param)->links() }} <br>
                    <strong>Total: </strong> {{ $products->total() }}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-th"></i>
                    <h3 class="box-title">Product By Category</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Product Type</th>
                                <th>Total Product ID</th>
                            </tr>
                        </thead>
                        @foreach ($groupProduct as $element)
                            <tr>
                                <td>{{ $element->type }}</td>
                                <td>{{ $element->count }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                </div>
                <div class="modal-body">
                    <form method="post" id="form-product">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Product ID</label>
                            <input type="text" name="productId" class="form-control" readonly="true">
                        </div>
                        <div class="form-group">
                            <label>Product Name</label>
                            <input type="text" name="label" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea type="text" name="description" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Remarks</label>
                            <textarea type="text" name="remarks" class="form-control"></textarea>
                        </div>
                        @if(\App\Models\Privilege::hasAccess('popbox/sepulsa/updatePrice'))
                            <div class="form-group">
                                <label>Publish Price</label>
                                <input type="text" name="publishPrice" class="form-control"></input>
                            </div>
                        @else
                            <div class="form-group">
                                <label>Publish Price</label>
                                <input type="text" name="publishPrice" class="form-control" readonly></input>
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="OPEN">OPEN</option>
                                <option value="MANUAL_OPEN">MANUAL OPEN</option>
                                <option value="CLOSED">CLOSED</option>
                                <option value="MANUAL_CLOSE">MANUAL CLOSE</option>
                                <option value="PERMANENT_CLOSE">PERMANENT CLOSE</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Update Product</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" id="tmp-status">
                            <option value="OPEN">OPEN</option>
                            <option value="MANUAL_OPEN">MANUAL OPEN</option>
                            <option value="CLOSED">CLOSED</option>
                            <option value="MANUAL_CLOSE">MANUAL CLOSE</option>
                            <option value="PERMANENT_CLOSE">PERMANENT CLOSE</option>
                        </select>
                        <div id="list-product">
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-change-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.btn-update').on('click', function (event) {
                var data = $(this).data();
                $('input[name=productId]').val(data.id);
                $('input[name=label]').val(data.label);
                $('textarea[name=description]').val(data.description);
                $('textarea[name=remarks]').val(data.remarks);
                $('select[name=status]').val(data.status);
                $('input[name=publishPrice]').val(data.publish);
                $('#myModal').modal('show');
            });
            $('#btn-submit').on('click', function (event) {
                $('#form-product').submit();
            });
            $('#btn-status').on('click', function(event) {
                $('#modalStatus').modal('show');
                var list = '<hr>';
                $('input[type="checkbox"]:checked').each(function() {
                   var value = this.value;
                   var label = $(this).data('label');

                   list += "Product ID : "+value+"<br>";
                   list += "Name : "+label+"<hr>";
                });
                $('#list-product').html(list);
            });
            $('#btn-change-submit').on('click', function() {
                var value = $('#tmp-status').val();
                $('input[type=hidden][name=status]').val(value);
                $('#form-status').submit();
            });

            // check all trigger
            $('#check-all').on('change', function(event) {
               var checked = $(this).is(':checked');
               if (checked == true) {
                    $('input[type=checkbox][class=checkbox-product]').prop('checked', true);
               } else {
                    $('input[type=checkbox][class=checkbox-product]').prop('checked', false);
               }
            });
        });

        function getDownloadData() {

            var statuss = document.getElementsByName('status')[0].value;
            var typee = document.getElementsByName('type')[0].value;
            var operatorr = document.getElementsByName('operator')[0].value;
            var labell = document.getElementsByName('label')[0].value;
            var product_idd = document.getElementsByName('product_id')[0].value;

            // console.log(statuss);
            // console.log(typee);
            // console.log(operatorr);
            // console.log(labell);
            // console.log(product_idd);

            showLoading('.content', 'content');

            $.ajax({
                url: '{{ url('popbox/sepulsa/ajax/getSepulsaProductDownload') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                    type : typee,
                    status : statuss,
                    operator : operatorr,
                    label : labell,
                    product_id : product_idd
                },
                type: 'POST',
                dataType: 'JSON',
                async : true,
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        window.location = '{{ url("ajax/download") }}' + '?file=' + data.data.link;

                    }  else {
                        alert(data.errorMsg);
                    }

                    hideLoading('.content', 'content');
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                }
            })
                .done(function() {
                    console.log("success - download");
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                })
                .fail(function() {
                    console.log("error - download");
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                })
                .always(function() {
                    // showHideLoading(type, 'hide');
                    hideLoading('.content', 'content');
                    console.log("complete - download");
                    console.log('===================');
                });

        }

    </script>
@endsection