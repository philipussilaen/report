@extends('layout.main')

@section('title')
    Locker Delivery
@endsection

@section('css')
    {{-- Date picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('pageTitle')
    Dashboard Transaksi
@endsection

@section('pageDesc')
    Detail Transaksi
@endsection

@section('content')
    <div class="box box-widget widget-user">
        <div class="widget-user-header bg-aqua">
            <h1 class="pull-right" style="font-size: 50px;">
                @php
                    $nowDateTime = time() *1000;
                @endphp
                @if ($expressData->status == 'CUSTOMER_TAKEN')
                    <i class="ion ion-checkmark-round text-white"></i>
                @elseif ($expressData->status == 'COURIER_TAKEN')
                    <i class="ion ion-checkmark-round text-white"></i>
                @elseif ($expressData->status == 'OPERATOR_TAKEN')
                    <i class="ion ion-checkmark-round text-white"></i>
                @elseif($expressData->status == 'IN_STORE' && $nowDateTime > $expressData->overdueTime && $expressData->expressType == 'COURIER_STORE')
                    <i class="ion ion-clock text-white"></i>
                @elseif($expressData->status == 'IN_STORE')
                    <i class="ion ion-navicon-round text-white"></i>
                @endif
            </h1>
            <h3 class="widget-user-username">
                @if ($expressData->expressType == 'COURIER_STORE')
                    {{ $expressData->expressNumber }}
                @else
                    {{ $expressData->customerStoreNumber }}
                @endif
            </h3>
            <h5 class="widget-user-desc">{{ $expressData->lockerName }}</h5>
            <h5 class="description-header">
                @if ($expressData->status == 'CUSTOMER_TAKEN')
                    <span class="label label-success">Customer Taken</span>
                @elseif ($expressData->status == 'COURIER_TAKEN')
                    <span class="label label-warning">Courier Taken</span>
                @elseif ($expressData->status == 'OPERATOR_TAKEN')
                    <span class="label label-warning">Operator Taken</span>
                @elseif($expressData->status == 'IN_STORE' && $nowDateTime > $expressData->overdueTime && $expressData->expressType == 'COURIER_STORE')
                    <span class="label label-info">In Store</span>
                    <span class="label label-danger">Overdue</span>
                @elseif($expressData->status == 'IN_STORE')
                    <span class="label label-info">In Store</span>
                @endif
            </h5>
        </div>
        <div class="box-footer" style="padding-top: 10px;">
            <div class="row">
                <div class="col-sm-6 border-right">
                    <div class="description-block">
                        <h5 class="description-header">
                            @if ($expressData->expressType == 'COURIER_STORE')
                                Last Mile
                            @elseif ($expressData->expressType == 'CUSTOMER_REJECT')
                                Return
                            @elseif ($expressData->expressType == 'CUSTOMER_STORE')
                                PopSend
                            @endif
                        </h5>
                        <span class="description-text">Express Type</span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="description-block">
                        <h5 class="description-header">
                            @if ($expressData->expressType == 'COURIER_STORE')
                                {{ $expressData->takeUserPhoneNumber }}
                            @else
                                {{ $expressData->storeUserPhoneNumber }}
                            @endif
                        </h5>
                        <span class="description-text">Customer Number</span>
                    </div>
                </div>
                <div class="col-sm-6 border-right">
                    <div class="description-block">
                        <h5 class="description-header">{{ date('j F Y H:i:s',($expressData->storeTime/1000)) }}</h5>
                        <span class="description-text">Store Time</span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="description-block">
                        <h5 class="description-header">{{ empty($expressData->takeTime)? "-" : date('j F Y H:i:s',($expressData->takeTime/1000)) }}</h5>
                        <span class="description-text">Take Time</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-body box-profile">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <div class="row">
                <div class="col-md-6">
                    <strong>Locker Name</strong>
                    <p class="text-muted">{{ $expressData->lockerName }}</p>

                    <strong>Express Number</strong>
                    <p class="text-muted">
                        @if ($expressData->expressType == 'COURIER_STORE')
                            {{ $expressData->expressNumber }}
                        @else
                            {{ $expressData->customerStoreNumber }}
                        @endif
                    </p>

                    <strong>Express Type</strong>
                    <p class="text-muted">{{ $expressData->expressType }}</p>

                    <strong>Customer Phone</strong>
                    <p class="text-muted">
                        @if ($expressData->expressType == 'COURIER_STORE')
                            {{ $expressData->takeUserPhoneNumber }}
                        @else
                            {{ $expressData->storeUserPhoneNumber }}
                        @endif
                    </p>

                    <strong>Status</strong>
                    <p class="text-muted">
                        @if ($expressData->status == 'CUSTOMER_TAKEN')
                            <span class="label label-success">Customer Taken</span>
                        @elseif ($expressData->status == 'COURIER_TAKEN')
                            <span class="label label-warning">Courier Taken</span>
                        @elseif ($expressData->status == 'OPERATOR_TAKEN')
                            <span class="label label-warning">Operator Taken</span>
                        @elseif($expressData->status == 'IN_STORE' && $nowDateTime > $expressData->overdueTime && $expressData->expressType == 'COURIER_STORE')
                            <span class="label label-info">In Store</span>
                            <span class="label label-danger">Overdue</span>
                        @elseif($expressData->status == 'IN_STORE')
                            <span class="label label-info">In Store</span>
                        @endif
                    </p>

                    <strong>PIN</strong>
                    {{--<p class="text-muted">{{ $expressData->validateCode }}</p>--}}
                    <p class="text-muted"> ****** </p>
                </div>
                <div class="col-md-6">
                    <strong>Store Time</strong>
                    <p class="text-muted">{{ date('Y-m-d H:i:s',($expressData->storeTime/1000)) }}</p>

                    <strong>Overdue Time</strong>
                    <p class="text-muted">{{ date('Y-m-d H:i:s',($expressData->overdueTime/1000)) }}</p>

                    <strong>Take Time</strong>
                    <p class="text-muted">{{ empty($expressData->takeTime)? "-" : date('Y-m-d H:i:s',($expressData->takeTime/1000)) }}</p>

                    <strong>Courier</strong>
                    <p class="text-muted">{{ $expressData->companyName }} / {{ $expressData->companyType }}</p>

                    <strong>Locker Number</strong>
                    <p class="text-muted">{{ $expressData->lockerNumber }} - {{ $expressData->lockerSize }}</p>

                    @isset ($expressData->storeUser_id)
                        @php
                            $userLockerDb = \App\Models\NewLocker\Users::find($expressData->storeUser_id);
                        @endphp
                        @if ($userLockerDb)
                            <strong>User Store</strong>
                            <p class="text-muted">{{ $userLockerDb->displayname }}</p>
                        @endif
                    @endisset
                    @isset ($expressData->takeUser_id)
                        @php
                            $userLockerDb = \App\Models\NewLocker\Users::find($expressData->takeUser_id);
                        @endphp
                        @if ($userLockerDb)
                            <strong>User Take</strong>
                            <p class="text-muted">{{ $userLockerDb->displayname }}</p>
                        @endif
                    @endisset
                </div>
                <div class="col-md-12">
                    <h4>SMS History</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Content</th>
                                <th>Status</th>
                                <th>Date Sent</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($smsList as $item)
                                <tr>
                                    {{--<td>{!! $item->sms_content !!}</td>--}}
                                    <td><italic> --Confidential Content-- </italic></td>
                                    <td>
                                        @if ($item->sms_status == 'SUCCESS')
                                            <span class="label label-success">SUCCESS</span>
                                        @else 
                                            <span class="label label-warning">{{ $item->sms_status }}</span>
                                        @endif
                                    </td>
                                    <td>{{ $item->sent_on }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @if ($expressData->status == 'IN_STORE')
                            <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modalChangePhone">Change Phone</button>
                        @else
                            <button class="btn btn-flat btn-info" disabled>Change Phone</button>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @if ($expressData->status == 'IN_STORE' && substr($expressData->expressNumber, 0, 3) != 'PDS')
                            <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modalResendSMS">Re-Send SMS</button>
                        @else
                            <button class="btn btn-flat btn-info" disabled>Re-Send SMS</button>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        <?php
                        if(substr($expressData->takeUserPhoneNumber, 0, 2) == '08') {
                            // indonesia
                            $phoneNumber = '62'.substr($expressData->takeUserPhoneNumber, 1, strlen($expressData->takeUserPhoneNumber));
                        } else if(substr($expressData->takeUserPhoneNumber, 0, 2) == '01') {
                            // malaysia
                            $phoneNumber = '60'.substr($expressData->takeUserPhoneNumber, 1, strlen($expressData->takeUserPhoneNumber));
                        } else {
                            $phoneNumber = $expressData->takeUserPhoneNumber;
                        }
                        $whatsappMessage = 'Kode buka: *'.$expressData->validateCode.'* order no: *'.$expressData->expressNumber.'* sudah tiba di PopBox@'.$expressData->lockerName.'. Valid s/d: '.date('d/m/Y H:i',($expressData->overdueTime/1000)).' bit.do/getpopbox';
                        ?>
                        @if ($expressData->status == 'IN_STORE' && substr($expressData->expressNumber, 0, 3) != 'PDS')
                            <a href="https://api.whatsapp.com/send?phone={{ $phoneNumber }}&text={{ $whatsappMessage }}" class="btn btn-flat btn-info">Re-Send via Whatsapp</a>
                        @else
                            <button class="btn btn-flat btn-info" disabled>Re-Send via Whatsapp</button>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @if ($expressData->status == 'IN_STORE' && substr($expressData->expressNumber, 0, 3) != 'PDS' && $expressData->expressType == 'COURIER_STORE')
                            <button class="btn btn-flat btn-warning" data-toggle="modal" data-target="#modalChangeOverdue">Change Overdue</button>
                        @else
                            <button class="btn btn-flat btn-warning" disabled>Change Overdue</button>
                        @endif
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                        @php
                            $isValid = false;
                            if (!empty($expressData->takeTime)) {
                                if ($nowDateTime - $expressData->takeTime < 3600000) {
                                    $isValid = true;
                                }
                            }
                        @endphp
                        @if ($expressData->status == 'IN_STORE')
                            <button class="btn btn-flat btn-danger" data-toggle="modal" data-target="#modalOpenDoor">Open Locker Door</button>
                        @elseif(!empty($expressData->takeTime) && $isValid)
                            <button class="btn btn-flat btn-danger" data-toggle="modal" data-target="#modalOpenDoor">Open Locker Door</button>
                        @else
                            <button class="btn btn-flat btn-danger" disabled>Open Locker Door</button>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($expressData->status == 'IN_STORE')
        <div class="modal fade" id="modalChangePhone">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Change Phone</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form-change" action="{{ url('popbox/locker/delivery/postChangePhone') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="parcelId" value="{{ $expressData->id }}">
                            <div class="form-group">
                                <label>Old Phone</label>
                                <input type="text" class="form-control" disabled value="{{ $expressData->takeUserPhoneNumber }}">
                            </div>
                            <div class="form-group">
                                <label>New Phone</label>
                                <input type="text" name="phone" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-change">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($expressData->status == 'IN_STORE')
        <div class="modal fade" id="modalResendSMS">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">ReSend SMS</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form-resend" action="{{ url('popbox/locker/delivery/postResendSMS') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="parcelId" value="{{ $expressData->id }}">
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" disabled value="{{ $expressData->takeUserPhoneNumber }}">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-resend">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if ($expressData->status == 'IN_STORE' &&  $expressData->expressType == 'COURIER_STORE')
        <div class="modal modal-warning fade" id="modalChangeOverdue">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Change Overdue</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form-overdue" action="{{ url('popbox/locker/delivery/postChangeOverdue') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="parcelId" value="{{ $expressData->id }}">
                            <div class="form-group">
                                <label>Overdue Date</label>
                                <input type="text" name="overdueDate" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-overdue">Send SMS</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="modal modal-danger fade" id="modalOpenDoor">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Open Door</h4>
                </div>
                <div class="modal-body">
                    <form id="form-open" action="{{ url('popbox/locker/delivery/postOpenDoor') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="parcelId" value="{{ $expressData->id }}">
                        <div class="form-group">
                            <label>Locker</label>
                            <input type="text" class="form-control" disabled value="{{ $expressData->lockerName }}">
                        </div>
                        <div class="form-group">
                            <label>Locker Size</label>
                            <input type="text" class="form-control" disabled value="{{ $expressData->lockerSize }}">
                        </div>
                        <div class="form-group">
                            <label>Locker Number</label>
                            <input type="text" class="form-control" disabled value="{{ $expressData->lockerNumber }}">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="btn-open">Open Door</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- DatePicker --}}
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            // Submit form change
            $('#btn-change').on('click', function() {
                $('#form-change').submit();
            });

            // Submit  form resend
            $('#btn-resend').on('click', function() {
                $('#form-resend').submit();
            });

            $('#btn-overdue').on('click', function() {
                $('#form-overdue').submit();
            });

            $('#btn-open').on('click', function() {
                $('#form-open').submit();
            });

            /*Modal Change overdue on open*/
            $('#modalChangeOverdue').on('shown.bs.modal', function() {
                $('input[name=overdueDate]').datepicker({
                    format: 'yyyy/mm/dd',
                    autoclose: true,
                    startDate :"0d"
                })
            });
        });
    </script>
@endsection