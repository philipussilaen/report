@extends('layout.main')

@section('title')
    Detail Branch
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    <style type="text/css">
        #maps {
            width: 100%;
            height: 384px;
        }
        .view-label {
            margin-top: 7px;
        }
        .center {
            text-align: center;
        }
        .margin-bottom-10 {
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('pageTitle')
    Branch
@endsection

@section('pageDesc')
    Detail Branch
@endsection

@section('content')
    <section class="content">
        <div class="row margin-bottom-10">
            <div class="col-md-12 center">
                <button class="btn btn-flat btn-info btn-small btn-update"
                        data-id = "{{ $branch->id }}"
                        data-location = "{{ $branch->location }}"
                >
                    EDIT
                </button>
                <button class="btn btn-flat btn-danger btn-small"  data-toggle="modal" id="btn-add-branch" data-target="#modal-delete-branch">
                    DELETE
                </button>
                <a href="{{ url('/popexpress/branches') }}" class="btn btn-flat btn-success btn-small">BACK</a>
            </div>
        </div>
        <div class="row margin-bottom-10">
            <div class="col-lg-3">
                <div class="box-body box-profile bg-aqua">
                    <span class="info-box-icon bg-aqua">
                        <i class="ion ion-android-pin text-white"></i>
                    </span>
                    <h3 style="vertical-align: middle; line-height: 30px;" class="widget-user-username" id="code_title">{{ $branch->code }}</h3>
                    <h4 id="name_title">{{ $branch->name }}</h4>
                </div>
                <div class="box box-widget widget-user">
                    <div class="box-footer" style="padding-top: 10px;">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" method="POST">
                                    <input type="hidden" id="branch_id" value="{{ $branch->id }}">
                                    <input type="hidden" id="location_id_label" value="{{ $branch->location }}">
                                    <input type="hidden" id="type_label_data" value="{{ $branch->type }}">
                                    <input type="hidden" id="last_sync_label" value="{{ $branch->last_sync }}">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Code:</label>
                                        <div class="col-md-8 view-label" id="code_label">
                                            {{ $branch->code }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Name:</label>
                                        <div class="col-md-8 view-label" id="name_label">
                                            {{ $branch->name }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Type:</label>
                                        <div class="col-md-8 view-label" id="type_label">
                                            {{ strtoupper($branch->type) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Location:</label>
                                        <div class="col-md-8 view-label" id="location_label">
                                            {{ $branch->destination }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Address:</label>
                                        <div class="col-md-8 view-label" id="address_label">
                                            {{ $branch->address }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Timezone:</label>
                                        <div class="col-md-8 view-label" id="timezone_label">
                                            {{ $branch->timezone }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Latitude:</label>
                                        <div class="col-md-8 view-label" id="latitude_label">
                                            {{ $branch->latitude }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Longitude:</label>
                                        <div class="col-md-8 view-label" id="longitude_label">
                                            {{ $branch->longitude }}
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="box box-primary">
                    <div id="map" style="height: 500px;"></div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2>List Pickup Branches</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modal-filter">Filter</button>
                        <button class="btn btn-flat btn-warning" id="btn-filter-search">Reset Filter</button>
                        <div class="pull-right">
                            <button class="btn btn-flat btn-success pull-right" id="btn-save-branch" disabled="disabled">Save</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover cell-border" id="data-table" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Origin</th>
                                    <th>Airport Code</th>
                                    <th>Detail Code</th>
                                    <th>Province</th>
                                    <th>County</th>
                                    <th>District</th>
                                    <th>Locker</th>
                                    <th>Pickup</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <div class="modal fade" id="modal-filter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Origin</label>
                                <select class="form-control" name="origin_id" id="origin_id_filter" required>
                                    <option value="">All</option>
                                    @foreach($origins as $origin)
                                        <option value="{{ $origin->id }}">{{ $origin->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Airport Code</label>
                                <input type="text" name="airport_code" id="airport_code_filter" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Detail Code</label>
                                <input type="text" name="detail_code" id="detail_code_filter" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Province</label>
                                <input type="text" name="province" id="province_filter" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Locker</label>
                                <select class="form-control" name="is_locker"  id="is_locker_filter" required>
                                    <option value="">Any</option>
                                    <option value="0">NO</option>
                                    <option value="1">YES</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Locker Name</label>
                                <select class="form-control select2" name="locker_id" id="locker_id_filter" required>
                                    <option value="">All</option>
                                    @foreach($lockerNames as $lockerName)
                                        <option value="{{ $lockerName->locker_id }}">{{ $lockerName->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>County</label>
                                <input type="text" name="county" id="county_filter" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>District</label>
                                <input type="text" name="district" id="district_filter" class="form-control" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-filter" onclick="doFilter()">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-delete-branch">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Delete Branch</h4>
                </div>
                <div class="modal-body">
                    <form id="form-delete" method="post" action="{{ url('/popexpress/branches/store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id_delete_branch" value="{{ $branch->id }}" class="form-control">
                        <input type="hidden" name="delete" id="delete" value="1" class="form-control">
                    </form>
                    Apakah anda yakin ingin menghapus branch ini ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="btn-delete-branch">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit-branch">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Branch</h4>
                </div>
                <div class="modal-body">
                    <form id="form-update-destination" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id_update" class="form-control">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mandatory">Code</label>
                                    <input type="text" name="code" id="code" class="form-control" required>
                                </div>
                                <div class="form-group" id="detailcodepart">
                                    <label class="mandatory">Name</label>
                                    <input type="text" name="name" id="name" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" name="latitude" id="latitude" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" name="longitude" id="longitude" class="form-control" required>
                                </div>

                                <div class="form-group" id="last_sync_div">
                                    <label>Last Sync</label><br>
                                    <span id="last_sync"></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mandatory">Type</label>
                                    <select class="form-control" name="type"  id="type" required>
                                        <option value=""></option>
                                        @foreach($types as $type)
                                            <option value="{{ $type }}">{{ strtoupper($type) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Location</label>
                                    <select class="form-control select2" name="location"  id="location_data" required>
                                        <option value=""></option>
                                        @foreach($destinationNames as $key => $destinationName)
                                            <option value="{{ $destinationName->id }}">{{ strtoupper($destinationName->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Timezone</label>
                                    <select class="form-control" name="timezone"  id="timezone" required>
                                        <option value=""></option>
                                        @foreach($timezones as $timezone)
                                            <option value="{{ $timezone }}">{{ strtoupper($timezone) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="mandatory">Address</label>
                                    <textarea name="address" id="address" class="form-control" required style="resize: none;"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-update-branch">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-confirm-save">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header alert-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title confirm-save-title"></h4>
                </div>
                <div class="modal-body confirm-save-body">
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="button" class="btn btn-success" id="confirm-save-yes">Yes</button>
                    <button type="button" class="btn btn-danger" id="confirm-save-no">No</button>
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="confirm-save-cancel">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBYRNe9Dgft0H5c-cos_y6-rQ2L2nEqMBI&callback=initMap"></script>
    <script type="text/javascript">
        $('#modal-edit-branch,#modal-filter').on('shown.bs.modal', function(event) {
            $('.select2').select2();
        });

        var defaultMarker = {lat: {{$branch->latitude}}, lng: {{$branch->longitude}}};

        var map;
        function initMap(){
            var emptyMarker = false;
            if(defaultMarker.lat == 0 || defaultMarker.lng == 0){
                defaultMarker = {lat: -6.203852, lng: 106.796650};
                emptyMarker = true;
            }

            map = new google.maps.Map(document.getElementById('map'), {
                center: defaultMarker,
                zoom: 10
            });

            if(!emptyMarker){
                var marker = new google.maps.Marker({
                    position: defaultMarker,
                    map: map
                });

                var destinationCircle = new google.maps.Circle({
                    strokeColor: 'white',
                    strokeWeight: 0,
                    fillColor: 'red',
                    fillOpacity: 0.2,
                    map: map,
                    center: defaultMarker
                });
            } else {
                $("#map").html("<iframe\n" +
                    "  width=\"100%\"\n" +
                    "  height=\"500\"\n" +
                    "  frameborder=\"0\" style=\"border:0\"\n" +
                    "  src=\"https://www.google.com/maps/embed/v1/place?key=AIzaSyD85eopJ4INcD6vnPl7JOW_0nvH-h1L2Rs\n" +
                    "    &q={{ $place }}\" allowfullscreen>\n" +
                    "</iframe>");
            }
        }
    </script>

    <script type="text/javascript">
        var listDestinations = {!! $listDestinationPickups !!};
        var isChecked = false;
        var selectedListBranch = [];
        var allListDestinations = [];
        $("#btn-save-branch,#confirm-save-yes").click(function(event){
            event.preventDefault();
            $('.modal').modal('hide');
            var dupes = [];
            var uniqueDestinations = [];
            $.each(allListDestinations, function (index, entry) {
                var setData = dupes[entry.id];
                if (setData) {
                    var index = uniqueDestinations.map(function (desc) { return desc.id; }).indexOf(entry.id);
                    uniqueDestinations.splice(index, 1);
                    dupes[entry.id] = true;
                    var objectEntry = Object.assign({}, entry);
                    uniqueDestinations.push(objectEntry);
                } else {
                    dupes[entry.id] = true;
                    var objectEntry = Object.assign({}, entry);
                    uniqueDestinations.push(objectEntry);
                }
                console.log(objectEntry);
            });

            console.log(uniqueDestinations);
            var objectDestination = Object.assign({}, uniqueDestinations);
            console.log(objectDestination);

            if(allListDestinations.length > 0) {
                $.LoadingOverlay("show");
                $.ajax({
                    url: '{{ url("/popexpress/branches/add_branch_pickup") }}',
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        _token: "{{ csrf_token() }}",
                        branch_id: $('#branch_id').val(),
                        list_destination: JSON.stringify(objectDestination)
                    },
                    success: function (data) {
                        if(data.status == 'success') {
                            listDestinations = data.list;
                            $("#modal-confirm-save").modal("hide");
                            $(".success-title").html('Informasi');
                            $(".success-body").html(data.message);
                            $("#modal-success").modal("show");
                        } else {
                            $("#modal-confirm-save").modal("hide");
                            $(".error-title").html('Error');
                            $(".error-body").html(data.message);
                            $("#modal-error").modal("show");
                        }
                        allListDestinations.splice(0, allListDestinations.length);
                        $("div").removeClass("modal-backdrop");
                        $("#btn-save-branch").attr("disabled", true);
                        doFilter();
                        $.LoadingOverlay("hide");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (textStatus == "timeout") {
                            $.LoadingOverlay("hide");
                            $(".error-title").html('Server Time Out');
                            $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                            $("#modal-error").modal("show");
                        }
                        else {
                            $.LoadingOverlay("hide");
                            $(".error-title").html('Error');
                            $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                            $("#modal-error").modal("show");
                        }
                    }
                });
            }

        });

        $(document).on('change', '#list_branch', function() {
            var dataDestination = new Array();
            if(this.checked) {
                selectedListBranch = $("input:checkbox:checked").map(function(){
                    return this.value;
                }).toArray();
                dataDestination['id'] = this.value;
                dataDestination['value'] = 1;
            } else {
                dataDestination['id'] = this.value;
                dataDestination['value'] = 0;
            }
            $('#btn-save-branch').attr('disabled', false);
            allListDestinations.push(dataDestination);
            console.log(this.value);
            console.log(dataDestination);
            console.log(allListDestinations);

            if(allListDestinations.length > 0) {
                $('a,button').click(function(event){
                    var addressUrl = $(this).attr("href");
                    if(allListDestinations.length > 0) {
                        window.beforeunload = function(e) {
                            e.preventDefault();
                        };

                        $(".confirm-save-title").html('Warning');
                        $(".confirm-save-body").html('You have make changes on this page. Do you wish to keep this changes?');
                        $("#modal-confirm-save").modal("show");
                        $('#modal-confirm-save').on('shown.bs.modal', function(event) {
                            $("#confirm-save-no").click(function(event){
                                allListDestinations.splice(0, allListDestinations.length);
                                $("#btn-save-branch").attr("disabled", true);
                                if(addressUrl == '#' || addressUrl == undefined) {
                                    $('.modal').modal('hide');
                                } else {
                                    window.location = addressUrl;
                                }
                            });
                            $("#confirm-save-cancel").click(function(event){
                                $('.modal').modal('hide');
                            });
                        });

                    }

                });
            }
        });

        $(document).ready(function(){
            $("#name").keypress(function(event){
                var inputValue = event.which;
                if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0 && inputValue != 8)) {
                    event.preventDefault();
                }
            });
            $('#btn-delete-branch').on('click', function(event) {
                $('#delete').val(1);
                $('#form-delete').submit();
            });
        });

        $('.btn-update').on('click', function() {
            var data = $(this).data();
            $('#modal-edit-branch').modal('show');
            $("#modal-edit-branch").on('shown.bs.modal', function(){

                $('#id_update').val(data.id);
                $('#code').val($.trim($('#code_label').html()));
                $('#name').val($.trim($('#name_label').html()));
                $('#type').val($.trim($('#type_label_data').val()));
                $('#address').val($.trim($('#address_label').html()));
                $('#latitude').val($.trim($('#latitude_label').html()));
                $('#longitude').val($.trim($('#longitude_label').html()));
                $('#timezone').val($.trim($('#timezone_label').html()));
                $('#last_sync').html($.trim($('#last_sync_label').val()));
                $('#location_data').val($.trim($('#location_id_label').val())).trigger("change");
            });
        });

        $('#btn-update-branch').on('click', function(event) {
            $.LoadingOverlay("show");
            $.ajax({
                url: '{{ url("/popexpress/branches/update_branch") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    id: $('#id_update').val(),
                    code: $('#code').val(),
                    name: $('#name').val(),
                    type: $('#type').val(),
                    address: $('#address').val(),
                    latitude: $('#latitude').val(),
                    longitude: $('#longitude').val(),
                    timezone: $('#timezone').val(),
                    location: $('#location_data').val(),
                },
                success: function (data) {
                    if(data.status == 'success') {
                        $.LoadingOverlay("hide");

                        $('#code_title').html(data.data.code);
                        $('#name_title').html(data.data.name);
                        $('#code_label').html(data.data.code);
                        $('#name_label').html(data.data.name);
                        $('#type_label').html(data.data.type.toUpperCase());
                        $('#address_label').html(data.data.address);
                        $('#latitude_label').html(data.data.latitude);
                        $('#longitude_label').html(data.data.longitude);
                        $('#timezone_label').html(data.data.timezone);
                        $('#location_id_label').val(data.data.location);
                        $('#location_label').html(data.data.destination);
                        $('#type_label_data').val(data.data.type);
                        $('#last_sync_label').val(data.data.last_sync);
                        $("#modal-edit-branch .close").click();

                        if(data.data.latitude == '' || data.data.latitude == '0' || data.data.longitude == '' ||  data.data.longitude == '0') {
                            initMap();
                        } else {
                            var map = new google.maps.Map(document.getElementById('map'), {
                                center: {lat: data.data.latitude, lng: data.data.longitude},
                                zoom: 10
                            });

                            var marker = new google.maps.Marker({
                                position: {lat: data.data.latitude, lng: data.data.longitude},
                                map: map
                            });

                            var destinationCircle = new google.maps.Circle({
                                strokeColor: 'white',
                                strokeWeight: 0,
                                fillColor: 'red',
                                fillOpacity: 0.2,
                                map: map,
                                center: {lat: data.data.latitude, lng: data.data.longitude}
                            });
                        }

                    } else {
                        $("#modal-edit-branch .close").click();
                        $(".error-title").html('Error');
                        $(".error-body").html(data.message);
                        $("#modal-error").modal("show");
                    }
                    $.LoadingOverlay("hide");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        });

        $('#modal-add').on('shown.bs.modal', function(event) {
            $('#branch_id').val('');
            $('#branch_id').attr('placeholder', '').select2();
        });

        $(document).ready(function() {
            loadTable();
        });

        function doFilter() {
            $("#modal-filter .close").click();
            reloadTable();
        }

        function reloadTable() {
            $('#data-table').DataTable().destroy();
            loadTable();
        }

        $('#btn-filter-search').on('click', function() {
            $('#origin_id_filter').val('');
            $('#airport_code_filter').val('');
            $('#detail_code_filter').val('');
            $('#province_filter').val('');
            $('#is_locker_filter').val('');
            $('#locker_id_filter').val('');
            $('#county_filter').val('');
            $('#district_filter').val('');
            doFilter();
        });

        function loadTable() {
            var BranchTable = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                lengthChange: false,
                paging: true,
                pageLength: 30,
                bAutoWidth: false,
                scrollX: true,
                scrollY: '50vh',
                scrollCollapse: true,
                order: [[ 3, "asc" ]],
                ajax: {
                    url: "{{ url("/popexpress/branches/grid_destinations") }}",
                    data: {
                        "origin_id": $('#origin_id_filter').val(),
                        "airport_code": $('#airport_code_filter').val(),
                        "detail_code": $('#detail_code_filter').val(),
                        "province": $('#province_filter').val(),
                        "is_locker": $('#is_locker_filter').val(),
                        "locker_id": $('#locker_id_filter').val(),
                        "county": $('#county_filter').val(),
                        "district": $('#district_filter').val(),
                        "length": 30,
                        "_token": "{{ csrf_token() }}"
                    }
                },
                columns: [
                    {
                        data: "id",
                        width: "3%",
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "origin_name", name: "origin_name", orderable: false, width: "8%"},
                    { data: "airport_code", name: "airport_code", orderable: false, width: "8%"},
                    { data: "detail_code", name: "detail_code", orderable: true, width: "15%"},
                    { data: "province", orderable: false, name: "province"},
                    { data: "county", orderable: false, name: "county" },
                    { data: "district", orderable: false, name: "district" },
                    {
                        data: "is_locker",
                        name: "is_locker",
                        width: "20%",
                        orderable: false,
                        render: function (data, type, row) {
                            return (data == 1 ? 'Yes' : 'No')
                        }
                    },
                    {
                        data: "id",
                        width: "3%",
                        orderable: false,
                        render: function (data, type, row, meta) {
                            return '<input type="checkbox" name="selected_data" id="list_branch" value="'+data+'" ' + (listDestinations.includes(data) ? 'checked="checked"' : '') + ' class="form-input selected-data">';
                        }
                    }
                ]
            });
        }

    </script>
@endsection