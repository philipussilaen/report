@extends('layout.main')

@section('title')
    List Pickup Details
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    <style>
        .table th {
            text-align: center;
        }
        .label {
            margin-bottom: 3px;
            display: inline-block;
        }
        .select2-container--default {
            width: 250px;
        }
        .less-border {
            border: none;
        }
        #modal-add {
            overflow-y:scroll;
        }
    </style>
@endsection

@section('pageTitle')
    Pickup Details
@endsection

@section('pageDesc')
    List Pickup Details
@endsection

@section('content')
    <div class="box box-solid">
        <div class="box-body">
            <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modal-default">Filter</button>
            <a href="{{ url('popexpress/pickups/list_pickup_details') }}"><button class="btn btn-flat btn-warning">Reset Filter</button></a>
            <div class="pull-right">
                <button class="btn btn-flat btn-primary" data-toggle="modal" data-target="#modal-add">Add Pickup Details</button>
            </div>

            <hr>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Table --}}
            <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Pickup No</th>
                        <th>AWB</th>
                        <th>Reseller</th>
                        <th>Recipient</th>
                        <th>Destination</th>
                        <th>Price</th>
                        <th>In Hub</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($pickupDetails as $index => $pickupDetail)
                            <tr>
                                <td width="75" align="center">{{ $pickupDetails->firstItem() + $index }}</td>
                                <td align="center">{{ $pickupDetail->pickup_no }}</td>
                                <td align="center">
                                    {!! (!empty($pickupDetail->awb) ? 'AWB : '.$pickupDetail->awb.'<br>' : '')  !!}
                                    {!! (!empty($pickupDetail->merchant_awb) ? 'Merchant AWB : '.$pickupDetail->merchant_awb.'<br>' : '')  !!}
                                    {!! (!empty($pickupDetail->third_party_awb) ? '3PL AWB : '.$pickupDetail->third_party_awb.'<br>' : '')  !!}
                                    <span class="label label-{{ $statuses[$pickupDetail->latest_status] }}">{{ ucwords($pickupDetail->latest_status) }} </span><br>
                                </td>
                                <td align="center">{{ $pickupDetail->reseller }}</td>
                                <td>{!! $pickupDetail->recipient."<br>".$pickupDetail->recipient_telephone."<br>".$pickupDetail->recipient_email !!}</td>
                                <td>{!! $pickupDetail->destination."<br>".$pickupDetail->recipient_address !!}</td>
                                <td>
                                    @if($pickupDetail->in_hub == 0)
                                        {!! '('.intval($pickupDetail->web_rounded_weight).' KG) Rp '.number_format($pickupDetail->web_total_price, 0, ',', '.') !!}
                                    @else
                                        {!! '('.intval($pickupDetail->hub_rounded_weight).' KG) Rp '.number_format($pickupDetail->hub_total_price, 0, ',', '.') !!}
                                    @endif
                                </td>
                                <td align="center">{!!  ($pickupDetail->in_hub == 1 ? '<span class="label label-primary">Yes</span>' : '<span class="label label-danger">No</span>')  !!}<br>{{ $pickupDetail->hub_date }}</td>
                                <td align="center">
                                    <button class="btn btn-flat btn-info btn-small btn-update"
                                            data-pickupid = "{{ $pickupDetail->pickupid }}"
                                            data-idpickup = "{{ $pickupDetail->idpickup }}"
                                            data-pickup_no = "{{ $pickupDetail->pickup_no }}"
                                            data-awb = "{{ $pickupDetail->awb }}"
                                            data-merchant_awb = "{{ $pickupDetail->merchant_awb }}"
                                            data-third_party_awb = "{{ $pickupDetail->third_party_awb }}"
                                            data-latest_status = "{{ $pickupDetail->latest_status }}"
                                            data-reseller = "{{ $pickupDetail->reseller }}"
                                            data-recipient = "{{ $pickupDetail->recipient }}"
                                            data-recipient_telephone = "{{ $pickupDetail->recipient_telephone }}"
                                            data-recipient_email = "{{ $pickupDetail->recipient_email }}"
                                            data-destination = "{{ $pickupDetail->destination }}"
                                            data-recipient_address = "{{ $pickupDetail->recipient_address }}"
                                            data-web_rounded_weight = "{{ $pickupDetail->web_rounded_weight }}"
                                            data-hub_rounded_weight = "{{ $pickupDetail->hub_rounded_weight }}"
                                            data-web_total_price = "{{ $pickupDetail->web_total_price }}"
                                            data-hub_total_price = "{{ $pickupDetail->hub_total_price }}"
                                            data-postcode = "{{ $pickupDetail->postcode }}"
                                            data-photo = "{{ $pickupDetail->photo }}"
                                            data-photo_date = "{{ $pickupDetail->photo_date }}"
                                            data-description = "{{ $pickupDetail->description }}"
                                            data-remarks = "{{ $pickupDetail->remarks }}"
                                            data-web_destination_id = "{{ $pickupDetail->web_destination_id }}"
                                            data-hub_destination_id = "{{ $pickupDetail->hub_destination_id }}"
                                            data-hub_service_type = "{{ $pickupDetail->hub_service_type }}"
                                            data-web_service_type = "{{ $pickupDetail->web_service_type }}"
                                            data-hub_weight = "{{ $pickupDetail->hub_weight }}"
                                            data-hub_length = "{{ $pickupDetail->hub_length }}"
                                            data-hub_width = "{{ $pickupDetail->hub_width }}"
                                            data-hub_height = "{{ $pickupDetail->hub_height }}"
                                            data-hub_price_per_kg = "{{ $pickupDetail->hub_price_per_kg }}"
                                            data-hub_insurance_price = "{{ $pickupDetail->hub_insurance_price }}"
                                            data-hub_discount_price = "{{ $pickupDetail->hub_discount_price }}"
                                            data-hub_item_price = "{{ $pickupDetail->hub_item_price }}"
                                            data-hub_nett_price = "{{ $pickupDetail->hub_nett_price }}"
                                            data-web_weight = "{{ $pickupDetail->web_weight }}"
                                            data-web_length = "{{ $pickupDetail->web_length }}"
                                            data-web_width = "{{ $pickupDetail->web_width }}"
                                            data-web_height = "{{ $pickupDetail->web_height }}"
                                            data-web_price_per_kg = "{{ $pickupDetail->web_price_per_kg }}"
                                            data-web_discount_price = "{{ $pickupDetail->web_discount_price }}"
                                            data-web_item_price = "{{ $pickupDetail->web_item_price }}"
                                            data-web_insurance_price = "{{ $pickupDetail->web_insurance_price }}"
                                            data-web_nett_price = "{{ $pickupDetail->web_nett_price }}"
                                            data-in_hub = "{{ $pickupDetail->in_hub }}"
                                            data-in_hub_date = "{{ $pickupDetail->in_hub_date }}"
                                            data-finance_remarks = "{{ $pickupDetail->finance_remarks }}"
                                    >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ (!empty($pickupDetails) ? $pickupDetails->appends($_GET)->links() : '')}}
                <button type="button" class="btn btn-default disabled pull-right" data-dismiss="modal"><strong>Total : {{ (!empty($pickupDetails) ? $pickupDetails->total() : '0')}}</strong></button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <div class="modal-body">
                    <form id="form-filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>AWB</label>
                                    <input type="text" name="awb" id="awb_filter" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Merchant AWB</label>
                                    <input type="text" name="merchant_awb" id="merchant_awb_filter" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>3PL AWB</label>
                                    <input type="text" name="third_party_awb" id="third_party_awb_filter" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Reseller</label>
                                    <input type="text" name="reseller" id="reseller_filter" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Recipient</label>
                                    <input type="text" name="recipient" id="recipient_filter" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="recipient_telephone" id="recipient_telephone_filter" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="recipient_email" id="recipient_email_filter" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>In Hub Date From</label>
                                    <input type="text" name="hub_from" id="hub_date_from_filter" maxlength="100" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <select class="form-control" name="latest_status" id="latest_status_filter" required style="width: 100%;">
                                        <option value="">Select</option>
                                        @foreach (array_keys($statuses) as $status)
                                            <option value="{{ $status }}">{{ strtoupper($status) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Destination</label><br>
                                    <select class="form-control select_destination_filter" name="hub_destination_id" id="hub_destination_id" required style="width: 100%;">
                                        <option value="">Select</option>
                                        @foreach ($destinations as $destination)
                                            <option value="{{ $destination->id }}">{{ $destination->detail_code.' - '.$destination->district.', '.$destination->county.', '.$destination->province }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" name="recipient_address" id="recipient_address_filter" required rows="8"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Postcode</label>
                                    <input type="text" name="postcode" id="postcode_filter" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <select class="form-control" name="in_hub" id="in_hub_filter" required style="width: 100%;">
                                        <option value="">Select</option>
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>In Hub Date To</label>
                                    <input type="text" name="hub_to" id="hub_date_to_filter" maxlength="100" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-filter">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-add">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Pickup Detail</h4>
                </div>
                <form method="post" action="{{ url('/popexpress/pickups/pickup_details/store') }}">
                    {{ csrf_field() }}
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pickup No</label><br>
                                    <select class="form-control select_pickup_id" name="pickup_id" required style="width: 100%;" onchange="getAccount()">
                                        <option value="">Select</option>
                                        @foreach ($pickups as $pickup)
                                            <option value="{{ $pickup->pickupid }}">{{ $pickup->pickup_no }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="customer_weight_rounding" id="customer_weight_rounding" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Reseller</label>
                                    <input type="text" name="reseller" id="reseller" maxlength="100" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Recipient</label>
                                    <input type="text" name="recipient" id="recipient" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="recipient_telephone" id="recipient_telephone" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="recipient_email" id="recipient_email" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" name="recipient_address" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Postcode</label>
                                    <input type="text" name="postcode" id="postcode" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>WEB</label>
                                </div>
                                <div class="form-group">
                                    <label>Destination</label><br>
                                    <select class="form-control select_destination" name="web_destination_id" id="web_destination_id" required style="width: 100%;" onchange="getPrice()">
                                        <option value="">Select</option>
                                        @foreach ($destinations as $destination)
                                            <option value="{{ $destination->id }}">{{ $destination->detail_code.' - '.$destination->district.', '.$destination->county.', '.$destination->province }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="price_regular" id="price_regular" maxlength="30" class="form-control" required>
                                    <input type="hidden" name="price_oneday" id="price_oneday" maxlength="30" class="form-control" required>
                                    <input type="hidden" name="current_price" id="current_price" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Weight (KG)</label>
                                    <input type="text" name="web_weight" id="web_weight" maxlength="10" class="form-control" onkeyup="countPrice()">
                                </div>
                                <div class="form-group">
                                    <label>Length (cm)</label>
                                    <input type="text" name="web_length" id="web_length" maxlength="10" class="form-control" onkeyup="countPrice()">
                                </div>
                                <div class="form-group">
                                    <label>Width (cm)</label>
                                    <input type="text" name="web_width" id="web_width" maxlength="10" class="form-control" onkeyup="countPrice()">
                                </div>
                                <div class="form-group">
                                    <label>Height (cm)</label>
                                    <input type="text" name="web_height" id="web_height" maxlength="10" class="form-control" onkeyup="countPrice()">
                                </div>
                                <div class="form-group">
                                    <label>Service Type</label><br>
                                    <select class="form-control" name="web_service_type" id="web_service_type" required onchange="changeType()">
                                        <option value="regular">Regular</option>
                                        <option value="one_day">One Day</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Merchant AWB</label>
                                    <input type="text" name="merchant_awb" id="merchant_awb" maxlength="30" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>3PL AWB</label>
                                    <input type="text" name="third_party_awb" id="third_party_awb" maxlength="30" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" name="description" id="description" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Item Price</label>
                                    <input type="text" name="hub_item_price" id="hub_item_price" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Rounded Weight</label>
                                    <input type="text" name="hub_rounded_weight" id="hub_rounded_weight" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Price/KG</label>
                                    <input type="text" name="hub_price_per_kg" id="hub_price_per_kg" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Insurance</label>
                                    <input type="text" name="hub_insurance_price" id="hub_insurance_price" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="hub_total_price" id="hub_total_price" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Item Price</label>
                                    <input type="text" name="web_item_price" id="web_item_price_label" maxlength="30" class="form-control" onkeyup="countPrice()">
                                </div>
                                <div class="form-group">
                                    <label>Rounded Weight</label>
                                    <input type="text" name="web_rounded_weight" id="web_rounded_weight_label" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Price/KG</label>
                                    <input type="text" name="web_price_per_kg" id="web_price_per_kg_label" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Insurance</label>
                                    <input type="text" name="web_insurance_price" id="web_insurance_price_label" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="web_total_price" id="web_total_price_label" maxlength="30" class="form-control less-border">
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" value="Save" class="btn btn-primary">
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Edit Pickup Detail</h4>
                </div>
                <form method="post" action="{{ url('/popexpress/pickups/pickup_details/store') }}">
                    <input type="hidden" name="id" id="id_edit" maxlength="30" class="form-control" required>
                    <input type="hidden" name="pickup_id" id="idpickup_edit" maxlength="30" class="form-control" required>

                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="panel-pickup-label">
                                    <label>Pickup No</label><br>
                                    <span id="data-pickup-label"></span><br>
                                </div>
                                <div class="form-group" id="panel-pickup">
                                    <label>Pickup No</label><br>
                                    <select class="form-control select_pickup_id_edit" name="pickup_id_new" id="pickup_id_edit" style="width: 100%;" onchange="getAccountEdit()">
                                        <option value="">Select</option>
                                        @foreach ($pickups as $pickup)
                                            <option value="{{ $pickup->pickupid }}">{{ $pickup->pickup_no }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="customer_weight_rounding" id="customer_weight_rounding_edit" maxlength="30" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Reseller</label>
                                    <input type="text" name="reseller" id="reseller_edit" maxlength="100" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Recipient</label>
                                    <input type="text" name="recipient" id="recipient_edit" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="text" name="recipient_telephone" id="recipient_telephone_edit" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="recipient_email" id="recipient_email_edit" maxlength="100" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" name="recipient_address" id="recipient_address_edit" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Postcode</label>
                                    <input type="text" name="postcode" id="postcode_edit" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Item Price</label><br>
                                    <span id="data-item-price-label-edit"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Rounded Weight</label><br>
                                    <span id="data-rounded-weight-label-edit"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Price/KG</label><br>
                                    <span id="data-price-per-kg-label-edit"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Insurance</label><br>
                                    <span id="data-insurance-label-edit"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Total</label><br>
                                    <span id="data-total-label-edit"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>WEB</label>
                                </div>
                                <div class="form-group">
                                    <label>Destination</label><br>
                                    <select class="form-control select_destination_edit" name="web_destination_id" id="web_destination_id_edit" required style="width: 100%;" onchange="getPriceEdit()">
                                        <option value="">Select</option>
                                        @foreach ($destinations as $destination)
                                            <option value="{{ $destination->id }}">{{ $destination->detail_code.' - '.$destination->district.', '.$destination->county.', '.$destination->province }}</option>
                                        @endforeach
                                    </select>
                                    <input type="hidden" name="price_regular" id="price_regular_edit" maxlength="30" class="form-control" required>
                                    <input type="hidden" name="price_oneday" id="price_oneday_edit" maxlength="30" class="form-control" required>
                                    <input type="hidden" name="current_price" id="current_price_edit" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Weight (KG)</label>
                                    <input type="text" name="web_weight" id="web_weight_edit" maxlength="10" class="form-control" onkeyup="countPriceEdit()">
                                </div>
                                <div class="form-group">
                                    <label>Length (cm)</label>
                                    <input type="text" name="web_length" id="web_length_edit" maxlength="10" class="form-control" onkeyup="countPriceEdit()">
                                </div>
                                <div class="form-group">
                                    <label>Width (cm)</label>
                                    <input type="text" name="web_width" id="web_width_edit" maxlength="10" class="form-control" onkeyup="countPriceEdit()">
                                </div>
                                <div class="form-group">
                                    <label>Height (cm)</label>
                                    <input type="text" name="web_height" id="web_height_edit" maxlength="10" class="form-control" onkeyup="countPriceEdit()">
                                </div>
                                <div class="form-group">
                                    <label>Service Type</label><br>
                                    <select class="form-control" name="web_service_type" id="web_service_type_edit" onchange="changeTypeEdit()">
                                        <option value="regular">Regular</option>
                                        <option value="one_day">One Day</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>HUB</label>
                                </div>
                                <div class="form-group">
                                    <label>Destination</label><br>
                                    <select class="form-control hub_select_destination_edit" name="hub_destination_id" id="hub_destination_id_edit"  disabled="disabled" style="width: 100%;">
                                        <option value="">Select</option>
                                        @foreach ($destinations as $destination)
                                            <option value="{{ $destination->id }}">{{ $destination->detail_code.' - '.$destination->district.', '.$destination->county.', '.$destination->province }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Weight (KG)</label>
                                    <input type="text" name="hub_weight" id="hub_weight_edit" maxlength="10" class="form-control"  disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Length (cm)</label>
                                    <input type="text" name="hub_length" id="hub_length_edit" maxlength="10" class="form-control"  disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Width (cm)</label>
                                    <input type="text" name="hub_width" id="hub_width_edit" maxlength="10" class="form-control"  disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Height (cm)</label>
                                    <input type="text" name="hub_height" id="hub_height_edit" maxlength="10" class="form-control"  disabled="disabled">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="panel-pickup-label">
                                    <label>AWB</label><br>
                                    <span id="data-awb-label"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Merchant AWB</label>
                                    <input type="text" name="merchant_awb" id="merchant_awb_edit" maxlength="30" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>3PL AWB</label>
                                    <input type="text" name="third_party_awb" id="third_party_awb_edit" maxlength="30" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <input type="text" name="description" id="description_edit" maxlength="30" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <textarea class="form-control" name="remarks" id="remarks_edit" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Photo</label><br>
                                    <span id="data-photo-label"></span><br>
                                    <span id="data-photo-date-label"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Status</label><br>
                                    <span id="data-status-label"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>In Hub</label><br>
                                    <span id="data-in-hub-label"></span><br>
                                    <span id="data-in-hub-date-label"></span><br>
                                </div>
                                <div class="form-group">
                                    <label>Finance Remark</label><br>
                                    <span id="data-finance-remark-label"></span><br>
                                </div>
                                <div class="form-group" style="padding-top: 210px;">
                                    <label>Item Price</label>
                                    <input type="text" name="web_item_price" id="web_item_price_label_edit" maxlength="30" class="form-control" onkeyup="countPriceEdit()">
                                </div>
                                <div class="form-group">
                                    <label>Rounded Weight</label>
                                    <input type="text" name="web_rounded_weight" id="web_rounded_weight_label_edit" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Price/KG</label>
                                    <input type="text" name="web_price_per_kg" id="web_price_per_kg_label_edit" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Insurance</label>
                                    <input type="text" name="web_insurance_price" id="web_insurance_price_label_edit" maxlength="30" class="form-control less-border">
                                </div>
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="web_total_price" id="web_total_price_label_edit" maxlength="30" class="form-control less-border">
                                </div>

                                <div class="form-group" style="padding-top: 110px;">
                                    <label>Item Price</label>
                                    <input type="text" name="hub_item_price" id="hub_item_price_edit" maxlength="30" class="form-control" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Rounded Weight</label>
                                    <input type="text" name="hub_rounded_weight" id="hub_rounded_weight_edit" maxlength="30" class="form-control" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Price/KG</label>
                                    <input type="text" name="hub_price_per_kg" id="hub_price_per_kg_edit" maxlength="30" class="form-control" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Insurance</label>
                                    <input type="text" name="hub_insurance_price" id="hub_insurance_price_edit" maxlength="30" class="form-control" disabled="disabled">
                                </div>
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" name="hub_total_price" id="hub_total_price_edit" maxlength="30" class="form-control" disabled="disabled">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <input type="submit" value="Save" class="btn btn-primary" id="btn-update-form">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }}>"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('plugins/accounting/accounting.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.select_pickup_id, .select_destination').select2({
                dropdownParent: $("#modal-add")
            });
            $('.select_destination_filter').select2({
                dropdownParent: $("#modal-default")
            });
            $('#hub_date_to_filter, #hub_date_from_filter').datepicker({ format: 'dd/mm/yyyy' });
        });
        $('#modal-default,#modal-add,#modal-edit').on('shown.bs.modal', function(event) {
            $("#recipient_telephone,#postcode,#web_weight,#web_length,#web_width,#web_height,#hub_item_price,#hub_rounded_weight,#hub_price_per_kg,#hub_insurance_price,#hub_total_price,#web_item_price_label,#web_rounded_weight_label,#web_price_per_kg_label,#web_insurance_price_label,#web_total_price_label,#web_weight_edit,#web_length_edit,#web_width_edit,#web_height_edit,#web_item_price_label_edit,#web_rounded_weight_label_edit,#web_price_per_kg_label_edit,#web_insurance_price_label_edit,#web_total_price_label_edit").keypress(function(event){
                var inputValue = event.which;
                if((inputValue >= 48 && inputValue <= 57) || inputValue == 13 || inputValue == 8 || inputValue == 46) {
                    return true;
                } else {
                    event.preventDefault();
                }
            });

            $("#hub_item_price,#hub_rounded_weight,#hub_price_per_kg,#hub_insurance_price,#hub_total_price,#web_rounded_weight_label,#web_price_per_kg_label,#web_insurance_price_label,#web_total_price_label").keypress(function(event){
                var inputValue = event.which;
                if(inputValue !== 0) {
                    event.preventDefault();
                }
            });
        });

        $('#btn-filter').on('click', function(event) {
            $('#form-filter').submit();
        });

        $('#btn-add').on('click', function(event) {
            $('#form-add').submit();
        });

        $('.btn-update').on('click', function() {
            var data = $(this).data();
            $('#modal-edit').modal('show');
            $('.select_pickup_id_edit, .select_destination_edit, .hub_select_destination_edit').select2({
                dropdownParent: $("#modal-edit")
            });
            $('#id_edit').val(data.pickupid);
            $('#idpickup_edit').val(data.idpickup);

            $('#hub_destination_id_edit').val(data.hub_destination_id).trigger('change');
            $('#web_destination_id_edit').val(data.web_destination_id).trigger('change');
            $('#pickup_id_edit').val(data.idpickup).trigger('change');

            setTimeout(function(){
                $.LoadingOverlay("show");
                if(data.latest_status == "new" || data.latest_status == "in hub") {
                    $('#panel-pickup-label').hide();
                    $('#panel-pickup').show();
                } else {
                    $('#panel-pickup-label').show();
                    $('#panel-pickup').hide();
                }
                $('#data-pickup-label').html(data.pickup_no);
                $('#data-awb-label').html(data.awb);

                $('#merchant_awb_edit').val(data.merchant_awb);
                $('#third_party_awb_edit').val(data.third_party_awb);
                $('#reseller_edit').val(data.reseller);
                $('#recipient_edit').val(data.recipient);
                $('#recipient_telephone_edit').val(data.recipient_telephone);
                $('#recipient_email_edit').val(data.recipient_email);
                $('#recipient_address_edit').val(data.recipient_address);
                $('#destination_edit').val(data.destination);
                $('#web_rounded_weight_edit').val(data.web_rounded_weight);
                $('#web_total_price_edit').val(data.web_total_price);

                if(data.in_hub == 1) {
                    $('#data-item-price-label-edit').html(accounting.formatMoney(parseInt(data.hub_item_price), "", 0, ".", ","));
                    $('#data-rounded-weight-label-edit').html(accounting.formatMoney(parseInt(data.hub_rounded_weight), "", 0, ".", ","));
                    $('#data-price-per-kg-label-edit').html(accounting.formatMoney(parseInt(data.hub_price_per_kg), "", 0, ".", ","));
                    $('#data-insurance-label-edit').html(accounting.formatMoney(parseInt(data.hub_insurance_price), "", 0, ".", ","));
                    $('#data-total-label-edit').html(accounting.formatMoney(parseInt(data.hub_nett_price), "", 0, ".", ","));
                } else {
                    $('#data-item-price-label-edit').html(accounting.formatMoney(parseInt(data.web_item_price), "", 0, ".", ","));
                    $('#data-rounded-weight-label-edit').html(accounting.formatMoney(parseInt(data.web_rounded_weight), "", 0, ".", ","));
                    $('#data-price-per-kg-label-edit').html(accounting.formatMoney(parseInt(data.web_price_per_kg), "", 0, ".", ","));
                    $('#data-insurance-label-edit').html(accounting.formatMoney(parseInt(data.web_insurance_price), "", 0, ".", ","));
                    $('#data-total-label-edit').html(accounting.formatMoney(parseInt(data.web_nett_price), "", 0, ".", ","));
                }

                $('#description_edit').val(data.description);
                $('#remarks_edit').val(data.remarks);
                $('#postcode_edit').val(data.postcode);
                $('#data-photo-label').html((data.photo !== '' ? '<img src="{{ url('/') }} ' + data.photo + '">' : '')   );
                $('#data-photo-date-label').html(data.photo_date);
                $('#data-status-label').html(data.latest_status);
                $('#data-in-hub-label').html((data.in_hub == 1 ? '<span class="label label-primary">Yes</span>' : '<span class="label label-danger">No</span>'));
                $('#data-in-hub-date-label').html(data.in_hub_date);
                $('#data-finance-remark-label').html(data.finance_remarks);


                $('#web_item_price_label_edit').val(parseInt(data.web_item_price));
                $('#web_rounded_weight_label_edit').val(accounting.formatMoney(parseInt(data.web_rounded_weight), "", 0, ".", ","));
                $('#web_price_per_kg_label_edit').val(accounting.formatMoney(parseInt(data.web_price_per_kg), "", 0, ".", ","));
                $('#web_insurance_price_label_edit').val(accounting.formatMoney(parseInt(data.web_insurance_price), "", 0, ".", ","));
                $('#web_total_price_label_edit').val(accounting.formatMoney(parseInt(data.web_nett_price), "", 0, ".", ","));


                $('#hub_item_price_edit').val(parseInt(data.hub_item_price));
                $('#hub_rounded_weight_edit').val(accounting.formatMoney(parseInt(data.hub_rounded_weight), "", 0, ".", ","));
                $('#hub_price_per_kg_edit').val(accounting.formatMoney(parseInt(data.hub_price_per_kg), "", 0, ".", ","));
                $('#hub_insurance_price_edit').val(accounting.formatMoney(parseInt(data.hub_insurance_price), "", 0, ".", ","));
                $('#hub_total_price_edit').val(accounting.formatMoney(parseInt(data.hub_nett_price), "", 0, ".", ","));

                $('#web_weight_edit').val(parseInt(data.web_weight));
                $('#web_length_edit').val(parseInt(data.web_length));
                $('#web_width_edit').val(parseInt(data.web_width));
                $('#web_height_edit').val(parseInt(data.web_height));
                $('#web_service_type_edit').val(data.web_service_type);

                $('#hub_weight_edit').val(parseInt(data.hub_weight));
                $('#hub_length_edit').val(parseInt(data.hub_length));
                $('#hub_width_edit').val(parseInt(data.hub_width));
                $('#hub_height_edit').val(parseInt(data.hub_height));
                $('#hub_service_type_edit').val(data.hub_service_type);
                $.LoadingOverlay("hide");
            },3000);
        });

        function getPrice() {

            var destination = $("#web_destination_id").val();
            var pickup = $(".select_pickup_id option:selected").val();

            if(pickup == "") {
                $("#web_destination_id").val('');
                $(".error-title").html('Warning');
                $(".error-body").html('Pilih pickup no terlebih dahulu.');
                $("#modal-error").modal("show");
                return false;
            }

            $.LoadingOverlay("show");

            $('#price_regular').val('0');
            $('#price_oneday').val('0');

            $.ajax({
                url: '{{ url("/popexpress/pickups/get_destination_price") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    destinationid: destination,
                    pickupid: pickup
                },
                success: function (data) {

                    $.LoadingOverlay("hide");
                    if(data.regular == 0 && data.one_day == 0) {
                        $(".error-title").html('Warning');
                        $(".error-body").html('Harga untuk destination ini belum diinput.');
                        $("#modal-error").modal("show");
                    } else {
                        $('#price_regular').val(data.regular);
                        $('#price_oneday').val(data.one_day);
                        $('#current_price').val(data.regular);

                        $('#hub_price_per_kg').val(accounting.formatMoney(data.regular, "Rp ", 0, ".", ","));
                        $('#web_price_per_kg_label').val(accounting.formatMoney(data.regular, "Rp ", 0, ".", ","));

                        countPrice();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function getAccount() {

            $.LoadingOverlay("show");
            var pickup = $(".select_pickup_id option:selected").val();
            $('#customer_weight_rounding').val('0');

            $.ajax({
                url: '{{ url("/popexpress/pickups/get_account") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    pickupid: pickup
                },
                success: function (data) {
                    $.LoadingOverlay("hide");
                    $('#customer_weight_rounding').val(data);
                    countPrice();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function getAccountEdit() {

            $.LoadingOverlay("show");
            var pickup = $(".select_pickup_id_edit option:selected").val();
            $('#customer_weight_rounding_edit').val('0');

            $.ajax({
                url: '{{ url("/popexpress/pickups/get_account") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    pickupid: pickup
                },
                success: function (data) {
                    $.LoadingOverlay("hide");
                    $('#customer_weight_rounding_edit').val(data);
                    countPriceEdit();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function countPrice() {

            var weight = $('#web_weight').val();
            var length = ($('#web_length').val() !== '' ? $('#web_length').val() : 0);
            var width = ($('#web_width').val() !== '' ? $('#web_width').val() : 0);
            var height = ($('#web_height').val() !== '' ? $('#web_height').val() : 0);
            var customerRound = $('#customer_weight_rounding').val();
            var itemPrice = $('#web_item_price_label').val();
            $('#hub_item_price').val(accounting.formatMoney(itemPrice, "Rp ", 0, ".", ","));


            if(customerRound == "") {
                $(".error-title").html('Warning');
                $(".error-body").html('Pilih pickup no terlebih dahulu.');
                $("#modal-error").modal("show");
                return false;
            }

            var volume = (length * width * height) / 6000;
            var biggerWeight = (weight > volume ? weight : volume);
            var stringWeight = biggerWeight.toString();
            var arrWeight = stringWeight.split('.');
            var getDecimal = (arrWeight[1] !== undefined ? arrWeight[1] : 0);
            var decimal = parseFloat('0.' + getDecimal);
            var rounded = (decimal > customerRound ? 1 : 0);
            var fixWeight = parseInt(arrWeight[0]) + rounded;

            var regular = $('#price_regular').val();
            var one_day = $('#price_oneday').val();
            var current_price = $('#current_price').val();

            fixWeight = (fixWeight < 1 ? 1 : fixWeight);

            var insurance = (itemPrice > 0 ? ((0.2 * itemPrice) + 5000) : 0);

            $('#hub_total_price').val(accounting.formatMoney(((fixWeight * current_price) + insurance), "Rp ", 0, ".", ","));
            $('#web_total_price_label').val(accounting.formatMoney(((fixWeight * current_price) + insurance), "Rp ", 0, ".", ","));

            if(weight !== "") {
                $('#hub_rounded_weight,#web_rounded_weight_label').val(fixWeight);
            }
            $('#hub_insurance_price,#web_insurance_price_label').val(accounting.formatMoney(insurance, "Rp ", 0, ".", ","));

        }
        
        function changeType() {
            var type = $('#web_service_type').val();
            var regular = $('#price_regular').val();
            var one_day = $('#price_oneday').val();
            if(type == 'regular') {
                $('#current_price').val(regular);
                $('#hub_price_per_kg').val(accounting.formatMoney(regular, "Rp ", 0, ".", ","));
                $('#web_price_per_kg_label').val(accounting.formatMoney(regular, "Rp ", 0, ".", ","));
            } else {
                $('#current_price').val(one_day);
                $('#hub_price_per_kg').val(accounting.formatMoney(one_day, "Rp ", 0, ".", ","));
                $('#web_price_per_kg_label').val(accounting.formatMoney(one_day, "Rp ", 0, ".", ","));
            }
            countPrice();
        }

        function getPriceEdit() {

            var destination = $("#web_destination_id_edit").val();
            var pickup = $("#idpickup_edit").val();

            if(pickup == "") {
                pickup = $("#pickup_id_edit").val();
            }

            if(pickup == "") {
                $("#web_destination_id").val('');
                $(".error-title").html('Warning');
                $(".error-body").html('Pilih pickup no terlebih dahulu.');
                $("#modal-error").modal("show");
                return false;
            }

            $.LoadingOverlay("show");

            $('#price_regular_edit').val('0');
            $('#price_oneday_edit').val('0');

            $.ajax({
                url: '{{ url("/popexpress/pickups/get_destination_price") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    destinationid: destination,
                    pickupid: pickup
                },
                success: function (data) {

                    $.LoadingOverlay("hide");
                    if(data.regular == 0 && data.one_day == 0) {
                        $(".error-title").html('Warning');
                        $(".error-body").html('Harga untuk destination ini belum diinput.');
                        $("#modal-error").modal("show");
                    } else {
                        $('#price_regular_edit').val(data.regular);
                        $('#price_oneday_edit').val(data.one_day);
                        $('#current_price_edit').val(data.regular);

                        $('#hub_price_per_kg_edit').val(accounting.formatMoney(data.regular, "Rp ", 0, ".", ","));
                        $('#web_price_per_kg_label_edit').val(accounting.formatMoney(data.regular, "Rp ", 0, ".", ","));
                        $('#data-price-per-kg-label-edit').html(accounting.formatMoney(data.regular, "Rp ", 0, ".", ","));

                        countPriceEdit();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        }

        function countPriceEdit() {

            var weight = $('#web_weight_edit').val();
            var length = ($('#web_length_edit').val() !== '' ? $('#web_length_edit').val() : 0);
            var width = ($('#web_width_edit').val() !== '' ? $('#web_width_edit').val() : 0);
            var height = ($('#web_height_edit').val() !== '' ? $('#web_height_edit').val() : 0);
            var customerRound = $('#customer_weight_rounding_edit').val();
            var itemPrice = $('#web_item_price_label_edit').val();
            $('#hub_item_price_edit').val(accounting.formatMoney(itemPrice, "Rp ", 0, ".", ","));
            $('#data-item-price-label-edit').html(accounting.formatMoney(itemPrice, "Rp ", 0, ".", ","));

            var volume = (length * width * height) / 6000;
            var biggerWeight = (weight > volume ? weight : volume);
            var stringWeight = biggerWeight.toString();
            var arrWeight = stringWeight.split('.');
            var getDecimal = (arrWeight[1] !== undefined ? arrWeight[1] : 0);
            var decimal = parseFloat('0.' + getDecimal);
            var rounded = (decimal > customerRound ? 1 : 0);
            var fixWeight = parseInt(arrWeight[0]) + rounded;

            var regular = $('#price_regular_edit').val();
            var one_day = $('#price_oneday_edit').val();
            var current_price = $('#current_price_edit').val();

            fixWeight = (fixWeight < 1 ? 1 : fixWeight);

            var insurance = (itemPrice > 0 ? ((0.2 * itemPrice) + 5000) : 0);

            $('#hub_total_price_edit').val(accounting.formatMoney(((fixWeight * current_price) + insurance), "Rp ", 0, ".", ","));
            $('#web_total_price_label_edit').val(accounting.formatMoney(((fixWeight * current_price) + insurance), "Rp ", 0, ".", ","));
            $('#data-total-label-edit').html(accounting.formatMoney(((fixWeight * current_price) + insurance), "Rp ", 0, ".", ","));

            if(weight !== "") {
                $('#hub_rounded_weight_edit,#web_rounded_weight_label_edit').val(fixWeight);
                $('#data-rounded-weight-label-edit').html(fixWeight);
            }

            $('#data-insurance-label-edit').html(accounting.formatMoney(insurance, "Rp ", 0, ".", ","));
            $('#hub_insurance_price_edit,#web_insurance_price_label_edit').val(accounting.formatMoney(insurance, "Rp ", 0, ".", ","));

        }

        function changeTypeEdit() {
            var type = $('#web_service_type_edit').val();
            var regular = $('#price_regular_edit').val();
            var one_day = $('#price_oneday_edit').val();
            if(type == 'regular') {
                $('#current_price_edit').val(regular);
                $('#hub_price_per_kg_edit').val(accounting.formatMoney(regular, "Rp ", 0, ".", ","));
                $('#web_price_per_kg_label_edit').val(accounting.formatMoney(regular, "Rp ", 0, ".", ","));
                $('#data-price-per-kg-label-edit').html(accounting.formatMoney(regular, "Rp ", 0, ".", ","));
            } else {
                $('#current_price_edit').val(one_day);
                $('#hub_price_per_kg_edit').val(accounting.formatMoney(one_day, "Rp ", 0, ".", ","));
                $('#web_price_per_kg_label_edit').val(accounting.formatMoney(one_day, "Rp ", 0, ".", ","));
                $('#data-price-per-kg-label-edit').html(accounting.formatMoney(one_day, "Rp ", 0, ".", ","));
            }
            countPriceEdit();
        }


    </script>
@endsection