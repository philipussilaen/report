@extends('layout.main')

@section('title')
    Transfer User
@endsection

@section('css')
@endsection

@section('pageTitle')
    Employee
@endsection

@section('pageDesc')
    Transfer User
@endsection

@section('content')
    <form method="post" method="post" action="{{ url('/popexpress/employees/store_transfer') }}">
        <input type="hidden" value="{{ $user->report_users_id }}" name="id">
        {{ csrf_field() }}
        <div class="box box-solid">
            <div class="box-body">
                <h3>Details</h3>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="form-group">
                            <label>Email</label><br>
                            <span>{{ $user->email }}</span>
                        </div>
                        <div class="form-group">
                            <label>Branch</label>
                            <select class="form-control" name="branch" required>
                                <option value="">Select</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" {!! (isset($employee->branch_id) ? ($branch->id == $employee->branch_id ? 'selected="selected"' : '') : '') !!}>{{ $branch->code.' - '.$branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone</label><br>
                            <span>{{ $user->phone }}</span>
                        </div>
                        <div class="form-group">
                            <br>
                            <input type="checkbox" name="all_branch" {!! (isset($employee->all_branch) ?($employee->all_branch == 1 ? 'checked="checked"' : '') : '') !!}> Access All Branch
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <h3>Groups</h3>
                <div class="row">
                    @foreach($groups as $group)
                        @php
                            $selectedGroup = "";
                            if(in_array($group->id, explode(',', $user->groupiduser))){$selectedGroup = "checked";}
                        @endphp
                        <div class="col-md-6">
                            <input type="checkbox" id="employee_group" name="employee_group[]" value="{{ $group->id }}" {{ $selectedGroup }}> {{ strtoupper(str_replace('_', ' ', $group->name)) }}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <a href="{{ url('popexpress/employees/transfer') }}" class="btn btn-flat btn-warning">Close</a>
                <input type="submit" value="Save" class="btn btn-primary" >
            </div>
        </div>
    </form>
@endsection

@section('js')

@endsection