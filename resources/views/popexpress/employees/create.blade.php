@extends('layout.main')

@section('title')
    Add Employee
@endsection

@section('css')
@endsection

@section('pageTitle')
    Employee
@endsection

@section('pageDesc')
    Add Employee
@endsection

@section('content')
    <form method="post" method="post" action="{{ url('/popexpress/employees/store') }}">
        {{ csrf_field() }}
        <div class="box box-solid">
            <div class="box-body">
                <h3>Details</h3>
                {{ csrf_field() }}
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6 border-right">
                        <div class="form-group">
                            <label>Code</label>
                            <input type="text" name="code" class="form-control" maxlength="30" required>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" maxlength="255" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" maxlength="255" required>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control" maxlength="20" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" maxlength="255">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" maxlength="100" required>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                @foreach ($statuses as $status)
                                    <option value="{{ $status }}">{{ $status }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Branch</label>
                            <select class="form-control" name="branch">
                                <option value="">Select</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="all_branch"> Access All Branch
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-body">
                <h3>Groups</h3>
                    <div class="row">
                        @foreach($groups as $group)
                            <div class="col-md-6">
                                <input type="checkbox" id="employee_group" name="employee_group[]" value="{{ $group->id }}"> {{ strtoupper(str_replace('_', ' ', $group->name)) }}
                            </div>
                        @endforeach
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <input type="submit" value="Save" class="btn btn-primary">
                <a href="{{ url('popexpress/employees') }}"><button class="btn btn-flat btn-warning">Back</button></a>
            </div>
        </div>
    </form>
@endsection

@section('js')
@endsection