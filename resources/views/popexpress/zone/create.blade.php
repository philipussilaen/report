@extends('layout.main')

@section('title')
    Tambah Zona
@endsection

@section('css')
    <style>
        .form-group.required .control-label:after {
            content:" *";
            color: red;
        }
    </style>
@endsection

@section('pageTitle')
    ZONA
@endsection

@section('pageDesc')
    Tambah Zona
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Tambah Zona</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <form class="form-horizontal" role="form">
                            <div class="form-group"  >
                                <label for="tujuan" class="col-md-4 control-label">Tujuan:</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" id="tujuan" name="tujuan" placeholder="Masukkan Tujuan (Kecamatan)" value="{{ old('tujuan') }}"/>
                                    <input type="hidden" class="form-control" id="tujuan_id" value="{{ old('tujuan_id') }}"/>
                                    {{ csrf_field() }}
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="zone" class="col-md-4 control-label">Zona:</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="zone" name="zone">
                                        @foreach($ListZona as $item)
                                            <option value="{{ $item['code']  }}" {{ (old('zone') == $item['code'] ? "selected":"") }}>{{ $item['name']  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label for="regular" class="col-md-4 control-label">Regular:</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="regular" name="regular">
                                        @foreach($ListThirdParties as $item)
                                            <option value="{{ $item->id  }}" {{ (old('regular') == $item->id? "selected":"") }}>{{ $item->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="one_day" class="col-md-4 control-label">One Day:</label>
                                <div class="col-md-4">
                                    <select class="form-control" id="one_day" name="one_day">
                                        @foreach($ListThirdParties as $item)
                                            <option value="{{ $item->id  }}" {{ (old('one_day') == $item->id? "selected":"") }}>{{ $item->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <button type="button" class="btn btn-success" onclick="kembali()">Kembali</button>
                                    <button type="button" class="btn btn-info" onclick="simpan()">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/bootstrap-ajax-typeahead/js/bootstrap-typeahead.min.js') }}"></script>
    <script type="text/javascript">

        var unsaved = false;

        $(function () {
            var tujuanSource = {!! $ArrTujuanKecamatan !!};
            $('#tujuan').typeahead({
                source: tujuanSource,
                items: 10,
                triggerLength: 1,
                onSelect: function(item){
                    $('#tujuan_id').val(item.value);
                    unsaved=true;
                }
            });
        });

        function kembali() {
            window.location = '{{ url('/popexpress/zone') }}';
        }

        function simpan() {

            if($('#tujuan').val()=='')
            {
                alert('Kolom Tujuan Harus diisi');
                return false;
            }

            $.ajax({
                url: '{{ url("/popexpress/zone/create") }}',
                type: 'POST',
                data: {
                    _token      : "{{ csrf_token() }}",
                    tujuan_id   : $('#tujuan_id').val(),
                    oneday      : $('#one_day option:selected').val(),
                    regular     : $('#regular option:selected').val(),
                    zona        : $('#zone option:selected').val(),
                    update      : 0
                },
                success: function (data) {
                    if(data['success'])
                    {
                        if(data['duplicate'])
                        {
                            var inputConfirm = confirm(data['message']);
                            if (inputConfirm == true) {
                                perbaharui();
                            } else {
                                return false;
                            }
                        }
                        else {
                            window.location = " {{ url('/popexpress/zone') }}";
                        }

                    }else{
                        Ext.getBody().unmask();
                        alert(data['message']);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        alert('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                    }
                    else {
                        alert('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                    }
                }
            });


        }

        function perbaharui() {

            $.ajax({
                url: '{{ url("/popexpress/zone/create") }}',
                type: 'POST',
                data: {
                    _token      : "{{ csrf_token() }}",
                    tujuan_id   : $('#tujuan_id').val(),
                    oneday      : $('#one_day option:selected').val(),
                    regular     : $('#regular option:selected').val(),
                    zona        : $('#zone option:selected').val(),
                    update      : 1
                },
                success: function (data) {
                    if(data['success'])
                    {
                        window.location = " {{ url('/popexpress/zone')  }}";
                    }else{
                        alert(data['message']);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        alert('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                    }
                    else {
                        alert('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                    }
                }
            });
        }
    </script>
@endsection