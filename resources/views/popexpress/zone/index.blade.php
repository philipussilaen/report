@extends('layout.main')

@section('title')
    Daftar Zona
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.css') }}">
    <style>
        table { table-layout: fixed; }
        td {
            overflow: hidden;
            word-wrap:break-word;
        }
    </style>
@endsection

@section('pageTitle')
    ZONA
@endsection

@section('pageDesc')
    Daftar Zona
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid box-info">
                    <div class="box-header with-border">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">Zona</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bt-sm bg-teal" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="row">
                            <form id="form-transaction">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="code" class="form-control" placeholder="Kode" id="code">
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" name="codedetail" class="form-control" placeholder="Kode Detail" id="codedetail">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="province" class="form-control" placeholder="Provinsi" id="province">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="city" class="form-control" placeholder="Kota" id="city">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="district" class="form-control" placeholder="Kecamatan" id="district">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-info" id="search">Cari</button>
                                    <a href="{{ url('/popexpress/zone/create') }}" class="btn btn-primary">Tambah</a>
                                    <button type="button" class="btn btn-success" id="excel" onclick="unduh_excel()">Unduh Excel</button>
                                    <a href="{{ url('/popexpress/zone/upload') }}" class="btn btn-danger">Upload</a>
                                </div>
                            </form>
                        </div>
                        <table class="table table-hover cell-border" id="data-table" cellspacing="0">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Kode Detail</th>
                                <th>Map</th>
                                <th>Provinsi</th>
                                <th>Kabupaten</th>
                                <th>Kecamatan</th>
                                <th>Zona</th>
                                <th>Regular</th>
                                <th>One Day</th>
                            </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            var deletedId = [];

            loadTable();

            $('#search').on('click', function(ev, picker) {
                reloadTable();
            });

            $('#code, #codedetail, #province, #city, #district').on('keyup', function (e){
                if(e.keyCode === 13){
                    reloadTable();
                }
            });

            $("#btn-trash").on("click", function (event) {
                $('input.selected-data:checkbox:checked').each(function () {
                    deletedId.push($(this).val());
                });
                if(deletedId.length == 0){
                    alert("Silahkan pilih data yang ingin di hapus terlebih dahulu.");
                } else {
                    $("#modal-default").modal("show");
                }
            });

            $("#confirm-delete").on("click", function (event) {
                console.log(deletedId);
                $.ajax( {
                    url: '{{ url("/popexpress/zone/delete") }}',
                    data: {
                        id: deletedId,
                        _token : '{{ csrf_token() }}'
                    },
                    type: 'POST',
                    success: function(response){
                        $("#modal-default").modal("hide");
                        alert(response.message);
                        reloadTable();
                        deletedId = [];
                    },
                    failure: function(response){
                        alert("Terdeteksi masalah koneksi ke server. Mohon dicoba lagi.");
                    }
                } );
            });

            function reloadTable() {
                $('#data-table').DataTable().destroy();
                loadTable();
            }

            function loadTable() {
                var BranchTable = $('#data-table').DataTable({
                    processing: true,
                    serverSide: true,
                    searching: false,
                    lengthChange: false,
                    paging: true,
                    pageLength: 30,
                    bAutoWidth: false,
                    scrollX: true,
                    scrollY: '50vh',
                    scrollCollapse: true,
                    order: [[ 2, "asc" ]],
                    ajax: {
                        url: "{{ url("/popexpress/zone/grid") }}",
                        data: {
                            "code": $("#code").val(),
                            "codedetail": $("#codedetail").val(),
                            "province": $("#province").val(),
                            "city": $("#city").val(),
                            "district": $("#district").val(),
                            "length": 30,
                            "_token": "{{ csrf_token() }}"
                        }
                    },
                    columns: [
                        {
                            data: "id",
                            width: "3%",
                            orderable: false,
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: "airport_code",name: "airport_code"},
                        {
                            data: "detail_code",
                            name: "detail_code",
                            render: function (data, type, row) {
                                return '<a href="{{ url('/popexpress/zone/destination') }}/'+ row.id +'">' + data + '</a>';
                            }
                        },
                        {
                            data: "latitude",
                            name: "latitude",
                            width: "8%",
                            render: function (data, type, row) {
                                if(data){
                                    return '&#x2713;';
                                }else{
                                    return '';
                                }
                            }
                        },
                        { data: "province", name: "province"},
                        { data: "county", name: "county", width: "8%" },
                        { data: "district", name: "district", width: "8%" },
                        {
                            data: "zone",
                            name: "zone",
                            width: "8%"
                        },
                        {
                            data: "tpacode",
                            name: "tpacode",
                            width: "8%"
                        },
                        {
                            data: "tpbcode",
                            name: "tpbcode",
                            width: "8%"
                        }
                    ]
                });
            }
        });

        function unduh_excel() {

            $.ajax({
                url: '{{ url("/popexpress/zone/excel") }}',
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    code : $('#code').val(),
                    codedetail: $('#codedetail').val(),
                    province: $('#province').val(),
                    city: $('#city').val(),
                    district: $('#district').val()
                },
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        window.location = '{{ url("/popexpress/zone/download") }}' + '?file=' + feedback['link'];
                    } else {
                        alert(feedback['message']);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        alert('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                    }
                    else {
                        alert('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                    }
                }
            });

        }

    </script>
@endsection