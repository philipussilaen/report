@extends('layout.main')

@section('title')
    List Pickups
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <style>
        .table th {
            text-align: center;
        }
        .label {
            margin-bottom: 3px;
            display: inline-block;
        }
    </style>
@endsection

@section('pageTitle')
    Pickups
@endsection

@section('pageDesc')
    List Pickups
@endsection

@section('content')
    <div class="box box-solid">
        <div class="box-body">
            <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modal-default">Filter</button>
            <a href="{{ url('popexpress/pickups') }}"><button class="btn btn-flat btn-warning">Reset Filter</button></a>
            <div class="pull-right">
                <a href="{{ url('popexpress/pickups/create') }}"><button class="btn btn-flat btn-primary">Add Pickup</button></a>
            </div>

            <hr>
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {{-- Table --}}
            <div class="table-responsive">
                <table class="table table-condensed table-striped">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Pickup</th>
                        <th>Details</th>
                        <th>Created Date</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($pickups as $index => $pickup)
                        <tr>
                            <td width="75" align="center">{{ $pickups->firstItem() + $index }}</td>
                            <td>
                                {{ $pickup->pickup_no }} <br>
                                {{ $pickup->branch_name }} <br>
                                <span class="label label-{{ $statuses[$pickup->pickup_status] }}">{{ ucwords($pickup->pickup_status) }} </span><br>
                                <span class="label label-primary">{{  ucwords($pickup->pickup_type) }}</span> <br>
                                <span class="label label-success">{{ ucwords($pickup->pickup_source) }}</span> <br>
                            </td>
                            <td>
                                {{ $pickup->pickup_customer_name.' - '.$pickup->account_name }} <br>
                                {{ $pickup->destination }} <br>
                                {{ $pickup->pickup_address }} <br>
                                {!! '<span class="label label-primary">'.$pickup->pickup_time.'</span> [Expected Item: '.$pickup->expected_total_items.']' !!} <br>
                            </td>
                            <td width="150" align="center">
                                {{ $pickup->created_at }}
                            </td>
                            <td>
                                {!! (!empty($pickup->pickup_remarks) ? $pickup->pickup_remarks.'<br><br>' : '') !!}
                                {!! (!empty($pickup->pickup_alternative_address) ? "Alternative Address:<br>".$pickup->pickup_alternative_address : '')  !!}<br>
                            </td>
                            <td align="center">
                                <a href="{{ url('/popexpress/pickups/view/'.$pickup->pickup_id) }}">
                                    <button class="btn btn-flat btn-info btn-small btn-update">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="{{ url('/popexpress/pickups/print/'.$pickup->pickup_id) }}">
                                    <button class="btn btn-flat btn-info btn-small btn-update">
                                        <i class="fa fa-print"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ (!empty($pickups) ? $pickups->appends($_GET)->links() : '')}}
                <button type="button" class="btn btn-default disabled pull-right" data-dismiss="modal"><strong>Total : {{ (!empty($pickups) ? $pickups->total() : '0')}}</strong></button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Filter</h4>
                </div>
                <div class="modal-body">
                    <form id="form-filter">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pickup No</label>
                                    <input type="text" name="pickup_no" id="pickup_no" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" id="pickup_status" name="pickup_status">
                                        <option value="">Select</option>
                                        @foreach ($allStatus as $status)
                                            <option value="{{ $status }}">{{ ucwords($status) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Created Date From</label>
                                    <input type="text" name="start_date" id="start_date_filter" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Created Date To</label>
                                    <input type="text" name="end_date" id="end_date_filter" class="form-control" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Branch</label>
                                    <select class="form-control" name="branch_id">
                                        <option value="">Select</option>
                                        @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}">{{ $branch->code.' - '.$branch->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Customers</label>
                                    <select class="form-control select2" name="customer_id">
                                        <option value="">Select</option>
                                        @foreach ($usersFilter as $filter)
                                            <option value="{{ $customers[$filter->id] }}">{{ $filter->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Accounts</label>
                                    <select class="form-control select2" name="account_id">
                                        <option value="">Select</option>
                                        @foreach ($accounts as $account)
                                            <option value="{{ $account->id }}">{{ $account->account_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-filter">Save</button>
                </div>
            </div>
        </div>
    </div>

    @include('popexpress.elements.alert')
@endsection

@section('js')
    <script src="{{ asset('js/popexpress.core.js') }}>"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('plugins/loadingoverlay/loadingoverlay_progress.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#start_date, #end_date, #start_date_filter, #end_date_filter').datepicker({ format: 'dd/mm/yyyy' });
        });

        $('#btn-filter').on('click', function(event) {
            $('#form-filter').submit();
        });

        $('#btn-add').on('click', function(event) {
            $('#form-add').submit();
        });

        $('.btn-update').on('click', function() {
            var data = $(this).data();
            $('#modal-add').modal('show');
            $('#id').val(data.id);
            $('#origin_id').val(data.originid);
            $('#account_id').val(data.accountid);
            $('#type').val(data.type);
            $('#value').val(parseInt(data.value));
            $('#start_date').val(data.startedit);
            $('#end_date').val(data.endedit);
            $('#airport_code').val(data.airport_code);
            $('#detail_code').val(data.detail_code);
            $('#province').val(data.province);
            $('#county').val(data.county);
            $('#district').val(data.district);
            if(data.is_locker == 1) {
                $('#is_locker').prop('checked', true);
            } else {
                $('#is_locker').prop('checked', false);
            }

            if(data.override == 'Yes') {
                $('#override').prop('checked', true);
            } else {
                $('#override').prop('checked', false);
            }
        });

        $('#btn-delete').on('click', function(event) {
            $('#form-add').submit();
        });

        $('#export').on('click', function(event) {
            var queries = {};
            var currentUrl = document.location.search;
            if(currentUrl.indexOf('?') != -1) {
                $.each(document.location.search.substr(1).split('&'),function(c,q){
                    var i = q.split('=');
                    queries[i[0].toString()] = i[1].toString();
                });
            }
            queries['_token'] = "{{ csrf_token() }}";
            $.LoadingOverlay("show");

            $.ajax({
                url: '{{ url("/popexpress/price_list/export") }}',
                type: 'POST',
                data: queries,
                success: function (data) {
                    var feedback = data;
                    if (feedback['success']) {
                        $.LoadingOverlay("hide");
                        window.location = '{{ url("/popexpress/destinations/download") }}' + '?file=' + feedback['link'];
                    } else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Informasi');
                        $(".error-body").html(feedback['message']);
                        $("#modal-error").modal("show");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (textStatus == "timeout") {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Server Time Out');
                        $(".error-body").html('Respon server terlalu lama<br>Mohon ulangi proses ini kembali');
                        $("#modal-error").modal("show");
                    }
                    else {
                        $.LoadingOverlay("hide");
                        $(".error-title").html('Error');
                        $(".error-body").html('Terjadi Kesalahan<br>Mohon Hubungi Administrator');
                        $("#modal-error").modal("show");
                    }
                }
            });
        });
    </script>
@endsection