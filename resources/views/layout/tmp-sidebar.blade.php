<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Main Menu</li>
            <li class="active"><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> <span>Home</span></a></li>
            {{-- PopBox --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-home"></i> <span>PopBox</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(\App\Models\Privilege::hasAccess('popbox'))
                        <li><a href="{{ url('popbox') }}"><i class="fa fa-home"></i>Summary</a></li>
                    @endif
                    @if(\App\Models\Privilege::hasAccess('popbox/apiManagement'))
                        <li><a href="{{ url('popbox/apiManagement') }}"><i class="fa fa-circle-o"></i>API Management</a>
                            @endif
                        </li>
                        {{-- Website --}}
                        @if (\App\Models\Privilege::hasAccess(['popbox/website/article','popbox/website/gallery']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-newspaper-o"></i> Website
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/website/article'))
                                        <li><a href="{{ url('popbox/website/article') }}"><i
                                                        class="fa fa-file-text-o"></i>Artikel</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/website/gallery'))
                                        <li><a href="{{ url('popbox/website/gallery') }}"><i
                                                        class="fa fa-picture-o"></i>Gallery</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        {{-- Product --}}
                        @if (\App\Models\Privilege::hasAccess(['popbox/sepulsa/product','popbox/sepulsa/location','popbox/sepulsa/transaction']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-th"></i> Product
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/website/gallery'))
                                        <li><a href="{{ url('popbox/sepulsa/product') }}"><i class="fa fa-mobile"></i>Sepulsa
                                                Product</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/sepulsa/location'))
                                        <li><a href="{{ url('popbox/sepulsa/location') }}"><i
                                                        class="fa fa-map-marker"></i>SepulsaLocation</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/sepulsa/transaction'))
                                        <li><a href="{{ url('popbox/sepulsa/transaction') }}"><i
                                                        class="fa fa-money"></i>SepulsaTransaction</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        {{-- Service --}}
                        @if (\App\Models\Privilege::hasAccess(['popbox/service/lastmile','popbox/service/return','popbox/service/cod','popbox/service/merchant']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-list"></i> Service
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/service/lastmile'))
                                        <li><a href="{{ url('popbox/service/lastmile') }}"><i class="fa fa-archive"></i>Last
                                                Mile</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/service/return'))
                                        <li><a href="{{ url('popbox/service/return') }}"><i class="fa fa-undo"></i>Return</a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/service/cod'))
                                        <li><a href="{{ url('popbox/service/cod') }}"><i class="fa fa-credit-card"></i>COD</a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/service/merchant'))
                                        <li><a href="{{ url('popbox/service/merchant') }}"><i
                                                        class="fa fa-circle-o"></i>Merchant Service</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        {{-- Merchant --}}
                        @if (\App\Models\Privilege::hasAccess(['popbox/sepulsa/product','popbox/sepulsa/location','popbox/sepulsa/transaction']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-money"></i> Merchant
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/merchant/setting'))
                                        <li><a href="{{ url('popbox/merchant/setting') }}"><i
                                                        class="fa fa-circle-o"></i>Setting</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/merchant/list'))
                                        <li><a href="{{ url('popbox/merchant/list') }}"><i class="fa fa-circle-o"></i>List</a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/merchant/transaction'))
                                        <li><a href="{{ url('popbox/merchant/transaction') }}"><i
                                                        class="fa fa-circle-o"></i>Transaction</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                </ul>
            </li>
            {{-- PopSend --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-envelope"></i> <span>PopSend</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if (\App\Models\Privilege::hasAccess('popsend/order'))
                        <li><a href="{{ url('popsend/order') }}"><i class="fa fa-inbox"></i>Order</a></li>
                    @endif
                    @if (\App\Models\Privilege::hasAccess('popsend/member'))
                        <li><a href="{{ url('popsend/member') }}"><i class="fa fa-users"></i>Member</a></li>
                    @endif
                    @if (\App\Models\Privilege::hasAccess('popsend/tariff'))
                        <li><a href="{{ url('popsend/tariff') }}"><i class="fa fa-list"></i>PopSend Tarif</a></li>
                    @endif
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Campaign
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('popsend/finance/member') }}"><i class="fa fa-circle-o"></i>Campaign</a>
                            </li>
                            <li><a href="{{ url('popsend/finance/member') }}"><i class="fa fa-circle-o"></i>Referal</a>
                            </li>
                            <li><a href="{{ url('popsend/finance/member') }}"><i class="fa fa-circle-o"></i>Promo
                                    Content</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Finance
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('popsend/finance/member') }}"><i class="fa fa-circle-o"></i>Member
                                    Summary</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- Pop Titip --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-archive"></i> <span>PopTitip</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Finance
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Member Summary</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- Pop Agent --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-home"></i> <span>Pop Agent</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('agent') }}"><i class="fa fa-home"></i>Summary</a></li>
                    <li><a href="{{ url('agent/list') }}"><i class="fa fa-group"></i>List</a></li>
                    <li><a href="{{ url('agent/transaction') }}"><i class="fa fa-money"></i>Transaction</a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Finance
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            @if(\App\Models\Privilege::hasAccess('agent/finance/credit'))
                                <li>
                                    <a href="{{ url('agent/finance/credit') }}"><i class="fa fa-plus-square"></i><span>Credit / Tambah</span></a>
                                </li>
                            @endif
                            @if(\App\Models\Privilege::hasAccess('agent/finance/debit'))
                                <li>
                                    <a href="{{ url('agent/finance/debit') }}"><i class="fa fa-minus-square"></i><span>Debit / Potong</span></a>
                                </li>
                            @endif
                            @if (\App\Models\Privilege::hasAccess('agent/finance/salesPending'))
                                <li>
                                    <a href="{{ url('agent/finance/salesPending') }}"><i class="fa fa-user-o"></i><span>Sales Agent Top Up</span></a>
                                </li>
                            @endif
                            <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-bookmark"></i>Member
                                    Summary</a></li>
                            <li><a href="{{ url('agent/finance/pulsa') }}"><i class="fa fa-bookmark"></i>Agent Pulsa
                                    Summary</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-suitcase"></i> Sales
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ url('agent/sales/addSales') }}"><i
                                            class="fa fa-user"></i><span>Add Sales</span></a>
                            </li>
                            <li>
                                <a href="{{ url('agent/sales/agentTopup') }}"><i class="fa fa-upload"></i><span>Top Up Agent</span></a>
                            </li>
                            <li>
                                <a href="{{ url('agent/sales/requestTopUp') }}"><i class="fa fa-money"></i><span>Transaction & Top Up</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- Pop Shop --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-shopping-cart"></i> <span>PopShop</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Finance
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Member Summary</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- Payment --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-credit-card"></i> <span>Payment</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Finance
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Member Summary</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            {{-- Locker --}}
            <li class="treeview">
                <a href="#"><i class="fa fa-th"></i> <span>Locker</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Summary</a></li>
                    <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Locker Location</a>
                    </li>
                    <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Locker Performance</a>
                    </li>
                    <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Locker Transaction</a>
                    </li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-money"></i> Finance
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-circle-o"></i>Member Summary</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-user"></i> <span>Privileges</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('privileges/module') }}">Module</a></li>
                    <li><a href="{{ url('privileges/group') }}">Groups</a></li>
                    <li><a href="{{ url('privileges/privilege') }}">Privileges</a></li>
                    <li><a href="{{ url('privileges/user') }}">Users</a></li>
                </ul>
            </li>
        </ul>
    </section>
</aside>