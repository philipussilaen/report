<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Main Menu</li>
            @if (!(\App\Models\Privilege::hasAccess(['angkasapura'])))
                <li class="active"><a href="{{ url('/') }}"><i class="fa fa-tachometer-alt"></i> <span>Home</span></a></li>
            @endif
            {{-- ANGKASA PURA 1 MENU --}}
            @if (\App\Models\Privilege::hasAccess(['angkasapura', 'map']))
                <!-- <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li> -->
                <li><a href="{{ url('angkasapura/locker/location') }}"><i class="fa fa-th"></i> <span>Locker Location</span></a></li>
                @if(\App\Models\Privilege::hasAccess('map'))
                    <li><a href="{{ route('map') }}"><i class="fa fa-map-marker"></i> Locker and Warung Location</a></li>
                @endif
                <li class="treeview">
                    <a href="#"><i class="fa fa-chart-area"></i><span>Transaction</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('angkasapura/parcelist') }}">Parcel List</a></li>
                        <li><a href="{{ url('angkasapura/transaction/v2') }}">Transaction List</a></li>
                        <li><a href="{{ url('angkasapura/payment/summary') }}">Payment</a></li>
                    </ul>
                </li>
                <!-- <li><a href="{{ url('/') }}"><i class="fa fa-users"></i> <span>User</span></a></li> -->
            @endif       
            {{-- PopBox --}}
            @if (\App\Models\Privilege::hasAccess(['popbox','popbox/locker/delivery','popbox/sepulsa/product','popbox/sepulsa/transactions','popbox/popshop/product', 'popapps/transaction', 'popapps/transaction/detail', 'popapps/delivery']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-list"></i> <span>PopBox</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(\App\Models\Privilege::hasAccess('popbox'))
                            <li><a href="{{ url('popbox') }}"><i class="fa fa-home"></i>Summary</a></li>
                        @endif
                        {{-- Locker --}}
                        @if (\App\Models\Privilege::hasAccess(['popbox','popbox/locker/delivery','popbox/locker/delivery/users','popbox/locker/delivery/pickup']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-list"></i> Locker
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/locker/location'))
                                        <li><a href="{{ url('popbox/locker/location') }}"><i class="fa fa-location-arrow"></i><span>Location</span></a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/locker/delivery'))
                                        <li><a href="{{ url('popbox/locker/delivery') }}"><i class="fa fa-list"></i><span>Delivery Activity</span></a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/locker/delivery/users'))
                                        <li><a href="{{ url('popbox/locker/delivery/users') }}"><i class="fa fa-user"></i><span>Users</span></a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/locker/delivery/pickup'))
                                        <li><a href="{{ url('popbox/locker/delivery/pickup') }}"><i class="fa fa-upload"></i><span>Order</span></a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        {{-- Product --}}
                        @if (\App\Models\Privilege::hasAccess(['popbox/sepulsa/product','popbox/sepulsa/location','popbox/sepulsa/transactions']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-phone"></i> Sepulsa
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/sepulsa/product'))
                                        <li><a href="{{ url('popbox/sepulsa/product') }}"><i class="fa fa-mobile"></i> Sepulsa Product</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/sepulsa/location'))
                                        <li><a href="{{ url('popbox/sepulsa/location') }}"><i class="fa fa-map-marker"></i> Sepulsa Location</a></li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popbox/sepulsa/transactions'))
                                        <li><a href="{{ url('popbox/sepulsa/transactions') }}"><i class="fa fa-money-bill-alt"></i> Transaction</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        {{-- Pepito --}}
                        @if(\App\Models\Privilege::hasAccess(['popapps/transaction', 'popapps/transaction/detail', 'popapps/delivery']))
                        <li class="treeview">
                            <a href="#"><i class="fa fa-desktop"></i> Digital Product
                                <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                            </a>
                            <ul class="treeview-menu">
                                @if(\App\Models\Privilege::hasAccess('popbox/pepito/transactions'))
                                    <li><a href="{{ route('transaction') }}"><i class="fa fa-money-bill-alt"></i> Transaction</a></li>
                                @endif
                            </ul>
                        </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['popbox/popshop/product']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-shopping-cart"></i> PopShop
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popbox/popshop/product'))
                                        <li><a href="{{ url('popbox/popshop/product') }}"><i class="fa fa-list-alt"></i> Product</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            {{--Popsend--}}
            @if (\App\Models\Privilege::hasAccess(['popapps/transaction']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-box-open"></i> <span> PopBox Apps</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(\App\Models\Privilege::hasAccess('popapps/popsafe'))
                            <li><a href="{{ url('popapps/popsafe') }}"><i class="fa fa-money-bill-alt"></i>PopSafe</a></li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('popapps/transaction'))
                            <li><a href="{{ url('popapps/transaction') }}"><i class="fa fa-money-bill-alt"></i>Transaction</a></li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('popapps/transaction'))
                            <li><a href="{{ url('popapps/transaction/v2') }}"><i class="fa fa-money-bill-alt"></i>Transaction V2</a></li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('popapps/delivery'))
                            <li><a href="{{ url('popapps/delivery') }}"><i class="fa fa-truck-moving"></i>Delivery List</a></li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('popapps/user/list'))
                            <li><a href="{{ url('popapps/user/list') }}"><i class="fa fa-users"></i> Users List</a></li>
                        @endif
                            @if(\App\Models\Privilege::hasAccess('popapps/user/list'))
                                <li><a href="{{ url('popapps/user/list/v2') }}"><i class="fa fa-users"></i> Users List V2</a></li>
                            @endif
                        @if (\App\Models\Privilege::hasAccess(['popapps/freeGift/list']))

                            <li class="treeview">
                                <a href="#"><i class="fa fa-gift"></i> Free Gift
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popapps/freeGift/landing'))
                                        <li>
                                            <a href="{{ url('popapps/freeGift/landing') }}"><i class="fa fa-users"></i><span>Free Gift Users</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popapps/freeGift/list'))
                                        <li>
                                            <a href="{{ url('popapps/freeGift/list') }}"><i class="fa fa-users"></i><span>List Usage</span></a>
                                        </li>
                                    @endif
									@if(\App\Models\Privilege::hasAccess('popapps/freeGift/transaction'))
                                        <li>
                                            <a href="{{ url('popapps/freeGift/transaction') }}"><i class="fa fa-users"></i><span>List Transaction</span></a>
                                        </li>
                                    @endif
									@if(\App\Models\Privilege::hasAccess('popapps/freeGift/category'))
                                        <li>
                                            <a href="{{ url('popapps/freeGift/category') }}"><i class="fa fa-users"></i><span>List Category</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['popapps/finance/credit','popapps/finance/debit']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-money-bill-alt"></i> Finance
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popapps/finance/credit'))
                                        <li>
                                            <a href="{{ url('popapps/finance/credit') }}"><i class="fa fa-plus-square"></i><span>Credit / Tambah</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popapps/finance/debit'))
                                        <li>
                                            <a href="{{ url('popapps/finance/debit') }}"><i class="fa fa-minus-square"></i><span>Debit / Potong</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['popapps/marketing/campaign','popapps/marketing/referral']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-briefcase"></i> Marketing
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popapps/marketing/campaign'))
                                        <li>
                                            <a href="{{ url('popapps/marketing/campaign') }}"><i class="fa fa-dollar-sign"></i><span>Campaign</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popapps/marketing/referral'))
                                        <li>
                                            <a href="{{ url('popapps/marketing/referral') }}"><i class="fa fa-retweet"></i><span>Referral</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('popapps/marketing/campaign'))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-briefcase"></i> Article
                                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popapps/article/list'))
                                        <li>
                                            <a href="{{ url('popapps/article/create') }}"><i class="fa fa-dollar-sign"></i><span>Create Article</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popapps/article/create'))
                                        <li>
                                            <a href="{{ url('popapps/article/list') }}"><i class="fa fa-retweet"></i><span>List Article</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popapps/article/create'))
                                        <li>
                                            <a href="{{ url('popapps/article/notification') }}"><i class="fa fa-retweet"></i><span>Push Notification</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            {{-- Pop Agent --}}
            @if(\App\Models\Privilege::hasAccess(['agent','agent/transaction','agent/finance/credit','agent/sales/agentTopup']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-home"></i> <span>Pop Agent</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(\App\Models\Privilege::hasAccess('agent'))
                            <li><a href="{{ url('agent') }}"><i class="fa fa-home"></i> Summary</a></li>
                        @endif
                            @if(\App\Models\Privilege::hasAccess('agent'))
                                <li><a href="{{ url('agent/topsales') }}"><i class="fa fa-home"></i> Top Sales</a></li>
                            @endif
                            @if(\App\Models\Privilege::hasAccess('agent'))
                                <li><a href="{{ url('agent/userinsight') }}"><i class="fa fa-home"></i> User Insight</a></li>
                            @endif
                        @if(\App\Models\Privilege::hasAccess('agent/list'))
                            <li><a href="{{ url('agent/list') }}"><i class="fa fa-users"></i> List</a></li>
                        @endif
                            @if(\App\Models\Privilege::hasAccess('agent/list/userlist'))
                                <li><a href="{{ url('agent/list/userlist') }}"><i class="fa fa-users"></i> User List</a></li>
                            @endif
                        @if(\App\Models\Privilege::hasAccess('agent/transaction'))
                            <li><a href="{{ url('agent/transaction') }}"><i class="fa fa-chart-line"></i> Transaction</a></li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('agent/transaction/list'))
                            <li><a href="{{ url('agent/transaction/list') }}"><i class="fa fa-money-bill-alt"></i> Transaction List</a></li>
                        @endif
                            @if(\App\Models\Privilege::hasAccess('agent/transaction/list/v2'))
                                <li><a href="{{ url('agent/transaction/list/v2') }}"><i class="fa fa-money-bill-alt"></i> Transaction List V2</a></li>
                            @endif
                        @if(\App\Models\Privilege::hasAccess('agent/transaction/report'))
                            <li><a href="{{ url('agent/transaction/report') }}"><i class="fa fa-globe"></i> Transaction Report</a></li>
                        @endif
                        @if(\App\Models\Privilege::hasAccess('agent/marketing/approval'))
                            <li><a href="{{ url('agent/marketing/approval') }}"><i class="fa fa-check-square"></i> Pending Approval</a></li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['agent/finance/credit','agent/finance/debit','agent/finance/salesPending','popbox/finance/member','agent/marketing/commission']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-money-bill-alt"></i> Finance
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('agent/finance/credit'))
                                        <li>
                                            <a href="{{ url('agent/finance/credit') }}"><i class="fa fa-plus-square"></i><span>Credit / Tambah</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('agent/finance/debit'))
                                        <li>
                                            <a href="{{ url('agent/finance/debit') }}"><i class="fa fa-minus-square"></i><span>Debit / Potong</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/finance/salesPending'))
                                        <li>
                                            <a href="{{ url('agent/finance/salesPending') }}"><i class="fa fa-user-circle"></i><span>Sales Agent Top Up</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess(['agent/finance/member','agent/finance/pulsa']))
                                        <li class="treeview">
                                            <a href="#"><i class="fa fa-book"></i> Report
                                                <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <li><a href="{{ url('agent/finance/member') }}"><i class="fa fa-bookmark"></i>Agent Report</a></li>
                                                <li><a href="{{ url('agent/finance/member/v2') }}"><i class="fa fa-bookmark"></i>Agent Report V2</a></li>
                                                <li><a href="{{ url('agent/finance/pulsa') }}"><i class="fa fa-bookmark"></i>Agent Pulsa Summary</a></li>
                                                <li><a href="{{ url('agent/finance/payment') }}"><i class="fa fa-bookmark"></i>Agent Payment Summary</a></li>
                                                <li><a href="{{ url('agent/finance/popshop') }}"><i class="fa fa-bookmark"></i>Agent PopShop Summary</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['agent/sales/addSales','agent/sales/agentTopup','agent/sales/requestTopUp']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-suitcase"></i> Sales
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if (\App\Models\Privilege::hasAccess('agent/sales/salesPending'))
                                        <li>
                                            <a href="{{ url('agent/sales/addSales') }}"><i class="fa fa-user"></i><span>Add Sales</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/sales/agentTopup'))
                                        <li>
                                            <a href="{{ url('agent/sales/agentTopup') }}"><i class="fa fa-upload"></i><span>Top Up Agent</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/sales/requestTopUp'))
                                        <li>
                                            <a href="{{ url('agent/sales/requestTopUp') }}"><i class="fa fa-money"></i><span>Transaction & Top Up</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['agent/marketing/commission','agent/marketing/campaign','agent/marketing/referral','agent/marketing/notification','agent/marketing/article']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-suitcase"></i> Marketing
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if (\App\Models\Privilege::hasAccess('agent/marketing/campaign'))
                                        <li>
                                            <a href="{{ url('agent/marketing/campaign') }}"><i class="fa fa-dollar-sign"></i><span>Campaign</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/marketing/commission'))
                                        <li>
                                            <a href="{{ url('agent/marketing/commission') }}"><i class="fa fa-money-bill-alt"></i><span>Commission Schema</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/marketing/referral'))
                                        <li>
                                            <a href="{{ url('agent/marketing/referral') }}"><i class="fa fa-user"></i><span>Referral Schema</span></a>
                                        </li>
                                    @endif
                                     @if (\App\Models\Privilege::hasAccess('agent/marketing/referral/transaction'))
                                        <li>
                                            <a href="{{ url('agent/marketing/referral/transaction') }}"><i class="fa fa-list-alt"></i><span>Referral Transaction</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/marketing/notification'))
                                        <li>
                                            <a href="{{ url('agent/marketing/notification') }}"><i class="fa fa-bell"></i><span>FCM Push Notification</span></a>
                                        </li>
                                    @endif
                                    @if (\App\Models\Privilege::hasAccess('agent/marketing/article'))
                                        <li>
                                            <a href="{{ url('agent/marketing/article') }}"><i class="fa fa-newspaper"></i><span>Article</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif						
                    </ul>
                </li>
            @endif
            {{-- Pop Warung --}}
            @if(\App\Models\Privilege::hasAccess(['warung','warung/catalog/category','warung/catalog/product','warung/catalog/region', 'agent/transaction/cod', 'topup-digital-registrasi', 'sales-stock', 'point-of-sales', 'executive-summary', 'purchase-summary', 'map']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-building"></i> <span>Pop Warung</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="treeview">
                            <a href="#"><i class="fa fa-suitcase"></i> Product Catalog
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/category'))
                                    <li>
                                        <a href="{{ url('warung/catalog/category') }}"><i class="fa fa-delicious"></i><span>Category</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/brand'))
                                    <li>
                                        <a href="{{ url('warung/catalog/brand') }}"><i class="fa fa-tag"></i><span>Brand</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/product'))
                                    <li>
                                        <a href="{{ url('warung/catalog/product') }}"><i class="fa fa-cubes"></i><span>Product</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/convertionuom'))
                                    <li>
                                        <a href="{{ url('warung/catalog/convertionuom') }}"><i class="fa fa-exchange"></i><span>Convertation UOM</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/region'))
                                    <li>
                                        <a href="{{ url('warung/catalog/region') }}"><i class="fa fa-map"></i><span>Region</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/uom'))
                                    <li>
                                        <a href="{{ url('warung/catalog/uom') }}"><i class="fa fa-balance-scale"></i><span>UOM</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/stock'))
                                    <li>
                                        <a href="{{ url('warung/catalog/stock') }}"><i class="fa fa-line-chart"></i><span>Stock Opname</span></a>
                                    </li>
                                @endif
                                @if(\App\Models\Privilege::hasAccess('warung/catalog/productbom'))
                                    <li>
                                        <a href="{{ url('warung/catalog/productbom') }}"><i class="fa fa-file-text-o"></i><span>Product BOM</span></a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        
                    	<li class="treeview">
                            <a href="#"><i class="fa fa-chart-area"></i> Dashboard PopWarung
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                
                                @if(\App\Models\Privilege::hasAccess('executive-summary'))
                                    <li><a href="{{ route('executive-summary') }}"><i class="fa fa-chart-line"></i> Executive Summary</a></li>
                                @endif
                                
                            	@if(\App\Models\Privilege::hasAccess('topup-digital-registrasi'))
                                    <li><a href="{{ route('topup-digital-regist') }}"><i class="fa fa-bar-chart"></i> Reg. Topup &amp; Digital</a></li>
                                @endif
                                
                                @if(\App\Models\Privilege::hasAccess('sales-stock'))
                                    <li><a href="{{ route('sales-stock') }}"><i class="fa fa-chart-line"></i> Stock Purchase</a></li>
                                @endif
                                
                                @if(\App\Models\Privilege::hasAccess('purchase-summary'))
                                    <li><a href="{{ route('purchase-summary') }}"><i class="fa fa-list"></i> Purchase Summary</a></li>
                                @endif
                                
                                @if(\App\Models\Privilege::hasAccess('point-of-sales'))
                                    <li><a href="{{ route('point-of-sales') }}"><i class="fa fa-bar-chart"></i> Point of Sales</a></li>
                                @endif
                            </ul>
                        </li>
                        
                        @if(\App\Models\Privilege::hasAccess('message-management'))
                            <li><a href="{{ route('message') }}"><i class="fa fa-envelope-square" aria-hidden="true"></i> Message Management </a></li>
                        @endif
                        
                        @if(\App\Models\Privilege::hasAccess('banner'))
                            <li><a href="{{ route('banner') }}"><i class="fa fa-picture-o" aria-hidden="true"></i> Banner Management </a></li>
                        @endif
                        
                        @if(\App\Models\Privilege::hasAccess('warung/transaction/warung'))
                            <li><a href="{{ url('warung/transaction/warung') }}"><i class="fa fa-money-bill-alt"></i> Transaction Warung</a></li>
                        @endif
                        
                        @if(\App\Models\Privilege::hasAccess('warung/report/transaction'))
                            <li><a href="{{ url('warung/report/transaction') }}"><i class="fa fa-paper-plane-o"></i> Email Transaction</a></li>
                        @endif
                        
                        @if(\App\Models\Privilege::hasAccess('agent/transaction/cod'))
                            <li><a href="{{ route('cod') }}"><i class="fa fa-check-square"></i> COD Confirmation </a></li>
                        @endif
                        
                        @if(\App\Models\Privilege::hasAccess('warung/receipt'))
                            <li><a href="{{ url('warung/receipt') }}"><i class="fa  fa-pencil-square-o"></i> Goods Delivery</a></li>
                        @endif

                        @if(\App\Models\Privilege::hasAccess('warung/version'))
                            <li><a href="{{ url('warung/version') }}"><i class="fa fa-fire"></i> Version</a></li>						
                        @endif
                    </ul>
                </li>
            @endif
            {{-- Pop Express --}}
            @if(\App\Models\Privilege::hasAccess('popexpress'))
                <li class="treeview">
                    <a href="#"><i class="fa fa-truck"></i> <span>PopExpress</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if (\App\Models\Privilege::hasAccess(['popexpress/employee', 'popexpress/branch', 'popexpress/zone', 'popexpress/origins', 'popexpress/routes', 'popexpress/branches_pickups']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-list"></i> Master Data
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popexpress/origins'))
                                        <li>
                                            <a href="{{ url('popexpress/origins') }}"><i class="fa fa-map-marker"></i><span> Origins</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/destinations'))
                                        <li>
                                            <a href="{{ url('popexpress/destinations') }}"><i class="fa fa-globe"></i><span> Destinations</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/prices'))
                                        <li>
                                            <a href="{{ url('popexpress/prices') }}"><i class="fa fa-bars"></i><span> Prices</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/branches'))
                                        <li>
                                            <a href="{{ url('popexpress/branches') }}"><i class="fa fa-building"></i><span> Branches</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/audit_trails'))
                                        <li>
                                            <a href="{{ url('popexpress/audit_trails') }}"><i class="fa fa-history"></i><span> Audit Trails</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/employees'))
                                        <li>
                                            <a href="{{ url('popexpress/employees') }}"><i class="fa fa-address-book"></i><span> Employees</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/customers'))
                                        <li>
                                            <a href="{{ url('popexpress/customers') }}"><i class="fa fa-user"></i><span> Customers</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/accounts'))
                                        <li>
                                            <a href="{{ url('popexpress/accounts') }}"><i class="fa fa-users"></i><span> Accounts</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/discounts'))
                                        <li>
                                            <a href="{{ url('popexpress/discounts') }}"><i class="fa fa-cut"></i><span> Discounts</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/price_list'))
                                        <li>
                                            <a href="{{ url('popexpress/price_list') }}"><i class="fa fa-user-circle"></i><span>Price List</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/routes'))
                                        <li>
                                            <a href="{{ url('popexpress/routes') }}"><i class="fa fa-arrows-alt"></i><span>Routes</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/branches_pickups'))
                                        <li>
                                            <a href="{{ url('popexpress/branches_pickups') }}"><i class="fa fa-archive"></i><span>Branch Pickup</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['popexpress/pickups', 'popexpress/list_pickup_details', 'popexpress/list_pickup_tasks']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-child"></i> Pickups
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                    @if(\App\Models\Privilege::hasAccess('popexpress/pickups'))
                                        <li>
                                            <a href="{{ url('popexpress/pickups') }}"><i class="fa fa-calendar"></i><span>List Pickups</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/list_pickup_details'))
                                        <li>
                                            <a href="{{ url('popexpress/pickups/list_pickup_details') }}"><i class="fa fa-align-justify"></i><span>List Pickup Details</span></a>
                                        </li>
                                    @endif
                                    @if(\App\Models\Privilege::hasAccess('popexpress/list_pickup_tasks'))
                                        <li>
                                            <a href="{{ url('popexpress/pickups/list_pickup_tasks') }}"><i class="fa fa-check-circle"></i><span>List Pickup Tasks</span></a>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                        @if (\App\Models\Privilege::hasAccess(['popexpress/employee']))
                            <li class="treeview">
                                <a href="#"><i class="fa fa-bar-chart-o"></i> Reports
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul class="treeview-menu">
                                </ul>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
            {{-- Payment --}}
            @if (\App\Models\Privilege::hasAccess(['payment/transaction','payment/channel','payment/client']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-money-bill-alt"></i> <span>Payment</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('payment/transaction') }}"><i class="fa fa-list-ul"></i> Transaction List</a></li>
                        <li><a href="{{ url('payment/channel') }}"><i class="fa fa-exchange-alt"></i> Channel</a></li>
                        <li><a href="{{ url('payment/client') }}"><i class="fa fa-users"></i> Client</a></li>
                    </ul> 
                </li>
            @endif
            {{-- Privilege --}}
            @if (\App\Models\Privilege::hasAccess(['privileges/module','privileges/user']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-user"></i> <span>Privileges</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('privileges/module') }}">Module</a></li>
                        <li><a href="{{ url('privileges/group') }}">Groups</a></li>
                        <li><a href="{{ url('privileges/privilege') }}">Privileges</a></li>
                        {{--<li><a href="{{ url('privileges/privilege/landing') }}">Privileges V2</a></li>--}}
                        <li><a href="{{ url('privileges/user') }}">Users</a></li>
                    </ul>
                </li>
            @endif
            {{-- Log Agent --}}
            @if (\App\Models\Privilege::hasAccess(['log/agent']))
                <li class="treeview">
                    <a href="#"><i class="fa fa-book"></i> <span>Log Access</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('log/agent') }}">Agent</a></li>
                        <li><a href="{{ url('log/logs') }}">Error Log</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </section>
</aside>