@extends('layout.main')

@section('title')
    Agent Detail
@endsection

@section('css')
    <style type="text/css">
        #maps {
            width: 100%;
            height: 384px;
        }
        .rotate90 {
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
        }
        img {
          display: block;
          margin: auto;
        }
    </style>
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/pretty-checkbox/dist/pretty-checkbox.min.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('pageTitle')
    Dashboard Agent
@endsection

@section('pageDesc')
    Agent Detail
@endsection

@section('content')
    <section class="content">
        <div class="row">
            {{-- left column --}}
            <div class="col-lg-3">
                {{-- Agent Profile --}}
                <div class="box box-widget widget-user">
                    {{--@if ($lockerData->type == 'warung')--}}
                        {{--<div class="widget-user-header bg-green">--}}
                            {{--<h3 class="widget-user-username">{{ $lockerData->locker_name }}</h3>--}}
                            {{--<h5 class="widget-user-desc">{{ $lockerData->locker_id }}</h5>--}}
                            {{--<h5 class="description-header">--}}
                                {{--@if ($lockerData->status==0)--}}
                                    {{--<label class="label bg-gray">Menunggu Aktivasi</label>--}}
                                {{--@elseif($lockerData->status==1)--}}
                                    {{--<label class="label bg-maroon">Online</label>--}}
                                {{--@elseif($lockerData->status==2)--}}
                                    {{--<label class="label bg-black text-white">Offline</label>--}}
                                {{--@endif--}}
                            {{--</h5>--}}
                        {{--</div>--}}
                    {{--@else--}}
                    {{--<div class="widget-user-header bg-aqua">--}}
                        {{--<h3 class="widget-user-username">{{ $lockerData->locker_name }}</h3>--}}
                        {{--<h5 class="widget-user-desc">{{ $lockerData->locker_id }}</h5>--}}
                        {{--<h5 class="description-header">--}}
                            {{--@if ($lockerData->status==0)--}}
                                {{--<label class="label bg-gray">Menunggu Aktivasi</label>--}}
                            {{--@elseif($lockerData->status==1)--}}
                                {{--<label class="label bg-maroon">Online</label>--}}
                            {{--@elseif($lockerData->status==2)--}}
                                {{--<label class="label bg-black text-white">Offline</label>--}}
                            {{--@endif--}}
                        {{--</h5>--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    <div class="widget-user-header bg-aqua">
                        <h3 class="widget-user-username">{{ $lockerData->locker_name }}</h3>
                        <h5 class="widget-user-desc">{{ $lockerData->locker_id }}</h5>
                        <h5 class="description-header">
                            @if ($lockerData->status==0)
                                <label class="label bg-gray">Menunggu Aktivasi</label>
                            @elseif($lockerData->status==1)
                                <label class="label bg-maroon">Online</label>
                            @elseif($lockerData->status==2)
                                <label class="label bg-black text-white">Offline</label>
                            @endif
                        </h5>
                    </div>
                    <div class="box-footer" style="padding-top: 10px;">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        @if ($lockerBalance->status_verification == 'disable')
                                            <label class="label bg-marron">Disabled</label>
                                        @elseif($lockerBalance->status_verification == 'enable' || empty($lockerBalance->status_verification))
                                            <label class="label bg-gray">Enable</label>
                                        @elseif($lockerBalance->status_verification == 'verified' )
                                            <label class="label bg-green text-white">Verified</label>
                                        @elseif($lockerBalance->status_verification == 'pending' )
                                            <label class="label bg-black text-white">Pending</label>
                                        @endif
                                    </h5>
                                    <span class="description-text">Verification Status</span>
                                </div>
                            </div>
                            <div class="col-sm-6 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">Rp {{ number_format($balance) }}</h5>
                                    <span class="description-text">Deposit</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        @if (empty($lockerData->buildingType))
                                            <label class="label bg-red">Undefined</label>
                                        @else
                                            <h5 class="description-header">{{ $lockerData->buildingType->building_types }}</h5>
                                        @endif
                                    </h5>
                                    <span class="description-text">Kategori</span>
                                </div>
                            </div>
                            <div class="col-sm-6 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{ date('j F Y',strtotime($lockerData->created_at)) }}</h5>
                                    <span class="description-text">Tanggal Daftar</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="description-block">
                                    <h5 class="description-header">{{ date('j F Y',strtotime($lockerData->last_ping)) }}</h5>
                                    <span class="description-text">Terakhir Aktif</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body box-profile">
                        <p class="text-center">
                            @if ($lockerData->status==0)
                                <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modalActivation">Aktivasi
                                </button>
                            @else
                                @if(\App\Models\Privilege::hasAccess('agent/update'))
                                    <button class="btn btn-flat btn-primary" data-toggle="modal" data-target="#modalUpdate">Update
                                    </button>
                                @endif
                            @endif
                            @if (empty($express))
                                @if(\App\Models\Privilege::hasAccess('agent/activateExpress'))
                                    <button class="btn btn-flat btn-info" data-toggle="modal" data-target="#modalExpress">Activate Delivery</button>
                                @endif
                            @endif
                            @if(\App\Models\Privilege::hasAccess('agent/changeStatus'))
                                <button class="btn btn-flat btn-warning" data-toggle="modal" data-target="#modalChange">Change Status</button>
                            @endif
                                @if(\App\Models\Privilege::hasAccess('agent/changeType'))
                                    @if ($lockerData->type != 'warung')
                                        <button class="btn btn-flat btn-success" onclick="convertToWarung('{{$lockerData->locker_id}}')">Convert To Warung</button>
                                    @endif
                                @endif
                        </p>
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if (session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif

                        <strong>Alamat</strong>
                        <p class="text-muted">{{ $lockerData->address }} {{ $lockerData->city->city_name }}</p>

                        <strong>Provinsi</strong>
                        <p class="text-muted">{{ $lockerData->city->province->province_name }}</p>

                        <strong>Alamat Detail</strong>
                        <p class="text-muted">{{ $lockerData->detail_address }}</p>

                        <strong>Lokasi Peta</strong>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="maps">

                                </div>
                            </div>
                        </div>

                        <strong>Jam Operasional</strong>
                        <table class="table">
                            @php
                                $days =['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
                            @endphp
                            @foreach ($lockerData->operational_time as $time)
                                @php
                                    $style = '';
                                    $class = '';
                                    if(date('w') == $time->day){
                                        $style = 'font-weight:bold;';
                                        $class = 'success';
                                    }
                                @endphp
                                <tr class="{{ $class }}">
                                    <td style="{{ $style }}">
                                        {{ $days[$time->day] }}
                                    </td>
                                    <td style="{{ $style }}">
                                        @if (empty($time->open_hour) && empty($time->close_hour))
                                            <span class="label label-danger">Tutup</span>
                                        @else
                                            {{ sprintf("%02d", $time->open_hour) }}.00
                                            -  {{ sprintf("%02d", $time->close_hour) }}.00
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <strong>Layanan</strong>
                        <p class="text-muted">
                            @foreach ($lockerData->services as $element)
                                <span class="label bg-gray">{{ $element->name }}</span>
                            @endforeach
                        </p>
                        <hr>
                        @if (!empty($express))
                            <strong>Branch ID</strong>
                            <p class="text-muted">{{ $express->branch_id }}</p>
                            <strong>Express Code</strong>
                            <p class="text-muted">{{ $express->agent_code }}</p>
                        @endif
                        <hr>
                        @if (!empty($lockerBalance))
                            @foreach ($lockerBalance->vaOpen as $item)
                                <strong>{{ strtoupper(str_replace('_', ' ', $item->va_type)) }}</strong>
                                <p class="text-muted">{{ $item->va_number }}</p>
                                <p class="text-muted">{{ $item->payment_id }}</p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            {{-- Right Column --}}
            <div class="col-lg-9">
                <div class="box">
                    <div class="box-header">
                        {{--<h4 class="box-title">User Data</h4>--}}
                        <div class="col-md-10">
                            <h3>Agent Data</h3>
                        </div>
                        <div class="col-md-2">
                            <button id="btn-show-hide-filter" type="button" class="btn btn-primary btn-sm pull-right" onclick="showHideBox()" style="margin-top: 10px">
                                Hide
                            </button>
                        </div>
                    </div>
                    <div id="box-user-data" class="box-body">
                        @if(!empty($agentData))
                            @foreach($agentData as $agent)
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>Nama</strong>
                                        <p class="text-muted">{{ $agent->name }}</p>
                                        <strong>No. Handphone</strong>
                                        <p class="text-muted">{{ $agent->phone }}</p>

                                    </div>
                                    <div class="col-md-6">
                                        <strong>Email</strong>
                                        <p class="text-muted">{{ $agent->email }}</p>
                                        <strong>Status</strong>
                                        <p class="text-muted">
                                            @if ($agent->status == 0)
                                                <span class="label bg-gray">Menunggu Aktivasi</span>
                                            @elseif ($agent->status == 1)
                                                <span class="label bg-maroon">Aktif Oleh User</span>
                                            @elseif ($agent->status ==2 )
                                                <span class="label bg-black text-white">Aktif oleh PopBox</span>
                                            @elseif ($agent->status ==3 )
                                                <span class="label bg-red text-white">Disabled</span>
                                            @endif
                                        </p>

                                    </div>
                                    <div class="col-md-12 table-responsive">
                                        <table class="table table-bordered" style="width: 100%">
                                            <thead>
                                            <tr>
                                                <th class="text-center" style="width: 25%;">ID Picture</th>
                                                <th class="text-center" style="width: 25%;">ID Selfie</th>
                                                <th class="text-center" style="width: 25%;">Store Picture</th>
                                                <th class="text-center" style="width: 25%;">NPWP Picture</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td style="vertical-align: middle;width: 25%;">
                                                    @if (@getimagesize( env('AGENT_URL').'agent/id/'.$agent->id_picture ))
                                                        <a href="{{ env('AGENT_URL').'agent/id/'.$agent->id_picture }}" target="_blank" >
                                                        	<img alt="ID Picture" src="{{ env('AGENT_URL').'agent/id/'.$agent->id_picture }}" style="max-width: 175px;height:auto;">
                                                    	</a>
                                                    @else
                                                        <img src="http://via.placeholder.com/100?text=Unvailable" data-link="{{ env('AGENT_URL').'agent/id/'.$agent->id_picture }}">
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;width: 25%;">
                                                    @if (@getimagesize( env('AGENT_URL').'agent/id_selfie/'.$agent->id_selfie ))
                                                        <a href="{{ env('AGENT_URL').'agent/id_selfie/'.$agent->id_selfie }}" target="_blank" >
                                                        	<img class="margin" alt="ID Selfie" src="{{ env('AGENT_URL').'agent/id_selfie/'.$agent->id_selfie }}" style="max-width: 175px;height:auto">
                                                    	</a>
                                                    @else
                                                        <img src="http://via.placeholder.com/100?text=Unvailable" data-link="{{ env('AGENT_URL').'agent/id_selfie/'.$agent->id_selfie }}">
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;width: 25%;">
                                                    @if (@getimagesize( env('AGENT_URL').'agent/store/'.$lockerData->locker_id.".jpg" ))
                                                        <a href="{{ env('AGENT_URL').'agent/store/'.$lockerData->locker_id.".jpg" }}" target="_blank" >
                                                        	<img alt="ID Selfie" src="{{ env('AGENT_URL').'agent/store/'.$lockerData->locker_id.".jpg" }}" style="max-width: 175px;height:auto;">
                                                        </a>
                                                    @else
                                                        <img src="http://via.placeholder.com/100?text=Unvailable" data-link="{{ env('VLOCKER_URL').$lockerData->locker_id.".jpg" }}">
                                                    @endif
                                                </td>
                                                <td style="vertical-align: middle;width: 25%;">
                                                    @if (@getimagesize( env('AGENT_URL').'agent/npwp/'.$agent->npwp_picture ))
                                                        <a href="{{ env('AGENT_URL').'agent/npwp/'.$agent->npwp_picture }}" target="_blank" >
                                                        	<img width="150" class="margin" alt="ID Picture" src="{{ env('AGENT_URL').'agent/npwp/'.$agent->npwp_picture }}" style="max-width: 175px">
                                                    	</a>
                                                    @else
                                                        <img src="http://via.placeholder.com/100?text=Unvailable" data-link="{{ env('AGENT_URL').'agent/id/'.$agent->npwp_picture }}">
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <p>{{ $lockerBalance->id_verification }}</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>{{ $lockerBalance->selfie_verification }}</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>{{ $lockerBalance->store_verification }}</p>
                                                </td>
                                                <td class="text-center">
                                                    <p>{{ $lockerBalance->npwp_verification }}</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-solid box-topall-type">
                            <div class="box-body border-radius-none">
                                <div class="box-header with-border" style="margin-top: -30px">
                                    <div class="col-md-1">
                                        <h3>Filter</h3>
                                    </div>
                                </div>
                            </div>
                            <div id="box-filter" class="box-body border-radius-none">
                                <div class="row col-md-12">
                                    <div class="col-md-6" style="margin-left: -10px; margin-right: 10px; margin-bottom: 45px">
                                        <h5>Transaction Date</h5>
                                        <div class="row" style="margin-left: 1px">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" id="daterange-transactions" name="dateRange-transactions" placeholder="Transaction Date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row" style="margin-top: 37px">
                                            <button id="btn-filter-submit" type="button" class="btn btn-primary btn-sm" onclick="getSalesData()" style="width: 100px">
                                                Filter
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- TRANSACTION, TOPUP, COMMISSION, REWARD, REFERRAL --}}
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- TRANSACTION --}}
                                    <div class="col-md-4">
                                        <div id="box-transaction" class="box box-solid box-transaction" style="background-color: whitesmoke; height: 135px">
                                            <div class="box-body border-radius-none" style="height: 135px;">
                                                <div style="margin-top: 0px; margin-left: 20px; color: black">TRANSACTION</div>
                                                <div id="transaction-amount" style="margin-top: 5px; margin-left: 20px; font-size: 30px; color: black">Rp0,00</div>
                                                <div style="margin-top: 20px; margin-left: -10px; margin-right: -10px; height: 2px; background-color: slategray"></div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="margin-left: -10px; margin-top: 5px; color: black">count:</div>
                                                    <div class="col-md-2" id="transaction-count" style="margin-top: 5px; margin-left: 10px; color: black">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- TOPUP --}}
                                    <div class="col-md-4">
                                        <div id="box-topup" class="box box-solid box-topup" style="background-color: whitesmoke; height: 135px">
                                            <div class="box-body border-radius-none" style="height: 135px;">
                                                <div style="margin-top: 0px; margin-left: 20px; color: black">TOP UP</div>
                                                <div id="topup-amount" style="margin-top: 5px; margin-left: 20px; font-size: 30px; color: black">Rp0,00</div>
                                                <div style="margin-top: 20px; margin-left: -10px; margin-right: -10px; height: 2px; background-color: slategray"></div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="margin-left: -10px; margin-top: 5px; color: black">count:</div>
                                                    <div class="col-md-2" id="topup-count" style="margin-top: 5px; margin-left: 10px; color: black">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- COMMISSION, REWARD, REFERRAL --}}
                                    <div class="col-md-4">
                                        <div id="box-commission" class="box box-solid box-commission" style="background-color: whitesmoke; height: 135px">
                                            <div class="box-body border-radius-none" style="height: 135px;">
                                                <div style="margin-top: 0px; margin-left: 20px; color: black">COMMISSION, REWARD, REFERRAL</div>
                                                <div id="commission-amount" style="margin-top: 5px; margin-left: 20px; font-size: 30px; color: black">Rp0,00</div>
                                                <div style="margin-top: 20px; margin-left: -10px; margin-right: -10px; height: 2px; background-color: slategray"></div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="margin-left: -10px; margin-top: 5px; color: black">count:</div>
                                                    <div class="col-md-2" id="commission-count" style="margin-top: 5px; margin-left: 10px; color: black">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- PULSA, BILLER, BELANJA --}}
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- PULSA --}}
                                    <div class="col-md-4">
                                        <div id="box-pulsa" class="box box-solid box-pulsa" style="background-color: #469BF4; height: 135px">
                                            <div class="box-body border-radius-none" style="height: 135px;">
                                                <div style="margin-top: 0px; margin-left: 20px; color: white">PULSA</div>
                                                <div id="pulsa-amount" style="margin-top: 5px; margin-left: 20px; font-size: 30px; color: white">Rp0,00</div>
                                                <div style="margin-top: 20px; margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="margin-left: -10px; margin-top: 5px; color: white">count:</div>
                                                    <div class="col-md-2" id="pulsa-count" style="margin-top: 5px; margin-left: 10px; color: white">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- BILLER --}}
                                    <div class="col-md-4">
                                        <div id="box-biller" class="box box-solid box-biller" style="background-color: #5FC4D7; height: 135px">
                                            <div class="box-body border-radius-none" style="height: 135px;">
                                                <div style="margin-top: 0px; margin-left: 20px; color: white">BILLER</div>
                                                <div id="biller-amount" style="margin-top: 5px; margin-left: 20px; font-size: 30px; color: white">Rp0,00</div>
                                                <div style="margin-top: 20px; margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="margin-left: -10px; margin-top: 5px; color: white">count:</div>
                                                    <div class="col-md-2" id="biller-count" style="margin-top: 5px; margin-left: 10px; color: white">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- BELANJA--}}
                                    <div class="col-md-4">
                                        <div id="box-popshop" class="box box-solid box-popshop" style="background-color: #E24458; height: 135px">
                                            <div class="box-body border-radius-none" style="height: 135px;">
                                                <div style="margin-top: 0px; margin-left: 20px; color: white">BELANJA</div>
                                                <div id="popshop-amount" style="margin-top: 5px; margin-left: 20px; font-size: 30px; color: white">Rp0,00</div>
                                                <div style="margin-top: 20px; margin-left: -10px; margin-right: -10px; height: 2px; background-color: white"></div>
                                                <div class="col-md-12">
                                                    <div class="col-md-2" style="margin-left: -10px; margin-top: 5px; color: white">count:</div>
                                                    <div class="col-md-2" id="popshop-count" style="margin-top: 5px; margin-left: 10px; color: white">0</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="javascript:void(0)" onclick="updateViewSalesData(1)">
                                        <h3 id="daily-sales" class="col-md-4" style="text-align: center; color: dimgray">DAILY</h3>
                                    </a>
                                    <a href="javascript:void(0)" onclick="updateViewSalesData(2)">
                                        <h3 id="weekly-sales" class="col-md-4" style="text-align: center; color: cornflowerblue">WEEKLY</h3>
                                    </a>
                                    <a href="javascript:void(0)" onclick="updateViewSalesData(3)">
                                        <h3 id="monthly-sales" class="col-md-4" style="text-align: center; color: dimgray">MONTHLY</h3>
                                    </a>
                                </div>
                                <div class="col-md-12">
                                    <h5 id="selected-daily-sales" class="col-md-4" style="margin-top: -5px; text-align: center; color: dimgray"></h5>
                                    <h5 id="selected-weekly-sales" class="col-md-4" style="margin-top: -5px; text-align: center; color: cornflowerblue">selected</h5>
                                    <h5 id="selected-monthly-sales" class="col-md-4" style="margin-top: -5px; text-align: center; color: dimgray"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="top-selling-graph" class="col-md-10 box-body">
                                        <canvas id="myChart" height= auto></canvas>
                                    </div>
                                    <div class="col-md-2" style="margin-top: 100px">
                                        <div class="row">
                                            <input id="switch-chart" type="checkbox" checked data-toggle="toggle"
                                                   data-on="Total" data-off="Amount" data-onstyle="primary" data-offstyle="primary" data-size="small">
                                        </div>
                                        <div class="row" style="margin-top: 50px; margin-bottom: 10px">
                                            <div class="pretty p-default p-round p-thick">
                                                <input id="pulsa-checkbox" type="checkbox" checked="true" />
                                                <div class="state p-primary-o">
                                                    <label>Pulsa</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px">
                                            <div class="pretty p-default p-round p-thick">
                                                <input id="popshop-checkbox" type="checkbox" checked="true"/>
                                                <div class="state p-primary-o">
                                                    <label>Belanja</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="pretty p-default p-round p-thick">
                                                <input id="biller-checkbox" type="checkbox" checked="true"/>
                                                <div class="state p-primary-o">
                                                    <label>Biller</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <div id="box-table-agent" class="box-body">
                                        <table id="table-agent" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Biller</th>
                                                <th>Biller Amount</th>
                                                <th>Non-Digital</th>
                                                <th>Non-Digital Amount</th>
                                                <th>Digital</th>
                                                <th>Digital Amount</th>
                                                <th>Total Transaction</th>
                                                <th>Total Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbody-table-agent"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Chart Transaction --}}
                <div class="nav-tabs-custom" id="box-graph" hidden="true">
                    <ul class="nav nav-tabs pull-right">
                        <li><a href="#tab_1" data-toggle="tab" data-type="daily">Daily</a></li>
                        <li class="active"><a href="#tab_2" data-toggle="tab" data-type="weekly">Weekly</a></li>
                        <li><a href="#tab_3" data-toggle="tab" data-type="monthly">Monthly</a></li>
                        <li class="pull-left header"><i class="fa fa-chart-line"></i>Success Transaction</li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab_1">
                            <div class="chart">
                                <canvas id="transaction-daily" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane active" id="tab_2">
                            <div class="chart">
                                <canvas id="transaction-weekly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="chart">
                                <canvas id="transaction-monthly" style="height: 180px; width: 1072px;"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon"
                                        id="transaction-count">{{ number_format($transactionCount) }}</h5>
                                    <span class="description-text text-uppercase">Jumlah Transaksi</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header" id="transaction-amount">
                                        Rp {{ number_format($transactionAmount) }}</h5>
                                    <span class="description-text text-uppercase">Total Transaksi</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header" id="topup-amount">
                                        Rp {{ number_format($topupAmount) }}</h5>
                                    <span class="description-text text-uppercase">Total Top Up</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header" id="reward-amount">
                                        Rp {{ number_format($rewardAmount) }}</h5>
                                    <span class="description-text text-uppercase">Total Komisi, Bonus & Referral</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon"
                                        id="pulsa-count">{{ number_format($pulsaCount) }}</h5>
                                    <span class="description-text text-uppercase">Total Pulsa</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon"
                                        id="payment-count">{{ number_format($paymentCount) }}</h5>
                                    <span class="description-text text-uppercase">Total Pembayaran</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <h5 class="description-header text-maroon"
                                        id="popshop-count">{{ number_format($popshopCount) }}</h5>
                                    <span class="description-text text-uppercase">Total Belanja</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block">
                                    <h5 class="description-header text-maroon"
                                        id="delivery-count">{{ number_format($deliveryCount) }}</h5>
                                    <span class="description-text text-uppercase">Total Pengiriman</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Update / Activate Modal --}}
    @if ($lockerData->status==0)
        <div class="modal fade" id="modalActivation" tabindex="-1" role="dialog" aria-labelledby="modalActivationLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalActivationLabel">Aktivasi Agen</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/activate') }}" id="form-activation">
                            {{ csrf_field() }}
                            <input type="hidden" name="lockerId" value="{{ $lockerData->locker_id }}">
                            <h3>Konfirmasi Data Agent</h3>

                            {{-- <div class="form-group">
                                <label>Top Up Deposit</label>
                                <input type="text" name="topupAmount" class="form-control" value="0">
                            </div> --}}
                            <div class="form-group">
                                <label>Registered By</label>
                                <select name="registered_by" class="form-control select2">
                                    @foreach ($userRegisteredBy as $element)
                                        <option value="{{ $element->id }}">{{ $element->name }}
                                            ({{ $element->group->name }})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Locker Kategori</label>
                                <select name="building_type" class="form-control" required>
                                    <option value="">Empty</option>
                                    @foreach ($listBuildingTypes as $element)
                                        <option value="{{ $element->id }}">{{ $element->building_types }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea class="form-control" rows="3"
                                          name="address">{{ $lockerData->address }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Alamat Detail</label>
                                <textarea class="form-control" rows="3"
                                          name="detail_address">{{ $lockerData->detail_address }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Kota, Provinsi</label>
                                <select class="form-control" name="province">
                                    @foreach ($provinces as $province)
                                        @php
                                            $selected = '';
                                            if ($province->id == $lockerData->city->province->id) {
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{ $province->id }}" {{ $selected }}>{{ $province->province_name }}</option>
                                    @endforeach
                                </select>
                                <select class="form-control" name="city">

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Layanan</label>
                                @php
                                    $listService = [];
                                    foreach ($lockerData->services as $element) {
                                        $listService[] = $element->code;
                                    }
                                @endphp
                                <select class="form-control" name="services[]" multiple="">
                                    @foreach ($services as $service)
                                        @php
                                            $selected = '';
                                            if (in_array($service->code, $listService)) {
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{ $service->code }}" {{ $selected }}>{{ $service->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Catatan Tambahan</label>
                                <textarea class="form-control" name="remarks" rows="3"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-activation">Aktivasi</button>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="modalUpdateLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalUpdateLabel">Update Data Agen</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/update') }}" id="form-update">
                            {{ csrf_field() }}
                            <input type="hidden" name="lockerId" value="{{ $lockerData->locker_id }}">
                            <h3>Update Data Agent</h3>
                            @if (empty($lockerData->registered_by))
                                <div class="form-group">
                                    <label>Registered By</label>
                                    <select name="registered_by" class="form-control select2">
                                        @foreach ($userRegisteredBy as $element)
                                            <option value="{{ $element->id }}">{{ $element->name }}
                                                ({{ $element->group->name }})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                            <div class="form-group">
                                <label>Locker Kategori</label>
                                <select name="building_type" class="form-control" required>
                                    <option value="">Empty</option>
                                    @foreach ($listBuildingTypes as $element)
                                        <option value="{{ $element->id }}">{{ $element->building_types }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea class="form-control" rows="3"
                                          name="address">{{ $lockerData->address }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Alamat Detail</label>
                                <textarea class="form-control" rows="3"
                                          name="detail_address">{{ $lockerData->detail_address }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Kota, Provinsi</label>
                                <select class="form-control" name="province">
                                    @foreach ($provinces as $province)
                                        @php
                                            $selected = '';
                                            if ($province->id == $lockerData->city->province->id) {
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{ $province->id }}" {{ $selected }}>{{ $province->province_name }}</option>
                                    @endforeach
                                </select>
                                <select class="form-control" name="city">

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Layanan</label>
                                @php
                                    $listService = [];
                                    foreach ($lockerData->services as $element) {
                                        $listService[] = $element->code;
                                    }
                                @endphp
                                <select class="form-control" name="services[]" multiple="">
                                    @foreach ($services as $service)
                                        @php
                                            $selected = '';
                                            if (in_array($service->code, $listService)) {
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{ $service->code }}" {{ $selected }}>{{ $service->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Catatan Tambahan</label>
                                <textarea class="form-control" name="remarks"
                                          rows="3">{{ $lockerData->remarks }}</textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn-update">Update</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
    {{-- Express Modal --}}
    <div class="modal fade" id="modalExpress" tabindex="-1" role="dialog" aria-labelledby="modalActivationLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalActivationLabel">Aktivasi Agen</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{ url('agent/activateExpress') }}" id="form-activation">
                        {{ csrf_field() }}
                        <input type="hidden" name="lockerId" value="{{ $lockerData->locker_id }}">
                        <h3>Konfirmasi Data Agent</h3>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-activation">Aktivasi Express</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Change Status --}}
    @if(\App\Models\Privilege::hasAccess('agent/changeStatus'))
        <div class="modal fade" id="modalChange" tabindex="-1" role="dialog" aria-labelledby="modalActivationLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalActivationLabel">Change Status Agen</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ url('agent/changeStatus') }}" id="form-change" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="lockerId[]" value="{{ $lockerData->locker_id }}">
                            <strong>Agent Name:</strong> {{ $lockerData->locker_name }} <br>
                            <strong>Agent Id:</strong> {{ $lockerData->locker_id }} <br>
                            @if ($lockerBalance->status_verification == 'disable')
                                <label class="label bg-marron">Disabled</label>
                            @elseif($lockerBalance->status_verification == 'enable' || empty($lockerBalance->status_verification))
                                <label class="label bg-gray">Enable</label>
                            @elseif($lockerBalance->status_verification == 'verified' )
                                <label class="label bg-green text-white">Verified</label>
                            @elseif($lockerBalance->status_verification == 'pending' )
                                <label class="label bg-black text-white">Pending</label>
                            @endif
                            <hr>
                            <div class="form-group">
                                <label>Registered By</label>
                                <select name="registered_by" class="form-control select2">
                                    @foreach ($userRegisteredBy as $element)
                                        @php
                                            $selected = '';
                                            if ($element->id == $lockerData->registered_by) {
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{ $element->id }}">{{ $element->name }}
                                            ({{ $element->group->name }})
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" id="status" name="status">
                                    <option value="DISABLE" @if ($lockerBalance->status_verification == 'disable') selected @endif>DISABLE</option>
                                    <option value="ENABLE" @if ($lockerBalance->status_verification == 'enable') selected @endif>ENABLE</option>
                                    <option value="VERIFIED" @if ($lockerBalance->status_verification == 'verified') selected @endif>VERIFIED</option>
                                </select>
                                <div id="list-agent">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ID</label>
                                <input type="file" name="idPicture[{{ $lockerData->locker_id }}]" class="form-control"> <br>
                                <input type="text" name="idStatus[{{ $lockerData->locker_id }}]" class="form-control" id="idStatus-{{ $lockerData->locker_id }}">
                                {{-- Button --}}
                                <button class="btn btn-xs btn-flat btn-danger idPicture" data-locker="{{ $lockerData->locker_id }}" value="invalid|Gagal Terverifikasi" type="button">Invalid</button>
                                <button class="btn btn-xs btn-flat btn-success idPicture" data-locker="{{ $lockerData->locker_id }}" value="valid|Sukses Terverifikasi" type="button">valid</button>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>ID Selfie</label>
                                <input type="file" name="idSelfiePicture[{{ $lockerData->locker_id }}]" class="form-control"> <br>
                                <input type="text" name="selfieStatus[{{ $lockerData->locker_id }}]" class="form-control" id="selfieStatus-{{ $lockerData->locker_id }}">
                                {{-- Button --}}
                                <button class="btn btn-xs btn-flat btn-danger idSelfie" data-locker="{{ $lockerData->locker_id }}" value="invalid|Silahkan upload foto selfie dengan KTP (KTP diposisikan di bawah dagu)" type="button">Invalid</button>
                                <button class="btn btn-xs btn-flat btn-success idSelfie" data-locker="{{ $lockerData->locker_id }}" value="valid|Sukses Terverifikasi" type="button">valid</button>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Store Photo</label>
                                <input type="file" name="storePhoto[{{ $lockerData->locker_id }}]" class="form-control"> <br>
                                <input type="text" name="storeStatus[{{ $lockerData->locker_id }}]" class="form-control" id="storeStatus-{{ $lockerData->locker_id }}">
                                {{-- Button --}}
                                <button class="btn btn-xs btn-flat btn-danger storePhoto" data-locker="{{ $lockerData->locker_id }}" value="invalid|Gagal Terverifikasi" type="button">Invalid</button>
                                <button class="btn btn-xs btn-flat btn-success storePhoto" data-locker="{{ $lockerData->locker_id }}" value="valid|Sukses Terverifikasi" type="button">valid</button>
                            </div>
                            <div class="form-group">
                                <label>NPWP Photo</label>
                                <input type="file" name="npwpPhoto[{{ $lockerData->locker_id }}]" class="form-control"> <br>
                                <input type="text" name="npwpStatus[{{ $lockerData->locker_id }}]" class="form-control" id="npwpStatus-{{ $lockerData->locker_id }}">
                                {{-- Button --}}
                                <button class="btn btn-xs btn-flat btn-danger npwpPhoto" data-locker="{{ $lockerData->locker_id }}" value="invalid|Gagal Terverifikasi" type="button">Invalid</button>
                                <button class="btn btn-xs btn-flat btn-success npwpPhoto" data-locker="{{ $lockerData->locker_id }}" value="valid|Sukses Terverifikasi" type="button">valid</button>
                            </div>
                            <hr>
                            <strong>User List : </strong> <br>
                            @if(!empty($agentData))
                                @foreach($agentData as $agent)
                                    <strong>Nama</strong>
                                    <p class="text-muted">{{ $agent->name }}</p>
                                    <strong>No. Handphone</strong>
                                    <p class="text-muted">{{ $agent->phone }}</p>
                                    <strong>Email</strong>
                                    <p class="text-muted">{{ $agent->email }}</p>
                                    <strong>Status</strong>
                                    <p class="text-muted">
                                        @if ($agent->status == 0)
                                            <span class="label bg-gray">Menunggu Aktivasi</span>
                                        @elseif ($agent->status == 1)
                                            <span class="label bg-maroon">Aktif Oleh User</span>
                                        @elseif ($agent->status ==2 )
                                            <span class="label bg-black text-white">Aktif oleh PopBox</span>
                                        @elseif ($agent->status ==3 )
                                            <span class="label bg-red text-white">Disabled</span>
                                        @endif
                                    </p>
                                @endforeach
                            @endif
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" id="btn-change">Change Status</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('js')
    <script src="{{ asset('plugins/jquery/dist/jquery.min.js')}}"></script>
    {{-- Morris.js charts --}}
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morris.js/morris.min.js') }}"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('plugins/bootstrap-notify-3.1.3/dist/bootstrap-notify.min.js')}}"></script>
    {{-- NUMERATOR --}}
    <script src="{{ asset('plugins/jquery-numerator/jquery-numerator.js')}}"></script>
    <script src="{{ asset('plugins/currencyformatter.js/dist/currencyFormatter.min.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    {{-- Chart JS --}}
    <script type="text/javascript">
        $(document).ready(function () {
            // Option Chart
            var chartOption = {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: true,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 10,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: false,
                //String - A legend template
                {{--legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",--}}
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                scaleLabel:
                    function(label){return  label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");},
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            return tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                    }
                }
            };

            /**
             * Create Daily Chart
             * @return {[type]} [description]
             */
            function createDaily(){
                var transactionChartCanvas = $("#transaction-daily");
                var transactionChart = new Chart(transactionChartCanvas, {
                    type : 'bar',
                    data : {
                        labels : {!! json_encode($dayLabels) !!},
                        datasets :[{
                            label: "Transaksi Penjualan Mingguan",
                            data: {!! json_encode($dayCountTransaction) !!},
                            backgroundColor : 'rgba(216, 27, 96, 0.8)'
                        }]
                    }
                });
            };

            /**
             * Create Weekly Charts
             * @return {[type]} [description]
             */
            function createWeekly(){
                var transactionChartCanvas = $("#transaction-weekly");
                var transactionChart = new Chart(transactionChartCanvas, {
                    type : 'bar',
                    data : {
                        labels : {!! json_encode($weekLabels) !!},
                        datasets :[{
                            label: "Transaksi Penjualan Mingguan",
                            data: {!! json_encode($weekCountTransaction) !!},
                            backgroundColor : 'rgba(216, 27, 96, 0.8)'
                        }]
                    }
                });
            }

            /**
             * Create Monthly Chart
             * @return {[type]} [description]
             */
            function createMonthly(){
                var transactionChartCanvas = $("#transaction-monthly");
                var transactionChart = new Chart(transactionChartCanvas, {
                    type : 'bar',
                    data : {
                        labels : {!! json_encode($monthLabels) !!},
                        datasets :[{
                            label: "Transaksi Penjualan Mingguan",
                            data: {!! json_encode($monthCountTransaction) !!},
                            backgroundColor : 'rgba(216, 27, 96, 0.8)'
                        }]
                    }
                });
            };
            createWeekly();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var type = $(this).data('type');
                if (type=='daily') {
                    createDaily();
                }
                if (type=='weekly') {
                    createWeekly();
                }
                if (type=='monthly') {
                    createMonthly();
                }
            });
        });

    </script>

    {{-- Modal --}}
    <script type="text/javascript">
        var citiesList = {!! json_encode($cities) !!};
        jQuery(document).ready(function($) {
            $('#btn-activation').on('click', function(event) {
                $('#form-activation').submit();
            });
            $('#btn-update').on('click', function(event) {
                $('#form-update').submit();
            });
            $('#btn-change').on('click', function(event) {
                $('#form-change').submit();
            });
            $('select[name=province]').on('change', function(event) {
                var value = $(this).val();
                var selectedVal = "{{ $lockerData->cities_id }}";
                var option='';
                $.each(citiesList, function(index, val) {
                    if (value==val.provinces_id) {
                        selected = '';
                        if (val.id == selectedVal) {selected='selected';}
                        option += "<option value='"+val.id+"' "+selected+">"+val.city_name+"</option>";
                    }
                });
                $('select[name=city]').html(option);
            });
            $('select[name=province]').trigger('change');
        });
    </script>

    {{-- Map --}}
    <script type="text/javascript" src="{{ asset('js/jquery.gmap.js') }}"></script>
    <script type="text/javascript">
        var map;
        var markers = Array();

        function initMap() {
            var currentLatLong =  {lat:{{ $lockerData->latitude }},lng:{{ $lockerData->longitude }} };
            var map = new google.maps.Map(document.getElementById('maps'), {
              zoom: 14,
              // center: {lat: -3.8752905, lng: 117.2135979}
              center : currentLatLong
            });

            var imageList = [
                {
                    url: '{{ asset('img/map/marker-waiting.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('img/map/marker-online.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                },
                {
                    url: '{{ asset('img/map/marker-offline.png') }}',
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(30, 49),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(19, 34)
                }
            ];
            var marker = new google.maps.Marker({
                position: currentLatLong,
                map: map,
                title: '{{ $lockerData->locker_name }}',
                icon : imageList[{{ $lockerData->status }}]
            });
        }
    </script>

    {{-- Element on modal on click --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#modalChange').on('click', '.idPicture', function(event) {
                var value = $(this).val();
                var lockerId = $(this).data('locker');
                $('#idStatus-'+lockerId).val(value);
            });
            $('#modalChange').on('click', '.idSelfie', function(event) {
                var value = $(this).val();
                var lockerId = $(this).data('locker');
                $('#selfieStatus-'+lockerId).val(value);
                console.log($(this).attr('class'));
            });
            $('#modalChange').on('click', '.storePhoto', function(event) {
                var value = $(this).val();
                var lockerId = $(this).data('locker');
                $('#storeStatus-'+lockerId).val(value);
                console.log($(this).attr('class'));
            });
            $('#modalChange').on('click', '.npwpPhoto', function(event) {
                var value = $(this).val();
                var lockerId = $(this).data('locker');
                $('#npwpStatus-'+lockerId).val(value);
                console.log($(this).attr('class'));
            });


        });

    </script>

    <script type="text/javascript">

        var locker_id = '';
        jQuery(document).ready(function ($) {

            locker_id = '{{$lockerData->locker_id}}';
            console.log(locker_id);

            dateRangeTransactions();
            getSalesData();

            $('#pulsa-checkbox').click(function() {
                // console.log(this.checked);
                checkViewChart();
            });
            $('#popshop-checkbox').click(function() {
                // console.log(this.checked);
                checkViewChart();
            });
            $('#biller-checkbox').click(function() {
                // console.log(this.checked);
                checkViewChart();
            });

            $('#switch-chart').change(function () {
                updateChart('Sales Data', dataToShowNow);
            });
        });

        var dailySalesData;
        var weeklySalesData;
        var monthlySalesData;
        var dataToShowNow;
        function getSalesData() {

            showLoading('.box-transaction', 'box-transaction');
            showLoading('.box-topup', 'box-topup');
            showLoading('.box-commission', 'box-commission');
            showLoading('.box-pulsa', 'box-pulsa');
            showLoading('.box-biller', 'box-biller');
            showLoading('.box-popshop', 'box-popshop');

            $.ajax({
                url: '{{ url('agent/ajax/getDetailSales') }}',
                data: {
                    _token : '{{ csrf_token() }}',
                    locker_id : locker_id,
                    date_range: transactionDateRange
                },
                type: 'POST',
                success: function(data){
                    if (data.isSuccess === true) {
                        console.log(data.data);

                        dailySalesData = data.data.dailyDataSalesDb;
                        weeklySalesData = data.data.weeklyDataSalesDb;
                        monthlySalesData = data.data.monthlyDataSalesDb;

                        if (isDailySales) {
                            dataToShowNow = dailySalesData;
                        } else if (isWeeklySales) {
                            dataToShowNow = weeklySalesData;
                        } else if (isMonthlySales) {
                            dataToShowNow = monthlySalesData;
                        }

                        showData(data);
                        showDataChart(dataToShowNow);
                        showDataTable(dataToShowNow);

                    }  else {
                        alert(data.errorMsg);

                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.box-transaction', 'box-transaction');
                    hideLoading('.box-topup', 'box-topup');
                    hideLoading('.box-commission', 'box-commission');
                    hideLoading('.box-pulsa', 'box-pulsa');
                    hideLoading('.box-biller', 'box-biller');
                    hideLoading('.box-popshop', 'box-popshop');

                }
            })
                .done(function() {
                    console.log("success - convert to warung");
                    hideLoading('.box-transaction', 'box-transaction');
                    hideLoading('.box-topup', 'box-topup');
                    hideLoading('.box-commission', 'box-commission');
                    hideLoading('.box-pulsa', 'box-pulsa');
                    hideLoading('.box-biller', 'box-biller');
                    hideLoading('.box-popshop', 'box-popshop');
                })
                .fail(function() {
                    console.log("fail - convert to warung");
                    hideLoading('.box-transaction', 'box-transaction');
                    hideLoading('.box-topup', 'box-topup');
                    hideLoading('.box-commission', 'box-commission');
                    hideLoading('.box-pulsa', 'box-pulsa');
                    hideLoading('.box-biller', 'box-biller');
                    hideLoading('.box-popshop', 'box-popshop');
                })
                .always(function() {
                    console.log("always - convert to warung");
                    hideLoading('.box-transaction', 'box-transaction');
                    hideLoading('.box-topup', 'box-topup');
                    hideLoading('.box-commission', 'box-commission');
                    hideLoading('.box-pulsa', 'box-pulsa');
                    hideLoading('.box-biller', 'box-biller');
                    hideLoading('.box-popshop', 'box-popshop');
                });
        }

        function convertToWarung(locker_id) {
            // console.log('asdasd');
            // console.log(locker_id);

            showLoading('.content', 'content');

            $.ajax({
                url: '{{ url('agent/changeType/{locker_id}') }}',
                type: 'GET',
                dataType: 'JSON',
                async : true,
                data: {
                    locker_id: locker_id
                },
                success: function(data){
                    if (data.isSuccess === true) {
                        // console.log(data.data);
                        alert('Convert To Warung Succeed');
                    }  else {
                        // alert(data.errorMsg);
                        alert('Convert To Warung Failed');
                    }
                },
                error: function(data){
                    console.log('error');
                    console.log(data);
                    hideLoading('.content', 'content');
                    alert('Convert To Warung Failed');
                }
            })
                .done(function() {
                    console.log("success - convert to warung");
                    hideLoading('.content', 'content');
                })
                .fail(function() {
                    console.log("fail - convert to warung");
                    hideLoading('.content', 'content');
                    alert('Convert To Warung Failed');
                })
                .always(function() {
                    console.log("always - convert to warung");
                    hideLoading('.content', 'content');
                });
        }

        var tableAllType;
        function showDataTable(data) {
            tableAllType = $('#table-agent').DataTable({
                data          : data,
                'destroy'     : true,
                'retreive'    : true,
                'paging'      : true,
                'searching'   : true,
                'ordering'    : false,
                'autoWidth'   : false,
                'responsive'  : true,
                'processing'  : true,
                'scrollY'     : 300,
                'deferRender' : true,
                'scroller'    : true,
                "pageLength"  : 50,
                'lengthMenu': [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                'order': [[ 1, "asc" ]],
                columnDefs: [
                    {
                        "targets": 0,
                        "data": "download_link",
                        "render": function ( data, type, full, meta ) {
                            // console.log(data);
                            // console.log('full');
                            // console.log(full);
                            return formatDate(full.date);

                        }
                    },
                    {
                        "targets": 1,
                        "render": function ( data, type, full, meta ) {
                            return full.payment_count;
                        }
                    },
                    {
                        "targets": 2,
                        "render": function ( data, type, full, meta ) {
                            return numberWithCommas(full.payment_amount);
                        }
                    },
                    {
                        "targets": 3,
                        "render": function ( data, type, full, meta ) {
                            return full.nondigital_count;
                        }
                    },
                    {
                        "targets": 4,
                        "render": function ( data, type, full, meta ) {
                            return numberWithCommas(full.nondigital_amount);
                        }
                    },
                    {
                        "targets": 5,
                        "render": function ( data, type, full, meta ) {
                            return full.digital_count;
                        }
                    },
                    {
                        "targets": 6,
                        "render": function ( data, type, full, meta ) {
                            return numberWithCommas(full.digital_amount);
                        }
                    },
                    {
                        "targets": 7,
                        "render": function ( data, type, full, meta ) {
                            var result = +full.payment_count + +full.nondigital_count + +full.digital_count;;
                            // result += full.payment_count;
                            // result += full.nondigital_count;
                            // result += full.digital_count;
                            return result;
                        }
                    },
                    {
                        "targets": 8,
                        "render": function ( data, type, full, meta ) {
                            var result = +full.payment_amount + +full.nondigital_amount + +full.digital_amount;
                            // result += full.payment_amount;
                            // result += full.nondigital_amount;
                            // result += full.digital_amount;
                            return numberWithCommas(result);
                        }
                    }
                ]
            });
        }

        function showData(data) {
            $('#transaction-amount').numerator({ easing: 'linear',
                duration: 1000,
                toValue: data.data.transactionAmount,
                onComplete: function(){
                    $('#transaction-amount').html(numberWithCommas(data.data.transactionAmount));
                }});
            $('#transaction-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.transactionCount });

            $('#topup-amount').numerator({ easing: 'linear',
                duration: 1000,
                toValue: data.data.topupAmount,
                onComplete: function(){
                    $('#topup-amount').html(numberWithCommas(data.data.topupAmount));
                }});
            $('#topup-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.topupCount });

            $('#commission-amount').numerator({ easing: 'linear',
                duration: 1000,
                toValue: data.data.commissionAmount,
                onComplete: function(){
                    $('#commission-amount').html(numberWithCommas(data.data.commissionAmount));
                }});
            $('#commission-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.commissionCount });

            $('#pulsa-amount').numerator({ easing: 'linear',
                duration: 1000,
                toValue: data.data.pulsaAmount,
                onComplete: function(){
                    $('#pulsa-amount').html(numberWithCommas(data.data.pulsaAmount));
                }});
            $('#pulsa-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.pulsaCount });

            $('#biller-amount').numerator({ easing: 'linear',
                duration: 1000,
                toValue: data.data.paymentAmount,
                onComplete: function(){
                    $('#biller-amount').html(numberWithCommas(data.data.paymentAmount));
                }});
            $('#biller-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.paymentCount });

            $('#popshop-amount').numerator({ easing: 'linear',
                duration: 1000,
                toValue: data.data.popshopAmount,
                onComplete: function(){
                    $('#popshop-amount').html(numberWithCommas(data.data.popshopAmount));
                }});
            $('#popshop-count').numerator({ easing: 'linear', duration: 1000, toValue: data.data.popshopCount });

        }

        function showDataChart(data) {
            if (chartSalesData === undefined) {
                chart(createChartData(data), "Sales Data");
            } else {
                updateChart('Sales Data', data);
            }
        }

        function notify(title, body) {
            $.notify(title + '<br>' + body, {
                animate: {
                    enter: 'animated fadeInRight',
                    exit: 'animated fadeOutRight',
                    delay: 5000
                }
            });
        }

        function checkViewChart() {
            chartt.data.datasets[0].hidden = !$('#pulsa-checkbox')[0].checked;
            chartt.data.datasets[1].hidden = !$('#popshop-checkbox')[0].checked;
            chartt.data.datasets[2].hidden = !$('#biller-checkbox')[0].checked;

            chartt.update();
        }

        var isDailySales = false;
        var isWeeklySales = true;
        var isMonthlySales = false;
        function updateViewSalesData(value) {
            if (value === 1) {
                isDailySales = true;
                isWeeklySales = false;
                isMonthlySales = false;

                dataToShowNow = dailySalesData;
                updateChart('Sales Data', dataToShowNow);

                showDataTable(dataToShowNow);

                $('#daily-sales').css('color', 'cornflowerblue');
                $('#selected-daily-sales').css('color', 'cornflowerblue').html('selected');

                $('#weekly-sales').css('color', 'dimgray');
                $('#selected-weekly-sales').css('color', 'dimgray').html('');

                $('#monthly-sales').css('color', 'dimgray');
                $('#selected-monthly-sales').css('color', 'dimgray').html('');

            } else if (value === 2) {
                isDailySales = false;
                isWeeklySales = true;
                isMonthlySales = false;

                dataToShowNow = weeklySalesData;
                updateChart('Sales Data', dataToShowNow);

                showDataTable(dataToShowNow);

                $('#daily-sales').css('color', 'dimgray');
                $('#selected-daily-sales').css('color', 'dimgray').html('');

                $('#weekly-sales').css('color', 'cornflowerblue');
                $('#selected-weekly-sales').css('color', 'cornflowerblue').html('selected');

                $('#monthly-sales').css('color', 'dimgray');
                $('#selected-monthly-sales').css('color', 'dimgray').html('');

            } else if (value === 3) {
                isDailySales = false;
                isWeeklySales = false;
                isMonthlySales = true;

                dataToShowNow = monthlySalesData;
                updateChart('Sales Data', dataToShowNow);

                showDataTable(dataToShowNow);

                $('#daily-sales').css('color', 'dimgray');
                $('#selected-daily-sales').css('color', 'dimgray').html('');

                $('#weekly-sales').css('color', 'dimgray');
                $('#selected-weekly-sales').css('color', 'dimgray').html('');

                $('#monthly-sales').css('color', 'cornflowerblue');
                $('#selected-monthly-sales').css('color', 'cornflowerblue').html('selected');
            }
        }

        var chartSalesIsTotal = true;
        var chartSalesData;
        var chartt;
        function getChart(charData, title) {
            var ctxx = document.getElementById("myChart");
            return myCharttt = new Chart(ctxx, {
                type: 'line',
                data: charData,
                options: {
                    title: {
                        display: true,
                        text: title
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Range Date'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    if ($('#switch-chart')[0].checked) {
                                        return value;
                                    }
                                    return numberWithCommas(value);
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'Transactions by Total'
                            }
                        }]
                    }
                }
            });
        }

        function chart(chartData, title) {
            var mmyChartt = getChart(chartData, title);
            chartt = mmyChartt;
        }

        function createChartData(data) {
            chartSalesData = data;
            var arr_date = [];
            var arr_digital = [];
            var arr_non_digital = [];
            var arr_biller = [];

            data.forEach(function (value) {
                if (isDailySales) {
                    arr_date.push(formatDate(value.date));
                } else if (isWeeklySales) {
                    arr_date.push(formatDate(value.date));
                } else if (isMonthlySales) {
                    arr_date.push(formatDate(value.date));
                }

                if ($('#switch-chart')[0].checked) {
                    arr_non_digital.push(value.nondigital_count);
                    arr_digital.push(value.digital_count);
                    arr_biller.push(value.payment_count);
                } else {
                    arr_non_digital.push(value.nondigital_amount);
                    arr_digital.push(value.digital_amount);
                    arr_biller.push(value.payment_amount);
                }
            });

            return barChartData = {
                labels: arr_date,
                datasets: [{
                    label: 'Pulsa',
                    backgroundColor: '#469BF4',
                    borderColor: '#469BF4',
                    data: arr_digital,
                    fill: false
                }, {
                    label: 'Belanja',
                    backgroundColor: '#E24458',
                    borderColor: '#E24458',
                    data: arr_non_digital,
                    fill: false
                }, {
                    label: 'Biller',
                    backgroundColor: '#5FC4D7',
                    borderColor: '#5FC4D7',
                    data: arr_biller,
                    fill: false
                }]

            };
        }

        function updateChart(label, data) {
            // var title = 'Transactions ' + moment(data.startDate).format('D MMM Y') + " - " + moment(data.endDate).format('D MMM Y') + label;
            removeData(chartt);
            chartt.options.title.text = label;
            chartt.options.scales.yAxes[0].scaleLabel.labelString = label;
            chartt.data = createChartData(data);
            chartt.update();
            checkViewChart();
        }

        function removeData(chart) {
            chart.data.labels.pop();
            chart.data.datasets.pop();
            chart.update();
        }

        var transactionDateRange = '';
        function dateRangeTransactions() {
            var startDate = moment().startOf('month').format('YYYY-MM-DD');
            var endDate = moment().endOf('month').format('YYYY-MM-DD');
            var inputDateRangeTransaction = $('#daterange-transactions');
            transactionDateRange = startDate + ',' + endDate;

            var startDateToShow = moment().startOf('month').format('DD/MM/YYYY');
            var endDateToShow = moment().endOf('month').format('DD/MM/YYYY');
            $('#daterange-transactions').val(startDateToShow + ' - ' + endDateToShow);

            // console.log(startDate);
            // console.log(endDate);

            inputDateRangeTransaction.daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY/MM/DD',
                    cancelLabel: 'Clear'
                },
                startDate: startDate,
                endDate: endDate,
                ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });
            inputDateRangeTransaction.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                transactionDateRange = picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD');
            });
            inputDateRangeTransaction.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                transactionDateRange = '';
            });
        }

        var isBoxHide = false;
        function showHideBox() {
            if (isBoxHide) {
                $('#box-user-data').show(300);
                $('#box-graph').show(300);
                $('#btn-show-hide-filter').html('Hide');
            } else {
                $('#box-user-data').hide(300);
                $('#box-graph').hide(300);
                $('#btn-show-hide-filter').html('Show');
            }
            isBoxHide = !isBoxHide;
        }

        function formatDate(date) {
            if (isDailySales) {
                return moment(date).format('DD MMM YYYY');
            } else if (isWeeklySales) {
                var str = date.split("-");
                var beginDate = moment().day('Monday').week(str[1]).format('DD MMM');
                var endDate = moment().day('Monday').week(str[1]).add(6, 'days').format('DD MMM');
                return beginDate + '-' + endDate + ' ' + str[0];
            } else if (isMonthlySales) {
                return moment(date, 'YYYY-MM').format('MMM YYYY');
            }
        }

        function numberWithCommas(x) {
            // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return OSREC.CurrencyFormatter.format(x, { currency: 'IDR', locale: 'id_ID' });
        }

        function replaceAll(str, find, replace) {
            return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }

        function escapeRegExp(str) {
            return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }

    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAijkBYFlPmwsLEAfvljffdrnhM7EStYF4&callback=initMap"></script>

@endsection