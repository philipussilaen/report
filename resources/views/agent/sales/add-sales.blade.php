@extends('layout.main')

@section('title')
    Sales
@endsection

@section('css')
    {{-- DataTables --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css">
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('pageTitle')
    Add Sales
@endsection

@section('pageDesc')
    Add new Sales User
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Module</h3>
        </div>
        <div class="box-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <form id="form-module" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>User List</label>
                    <select class="form-control select2" name="user">
                        @foreach ($userDashboardDb as $user)
                            <option value="{{ $user->id }}">{{ $user->name }} ({{ $user->group->name }})</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Deposit</label>
                    <input type="text" name="deposit" class="form-control">
                </div>
                <button class="btn btn-info" type="submit">Tambah Sales</button>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table class="display responsive nowrap" cellspacing="0" width="100%" id="table">
                        <thead>
                        <tr>
                            <td>User Name</td>
                            <td>Group</td>
                            <td>Current Balance</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($salesDb as $sales)
                            <tr>
                                <td>{{ $sales->user->name }}</td>
                                <td>
                                    @php
                                        $userDb = \App\User::where('email',$sales->user->email)->first();
                                    @endphp
                                    @if($userDb)
                                        {{ $userDb->group->name }}
                                    @endif
                                </td>
                                <td>{{ number_format($sales->balance) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- DataTables --}}
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.colVis.min.js"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
        });
    </script>

    {{-- Data Tables --}}
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "dom": 'Bfrtip',
                "buttons": ['colvis']
            });
        });
    </script>
@endsection