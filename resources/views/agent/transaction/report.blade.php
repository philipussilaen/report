@extends('layout.main')
@section('css')
     <style type="text/css">
        #gmap {
            width: 100%;
            height: 384px;
        }
    </style>
    {{-- select 2 --}}
    <link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
    {{-- Date Range picker --}}
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}">
     <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">
@stop

@section('title')
    Agent Transaksi
@endsection

@section('pageTitle')
    Dashboard Transaksi
@endsection

@section('pageDesc')
    Transaksi
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                {{-- Box Filter --}}
                <div class="box box-solid">
                    <div class="box-body">
                        <form>
                            @foreach ($parameter as $key => $element)
                                @php
                                    if (in_array($key, ['dateRange','beginDate','endDate','dateEncoded'])) continue;
                                @endphp
                                <input type="hidden" name="{{ $key }}" value="{{ $element }}">
                            @endforeach
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label>Date</label>
                                        <input type="text" name="dateRange" id="dateRange" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Report Type</label> <br>
                                        <label>
                                            <input type="radio" name="reportType" value="sales" class="form-control icheck" @if($parameter['reportType'] == 'sales') checked @endif> Sales
                                        </label> <br>
                                        <label>
                                            <input type="radio" name="reportType" value="geo" class="form-control icheck" @if($parameter['reportType'] == 'geo') checked @endif> Geographic
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <br>
                                    <button class="btn btn-flat btn-block btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                {{-- Graph --}}
                <div class="box box-solid">
                    <div class="nav-tabs-custom box-graph">
                        <ul class="nav nav-tabs pull-right">
                            <li><a href="#tab_count_1" data-toggle="tab" data-type="daily">Daily</a></li>
                            <li class="active"><a href="#tab_count_2" data-toggle="tab" data-type="weekly">Weekly</a></li>
                            <li><a href="#tab_count_3" data-toggle="tab" data-type="monthly">Monthly</a></li>
                            <li class="pull-left header"><i class="fa fa-chart-line"></i> Transaction</li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_count_1">
                                <div class="chart">
                                    <canvas id="quantity-daily" style="height: 180px; width: 1072px;"></canvas>
                                </div>
                            </div>
                            <div class="tab-pane active" id="tab_count_2">
                                <div class="chart">
                                    <canvas id="quantity-weekly" style="height: 180px; width: 1072px;"></canvas>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_count_3">
                                <div class="chart">
                                    <canvas id="quantity-monthly" style="height: 180px; width: 1072px;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Table --}}
                @if ($parameter['reportType'] == 'geo')
                    <div class="box box-solid">
                        <div class="box-body no-padding">
                            <div class="table-responsive">
                                <table class="table table-hover table-striped" id="transaction-table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Report</th>
                                            <th>Province Name</th>
                                            @php
                                                $firstItem = $transactionReportDb->first();
                                            @endphp
                                            @isset ($firstItem->city_name)
                                                <th>City</th>
                                            @endisset
                                            @isset ($firstItem->district_name)
                                                <th>District</th>
                                                
                                            @endisset
                                            @isset ($firstItem->locker_name)
                                                <th>Sub District</th>
                                                <th>Agent</th>
                                            @endisset
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $tmpParam = $parameter;
                                    @endphp
                                    <tbody>
                                        @foreach ($transactionReportDb as $index => $item)
                                            <tr>
                                                <td>{{ $transactionReportDb->firstItem() + $index }}</td>
                                                <td>
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['reportType'] = $parameter['reportType'];
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <a href="{{ $categoryUrl }}">
                                                        {{ ucfirst($parameter['reportType']) }}
                                                    </a>
                                                </td>
                                                <td>
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['province'] = $item->province_id;
                                                        $tmpParam['reportType'] = $parameter['reportType'];
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <a href="{{ $categoryUrl }}">
                                                        <span class="label label-primary">{{ $item->province_name }}</span>
                                                    </a>
                                                </td>
                                                @isset ($firstItem->city_name)
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['province'] = $item->province_id;
                                                        $tmpParam['reportType'] = $parameter['reportType'];
                                                        $tmpParam['city'] = $item->city_id;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <td>
                                                        <a href="{{ $categoryUrl }}">
                                                            {{ $item->city_name }}
                                                        </a>
                                                    </td>
                                                @endisset
                                                @isset ($firstItem->district_name)
                                                    <td>
                                                        @php
                                                            $tmpParam = [];
                                                            $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                            $tmpParam['province'] = $item->province_id;
                                                            $tmpParam['reportType'] = $parameter['reportType'];
                                                            $tmpParam['city'] = $item->city_id;
                                                            $tmpParam['districtName'] = $item->district_name;
                                                            $tmpParamHttp = http_build_query($tmpParam);
                                                            $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                        @endphp
                                                        <a href="{{ $categoryUrl }}">
                                                            {{ $item->district_name }} ({{ $item->number_agent }} Agents)
                                                        </a>
                                                    </td>
                                                @endisset
                                                @isset ($firstItem->locker_name)
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['reportType'] = 'sales';
                                                        $tmpParam['lockerId'] = $item->locker_id;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <td>{{ $item->sub_district_name }}</td>
                                                    <td>
                                                        <a href="{{ url('agent/list/detail') }}/{{ $item->locker_id }}" target="_blank">
                                                            {{ $item->locker_name }}
                                                        </a>
                                                    </td>
                                                @endisset
                                                <td>{{ number_format($item->sum) }}</td>
                                                <td>Rp{{ number_format($item->price) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <thead>
                                        <tr>
                                            @php
                                                $colSpan = 3;
                                                $firstItem = $transactionReportDb->first();

                                                if (isset($firstItem->city_name)) $colSpan++;
                                                if (isset($firstItem->district_name)) $colSpan++;
                                                if (isset($firstItem->locker_name)) $colSpan += 2;
                                            @endphp
                                            <th colspan="{{ $colSpan }}" style="text-align: center;vertical-align: middle;">Total</th>
                                            <th>{{ number_format($totalQuantity) }}</th>
                                            <th>Rp{{ number_format($totalAmount) }}</th>
                                        </tr>
                                    </thead>
                                </table>
                                {{ $transactionReportDb->links() }}
                            </div>
                        </div>
                    </div>
                @elseif ($parameter['reportType'] == 'sales')
                    <div class="box box-solid">
                        <div class="box-body no-padding">
                            <div class="table-responsive">
                                <table class="table table-hover table-striped" id="transaction-table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Report</th>
                                            <th>Category</th>
                                            @php
                                                $firstItem = $transactionReportDb->first();
                                            @endphp
                                            @isset ($firstItem->type)
                                                <th>Type</th>
                                            @endisset
                                            @isset ($firstItem->name)
                                                <th>Product Name</th>
                                            @endisset
                                            @isset ($firstItem->province_name)
                                                <th>Province</th>
                                            @endisset
                                            @isset ($firstItem->city_name)
                                                <th>City</th>
                                            @endisset
                                            @isset ($firstItem->district_name)
                                                <th>District</th>
                                            @endisset
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    @php
                                        $tmpParam = $parameter;
                                    @endphp
                                    <tbody>
                                        @foreach ($transactionReportDb as $index => $item)
                                            <tr>
                                                <td>{{ $transactionReportDb->firstItem() + $index }}</td>
                                                <td>
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['reportType'] = $parameter['reportType'];
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <a href="{{ $categoryUrl }}">
                                                        {{ ucfirst($parameter['reportType']) }}
                                                    </a>
                                                </td>
                                                <td>
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['category'] = $item->category;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    @if ($item->category == 'non_digital')
                                                        <a href="{{ $categoryUrl }}">
                                                            <span class="label label-primary">Non-Digital</span>
                                                        </a>
                                                    @else
                                                        <a href="{{ $categoryUrl }}">
                                                            <span class="label label-success">Digital</span>
                                                        </a>
                                                    @endif
                                                </td>
                                                @isset ($firstItem->type)
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['category'] = $item->category;
                                                        $tmpParam['type'] = $item->type;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <td>
                                                        <a href="{{ $categoryUrl }}">
                                                            @if (isset($typeList[$item->type]))
                                                                {{ $typeList[$item->type] }}
                                                            @else
                                                                {{ $item->type }}
                                                            @endif
                                                        </a>
                                                    </td>
                                                @endisset
                                                @isset ($firstItem->name)
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['category'] = $item->category;
                                                        $tmpParam['type'] = $item->type;
                                                        $tmpParam['productId'] = $item->product_id;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <td>
                                                       <a href="{{ $categoryUrl }}">{{ $item->name }}</a> 
                                                    </td>
                                                @endisset
                                                @isset ($firstItem->province_name)
                                                    @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['category'] = $item->category;
                                                        $tmpParam['type'] = $item->type;
                                                        $tmpParam['productId'] = $item->product_id;
                                                        $tmpParam['province'] = $item->province_id;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <td>
                                                        <a href="{{ $categoryUrl }}">{{ $item->province_name }}</a>
                                                    </td>
                                                @endisset
                                                @isset ($firstItem->city_name)
                                                     @php
                                                        $tmpParam = [];
                                                        $tmpParam['dateRange'] = $parameter['dateEncoded'];
                                                        $tmpParam['category'] = $item->category;
                                                        $tmpParam['type'] = $item->type;
                                                        $tmpParam['productId'] = $item->product_id;
                                                        $tmpParam['province'] = $item->province_id;
                                                        $tmpParam['city'] = $item->city_id;
                                                        $tmpParamHttp = http_build_query($tmpParam);
                                                        $categoryUrl = url('agent/transaction/report')."?$tmpParamHttp";
                                                    @endphp
                                                    <td>
                                                        <a href="{{ $categoryUrl }}">{{ $item->city_name }}</a>
                                                    </td>
                                                @endisset
                                                @isset ($firstItem->district_name)
                                                    <td>{{ $item->district_name }}</td>
                                                    <td>{{ $item->sub_district_name }}</td>
                                                @endisset
                                                <td>{{ number_format($item->sum) }}</td>
                                                <td>Rp{{ number_format($item->price) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <thead>
                                        <tr>
                                            @php
                                                $colSpan = 3;
                                                $firstItem = $transactionReportDb->first();

                                                if (isset($firstItem->type)) $colSpan++;
                                                if (isset($firstItem->name)) $colSpan++;
                                                if (isset($firstItem->province_name)) $colSpan += 1;
                                                if (isset($firstItem->city_name)) $colSpan +=1;
                                                if (isset($firstItem->district_name)) $colSpan +=2;
                                            @endphp
                                            <th colspan="{{ $colSpan }}" style="text-align: center;vertical-align: middle;">Total</th>
                                            <th>{{ number_format($totalQuantity) }}</th>
                                            <th>Rp{{ number_format($totalAmount) }}</th>
                                        </tr>
                                    </thead>
                                </table>
                                {{ $transactionReportDb->links() }}
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
@stop

@section('js')
    {{-- Morris.js charts --}}
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morris.js/morris.min.js') }}"></script>
    {{-- ChartJS 1.0.1 --}}
    <script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
    {{-- Select 2 --}}
    <script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    {{-- DateRange Picker --}}
    <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    {{-- Google Maps --}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClLN5wQ219u9_vMwi9JKhqvFm9EbRnxFU"></script>
    {{-- MaPlace JS --}}
    <script src="{{ asset('js/maplace.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

    {{-- Select 2 --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.select2').select2();
            @if (!empty($parameter['type']))
            	$('select[name="type[]"]').val({!! json_encode($parameter['type']) !!}).trigger('change');
            @endif
            @if (!empty($parameter['province']))
            	$('select[name="province[]"]').val({!! json_encode($parameter['province']) !!}).trigger('change');
            @endif
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('.icheck').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue'
            });
        });
    </script>

    {{-- Date Range Picker --}}
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //Date range picker
            var startDate = '{{ $parameter['beginDate'] }}';
            var endDate = '{{ $parameter['endDate'] }};'
            $('#dateRange').daterangepicker({
                locale: {
                    format: 'YYYY/MM/DD'
                },
                startDate: startDate,
                endDate: endDate,
            });
        });
    </script>

    {{-- Button Click --}}
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#btn-submit').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/report') }}');
                $('#form-transaction').attr('method', 'get');
                $('#form-transaction').submit();
            });
            $('#btn-export').on('click', function(event) {
                $('#form-transaction').attr('action', '{{ url('agent/transaction/export') }}');
                $('#form-transaction').attr('method', 'post');
                $('#form-transaction').submit();
            });
        });
    </script>

    <script type="text/javascript">
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>

    {{-- Chart --}}
    <script type="text/javascript">
        var dayLabels = {!! json_encode($dayLabels) !!};

        var weekLabels = {!! json_encode($weekLabels) !!};
        var weeklyData = [];
        var weeklySumData = [];

        var monthLabels = {!! json_encode($monthLabels) !!};
        var monthlyData = [];
        var monthlySumData = [];

        var option =  {
                    responsive : true,
                    tooltips : {
                        mode : 'index',
                        intersect : false
                    },
                    hover : {
                        mode : 'nearest',
                        intersect : true
                    }
                };

        function createDaily(){
            $('#quantity-daily').css({
                "height": '350px',
                "width": '1072px'
            });

            /*Daily Chart*/
            var transactionChartCanvas = $("#quantity-daily");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : dayLabels,
                    datasets :{!! json_encode($listDayQtyDataSets) !!}
                },
                options: option
            });
        };

        function createWeekly(){
            $('#quantity-weekly').css({
                "height": '350px',
                "width": '1072px'
            });

            /*Weekly Chart*/
            var transactionChartCanvas = $("#quantity-weekly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : weekLabels,
                    datasets :{!! json_encode($listWeekQtyDataSets) !!}
                },
                options: option
            });
        }

        function createMonthly(){
            $('#quantity-monthly').css({
                "height": '350px',
                "width": '1072px'
            });

            /*Monthly Chart*/
            var transactionChartCanvas = $("#quantity-monthly");
            var transactionChart = new Chart(transactionChartCanvas, {
                type : 'line',
                data : {
                    labels : monthLabels,
                    datasets :{!! json_encode($listMonthQtyDataSets) !!}
                },
                options: option
            });
        };

         $(document).ready(function() {
            createWeekly();
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var type = $(this).data('type');
                if (type=='daily') {
                    createDaily();
                }
                if (type=='weekly') {
                    createWeekly();
                }
                if (type=='monthly') {
                    createMonthly();
                }
            });
            $('#type').on('change', function() {
                type = $(this).val();
                createWeekly();
            });
        });
    </script>


@stop